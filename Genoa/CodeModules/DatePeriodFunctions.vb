

'Class DatePeriodFunctions

'	Public Shared Function AreDatesEquivalent(ByVal PricePeriod As RenaissanceGlobals.DealingPeriod, ByVal BaseDate As Date, ByVal SecondDate As Date) As Boolean
'		' *********************************************************************************************
'		' Function to determine if two dates fall within the same time period.
'		' 
'		' *********************************************************************************************

'		Select Case PricePeriod
'			Case RenaissanceGlobals.DealingPeriod.Daily
'				If BaseDate.Date.CompareTo(SecondDate.Date) = 0 Then
'					Return True
'				Else
'					Return False
'				End If

'			Case RenaissanceGlobals.DealingPeriod.Weekly
'				Dim StartOfWeek As Date

'				StartOfWeek = BaseDate.Date
'				While (StartOfWeek.DayOfWeek <> DayOfWeek.Monday)
'					StartOfWeek = StartOfWeek.AddDays(-1)
'				End While

'				If StartOfWeek.CompareTo(SecondDate.Date) > 0 Then
'					Return False
'				End If

'				If (SecondDate - StartOfWeek).Days <= 6 Then
'					Return True
'				Else
'					Return False
'				End If

'			Case RenaissanceGlobals.DealingPeriod.Fortnightly
'				If FitDateToPeriod(PricePeriod, BaseDate, True).Date.CompareTo(FitDateToPeriod(PricePeriod, SecondDate, True).Date) = 0 Then
'					Return True
'				Else
'					Return False
'				End If

'			Case RenaissanceGlobals.DealingPeriod.Monthly
'				If (BaseDate.Year = SecondDate.Year) And (BaseDate.Month = SecondDate.Month) Then
'					Return True
'				Else
'					Return False
'				End If

'			Case RenaissanceGlobals.DealingPeriod.Quarterly
'				If (BaseDate.Year = SecondDate.Year) Then
'					If ((BaseDate.Month - 1) \ 3) = ((SecondDate.Month - 1) \ 3) Then
'						Return (True)
'					End If

'					Return False

'				End If
'				Return False

'			Case RenaissanceGlobals.DealingPeriod.SemiAnnually
'				If (BaseDate.Year = SecondDate.Year) Then
'					If ((BaseDate.Month - 1) \ 6) = ((SecondDate.Month - 1) \ 6) Then
'						Return (True)
'					End If

'					Return True

'				End If
'				Return False

'			Case RenaissanceGlobals.DealingPeriod.Annually
'				If (BaseDate.Year = SecondDate.Year) Then
'					Return (True)
'				End If

'				Return False

'			Case Else
'				Return False

'		End Select

'		Return False

'	End Function

'	Public Shared Function AddPeriodToDate(ByVal PricePeriod As RenaissanceGlobals.DealingPeriod, ByVal BaseDate As Date, Optional ByVal PeriodCount As Integer = 1) As Date
'		' *********************************************************************************************
'		' Function to add 'n' periods to a given date.
'		' *********************************************************************************************

'		Try
'			If IsDate(BaseDate) = False Then
'				Return BaseDate
'			End If

'			Select Case PricePeriod

'				Case RenaissanceGlobals.DealingPeriod.Daily
'					Return BaseDate.AddDays(1 * PeriodCount)

'				Case RenaissanceGlobals.DealingPeriod.Weekly
'					Return BaseDate.AddDays(7 * PeriodCount)

'				Case RenaissanceGlobals.DealingPeriod.Fortnightly
'					Return BaseDate.AddDays(14 * PeriodCount)

'				Case RenaissanceGlobals.DealingPeriod.Monthly
'					If BaseDate.Day = 1 Then
'						Return BaseDate.AddMonths(1 * PeriodCount)
'					Else
'						Return FitDateToPeriod(PricePeriod, BaseDate.AddMonths(1 * PeriodCount), True)
'					End If

'				Case RenaissanceGlobals.DealingPeriod.Quarterly
'					If BaseDate.Day = 1 Then
'						Return BaseDate.AddMonths(3 * PeriodCount)
'					Else
'						Return FitDateToPeriod(PricePeriod, BaseDate.AddMonths(3 * PeriodCount), True)
'					End If

'				Case RenaissanceGlobals.DealingPeriod.SemiAnnually
'					If BaseDate.Day = 1 Then
'						Return BaseDate.AddMonths(6 * PeriodCount)
'					Else
'						Return FitDateToPeriod(PricePeriod, BaseDate.AddMonths(6 * PeriodCount), True)
'					End If

'				Case RenaissanceGlobals.DealingPeriod.Annually
'					If BaseDate.Day = 1 Then
'						Return BaseDate.AddYears(1 * PeriodCount)
'					Else
'						Return FitDateToPeriod(PricePeriod, BaseDate.AddYears(1 * PeriodCount), True)
'					End If

'				Case Else
'					Return BaseDate

'			End Select
'		Catch ex As Exception
'			Return BaseDate
'		End Try

'	End Function

'	Private Shared FitDateToPeriodLock As New Object

'	Public Shared Function FitDateToPeriod(ByVal PricePeriod As RenaissanceGlobals.DealingPeriod, ByVal BaseDate As Date, Optional ByVal EndOfPeriod As Boolean = True) As Date
'		' *********************************************************************************************
'		' Function to fit a given date to the boundary of a given period.
'		'
'		' By default the returned date will be on the last day of the period, if
'		' 'EndOfPeriod' is FALSE then the returned date will be the first day of the applicable period.
'		' *********************************************************************************************

'		SyncLock FitDateToPeriodLock

'			Dim TempDate As Date
'			TempDate = BaseDate.Date

'			Select Case PricePeriod

'				Case RenaissanceGlobals.DealingPeriod.Daily
'					Return BaseDate.Date

'				Case RenaissanceGlobals.DealingPeriod.Weekly
'					If (EndOfPeriod = True) Then ' Fit to Sunday
'						While (TempDate.DayOfWeek <> DayOfWeek.Sunday)
'							TempDate = TempDate.AddDays(1)
'						End While
'					Else ' Fit to Monday
'						While (TempDate.DayOfWeek <> DayOfWeek.Monday)
'							TempDate = TempDate.AddDays(-1)
'						End While
'					End If

'					Return TempDate

'				Case RenaissanceGlobals.DealingPeriod.Fortnightly
'					TempDate = FitDateToPeriod(RenaissanceGlobals.DealingPeriod.Weekly, TempDate, True)
'					If ((TempDate.Date - FitDateToPeriod(RenaissanceGlobals.DealingPeriod.Weekly, Renaissance_BaseDate, True)).Days Mod 14) > 0 Then
'						TempDate = TempDate.AddDays(7)
'					End If

'					If (EndOfPeriod = False) Then
'						TempDate = TempDate.AddDays(-13)
'					End If

'					Return TempDate

'				Case RenaissanceGlobals.DealingPeriod.Monthly
'					If (EndOfPeriod = True) Then
'						TempDate = TempDate.AddMonths(1)
'						TempDate = TempDate.AddDays(0 - TempDate.Day)
'					Else
'						TempDate = TempDate.AddDays(1 - TempDate.Day)
'					End If

'					Return TempDate

'				Case RenaissanceGlobals.DealingPeriod.Quarterly
'					If (EndOfPeriod = True) Then
'						TempDate = TempDate.AddMonths(2 - ((TempDate.Month - 1) Mod 3))

'						TempDate = TempDate.AddMonths(1)
'						TempDate = TempDate.AddDays(0 - TempDate.Day)

'					Else
'						TempDate = TempDate.AddMonths(0 - ((TempDate.Month - 1) Mod 3))

'						TempDate = TempDate.AddDays(1 - TempDate.Day)

'					End If

'					Return TempDate

'				Case RenaissanceGlobals.DealingPeriod.SemiAnnually
'					If (EndOfPeriod = True) Then
'						TempDate = TempDate.AddMonths(5 - ((TempDate.Month - 1) Mod 6))

'						TempDate = TempDate.AddMonths(1)
'						TempDate = TempDate.AddDays(0 - TempDate.Day)

'					Else
'						TempDate = TempDate.AddMonths(0 - ((TempDate.Month - 1) Mod 6))

'						TempDate = TempDate.AddDays(1 - TempDate.Day)

'					End If

'					Return TempDate

'				Case RenaissanceGlobals.DealingPeriod.Annually
'					If (EndOfPeriod = True) Then
'						TempDate = TempDate.AddMonths(12 - TempDate.Month)
'						TempDate = TempDate.AddDays(31 - TempDate.Day)
'					Else
'						TempDate = TempDate.AddMonths(1 - TempDate.Month)
'						TempDate = TempDate.AddDays(1 - TempDate.Day)
'					End If

'					Return TempDate

'				Case Else
'					Return BaseDate

'			End Select

'		End SyncLock

'	End Function

'	Private Shared GetPriceIndexLock As New Object

'	Public Shared Function GetPriceIndex(ByVal PricePeriod As RenaissanceGlobals.DealingPeriod, ByVal StartDate As Date, ByVal EndDate As Date) As Integer
'		' **********************************************************************************************
'		' Basis Index Zero is the period of StartDate.
'		'
'		' **********************************************************************************************

'		SyncLock GetPriceIndexLock

'			Dim thisStartDate As Date
'			Dim thisEndDate As Date

'			thisStartDate = FitDateToPeriod(PricePeriod, StartDate, False)
'			thisEndDate = FitDateToPeriod(PricePeriod, EndDate, False)

'			Select Case PricePeriod

'				Case RenaissanceGlobals.DealingPeriod.Daily
'					Return (EndDate.Date - StartDate.Date).Days

'				Case RenaissanceGlobals.DealingPeriod.Weekly
'					Return ((thisEndDate.Date - thisStartDate.Date).Days \ 7)

'				Case RenaissanceGlobals.DealingPeriod.Fortnightly
'					Return ((thisEndDate.Date - thisStartDate.Date).Days \ 14)

'				Case RenaissanceGlobals.DealingPeriod.Monthly
'					Return (EndDate.Month - StartDate.Month) + ((EndDate.Year - StartDate.Year) * 12)

'				Case RenaissanceGlobals.DealingPeriod.Quarterly
'					Return ((EndDate.Month - StartDate.Month) + ((EndDate.Year - StartDate.Year) * 12)) \ 3

'				Case RenaissanceGlobals.DealingPeriod.SemiAnnually
'					Return ((EndDate.Month - StartDate.Month) + ((EndDate.Year - StartDate.Year) * 12)) \ 6

'				Case RenaissanceGlobals.DealingPeriod.Annually
'					Return (EndDate.Year - StartDate.Year)

'			End Select

'		End SyncLock

'	End Function

'	'Public Shared Function GetPeriodCount(ByVal PricePeriod As RenaissanceGlobals.DealingPeriod, ByVal pStartDate As Date, ByVal pPeriod As TimeSpan) As Integer
'	'	Dim StartDate As Date = FitDateToPeriod(PricePeriod, pStartDate)
'	'	Dim EndDate As Date = FitDateToPeriod(PricePeriod, pStartDate.Add(pPeriod))

'	'	Return GetPeriodCount(PricePeriod, StartDate, EndDate)
'	'End Function

'	Private Shared GetPeriodCountLock As New Object

'	Public Shared Function GetPeriodCount(ByVal PricePeriod As RenaissanceGlobals.DealingPeriod, ByVal StartDate As Date, ByVal EndDate As Date) As Integer
'		' *************************************************************************************
'		'
'		'
'		' *************************************************************************************
'		SyncLock GetPeriodCountLock
'			Dim thisdate As Date
'			Dim PeriodCount As Integer

'			PeriodCount = 0

'			Select Case PricePeriod
'				Case RenaissanceGlobals.DealingPeriod.Daily
'					PeriodCount = Math.Abs((EndDate.Date - StartDate.Date).Days) + 1

'				Case Else
'					thisdate = StartDate
'					While thisdate.CompareTo(EndDate) <= 0 '  (thisdate.Year < EndDate.Year) Or (thisdate.Month <= EndDate.Month)
'						PeriodCount += 1

'						thisdate = AddPeriodToDate(PricePeriod, thisdate)
'					End While

'			End Select

'			Return PeriodCount
'		End SyncLock

'	End Function

'	Public Shared Function GetBestGuessDataDate(ByVal PricePeriod As RenaissanceGlobals.DealingPeriod, ByVal ReferenceDate As Date) As Date
'		' *************************************************************************************
'		'
'		'
'		' *************************************************************************************

'		If Not ((PricePeriod = RenaissanceGlobals.DealingPeriod.Daily) Or (PricePeriod = RenaissanceGlobals.DealingPeriod.Weekly) Or (PricePeriod = RenaissanceGlobals.DealingPeriod.Fortnightly)) Then
'			ReferenceDate = ReferenceDate.AddDays(-7)
'		End If

'		Return FitDateToPeriod(PricePeriod, AddPeriodToDate(PricePeriod, ReferenceDate, -1), True)

'	End Function

'	Public Shared Function PeriodName(ByVal PricePeriod As RenaissanceGlobals.DealingPeriod) As String
'		' *************************************************************************************
'		'
'		'
'		' *************************************************************************************

'		Select Case PricePeriod
'			Case RenaissanceGlobals.DealingPeriod.Annually
'				Return "yrs"

'			Case RenaissanceGlobals.DealingPeriod.SemiAnnually
'				Return "semi annual"

'			Case RenaissanceGlobals.DealingPeriod.Quarterly
'				Return "qtrs"

'			Case RenaissanceGlobals.DealingPeriod.Monthly
'				Return "mths"

'			Case RenaissanceGlobals.DealingPeriod.Fortnightly
'				Return "fortnights"

'			Case RenaissanceGlobals.DealingPeriod.Weekly
'				Return "wks"

'			Case RenaissanceGlobals.DealingPeriod.Daily
'				Return "days"

'		End Select

'		Return ""
'	End Function

'End Class
