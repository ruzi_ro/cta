Option Strict On

Module CTAGlobals

#Region " Global Declarations"

  Public Const SPLASH_DISPLAY As Integer = 3 ' Minimum Duration of Splash Display

  ' Base registry key for program settings

  Public Const REGISTRY_BASE As String = "Software\FandCPartners\CTA"

  ' Standard Connection Name, as used with the DataHandler object.

  Public Const CTA_CONNECTION As String = "cnnCTA"

  ' Standard Form Caching Counters

  Public Const STANDARD_EntryForm_CACHE_COUNT As Integer = 1
  Public Const STANDARD_ReportForm_CACHE_COUNT As Integer = 1
  Public Const STANDARD_SingleForm_CACHE_COUNT As Integer = 1

  Public Const EPSILON As Double = 0.00000001

  ' Standard reference Dates & timings.
  ' LAST_Second is used to convert KDs to a precise 'Whole Day' value, also used to
  ' evaluate the 'Whole Day' status of a given Date.

  'Public Const KNOWLEDGEDATE_NOW As Date = #1/1/1900#
  'Public Const Renaissance_BaseDate As Date = #1/1/1900#
  'Public Const Renaissance_BaseDateDATA As Date = #1/1/2000#
  'Public Const Renaissance_EndDate_Data As Date = #1/1/2100#
  'Public Const LAST_SECOND As Integer = 86399

  ' Standard SQL Command Timeouts

  Public Const DEFAULT_SQLCOMMAND_TIMEOUT As Integer = 600

  ' Const used to compare two doubles. Differences smaller than this are not deemed to be significant.
  Public Const CTA_RoundingError As Double = 0.000001

  Public Const CTA_HoldingPrecision As Integer = 6

  ' Paint Timer Pause - Seconds to delay after parameter change before repainting charts
  Public Const PAINT_TIMER_DELAY As Double = 0.75

  ' If Enabled, Trigger an Update Event after calculating this many Custom Field Data points.
  ' <Used on the 'frmCustomDataMaintenance' form>.
  Public Const CTA_CalculateCustomField_EventTrigger As Integer = 1000

  ' If Enabled, Refresh the Status grid after this many seconds during a 
  ' Custom Field Calculation procedure.
  ' <Used on the 'frmCustomDataMaintenance' form>.
  Public Const CTA_UpdateStatusGrid_Period As Integer = 300

  ' CTA DragDrop Headers
  Public Const CTA_DRAG_IDs_HEADER As String = "CTA_DRAG_IDs:"

  ' Standard CTA form interface.
  ' Must be implemented by any form that is cached.

  Public Interface StandardCTAForm
    Property IsOverCancelButton() As Boolean
    ReadOnly Property IsInPaint() As Boolean
    ReadOnly Property InUse() As Boolean
    ReadOnly Property FormOpenFailed() As Boolean
    ReadOnly Property MainForm() As CTAMain
    Sub ResetForm()
    Sub CloseForm()
  End Interface


#End Region

  ' Fund Search Enumerations

  Public Enum CustomDataCachePresentFlag As Integer
    AllItemsAdded = 1
    SelectiveItemsAdded = 2
  End Enum

  ' CTA Constants

  Public Enum CTAChartTypes As Integer
    ' Series Charts :-

    VAMI
    MonthlyReturns
    RollingReturn
    ReturnScatter
    StdDev
    Omega
    DrawDown
    Correlation
    Alpha
    Beta
    Quartile
    Ranking

    ' Single Point Charts
    Compare_Correlation
    Compare_CorrelationUpDown
    Compare_Volatility
    Compare_APR
    Compare_Alpha
    Compare_Beta
    Compare_BetaupDown

  End Enum

  Public Enum CTAQuartileDisplayData As Integer
    ' Data Series to display on the Quartile and Ranking charts

    MonthlyReturns
    RollingReturns
    Volatility
    Alpha
    Beta
    Correlation

  End Enum

  ' Form ID Enumeration. Intended to uniquely identify each Form Type

  Public Enum CTAFormID
    ' **************************************************************************************
    '
    ' Enumeration member name MUST be the same as the associated form object.
    ' this is so that the generic 'New_CTAForm()' function in the CTA Main form will 
    ' be able to instantiate the correct object.
    '
    ' **************************************************************************************

    None = 0
    frmCTAMain

    frmAbout

    frmChangeControlReview


    frmSimulationBrowser
    frmFundSearch

    frmGroupCovariance
    frmGroupList
    frmGroupMembers
    frmGroupDetails
    frmGroupReturns

    frmInformation
    frmManageFolders
    frmMasterName
    frmOptimise
    frmPerformance

    frmSelectChangeControl
    frmSetKnowledgeDate


    frmUserPermissions

    ' Reports :-

    frmViewReport
    frmViewChart

    frmInformationReport

    ' Non-Form permisioning for Change Control roles.

    'ChangeCtrlAddNew
    ChangeCtrlITReview
    ChangeCtrlAuthoriseIT
    ChangeCtrlAuthoriseOwner
    ChangeCtrlAuthoriseBusiness
    ChangeCtrlAcceptIT
    ChangeCtrlAcceptOwner
    ChangeCtrlAcceptBusiness

    MaxValue = 100
  End Enum

  Public Enum CTAReportID
    ' **************************************************************************************
    '
    ' Enumeration member name MUST be the same as the associated report.
    ' this is so that report permissioning can be performed.
    '
    ' **************************************************************************************

    None = 0

    InformationReport
    MonthlyTableReport
    DrawDownReport
    StatisticsReport

    ChartHeaderReport
    ChartPageReport
    ChartMultiPageReport

    rptGroupReport
    rptCompetitorGroupsReport
    rptOptimiserGroupReport

    ' End Stop

    MaxValue
  End Enum

  ' Marginals Status Enumeration.

  <FlagsAttribute()> _
  Public Enum MarginalsStatusEnum As UInteger
    OK = 0
    InstrumentBreak = 1
    SectorBreak = 2
    LiquidityBreak = 4
    CustomFieldBreak = 8
  End Enum

  ' Grid Selection Enum
  Public Enum SelectionEnum As Integer
    None = 0
    SingleRow = 1
    MultipleRows = 2
    AllRows = 3
  End Enum

End Module
