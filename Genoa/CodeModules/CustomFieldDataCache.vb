Imports System.Threading
Imports System.Data.SqlClient
Imports RenaissanceGlobals

Module CustomFieldDataCacheModule


	Public Class CustomFieldDataCacheClass
		' ***************************************************************************************************
    ' Integrated Cache class for the CTA Custom Fields.
		'
		' Re-Written, NPP, April 2008.
		'
		' ---------------------------------------------------------------------------
		'
		' Since Instrument (Pertrac) IDs ar not necessarily contiguous and starting from One, particularly
		' since the Pertrac Table was split into the Renaissance Database where All IDs (currently) are converted
		' to start at 1 Million or so, I have decided to Normalise the cached IDs. This is achieved through the use
		' of the '_IdNormalisationDictionary'. This gives each Instrument ID a unique Index, based at 0.
		' 
		' Custom Field data is then stored in a collection of 'FieldDataCacheClass' objects, held in the
		' '_DataCacheDictionary' collection.
		' Each FieldDataCacheClass object holds a Data array for the entire of Group Zero ('_FieldData'), data
		' specific to a Non-Zero Group is held in a Dictionary (Index = Normalised Index) which in turn is held
		' in a management class (NonZeroGroupDataClass). These Group Specific Data collections are then held in 
		' yet another Dictionary (_NonZeroGroupData).
		' It is hoped that this will allow the rapid recovery of data relating to any given group and that since
		' the group specific data is quite sparse that there will not be an un-acceptable memory overhead.
		'
		' ***************************************************************************************************

#Region " Local Classes & Enumerations"

		Public Enum CacheStatus As Integer
			Empty = 0
			PartiallyPopulated = 1
			FullyPopulated = 2
		End Enum

		' Data Type Enumeration as Defined in Renaissance Globals.
		'
		'<FlagsAttribute()> _
		' Public Enum RenaissanceDataType As Integer
		'	None = 0
		'	TextType = 1
		'	DateType = 2
		'	NumericType = 4
		'	PercentageType = 8
		'	BooleanType = 16
		'End Enum

		Private Class FieldDataCacheClass
			' ***************************************************************************
			' Individual Custom Field Data Cache Class
			'
			' Custom Field Data is held in the '_FieldData' Array for Group Zero and in a
			' collection of 'NonZeroGroupDataClass' objects held in the '_NonZeroGroupData' Dictionary.
			' It is envisaged that there are a lot of Data Items recorded against Group Zero, The Baseline Data'
			' and only very sparsely populated data for specific groups.
			'
			' ***************************************************************************

			Private Class NonZeroGroupDataClass
				Public DataStatus As CacheStatus
				Public GroupData As Dictionary(Of Integer, Object)

				Public Sub New()
					DataStatus = CacheStatus.Empty
					GroupData = New Dictionary(Of Integer, Object)
				End Sub
			End Class

			Private _IdStatus() As Boolean								' Boolean Array to indicate the presence of each data item.
			Private _FieldDataType As RenaissanceDataType	' Data Type
			Private _FieldDefault As Object
			Private _FieldData As Array										' Array of Group Zero Data.
			Private _NonZeroGroupData As Dictionary(Of Integer, NonZeroGroupDataClass) ' Collection of Data objects relating to Non-GroupZero Data.
			Private _DataStatus As CacheStatus						' Data Status (Group Zero) : Empty, Partial or Fully populated.
			Private _LockObject As Object

			Private Sub New()
				' ***************************************************************************
				' Basic Constructor.
				' ***************************************************************************

				_FieldDataType = RenaissanceDataType.None
				_FieldData = Nothing
				_NonZeroGroupData = New Dictionary(Of Integer, NonZeroGroupDataClass)
				_DataStatus = CacheStatus.Empty
				_IdStatus = Nothing
				_LockObject = New Object
				_FieldDefault = Nothing

			End Sub

			Public Sub New(ByVal pInitialDataType As RenaissanceDataType, ByVal pDefaultValue As Object, ByVal pInitialArraySize As Integer)
				' ***************************************************************************
				'
				' ***************************************************************************

				Me.New()

				_FieldDataType = pInitialDataType
				Resize(pInitialArraySize, False)

				Try

					If (_FieldDataType And RenaissanceDataType.BooleanType) Then
						If (pDefaultValue Is Nothing) OrElse (pDefaultValue.ToString.Length = 0) Then
							_FieldDefault = False
						Else
							_FieldDefault = CBool(pDefaultValue)
						End If

					ElseIf (_FieldDataType And RenaissanceDataType.DateType) Then
						If (pDefaultValue Is Nothing) OrElse (Not IsDate(pDefaultValue)) Then
							_FieldDefault = RenaissanceGlobals.Globals.Renaissance_BaseDate
						Else
							_FieldDefault = CDate(pDefaultValue)
						End If

					ElseIf ((_FieldDataType And RenaissanceDataType.NumericType) Or (_FieldDataType And RenaissanceDataType.PercentageType)) Then
						If (pDefaultValue Is Nothing) OrElse (Not IsNumeric(pDefaultValue)) Then
							_FieldDefault = 0.0#
						Else
							_FieldDefault = CDbl(pDefaultValue)
						End If

					ElseIf (_FieldDataType And RenaissanceDataType.TextType) Then
						If (pDefaultValue Is Nothing) Then
							_FieldDefault = ""
						Else
							_FieldDefault = pDefaultValue.ToString
						End If

					End If

				Catch ex As Exception
					_FieldDefault = Nothing
				End Try

			End Sub

			Public Sub Resize(ByVal pNewArraySize As Integer, ByVal pPreserve As Boolean)
				' ***************************************************************************
				' Resize Flag and Data Arrays.
				'
				' This function is also designed to Clear existing arrays and Re-Allocate the data
				' array if the Data Array Type does not match the type specified in '_FieldDataType'.
				'
				' ***************************************************************************

				SyncLock _LockObject

					Try

						If (pPreserve) AndAlso (_FieldData IsNot Nothing) AndAlso (_FieldData.Length > 0) Then

							' If you have elected to preserve data and there is data to preserve...

							ReDim Preserve _IdStatus(pNewArraySize)

							Dim NewData As Array = Nothing

							If (_FieldDataType And RenaissanceDataType.BooleanType) Then
								NewData = Array.CreateInstance(GetType(Boolean), pNewArraySize + 1)

							ElseIf (_FieldDataType And RenaissanceDataType.DateType) Then
								NewData = Array.CreateInstance(GetType(Date), pNewArraySize + 1)

							ElseIf ((_FieldDataType And RenaissanceDataType.NumericType) Or (_FieldDataType And RenaissanceDataType.PercentageType)) Then
								NewData = Array.CreateInstance(GetType(Double), pNewArraySize + 1)

							ElseIf (_FieldDataType And RenaissanceDataType.TextType) Then
								NewData = Array.CreateInstance(GetType(String), pNewArraySize + 1)

							End If

							' Copy exisitng data to the new array (If the data types match).

							If (NewData IsNot Nothing) Then
								If NewData.GetType Is _FieldData.GetType Then
									Array.Copy(_FieldData, NewData, Math.Min(_FieldData.Length, NewData.Length))
								Else
									' Data Type has changed, Clear the Status array as all items are now not set.
									Array.Clear(_IdStatus, 0, _IdStatus.Length)

									For Each SubDictionary As NonZeroGroupDataClass In _NonZeroGroupData.Values
										SubDictionary.GroupData.Clear()
										SubDictionary.DataStatus = CacheStatus.Empty
									Next

								End If
							End If

							' Set Data Array and status flag.

							_FieldData = NewData
							NewData = Nothing
							DataStatus(0) = CacheStatus.PartiallyPopulated

						Else
							' Do nopt preserve data.

							Dim CurrentSpecifiedType As Type = Nothing

							' Establish 'Correct' data type.

							If (_FieldDataType And RenaissanceDataType.BooleanType) Then
								CurrentSpecifiedType = GetType(Boolean)

							ElseIf (_FieldDataType And RenaissanceDataType.DateType) Then
								CurrentSpecifiedType = GetType(Date)

							ElseIf ((_FieldDataType And RenaissanceDataType.NumericType) Or (_FieldDataType And RenaissanceDataType.PercentageType)) Then
								CurrentSpecifiedType = GetType(Double)

							ElseIf (_FieldDataType And RenaissanceDataType.TextType) Then
								CurrentSpecifiedType = GetType(String)

							End If

							' Clear existing data if the arrays are already big enough (and of the correct type) , otherwise re-dimension appropriately.

							If (_IdStatus IsNot Nothing) AndAlso (_IdStatus.Length >= pNewArraySize) AndAlso (_FieldData IsNot Nothing) AndAlso (_FieldData.Length >= pNewArraySize) AndAlso (_FieldData(0).GetType Is CurrentSpecifiedType) Then

								Array.Clear(_IdStatus, 0, _IdStatus.Length)
								Array.Clear(_FieldData, 0, _FieldData.Length)

							Else

								ReDim _IdStatus(pNewArraySize)

								If (_FieldData IsNot Nothing) Then
									_FieldData = Nothing
								End If

								If (CurrentSpecifiedType IsNot Nothing) Then
									_FieldData = Array.CreateInstance(CurrentSpecifiedType, pNewArraySize + 1)
								End If

							End If

							' Clear Non-ZeroGroup Data.

							For Each SubDictionary As NonZeroGroupDataClass In _NonZeroGroupData.Values
								SubDictionary.GroupData.Clear()
								SubDictionary.DataStatus = CacheStatus.Empty
							Next

							DataStatus(0) = CacheStatus.Empty

						End If

					Catch ex As Exception

						_IdStatus = Nothing
						_FieldData = Nothing

						For Each SubDictionary As NonZeroGroupDataClass In _NonZeroGroupData.Values
							SubDictionary.GroupData.Clear()
							SubDictionary.DataStatus = CacheStatus.Empty
						Next

						DataStatus(0) = CacheStatus.Empty

					End Try

				End SyncLock
			End Sub

			Public Sub ClearData()
				' ***************************************************************************
				' Clear ALL Data relating to ALL Groups.
				' ***************************************************************************

				Try
					SyncLock _LockObject
						If (_IdStatus IsNot Nothing) AndAlso (_FieldData IsNot Nothing) Then
							Resize(_IdStatus.Length, False)
						End If
					End SyncLock
				Catch ex As Exception
				End Try

			End Sub

			Public Sub ClearData(ByVal pGroupID As Integer)
				' ***************************************************************************
				'
				' Clear Data relating to a specific Group. 
				'
				' ***************************************************************************

				Try

					SyncLock _LockObject

						' If this object contains Data ...

						If (_IdStatus IsNot Nothing) AndAlso (_FieldData IsNot Nothing) Then

							If (pGroupID <= 0) Then

								' For Group Zero, Clear the base data arrays.

								Array.Clear(_IdStatus, 0, _IdStatus.Length)
								Array.Clear(_FieldData, 0, _FieldData.Length)
								DataStatus(0) = CacheStatus.Empty

							Else

								' For Non-GroupZero, clear the relevant Data class

								Dim SubDictionary As NonZeroGroupDataClass

								If (_NonZeroGroupData.ContainsKey(pGroupID)) Then
									SubDictionary = _NonZeroGroupData(pGroupID)

									SubDictionary.GroupData.Clear()
									SubDictionary.DataStatus = CacheStatus.Empty
								End If

							End If
						End If

					End SyncLock

				Catch ex As Exception
				End Try

			End Sub

			Public Sub ClearData(ByVal pGroupID As Integer, ByVal pIndex As Integer)
				' ***************************************************************************
				'
				' Clear Data relating to a specific Group and Index Entry. 
				'
				' ***************************************************************************

				Try

					SyncLock _LockObject

						If (_IdStatus IsNot Nothing) AndAlso (_FieldData IsNot Nothing) Then
							If (pGroupID <= 0) Then
								If (pIndex < _IdStatus.Length) AndAlso (_IdStatus(pIndex)) Then
									_IdStatus(pIndex) = False
									Array.Clear(_FieldData, pIndex, 1)

									If (DataStatus(0) = CacheStatus.FullyPopulated) Then
										DataStatus(0) = CacheStatus.PartiallyPopulated
									End If
								End If
							Else
								Dim SubDictionary As NonZeroGroupDataClass

								If (_NonZeroGroupData.ContainsKey(pGroupID)) Then
									SubDictionary = _NonZeroGroupData(pGroupID)

									If (SubDictionary.GroupData.ContainsKey(pIndex)) Then
										SubDictionary.GroupData.Remove(pIndex)

										If (SubDictionary.DataStatus = CacheStatus.FullyPopulated) Then
											SubDictionary.DataStatus = CacheStatus.PartiallyPopulated
										End If
									End If
								End If
							End If
						End If

					End SyncLock

				Catch ex As Exception
				End Try

			End Sub

			Public Sub ClearGroupData(ByVal pGroupID As Integer)
				' ***************************************************************************
				'
				' Clear Data relating to a specific Group. OR ALL Cached Data if GroupID <= 0
				'
				' ***************************************************************************

				Try

					SyncLock _LockObject

						If (_IdStatus IsNot Nothing) AndAlso (_FieldData IsNot Nothing) Then
							Dim SubDictionary As NonZeroGroupDataClass

							If (pGroupID <= 0) Then

								For Each SubDictionary In _NonZeroGroupData.Values
									SubDictionary.GroupData.Clear()
									SubDictionary.DataStatus = CacheStatus.Empty
								Next

							Else

								If (_NonZeroGroupData.ContainsKey(pGroupID)) Then
									SubDictionary = _NonZeroGroupData(pGroupID)

									SubDictionary.GroupData.Clear()
									SubDictionary.DataStatus = CacheStatus.Empty
								End If

							End If
						End If

					End SyncLock

				Catch ex As Exception
				End Try

			End Sub

			Public Property FieldDataType() As RenaissanceDataType
				Get
					Return _FieldDataType
				End Get
				Set(ByVal value As RenaissanceDataType)
					Try
						SyncLock _LockObject
							If (value <> _FieldDataType) Then
								_FieldDataType = value

								If (_IdStatus IsNot Nothing) Then
									Me.Resize(_IdStatus.Length, False)
								End If
							End If
						End SyncLock
					Catch ex As Exception
					End Try
				End Set
			End Property

			Public Property DefaultValue() As Object
				Get
					Return _FieldDefault
				End Get
				Set(ByVal pDefaultValue As Object)

					If (_FieldDataType And RenaissanceDataType.BooleanType) Then
						If (pDefaultValue Is Nothing) Then
							_FieldDefault = False
						Else
							_FieldDefault = CBool(pDefaultValue)
						End If

					ElseIf (_FieldDataType And RenaissanceDataType.DateType) Then
						If (pDefaultValue Is Nothing) OrElse (Not IsDate(pDefaultValue)) Then
							_FieldDefault = RenaissanceGlobals.Globals.Renaissance_BaseDate
						Else
							_FieldDefault = CDate(pDefaultValue)
						End If

					ElseIf ((_FieldDataType And RenaissanceDataType.NumericType) Or (_FieldDataType And RenaissanceDataType.PercentageType)) Then
						If (pDefaultValue Is Nothing) OrElse (Not IsNumeric(pDefaultValue)) Then
							_FieldDefault = 0.0#
						Else
							_FieldDefault = CDbl(pDefaultValue)
						End If

					ElseIf (_FieldDataType And RenaissanceDataType.TextType) Then
						If (pDefaultValue Is Nothing) Then
							_FieldDefault = ""
						Else
							_FieldDefault = pDefaultValue.ToString
						End If

					Else
						_FieldDefault = Nothing
					End If

				End Set
			End Property

			Public Property DataStatus(ByVal pGroupID As Integer) As CacheStatus
				Get
					Try
						SyncLock _LockObject
							If (pGroupID <= 0) Then
								Return Me._DataStatus
							Else
								If (_NonZeroGroupData.ContainsKey(pGroupID)) Then
									Return _NonZeroGroupData(pGroupID).DataStatus
								End If
							End If
						End SyncLock
					Catch ex As Exception
					End Try

					Return CacheStatus.Empty
				End Get
				Set(ByVal value As CacheStatus)
					Try
						SyncLock _LockObject
							If (pGroupID <= 0) Then
								_DataStatus = value
							Else
								If (_NonZeroGroupData.ContainsKey(pGroupID)) Then

									_NonZeroGroupData(pGroupID).DataStatus = value

								Else

									_NonZeroGroupData.Add(pGroupID, New NonZeroGroupDataClass)
									_NonZeroGroupData(pGroupID).DataStatus = value

								End If
							End If
						End SyncLock
					Catch ex As Exception
					End Try

				End Set
			End Property

			Public ReadOnly Property IdStatus() As Boolean()
				Get
					Return _IdStatus
				End Get
			End Property

			Public ReadOnly Property BooleanData() As Array
				Get
					If (_FieldDataType = RenaissanceDataType.BooleanType) Then
						Return CType(_FieldData, Boolean())
					Else
						Return Nothing
					End If
				End Get
			End Property

			Public ReadOnly Property DateData() As Array
				Get
					If (_FieldDataType = RenaissanceDataType.DateType) Then
						Return CType(_FieldData, Date())
					Else
						Return Nothing
					End If
				End Get
			End Property

			Public ReadOnly Property NumericData() As Array
				Get
					If (_FieldDataType = RenaissanceDataType.NumericType) OrElse (_FieldDataType = RenaissanceDataType.PercentageType) Then
						Return CType(_FieldData, Double())
					Else
						Return Nothing
					End If
				End Get
			End Property

			Public ReadOnly Property TextData() As Array
				Get
					If (_FieldDataType = RenaissanceDataType.TextType) Then
						Return CType(_FieldData, String())
					Else
						Return Nothing
					End If
				End Get
			End Property

			Public Function DataPointExists(ByVal pIndex As Integer, ByVal pGroupID As Integer, ByVal LookThroughToGroupZero As Boolean) As Boolean
				' ***************************************************************************
				' 
				'
				' ***************************************************************************

				Dim RVal As Boolean = False

				If (pIndex < 0) Then
					Return True
					Exit Function
				ElseIf (_IdStatus Is Nothing) OrElse (_IdStatus.Length <= pIndex) Then
					Return False
					Exit Function
				End If

				Try
					SyncLock _LockObject

						If (pGroupID <= 0) Then
							' For Group Zero, return Group Zero status.

							RVal = _IdStatus(pIndex)
						Else
							' Group ID > 0

							Dim ThisNonZeroData As NonZeroGroupDataClass = Nothing

							If (_NonZeroGroupData.ContainsKey(pGroupID)) Then
								' Specific Group Data Exists...

								ThisNonZeroData = _NonZeroGroupData(pGroupID)

								If (ThisNonZeroData.GroupData.ContainsKey(pIndex)) Then
									' Specific Data point exists 

									RVal = True

								Else
									' Specific Data point DOES NOT exist.
									' Now, If we believe all specific data relating to this Group has been loaded, then
									' fall through to Group Zero status. If all Group Specific data has not been loaded, then 
									' return False.

									If (LookThroughToGroupZero) AndAlso (ThisNonZeroData.DataStatus = CacheStatus.FullyPopulated) Then
										RVal = _IdStatus(pIndex)
									Else
										RVal = False
									End If

								End If

							Else
								' Specific Group data has not been loaded.

								RVal = False
							End If

						End If

					End SyncLock
				Catch ex As Exception
				End Try

				Return RVal

			End Function

			Public Function GetDataPoint(ByVal pIndex As Integer, ByVal pGroupID As Integer) As Object
				' ***************************************************************************
				' 
				'
				' ***************************************************************************
				Dim RVal As Object = Nothing

				Try
					SyncLock _LockObject

						If (pIndex >= 0) AndAlso (_IdStatus IsNot Nothing) AndAlso (_IdStatus.Length > pIndex) Then
							Try
								SyncLock _LockObject

									If (pGroupID <= 0) Then
										' For Group Zero, return Group Zero Data.

										If (_IdStatus(pIndex)) Then
											RVal = _FieldData(pIndex)
										End If
									Else
										' Group ID > 0

										Dim ThisNonZeroData As NonZeroGroupDataClass = Nothing

										If (_NonZeroGroupData.ContainsKey(pGroupID)) Then
											' Specific Group Data Exists...

											ThisNonZeroData = _NonZeroGroupData(pGroupID)

											If (ThisNonZeroData.GroupData.ContainsKey(pIndex)) Then
												' Specific Data point exists 

												RVal = ThisNonZeroData.GroupData(pIndex)

											Else
												' Specific Data point DOES NOT exist.
												' Now, If we believe all specific data relating to this Group has been loaded, then
												' fall through to Group Zero data. If all Group Specific data has not been loaded, then 
												' return Nothing.

												If (ThisNonZeroData.DataStatus = CacheStatus.FullyPopulated) Then
													RVal = _FieldData(pIndex)
												Else
													RVal = Nothing
												End If

											End If

										Else
											' Specific Group data has not been loaded.

											RVal = Nothing
										End If

									End If

								End SyncLock
							Catch ex As Exception
								RVal = Nothing
							End Try
						End If

					End SyncLock
				Catch ex As Exception
				End Try

				' Now, given RVal, return data of the correct type, or an appropriate default.

				If (RVal Is Nothing) Then
					RVal = _FieldDefault
				End If

				Try
					If (_FieldDataType And RenaissanceDataType.BooleanType) Then
						Try
							If (RVal Is Nothing) Then
								Return False
							Else
								Return CBool(RVal)
							End If
						Catch ex As Exception
							RVal = False
						End Try

					ElseIf (_FieldDataType And RenaissanceDataType.DateType) Then
						Try
							If (RVal Is Nothing) OrElse (Not IsDate(RVal)) Then
								Return RenaissanceGlobals.Globals.Renaissance_BaseDate
							Else
								Return CDate(RVal)
							End If
						Catch ex As Exception
							RVal = RenaissanceGlobals.Globals.Renaissance_BaseDate
						End Try

					ElseIf ((_FieldDataType And RenaissanceDataType.NumericType) Or (_FieldDataType And RenaissanceDataType.PercentageType)) Then
						Try
							If (RVal Is Nothing) OrElse (Not IsNumeric(RVal)) Then
								Return 0.0#
							Else
								Return CDbl(RVal)
							End If
						Catch ex As Exception
							RVal = 0.0#
						End Try

					ElseIf (_FieldDataType And RenaissanceDataType.TextType) Then
						Try
							If (RVal Is Nothing) Then
								RVal = ""
								Return RVal
							Else
								Return RVal.ToString
							End If
						Catch ex As Exception
							RVal = ""
						End Try

					End If

				Catch ex As Exception
				End Try

				Return RVal

			End Function

			Public Sub SetDataPoint(ByVal pIndex As Integer, ByVal pGroupID As Integer, ByVal pDatum As Object)
				' ***************************************************************************
				' 
				'
				' Just create an Empty dictionary object if (pGroupID > 0) AND (pIndex < 0) and the dictionary object does not already exist
				' ***************************************************************************

				Try
					SyncLock _LockObject

						' Check destination arrays are big enough. This Code should be un-necessary as this should not occur.

						If (_IdStatus Is Nothing) OrElse (_IdStatus.Length < pIndex) OrElse (_FieldData Is Nothing) OrElse (_FieldData.Length < pIndex) Then
							Me.Resize(Math.Max(pIndex, 20000) + 2000, True)
						End If

						If (pGroupID <= 0) AndAlso (pIndex >= 0) Then
							' Set Group Zero Data

							' Set Status Bit
							_IdStatus(pIndex) = True

							' Set Data Item

							If (_FieldDataType And RenaissanceDataType.BooleanType) Then
								Try
									_FieldData(pIndex) = CBool(pDatum)
								Catch ex As Exception
								End Try

							ElseIf (_FieldDataType And RenaissanceDataType.DateType) Then
								If IsDate(pDatum) Then
									_FieldData(pIndex) = CDate(pDatum)
								Else
									_FieldData(pIndex) = RenaissanceGlobals.Globals.Renaissance_BaseDate
								End If

							ElseIf ((_FieldDataType And RenaissanceDataType.NumericType) Or (_FieldDataType And RenaissanceDataType.PercentageType)) Then
								If IsNumeric(pDatum) Then
									_FieldData(pIndex) = CDbl(pDatum)
								Else
									_FieldData(pIndex) = 0
								End If

							ElseIf (_FieldDataType And RenaissanceDataType.TextType) Then
								_FieldData(pIndex) = pDatum.ToString

							End If

						Else
							' Set Specific Group Data.

							Dim ThisNonZeroData As NonZeroGroupDataClass

							If (_NonZeroGroupData.ContainsKey(pGroupID)) Then
								ThisNonZeroData = _NonZeroGroupData(pGroupID)
							Else
								ThisNonZeroData = New NonZeroGroupDataClass
								_NonZeroGroupData.Add(pGroupID, ThisNonZeroData)
							End If

							If (pIndex >= 0) Then
								If (ThisNonZeroData.GroupData.ContainsKey(pIndex)) Then
									ThisNonZeroData.GroupData(pIndex) = pDatum
								Else
									ThisNonZeroData.GroupData.Add(pIndex, pDatum)
								End If

								If (ThisNonZeroData.DataStatus = CacheStatus.Empty) Then
									ThisNonZeroData.DataStatus = CacheStatus.PartiallyPopulated
								End If
							End If

						End If

					End SyncLock
				Catch ex As Exception
				End Try
			End Sub

		End Class

#End Region

#Region " Class Locals"

		Private MainForm As RenaissanceGlobals.StandardRenaissanceMainForm
		Private CacheLockObject As Threading.ReaderWriterLock
		Private _IdNormalisationDictionary As Dictionary(Of Integer, Integer)	' Dictionary to convert InstrumentID to Normalised ID (Index to Data Arrays)
		Private NumeratorInitialised As Boolean
		Private _IdLookupArray() As Integer	' Array of InstrumentIDs in the Normalised order (to lookup an ID from a Data Index).
		Private _DataCacheDictionary As Dictionary(Of Integer, FieldDataCacheClass)

		Private Const DEFAULT_DataItemArraySize As Integer = 20000
		Private Const LockDelayTime As Integer = 10000
		Private Current_DataItemArraySize As Integer

    Public Const STATIC_FIELD_FLAG As Integer = 2 ^ 30

#End Region

#Region " Class Properties"


#End Region

#Region " Class Constructors"

		Private Sub New()
			' ***************************************************************************************************
			'
			'
			' ***************************************************************************************************

			_IdNormalisationDictionary = New Dictionary(Of Integer, Integer)
			_IdLookupArray = Nothing
			_DataCacheDictionary = New Dictionary(Of Integer, FieldDataCacheClass)
			Current_DataItemArraySize = DEFAULT_DataItemArraySize
			CacheLockObject = New Threading.ReaderWriterLock
			NumeratorInitialised = False

		End Sub

		Public Sub New(ByVal pMainForm As RenaissanceGlobals.StandardRenaissanceMainForm)
			' ***************************************************************************************************
			'
			'
			' ***************************************************************************************************

			Me.New()

			MainForm = pMainForm

      Current_DataItemArraySize = Math.Max(DEFAULT_DataItemArraySize, CInt(MasterNameInstrumentCount() * 1.1#))

			RefreshInstrumentNumerator()

		End Sub

		Public Sub New(ByVal pMainForm As RenaissanceGlobals.StandardRenaissanceMainForm, ByVal pInitialDataArraySize As Integer)
			' ***************************************************************************************************
			'
			'
			' ***************************************************************************************************
			Me.New()

			MainForm = pMainForm

      Current_DataItemArraySize = Math.Max(Math.Max(DEFAULT_DataItemArraySize, pInitialDataArraySize), CInt(MasterNameInstrumentCount() * 1.1#))

			RefreshInstrumentNumerator()

		End Sub

#End Region

#Region " Class Methods / Functions"

		' Clear Data Functions 

    Public Sub ClearDataCache(Optional ByVal StaticNotCustomFields As Boolean = False)
      ' ***************************************************************************************************
      '
      ' ***************************************************************************************************
      Dim UpgradedLock As Boolean = False
      Dim LockCookie As Threading.LockCookie = Nothing

      Try
        If (Not CacheLockObject.IsReaderLockHeld) Then
          CacheLockObject.AcquireWriterLock(LockDelayTime)
        Else
          LockCookie = CacheLockObject.UpgradeToWriterLock(LockDelayTime)
          UpgradedLock = True
        End If

        If (_DataCacheDictionary.Count > 0) Then
          Dim KeyArray(_DataCacheDictionary.Keys.Count - 1) As Integer
          Dim KeyIndex As Integer

          _DataCacheDictionary.Keys.CopyTo(KeyArray, 0)

          For KeyIndex = 0 To (KeyArray.Length - 1)
            If (StaticNotCustomFields) Then
              If (KeyArray(KeyIndex) >= STATIC_FIELD_FLAG) Then
                ClearDataCache(KeyArray(KeyIndex))
              End If
            Else
              If (KeyArray(KeyIndex) < STATIC_FIELD_FLAG) Then
                ClearDataCache(KeyArray(KeyIndex))
              End If
            End If
          Next
        End If

      Catch ex As Exception
      Finally
        If UpgradedLock Then
          CacheLockObject.DowngradeFromWriterLock(LockCookie)
        Else
          CacheLockObject.ReleaseWriterLock()
        End If
      End Try

    End Sub

    Public Sub ClearStaticFieldDataCache()

      Try

        ClearDataCache(True)

      Catch ex As Exception
      End Try

    End Sub

    Public Sub ClearDataCache(ByVal pCustomFieldID As Integer)
      ' ***************************************************************************************************
      '
      ' ***************************************************************************************************
      Dim UpgradedLock As Boolean = False
      Dim LockCookie As Threading.LockCookie = Nothing

      Try
        If (Not CacheLockObject.IsReaderLockHeld) Then
          CacheLockObject.AcquireWriterLock(LockDelayTime)
        Else
          LockCookie = CacheLockObject.UpgradeToWriterLock(LockDelayTime)
          UpgradedLock = True
        End If

        Dim ThisDataCache As FieldDataCacheClass

        If (_DataCacheDictionary.ContainsKey(pCustomFieldID)) Then
          ThisDataCache = _DataCacheDictionary(pCustomFieldID)

          ThisDataCache.ClearData()
        End If

      Catch ex As Exception
      Finally
        If UpgradedLock Then
          CacheLockObject.DowngradeFromWriterLock(LockCookie)
        Else
          CacheLockObject.ReleaseWriterLock()
        End If
      End Try

    End Sub

    Public Sub ClearDataCache(ByVal pUpdateDetail As String)
      ' ***************************************************************************************************
      '
      ' 'pUpdateDetail' provides a delimited list of Custom Field IDs to clear.
      ' The field IDs may be specified as simple Integers, in which case all data relating to a Custom Field
      ' is cleared. 
      ' Alternatively the ID may be specified as XX.YY where 'XX' is the Custom Field ID and 
      ' 'YY' is the specific Group ID to clear.
      ' Alternatively the ID may be specified as XX.YY.ZZ[.ZZ] where 'XX' is the Custom Field ID and 
      ' 'YY' is the specific Group ID to clear and ZZ(s) is (are) the specific instrument(s) to clear.
      '
      ' ***************************************************************************************************

      Dim UpgradedLock As Boolean = False
      Dim LockCookie As Threading.LockCookie = Nothing

      Try
        If (Not CacheLockObject.IsReaderLockHeld) Then
          CacheLockObject.AcquireWriterLock(LockDelayTime)
        Else
          LockCookie = CacheLockObject.UpgradeToWriterLock(LockDelayTime)
          UpgradedLock = True
        End If

        Dim FieldIDs() As String
        Dim FieldComponents() As String
        Dim FieldComponentsSplitChars() As Char
        Dim FieldCounter As Integer
        Dim ThisUpdateItem As String
        Dim CustomFieldID As Integer
        Dim GroupID As Integer
        Dim InstrumentID As Integer
        Dim ICounter As Integer

        FieldIDs = pUpdateDetail.Split(New Char() {CChar(","), CChar("|"), CChar("�"), Chr(9)})
        FieldComponentsSplitChars = New Char() {CChar(".")}

        If (FieldIDs IsNot Nothing) AndAlso (FieldIDs.Length > 0) Then
          For FieldCounter = 0 To (FieldIDs.Length - 1)
            ThisUpdateItem = FieldIDs(FieldCounter).Trim

            If (ThisUpdateItem IsNot Nothing) AndAlso (ThisUpdateItem.Length > 0) Then
              If (ThisUpdateItem.Contains(".")) Then

                CustomFieldID = 0
                GroupID = 0

                FieldComponents = FieldIDs(FieldCounter).Split(FieldComponentsSplitChars) ' New Char() {"."}

                If (FieldComponents IsNot Nothing) AndAlso (FieldComponents.Length > 0) Then
                  If IsNumeric(FieldComponents(0)) Then
                    CustomFieldID = CInt(FieldComponents(0))
                  End If

                  If (FieldComponents.Length > 1) AndAlso IsNumeric(FieldComponents(1)) Then
                    GroupID = CInt(FieldComponents(1))
                  End If

                  If (FieldComponents.Length <= 2) Then
                    ClearDataCache(CustomFieldID, GroupID)
                  Else
                    For ICounter = 2 To (FieldComponents.Length - 1)
                      If IsNumeric(FieldComponents(ICounter)) Then
                        InstrumentID = CInt(FieldComponents(ICounter))

                        ClearDataCache(CustomFieldID, GroupID, InstrumentID)
                      End If
                    Next
                  End If

                End If

              Else
                If (IsNumeric(FieldIDs(FieldCounter))) Then
                  ClearDataCache(CInt(FieldIDs(FieldCounter)))
                End If
              End If
            End If

          Next
        End If

      Catch ex As Exception
      Finally
        If UpgradedLock Then
          CacheLockObject.DowngradeFromWriterLock(LockCookie)
        Else
          CacheLockObject.ReleaseWriterLock()
        End If
      End Try

    End Sub

    Public Sub ClearStaticDataCache(ByVal pUpdateDetail As String)
      ' ***************************************************************************************************
      '
      ' 'pUpdateDetail' provides a delimited list of Static Field IDs to clear.
      ' The field IDs may be specified as simple Integers, in which case all data relating to a Custom Field
      ' is cleared. 
      ' Alternatively the ID may be specified as XX.YY where 'XX' is the Custom Field ID and 
      ' 'YY' is the specific Group ID to clear.
      ' Alternatively the ID may be specified as XX.YY.ZZ[.ZZ] where 'XX' is the Custom Field ID and 
      ' 'YY' is the specific Group ID to clear and ZZ(s) is (are) the specific instrument(s) to clear.
      '
      ' ***************************************************************************************************

      Dim UpgradedLock As Boolean = False
      Dim LockCookie As Threading.LockCookie = Nothing

      Try
        If (Not CacheLockObject.IsReaderLockHeld) Then
          CacheLockObject.AcquireWriterLock(LockDelayTime)
        Else
          LockCookie = CacheLockObject.UpgradeToWriterLock(LockDelayTime)
          UpgradedLock = True
        End If

        Dim FieldIDs() As String
        Dim FieldComponents() As String
        Dim FieldComponentsSplitChars() As Char
        Dim FieldCounter As Integer
        Dim ThisUpdateItem As String
        Dim CustomFieldID As Integer
        Dim GroupID As Integer
        Dim InstrumentID As Integer
        Dim ICounter As Integer

        FieldIDs = pUpdateDetail.Split(New Char() {CChar(","), CChar("|"), CChar("�"), Chr(9)})
        FieldComponentsSplitChars = New Char() {CChar(".")}

        If (FieldIDs IsNot Nothing) AndAlso (FieldIDs.Length > 0) Then
          For FieldCounter = 0 To (FieldIDs.Length - 1)
            ThisUpdateItem = FieldIDs(FieldCounter).Trim

            If (ThisUpdateItem IsNot Nothing) AndAlso (ThisUpdateItem.Length > 0) Then
              If (ThisUpdateItem.Contains(".")) Then

                CustomFieldID = 0
                GroupID = 0

                FieldComponents = FieldIDs(FieldCounter).Split(FieldComponentsSplitChars) ' New Char() {"."}

                If (FieldComponents IsNot Nothing) AndAlso (FieldComponents.Length > 0) Then
                  If IsNumeric(FieldComponents(0)) Then
                    CustomFieldID = CInt(FieldComponents(0)) + STATIC_FIELD_FLAG
                  End If

                  If (FieldComponents.Length > 1) AndAlso IsNumeric(FieldComponents(1)) Then
                    GroupID = CInt(FieldComponents(1))
                  End If

                  If (FieldComponents.Length <= 2) Then
                    ClearDataCache(CustomFieldID, GroupID)
                  Else
                    For ICounter = 2 To (FieldComponents.Length - 1)
                      If IsNumeric(FieldComponents(ICounter)) Then
                        InstrumentID = CInt(FieldComponents(ICounter))

                        ClearDataCache(CustomFieldID, GroupID, InstrumentID)
                      End If
                    Next
                  End If

                End If

              Else
                If (IsNumeric(FieldIDs(FieldCounter))) Then
                  ClearDataCache(CInt(FieldIDs(FieldCounter)) + STATIC_FIELD_FLAG)
                End If
              End If
            End If

          Next
        End If

      Catch ex As Exception
      Finally
        If UpgradedLock Then
          CacheLockObject.DowngradeFromWriterLock(LockCookie)
        Else
          CacheLockObject.ReleaseWriterLock()
        End If
      End Try

    End Sub

    Public Sub ClearDataCache(ByVal pCustomFieldID As Integer, ByVal pGroupID As Integer, Optional ByVal pInstrumentID As Integer = 0)
      ' ***************************************************************************************************
      ' Clear All (or Specific) Group Data for All (or specific) Custom Field IDs.
      '
      ' ***************************************************************************************************
      Dim UpgradedLock As Boolean = False
      Dim LockCookie As Threading.LockCookie = Nothing

      Try
        If (Not CacheLockObject.IsReaderLockHeld) Then
          CacheLockObject.AcquireWriterLock(LockDelayTime)
        Else
          LockCookie = CacheLockObject.UpgradeToWriterLock(LockDelayTime)
          UpgradedLock = True
        End If

        Dim ThisDataCache As FieldDataCacheClass

        If (pCustomFieldID <= 0) Then

          If (_DataCacheDictionary.Count > 0) Then
            Dim KeyArray(_DataCacheDictionary.Keys.Count - 1) As Integer
            Dim KeyIndex As Integer

            _DataCacheDictionary.Keys.CopyTo(KeyArray, 0)

            For KeyIndex = 0 To (KeyArray.Length - 1)
              If (KeyArray(KeyIndex) > 0) Then
                ClearDataCache(KeyArray(KeyIndex), pGroupID)
              End If
            Next
          End If

        Else
          If (_DataCacheDictionary.ContainsKey(pCustomFieldID)) Then
            ThisDataCache = _DataCacheDictionary(pCustomFieldID)

            If (pInstrumentID <= 0) Then
              ThisDataCache.ClearData(pGroupID)
            Else
              If (_IdNormalisationDictionary.ContainsKey(pInstrumentID)) Then
                ThisDataCache.ClearData(pGroupID, _IdNormalisationDictionary(pInstrumentID))
              End If
            End If
          End If
        End If

        ThisDataCache = Nothing

      Catch ex As Exception
      Finally
        If UpgradedLock Then
          CacheLockObject.DowngradeFromWriterLock(LockCookie)
        Else
          CacheLockObject.ReleaseWriterLock()
        End If
      End Try

    End Sub

    Public Sub ClearDataCacheGroupData(ByVal pCustomFieldID As Integer, ByVal pGroupID As Integer)
      ' ***************************************************************************************************
      ' Clear All (or Specific) Group Data for All (or specific) Custom Field IDs.
      '
      ' Does NOT Clear Group Zero Data.
      ' ***************************************************************************************************
      Dim UpgradedLock As Boolean = False
      Dim LockCookie As Threading.LockCookie = Nothing

      Try
        If (Not CacheLockObject.IsReaderLockHeld) Then
          CacheLockObject.AcquireWriterLock(LockDelayTime)
        Else
          LockCookie = CacheLockObject.UpgradeToWriterLock(LockDelayTime)
          UpgradedLock = True
        End If

        Dim ThisDataCache As FieldDataCacheClass

        If (pCustomFieldID <= 0) Then

          If (_DataCacheDictionary.Count > 0) Then
            Dim KeyArray(_DataCacheDictionary.Keys.Count - 1) As Integer
            Dim KeyIndex As Integer

            _DataCacheDictionary.Keys.CopyTo(KeyArray, 0)

            For KeyIndex = 0 To (KeyArray.Length - 1)
              If (KeyArray(KeyIndex) > 0) Then
                _DataCacheDictionary(KeyArray(KeyIndex)).ClearGroupData(pGroupID)
              End If
            Next
          End If

        Else

          If (_DataCacheDictionary.ContainsKey(pCustomFieldID)) Then
            ThisDataCache = _DataCacheDictionary(pCustomFieldID)

            ThisDataCache.ClearGroupData(pGroupID)
          End If
        End If

        ThisDataCache = Nothing

      Catch ex As Exception
      Finally
        If UpgradedLock Then
          CacheLockObject.DowngradeFromWriterLock(LockCookie)
        Else
          CacheLockObject.ReleaseWriterLock()
        End If
      End Try

    End Sub

    Public Sub ClearDataCacheGroupData(ByVal pUpdateDetail As String)
      ' ***************************************************************************************************
      '
      ' 'pUpdateDetail' provides a delimited list of Group IDs to clear.
      '
      ' ***************************************************************************************************
      Dim UpgradedLock As Boolean = False
      Dim LockCookie As Threading.LockCookie = Nothing

      Try

        If (Not CacheLockObject.IsReaderLockHeld) Then
          CacheLockObject.AcquireWriterLock(LockDelayTime)
        Else
          LockCookie = CacheLockObject.UpgradeToWriterLock(LockDelayTime)
          UpgradedLock = True
        End If

        Dim FieldIDs() As String
        Dim FieldCounter As Integer

        FieldIDs = pUpdateDetail.Split(New Char() {CChar(","), CChar("|"), CChar("�"), Chr(9)})

        If (FieldIDs IsNot Nothing) AndAlso (FieldIDs.Length > 0) Then
          For FieldCounter = 0 To (FieldIDs.Length - 1)
            If (IsNumeric(FieldIDs(FieldCounter))) Then

              ClearDataCacheGroupData(0, CInt(FieldIDs(FieldCounter)))

            End If
          Next
        End If

      Catch ex As Exception
      Finally
        If UpgradedLock Then
          CacheLockObject.DowngradeFromWriterLock(LockCookie)
        Else
          CacheLockObject.ReleaseWriterLock()
        End If
      End Try

    End Sub

    Public Sub VerifyCustomFieldTypes(ByVal pCustomFieldID As Integer)
      ' ***************************************************************************************************
      ' Check that the Custom Field Cache data type is consistent with the Field Definition in tblPertracCustomFields.
      ' This should be performed after any changed to the field definition table since the data type could be changed.
      '
      ' At the same time, check the default value.
      '
      ' ***************************************************************************************************
      Dim UpgradedLock As Boolean = False
      Dim LockCookie As Threading.LockCookie = Nothing

      Try
        If (Not CacheLockObject.IsReaderLockHeld) Then
          CacheLockObject.AcquireWriterLock(LockDelayTime)
        Else
          LockCookie = CacheLockObject.UpgradeToWriterLock(LockDelayTime)
          UpgradedLock = True
        End If

        Dim ThisDataCache As FieldDataCacheClass

        If (pCustomFieldID <= 0) Then
          ' For Field value of Zero, Cycle through all Fields.

          If (_DataCacheDictionary.Count > 0) Then
            Dim KeyArray(_DataCacheDictionary.Keys.Count - 1) As Integer
            Dim KeyIndex As Integer

            _DataCacheDictionary.Keys.CopyTo(KeyArray, 0)

            For KeyIndex = 0 To (KeyArray.Length - 1)
              If (KeyArray(KeyIndex) > 0) Then
                VerifyCustomFieldTypes(KeyArray(KeyIndex))
              End If
            Next
          End If

        Else

          If (_DataCacheDictionary.ContainsKey(pCustomFieldID)) Then
            Dim CustomFieldRow As RenaissanceDataClass.DSPertracCustomFields.tblPertracCustomFieldsRow
            Dim TmpObject As Object

            TmpObject = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblPertracCustomFields, pCustomFieldID, "")
            If (TmpObject IsNot Nothing) Then
              CustomFieldRow = CType(TmpObject, RenaissanceDataClass.DSPertracCustomFields.tblPertracCustomFieldsRow)

              ThisDataCache = _DataCacheDictionary(pCustomFieldID)

              ' Chect Data Types.

              If (ThisDataCache.FieldDataType <> CType(CustomFieldRow.FieldDataType, RenaissanceDataType)) Then
                ' Setting the FieldDataType to a different value should case a 'Resize()' action which will 
                ' Re-Dimension the storage Array(s) to the correct data type.

                ThisDataCache.FieldDataType = CType(CustomFieldRow.FieldDataType, RenaissanceDataType)

              End If

              ' Check the default value while we're at it.

              ThisDataCache.DefaultValue = CustomFieldRow.DefaultValue

            End If
          End If
        End If

        ThisDataCache = Nothing

      Catch ex As Exception
      Finally
        If UpgradedLock Then
          CacheLockObject.DowngradeFromWriterLock(LockCookie)
        Else
          CacheLockObject.ReleaseWriterLock()
        End If
      End Try
    End Sub

    Public Sub VerifyCustomFieldTypes(ByVal UpdateDetail As String)
      ' ***************************************************************************
      '
      ' Parse the given Update Detail and process accordingly.
      '
      ' Allows a Comma separated list of Custom Field IDs.
      ' ***************************************************************************

      Try
        If (UpdateDetail Is Nothing) OrElse (UpdateDetail.Length <= 0) Then
          Exit Sub
        End If
      Catch ex As Exception
      End Try

      Dim UpgradedLock As Boolean = False
      Dim LockCookie As Threading.LockCookie = Nothing

      Try
        If (Not CacheLockObject.IsReaderLockHeld) Then
          CacheLockObject.AcquireWriterLock(LockDelayTime)
        Else
          LockCookie = CacheLockObject.UpgradeToWriterLock(LockDelayTime)
          UpgradedLock = True
        End If

        Dim FieldIDs() As String
        Dim FieldCounter As Integer

        FieldIDs = UpdateDetail.Split(New Char() {CChar(","), CChar("|"), CChar("�"), Chr(9)})

        If (FieldIDs IsNot Nothing) AndAlso (FieldIDs.Length > 0) Then
          For FieldCounter = 0 To (FieldIDs.Length - 1)
            If (FieldIDs(FieldCounter) IsNot Nothing) AndAlso (FieldIDs(FieldCounter).Length > 0) AndAlso (IsNumeric(FieldIDs(FieldCounter))) Then
              VerifyCustomFieldTypes(CInt(FieldIDs(FieldCounter)))
            End If
          Next
        End If

      Catch ex As Exception

      Finally
        If UpgradedLock Then
          CacheLockObject.DowngradeFromWriterLock(LockCookie)
        Else
          CacheLockObject.ReleaseWriterLock()
        End If
      End Try
    End Sub

    ' Initialisation Functions

    Private Function MasterNameInstrumentCount() As Integer
      ' ***************************************************************************
      '
      '
      ' ***************************************************************************

      Dim RVal As Integer = 0
      'Dim MasterNameDS As RenaissanceDataClass.DSMastername

      Try

        If (MainForm.MasternameDictionary IsNot Nothing) Then
          RVal = MainForm.MasternameDictionary.Count
        End If

        'MasterNameDS = MainForm.Load_Table(RenaissanceStandardDatasets.Mastername, False)

        'If (MasterNameDS IsNot Nothing) AndAlso (MasterNameDS.tblMastername IsNot Nothing) Then
        '	RVal = MasterNameDS.tblMastername.Rows.Count
        'End If

      Catch ex As Exception
        RVal = 0
        'Finally
        'MasterNameDS = Nothing
      End Try

      Return RVal

    End Function

    Public Sub RefreshInstrumentNumerator()
      ' ***************************************************************************
      ' Check that all Instrument IDs represented in the Mastername table have 
      ' Normalised Index entries.
      ' ***************************************************************************
      Dim UpgradedLock As Boolean = False
      Dim LockCookie As Threading.LockCookie = Nothing
      Dim NewEntriesAdded As Boolean = False
      Dim ResizeRequired As Boolean = False
      ' Dim MasterNameDS As RenaissanceDataClass.DSMastername

      Try
        If (Not CacheLockObject.IsReaderLockHeld) Then
          CacheLockObject.AcquireWriterLock(LockDelayTime)
        Else
          LockCookie = CacheLockObject.UpgradeToWriterLock(LockDelayTime)
          UpgradedLock = True
        End If

        Try
          If (MainForm.MasternameDictionary IsNot Nothing) AndAlso (MainForm.MasternameDictionary.Count > 0) Then
            Dim MasternameIDArray() As Integer = MainForm.MasternameDictionary.KeyArray

            If (_IdLookupArray Is Nothing) OrElse (_IdLookupArray.Length < Math.Max(MainForm.MasternameDictionary.Count, Current_DataItemArraySize)) Then
              ReDim _IdLookupArray(Math.Max(MainForm.MasternameDictionary.Count, Current_DataItemArraySize))
            End If

            Dim ThisID As Integer
            For IndexID As Integer = 0 To (MasternameIDArray.Count - 1)
              ThisID = MasternameIDArray(IndexID)

              If (Not _IdNormalisationDictionary.ContainsKey(ThisID)) Then
                _IdNormalisationDictionary.Add(ThisID, _IdNormalisationDictionary.Count)
                NewEntriesAdded = True
              End If

              _IdLookupArray(_IdNormalisationDictionary(ThisID)) = ThisID
            Next

          End If

          'MasterNameDS = MainForm.Load_Table(RenaissanceStandardDatasets.Mastername, False)

          'If (MasterNameDS IsNot Nothing) AndAlso (MasterNameDS.tblMastername IsNot Nothing) Then
          '	If (_IdLookupArray Is Nothing) OrElse (_IdLookupArray.Length < Math.Max(MasterNameDS.tblMastername.Rows.Count, Current_DataItemArraySize)) Then
          '		ReDim _IdLookupArray(Current_DataItemArraySize)
          '	End If

          '	For Each thisRow As RenaissanceDataClass.DSMastername.tblMasternameRow In MasterNameDS.tblMastername.Rows

          '		If (Not _IdNormalisationDictionary.ContainsKey(thisRow.ID)) Then
          '			_IdNormalisationDictionary.Add(thisRow.ID, _IdNormalisationDictionary.Count)
          '			NewEntriesAdded = True
          '		End If

          '		_IdLookupArray(_IdNormalisationDictionary(thisRow.ID)) = thisRow.ID
          '	Next
          'End If

        Catch ex As Exception
          NewEntriesAdded = True
        End Try

        ' If there have been new Instruments added, then mark all Data Cache objects as Partial (if they were Complete).
        ' Also Resize as necessary.

        If (NewEntriesAdded) Then

          If (_IdNormalisationDictionary.Count >= (Current_DataItemArraySize - 1)) Then
            ResizeRequired = True
            Current_DataItemArraySize = (_IdNormalisationDictionary.Count * 1.1)
          End If

          If (_DataCacheDictionary.Keys.Count > 0) Then
            Dim ThisFieldDataCache As FieldDataCacheClass
            Dim KeyArray(_DataCacheDictionary.Keys.Count - 1) As Integer
            Dim KeyIndex As Integer

            _DataCacheDictionary.Keys.CopyTo(KeyArray, 0)

            For KeyIndex = 0 To (KeyArray.Length - 1)
              ThisFieldDataCache = _DataCacheDictionary(KeyArray(KeyIndex))

              If (ResizeRequired) Then
                ThisFieldDataCache.Resize(Current_DataItemArraySize, True)
              End If

              If (ThisFieldDataCache.DataStatus(0) = CacheStatus.FullyPopulated) Then
                ThisFieldDataCache.DataStatus(0) = CacheStatus.PartiallyPopulated
              End If
            Next
          End If

        End If

        NumeratorInitialised = True

      Catch ex As Exception
      Finally
        ' MasterNameDS = Nothing
        If UpgradedLock Then
          CacheLockObject.DowngradeFromWriterLock(LockCookie)
        Else
          CacheLockObject.ReleaseWriterLock()
        End If
      End Try

    End Sub

    Public Function AddInstrumentNumerator(ByVal pInstrumentID As Integer) As Integer
      ' ***************************************************************************
      '
      ' ***************************************************************************
      Dim RVal As Integer = (-1)

      Try
        Dim ThisIndex As Integer
        Dim ResizeRequired As Boolean = False

        If (_IdNormalisationDictionary.ContainsKey(pInstrumentID)) Then
          RVal = _IdNormalisationDictionary(pInstrumentID)
        Else

          ' Set New Normalisation Value.

          ThisIndex = _IdNormalisationDictionary.Count
          _IdNormalisationDictionary.Add(pInstrumentID, ThisIndex)
          RVal = ThisIndex

          ' Resize IdLookupArray

          If (_IdLookupArray Is Nothing) OrElse (_IdLookupArray.Length < Current_DataItemArraySize) Then
            ReDim Preserve _IdLookupArray(Current_DataItemArraySize)
          End If
          _IdLookupArray(ThisIndex) = pInstrumentID

          ' Resize Data Cache Items if necessary and Set Data Cache Item Status's

          If (_IdNormalisationDictionary.Count >= (Current_DataItemArraySize - 1)) Then
            ResizeRequired = True
            Current_DataItemArraySize = (_IdNormalisationDictionary.Count * 1.1)
          End If

          If (_DataCacheDictionary.Keys.Count > 0) Then
            Dim ThisFieldDataCache As FieldDataCacheClass
            Dim KeyArray(_DataCacheDictionary.Keys.Count - 1) As Integer
            Dim KeyIndex As Integer

            _DataCacheDictionary.Keys.CopyTo(KeyArray, 0)

            For KeyIndex = 0 To (KeyArray.Length - 1)
              ThisFieldDataCache = _DataCacheDictionary(KeyArray(KeyIndex))

              If (ResizeRequired) Then
                ThisFieldDataCache.Resize(Current_DataItemArraySize, True)
              End If

              If (ThisFieldDataCache.DataStatus(0) = CacheStatus.FullyPopulated) Then
                ThisFieldDataCache.DataStatus(0) = CacheStatus.PartiallyPopulated
              End If
            Next
          End If

        End If

      Catch ex As Exception
        RVal = (-1)
      End Try

      Return RVal

    End Function

    ' Set Data Function(s)

    Private Sub SetData(ByRef DataCacheObject As FieldDataCacheClass, ByVal pGroupID As Integer, ByRef tblCustomData As DataTable, ByVal pCompleteDataSet As Boolean)
      ' ***************************************************************************
      '
      ' ***************************************************************************
      Dim GroupIDOrdinal As Integer = (-1)
      Dim PertacIDOrdinal As Integer = (-1)
      Dim FieldDataOrdinal As Integer = (-1)
      Dim thisRow As DataRow
      Dim ThisInstrumentID As Integer
      Dim ThisIndex As Integer

      Try
        ' First Check Instrument IDs are Numerated.

        If (Not NumeratorInitialised) Then
          RefreshInstrumentNumerator()
        End If

        ' Extend 

        GroupIDOrdinal = tblCustomData.Columns.IndexOf("GroupID")
        PertacIDOrdinal = tblCustomData.Columns.IndexOf("PertracID")
        FieldDataOrdinal = tblCustomData.Columns.IndexOf("DataField")

        If ((tblCustomData Is Nothing) OrElse (tblCustomData.Rows.Count <= 0)) AndAlso (pGroupID > 0) Then
          ' For empty dataset on Non-Zero Group, cause empty dictionary to be created (To store Group Status).

          DataCacheObject.SetDataPoint(-1, pGroupID, 0)

        Else

          For Each thisRow In tblCustomData.Rows
            ThisInstrumentID = CInt(thisRow(PertacIDOrdinal))

            Try
              If (_IdNormalisationDictionary.ContainsKey(ThisInstrumentID)) Then
                ' Get existing item.

                ThisIndex = _IdNormalisationDictionary(ThisInstrumentID)

              Else
                ' Add Item

                ThisIndex = AddInstrumentNumerator(ThisInstrumentID)

              End If

            Catch ex As KeyNotFoundException
              ' Should not occur...

              ThisIndex = AddInstrumentNumerator(ThisInstrumentID)

            Catch ex As Exception
              ThisIndex = 0
            End Try

            If (ThisIndex >= 0) Then
              DataCacheObject.SetDataPoint(ThisIndex, CInt(thisRow(GroupIDOrdinal)), thisRow(FieldDataOrdinal))
            End If
          Next

        End If

        If (pCompleteDataSet) Then
          DataCacheObject.DataStatus(pGroupID) = CacheStatus.FullyPopulated
        Else
          DataCacheObject.DataStatus(pGroupID) = CacheStatus.PartiallyPopulated
        End If

        If (pGroupID > 0) AndAlso (DataCacheObject.DataStatus(0) = CacheStatus.Empty) Then
          DataCacheObject.DataStatus(0) = CacheStatus.PartiallyPopulated
        End If

      Catch ex As Exception
      End Try

    End Sub

    ' Get Data Functions

    Public Function GetDataPoint(ByVal pCustomFieldID As Integer, ByVal pGroupID As Integer, ByVal pInstrumentID As Integer) As Object
      ' ***************************************************************************
      '
      ' Return Requested Data Point, (re)Loading Group data if necessary.
      '
      ' ***************************************************************************

      Dim ThisCacheObject As FieldDataCacheClass = Nothing
      Dim RVal As Object = Nothing
      Dim ReloadData As Boolean = True

      Try

        Try
          CacheLockObject.AcquireReaderLock(LockDelayTime)

          If (_DataCacheDictionary.ContainsKey(pCustomFieldID)) Then
            ThisCacheObject = _DataCacheDictionary(pCustomFieldID)

            ' Only Test to re-load data if either Group Zero or specified group is not fully loaded.

            If ((ThisCacheObject.DataStatus(pGroupID) = CacheStatus.FullyPopulated) AndAlso (ThisCacheObject.DataStatus(0) = CacheStatus.FullyPopulated)) Then
              ReloadData = False
            Else

              If (pGroupID <= 0) Then
                ' Group Zero 
                ' If the data point does not exist (and at this point Group Zero is not Fully populated), Load complete Group Zero Data.

                If ThisCacheObject.DataPointExists(_IdNormalisationDictionary(pInstrumentID), 0, False) Then
                  ReloadData = False
                End If
              Else
                ' Not Group Zero. 

                ' If Specific Group Data point exists then there is no need to load the Group Data.

                If ThisCacheObject.DataPointExists(_IdNormalisationDictionary(pInstrumentID), pGroupID, False) Then
                  ReloadData = False
                Else
                  ' If the Specific Group is fully populated and the datum exists in Group Zero, Don't load data.

                  If (ThisCacheObject.DataStatus(pGroupID) = CacheStatus.FullyPopulated) Then
                    If ThisCacheObject.DataPointExists(_IdNormalisationDictionary(pInstrumentID), 0, False) Then
                      ReloadData = False
                    End If
                  End If
                End If
              End If

            End If

          End If

          ' Load data.

          If (ReloadData) Then
            If (ThisCacheObject IsNot Nothing) AndAlso (ThisCacheObject.DataStatus(pGroupID) = CacheStatus.FullyPopulated) Then
              LoadCustomFieldData(pCustomFieldID, 0)
            Else
              LoadCustomFieldData(pCustomFieldID, pGroupID)
            End If
          End If

          ' Return Requested Data.
          ' The DataCache function will return the specied data point, or a default value if it does not exist.

          If (_DataCacheDictionary.ContainsKey(pCustomFieldID)) Then
            ThisCacheObject = _DataCacheDictionary(pCustomFieldID)

            Try

              RVal = ThisCacheObject.GetDataPoint(_IdNormalisationDictionary(pInstrumentID), pGroupID)

            Catch KeyNotFound As KeyNotFoundException

              ' Key can be missing if an instrument is in the 'Information' table but not the 'Mastername' table

              Select Case ThisCacheObject.FieldDataType
                Case RenaissanceDataType.BooleanType
                  RVal = False
                Case RenaissanceDataType.DateType
                  RVal = RenaissanceGlobals.Globals.Renaissance_BaseDate
                Case RenaissanceDataType.NumericType, RenaissanceDataType.PercentageType
                  RVal = 0.0#
                Case RenaissanceDataType.TextType
                  RVal = ""
                Case Else
                  RVal = Nothing
              End Select

            Catch OtherEx As Exception
            End Try
          End If

        Catch ex As Exception
        Finally
          CacheLockObject.ReleaseReaderLock()
        End Try

      Catch ex As Exception
        RVal = Nothing
      Finally
        ThisCacheObject = Nothing
      End Try

      Return RVal

    End Function

    Public Function GetDataPoint(ByVal pCustomFieldID As Integer, ByVal pGroupID As Integer, ByVal pInstrumentIDs() As Integer) As Object()
      ' ***************************************************************************
      '
      ' Return Requested Data Points, (re)Loading Group data if necessary.
      '
      ' ***************************************************************************

      Dim ThisCacheObject As FieldDataCacheClass = Nothing
      Dim RVal(-1) As Object
      Dim ReloadData As Boolean = False
      Dim ICounter As Integer

      Try

        If (pInstrumentIDs IsNot Nothing) AndAlso (pInstrumentIDs.Length > 0) Then
          Try
            ReDim RVal(pInstrumentIDs.Length - 1)

            CacheLockObject.AcquireReaderLock(LockDelayTime)

            If (_DataCacheDictionary.ContainsKey(pCustomFieldID)) Then
              ThisCacheObject = _DataCacheDictionary(pCustomFieldID)

              ' Only Test to re-load data if either Group Zero or specified group is not fully loaded.
              If Not ((ThisCacheObject.DataStatus(pGroupID) = CacheStatus.FullyPopulated) AndAlso (ThisCacheObject.DataStatus(0) = CacheStatus.FullyPopulated)) Then

                For ICounter = 0 To (pInstrumentIDs.Length - 1)

                  If (Not ReloadData) Then

                    ' If the Data does not exist in the specified Group...

                    If Not ThisCacheObject.DataPointExists(_IdNormalisationDictionary(pInstrumentIDs(ICounter)), pGroupID, False) Then

                      ' For Group Zero, If the group is not fully loaded then thats it...

                      If (pGroupID <= 0) AndAlso (ThisCacheObject.DataStatus(0) <> CacheStatus.FullyPopulated) Then
                        ReloadData = True

                      Else

                        ' For Non-Group Zero, If it's not fully loaded then reload, otherwise check Group Zero.

                        If (ThisCacheObject.DataStatus(pGroupID) = CacheStatus.FullyPopulated) Then
                          If Not ThisCacheObject.DataPointExists(_IdNormalisationDictionary(pInstrumentIDs(ICounter)), 0, False) Then
                            ReloadData = True
                          End If
                        Else
                          ReloadData = True
                        End If
                      End If
                    End If

                  End If
                Next

              End If
            End If

            If (ReloadData) Then
              If (ThisCacheObject IsNot Nothing) AndAlso (ThisCacheObject.DataStatus(pGroupID) = CacheStatus.FullyPopulated) Then
                LoadCustomFieldData(pCustomFieldID, 0)
              Else
                LoadCustomFieldData(pCustomFieldID, pGroupID)
              End If
            End If

            If (_DataCacheDictionary.ContainsKey(pCustomFieldID)) Then
              ThisCacheObject = _DataCacheDictionary(pCustomFieldID)

              For ICounter = 0 To (pInstrumentIDs.Length - 1)
                Try

                  RVal(ICounter) = ThisCacheObject.GetDataPoint(_IdNormalisationDictionary(pInstrumentIDs(ICounter)), pGroupID)

                Catch KeyNotFound As KeyNotFoundException

                  ' Key can be missing if an instrument is in the 'Information' table but not the 'Mastername' table

                  Select Case ThisCacheObject.FieldDataType
                    Case RenaissanceDataType.BooleanType
                      RVal(ICounter) = False
                    Case RenaissanceDataType.DateType
                      RVal(ICounter) = RenaissanceGlobals.Globals.Renaissance_BaseDate
                    Case RenaissanceDataType.NumericType, RenaissanceDataType.PercentageType
                      RVal(ICounter) = 0.0#
                    Case RenaissanceDataType.TextType
                      RVal(ICounter) = ""
                    Case Else
                      RVal(ICounter) = Nothing
                  End Select

                Catch ex As Exception
                  RVal(ICounter) = Nothing
                End Try

              Next

            End If

          Catch ex As Exception
          Finally
            CacheLockObject.ReleaseReaderLock()
          End Try
        End If

      Catch ex As Exception
        RVal = Nothing
      Finally
        ThisCacheObject = Nothing
      End Try

      Return RVal

    End Function

    ' Load Data Functions 

    Public Function LoadCustomFieldData(ByVal pCustomFieldID As Integer, ByVal pGroupID As Integer) As Boolean
      ' ***************************************************************************************************
      '
      '
      ' ***************************************************************************************************

      Dim RVal As Boolean = False
      Dim ThisDataCache As FieldDataCacheClass
      Dim CustomFieldRow As RenaissanceDataClass.DSPertracCustomFields.tblPertracCustomFieldsRow = Nothing
      Dim TmpObject As Object
      Dim UpgradedLock As Boolean = False
      Dim LockCookie As Threading.LockCookie = Nothing
      Dim IsStaticField As Boolean

      Try
        If (Not CacheLockObject.IsReaderLockHeld) Then
          CacheLockObject.AcquireWriterLock(LockDelayTime)
        Else
          LockCookie = CacheLockObject.UpgradeToWriterLock(LockDelayTime)
          UpgradedLock = True
        End If

        IsStaticField = CBool(pCustomFieldID And STATIC_FIELD_FLAG)

        ' Get Field Details

        If (IsStaticField) Then

          pGroupID = 0

        Else

          TmpObject = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblPertracCustomFields, pCustomFieldID, "")
          If (TmpObject Is Nothing) Then
            Return False
            Exit Function
          End If
          CustomFieldRow = CType(TmpObject, RenaissanceDataClass.DSPertracCustomFields.tblPertracCustomFieldsRow)

        End If

        ' First Check Instrument IDs are Numerated.

        If (Not NumeratorInitialised) Then
          RefreshInstrumentNumerator()
        End If

        ' Get Cache Data 

        If (_DataCacheDictionary.ContainsKey(pCustomFieldID)) Then
          ThisDataCache = _DataCacheDictionary(pCustomFieldID)
          ThisDataCache.ClearData(pGroupID)
        Else
          If (IsStaticField) Then
            ThisDataCache = New FieldDataCacheClass(RenaissanceDataType.TextType, "", Current_DataItemArraySize)
          ElseIf (CustomFieldRow.IsDefaultValueNull) Then
            ThisDataCache = New FieldDataCacheClass(CustomFieldRow.FieldDataType, Nothing, Current_DataItemArraySize)
          Else
            ThisDataCache = New FieldDataCacheClass(CustomFieldRow.FieldDataType, CustomFieldRow.DefaultValue, Current_DataItemArraySize)
          End If

          _DataCacheDictionary.Add(pCustomFieldID, ThisDataCache)
        End If


        ' Get Field Data 
        Dim DataFieldName As String = ""
        Dim SelectString As String = ""

        DataFieldName = "FieldNumericData"

        If (IsStaticField) Then
          DataFieldName = "FieldTextData"

        ElseIf (CustomFieldRow.FieldDataType And RenaissanceDataType.TextType) = RenaissanceDataType.TextType Then
          DataFieldName = "FieldTextData"

        ElseIf (CustomFieldRow.FieldDataType And RenaissanceDataType.DateType) = RenaissanceDataType.DateType Then
          DataFieldName = "FieldDateData"

        ElseIf (CustomFieldRow.FieldDataType And RenaissanceDataType.BooleanType) = RenaissanceDataType.BooleanType Then
          DataFieldName = "FieldBooleanData"

        End If

        If (IsStaticField) Then

          Dim InformationField As PertracInformationFields
          Dim tblFieldMappings As RenaissanceDataClass.DSPertracFieldMapping.tblPertracFieldMappingDataTable = Nothing
          Dim SelectedFieldMappings() As RenaissanceDataClass.DSPertracFieldMapping.tblPertracFieldMappingRow
          Dim ThisFieldMapping As RenaissanceDataClass.DSPertracFieldMapping.tblPertracFieldMappingRow
          Dim FieldCount As Integer

          SelectString = ""

          InformationField = CType(pCustomFieldID And (STATIC_FIELD_FLAG - 1), PertracInformationFields)

          Try
            tblFieldMappings = CType(MainForm.Load_Table(RenaissanceStandardDatasets.tblPertracFieldMapping), RenaissanceDataClass.DSPertracFieldMapping).tblPertracFieldMapping
          Catch ex As Exception
          End Try

          If (tblFieldMappings Is Nothing) OrElse (tblFieldMappings.Rows.Count <= 0) Then
            ' Apparently failed to get the table
            Return False
            Exit Function
          End If

          SelectedFieldMappings = tblFieldMappings.Select("FCP_FieldName='" & InformationField.ToString & "'", "DataProvider")
          If (SelectedFieldMappings Is Nothing) OrElse (SelectedFieldMappings.Length <= 0) Then
            ' No Mapped Fields
            Return False
            Exit Function
          End If

          ' ************************************
          ' Construct the select String.
          ' ************************************

          For FieldCount = 0 To (SelectedFieldMappings.Length - 1)
            ThisFieldMapping = SelectedFieldMappings(FieldCount)

            If (ThisFieldMapping.PertracField.Length > 0) Then
              Dim TempSelectString As String

              ' Original Pertrac 

              TempSelectString = "SELECT 0 AS GroupID, [ID] AS PertracID, " & ThisFieldMapping.PertracField & " AS DataField FROM [MASTERSQL].[dbo].[Information] WHERE (DataVendorName='" & ThisFieldMapping.DataProvider & "')"
              If SelectString.Length > 0 Then
                SelectString &= " UNION "
              End If
              SelectString &= TempSelectString

              ' New Tables.

              TempSelectString = "SELECT 0 AS GroupID, ([InstrumentID] + 1048576) AS PertracID, " & ThisFieldMapping.PertracField & " AS DataField FROM [MASTERSQL].[dbo].[tblInstrumentInformation] WHERE (DataVendorName='" & ThisFieldMapping.DataProvider & "')"
              If SelectString.Length > 0 Then
                SelectString &= " UNION "
              End If
              SelectString &= TempSelectString
            End If
          Next

        ElseIf (pGroupID = 0) Then
          SelectString = "SELECT GroupID, PertracID, [" & DataFieldName & "] AS DataField FROM fn_tblPertracCustomFieldData_CacheSelect(@GroupID, @CustomFieldID, @KnowledgeDate)"
        Else
          SelectString = "SELECT GroupID, PertracID, [" & DataFieldName & "] AS DataField FROM fn_tblPertracCustomFieldData_CacheSelectGroupBackfill(@GroupID, @CustomFieldID, @KnowledgeDate)"
        End If

        Dim tblCustomSelection As New DataTable
        Dim tmpCommand As New SqlCommand

        If SelectString.Length > 0 Then
          Try
            tmpCommand.CommandType = CommandType.Text
            tmpCommand.CommandText = SelectString
            tmpCommand.Parameters.Add("@GroupID", SqlDbType.Int).Value = pGroupID
            tmpCommand.Parameters.Add("@CustomFieldID", SqlDbType.Int).Value = pCustomFieldID
            tmpCommand.Parameters.Add("@KnowledgeDate", SqlDbType.DateTime).Value = MainForm.Main_Knowledgedate

            tmpCommand.Connection = MainForm.GetRenaissanceConnection
            tmpCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT

            SyncLock tmpCommand.Connection
              tblCustomSelection.Load(tmpCommand.ExecuteReader)
            End SyncLock

          Catch ex As Exception
            ' MainForm.LogError("LoadCustomFieldData()", LOG_LEVELS.Error, ex.Message, "Error Selecting from Custom Field Data" & vbCrLf & tmpCommand.CommandText, ex.StackTrace, True)
            tblCustomSelection = Nothing
            Exit Function
          Finally
            Try
              If (tmpCommand IsNot Nothing) AndAlso (tmpCommand.Connection IsNot Nothing) Then
                tmpCommand.Connection.Close()
                tmpCommand.Connection = Nothing
              End If
            Catch ex As Exception
            End Try
          End Try
        End If

        ' Set Data in Cache.

        If (tblCustomSelection IsNot Nothing) Then
          SetData(ThisDataCache, pGroupID, tblCustomSelection, True)
          tblCustomSelection.Rows.Clear()
          tblCustomSelection = Nothing
        End If

        RVal = True

      Catch ex As Exception

        RVal = False

      Finally
        If UpgradedLock Then
          CacheLockObject.DowngradeFromWriterLock(LockCookie)
        Else
          CacheLockObject.ReleaseWriterLock()
        End If

        TmpObject = Nothing
        CustomFieldRow = Nothing
      End Try

      Return RVal

    End Function

    Public Function LoadCustomFieldData(ByVal pCustomFieldID As Integer) As Boolean
      ' ***************************************************************************************************
      '
      '
      ' ***************************************************************************************************

      Dim RVal As Boolean = False

      Try

        RVal = LoadCustomFieldData(pCustomFieldID, 0)

      Catch ex As Exception
        RVal = False
      End Try

      Return RVal

    End Function

    Public Function GetCustomFieldDataStatus(ByVal pCustomFieldID As Integer) As CacheStatus
      ' ***************************************************************************
      '
      ' ***************************************************************************

      Return GetCustomFieldDataStatus(pCustomFieldID, 0)

    End Function

    Public Function GetCustomFieldDataStatus(ByVal pCustomFieldID As Integer, ByVal pGroupID As Integer) As CacheStatus
      ' ***************************************************************************
      '
      ' ***************************************************************************

      Dim RVal As CacheStatus = CacheStatus.Empty

      Try
        CacheLockObject.AcquireReaderLock(LockDelayTime)

        If (_DataCacheDictionary.ContainsKey(pCustomFieldID)) Then
          RVal = _DataCacheDictionary(pCustomFieldID).DataStatus(pGroupID)
        End If

      Catch ex As Exception
      Finally
        CacheLockObject.ReleaseReaderLock()
      End Try

      Return RVal

    End Function

    Public Function GetCustomFieldDataType(ByVal pCustomFieldID As Integer) As RenaissanceDataType
      ' ***************************************************************************
      '
      ' ***************************************************************************

      Dim RVal As RenaissanceDataType = RenaissanceDataType.None

      Try
        CacheLockObject.AcquireReaderLock(LockDelayTime)

        If (_DataCacheDictionary.ContainsKey(pCustomFieldID)) Then
          RVal = _DataCacheDictionary(pCustomFieldID).FieldDataType
        End If

      Catch ex As Exception
      Finally
        CacheLockObject.ReleaseReaderLock()
      End Try

      Return RVal

    End Function

    Public Function GetCustomField_InstrumentDataPresent(ByVal pCustomFieldID As Integer, ByVal pInstrumentID As Integer) As Boolean
      ' ***************************************************************************
      '
      ' Check the presence of data in the cache relating to the given Instrument ID.
      '
      ' ***************************************************************************

      Dim RVal As Boolean = False

      Try

        RVal = GetCustomField_InstrumentDataPresent(pCustomFieldID, 0, New Integer() {pInstrumentID})

      Catch ex As Exception

        RVal = False

      End Try

      Return RVal

    End Function

    Public Function GetCustomField_InstrumentDataPresent(ByVal pCustomFieldID As Integer, ByVal pInstrumentID() As Integer) As Boolean
      ' ***************************************************************************
      '
      ' Check the presence of data in the cache relating to the given Instrument ID.
      '
      ' ***************************************************************************

      Dim RVal As Boolean = False

      Try

        RVal = GetCustomField_InstrumentDataPresent(pCustomFieldID, 0, pInstrumentID)

      Catch ex As Exception

        RVal = False

      End Try

      Return RVal

    End Function

    Public Function GetCustomField_InstrumentDataPresent(ByVal pCustomFieldID As Integer, ByVal pGroupID As Integer, ByVal pInstrumentID As Integer) As Boolean
      ' ***************************************************************************
      '
      ' Check the presence of data in the cache relating to the given Instrument ID.
      '
      ' ***************************************************************************

      Dim RVal As Boolean = False

      Try

        RVal = GetCustomField_InstrumentDataPresent(pCustomFieldID, pGroupID, New Integer() {pInstrumentID})

      Catch ex As Exception

        RVal = False

      End Try

      Return RVal

    End Function

    Public Function GetCustomField_InstrumentDataPresent(ByVal pCustomFieldID As Integer, ByVal pGroupID As Integer, ByVal pInstrumentIDs() As Integer) As Boolean
      ' ***************************************************************************
      '
      ' Check the presence of data in the cache relating to the given array of 
      ' Instrument IDs.
      '
      ' ***************************************************************************


      Dim RVal As Boolean = False
      Dim ThisID As Integer
      Dim ThisNumerator As Integer
      Dim ThisDataCache As FieldDataCacheClass = Nothing
      Dim IsInGroupZero As Boolean = False

      Try

        If (Not NumeratorInitialised) Then
          Return False
        End If

        If (pInstrumentIDs Is Nothing) OrElse (pInstrumentIDs.Length <= 0) Then
          Return True
        End If

      Catch ex As Exception
      End Try

      Try
        CacheLockObject.AcquireReaderLock(LockDelayTime)

        If (_DataCacheDictionary.ContainsKey(pCustomFieldID)) Then
          ThisDataCache = _DataCacheDictionary(pCustomFieldID)
          RVal = True
          IsInGroupZero = True

          For Each ThisID In pInstrumentIDs
            Try

              ThisNumerator = _IdNormalisationDictionary(ThisID)

              ' Does this Data Point exist in the given Group ?

              If (Not ThisDataCache.DataPointExists(ThisNumerator, pGroupID, False)) Then

                ' NO. OK,  If this was group Zero then return False
                ' If Non-Zero Group and If the Group is fully populated then check Group Zero.

                If (pGroupID > 0) AndAlso (ThisDataCache.DataStatus(pGroupID) = CacheStatus.FullyPopulated) Then
                  If (Not ThisDataCache.DataPointExists(ThisNumerator, 0, False)) Then
                    RVal = False
                    Exit For
                  End If
                Else
                  RVal = False
                  Exit For
                End If

              End If

            Catch KeyError As KeyNotFoundException
              ' If KeyNotFoundException, then instrument ID probably does not exist in the Mastername table.
              ' Thus do not consider this as a missing Data Point.

            Catch ex As Exception
              RVal = False
              Exit For
            End Try

          Next
        End If

      Catch ex As Exception
        RVal = False
      Finally
        CacheLockObject.ReleaseReaderLock()
      End Try

      Return RVal

    End Function

    ' Search Functions

    Public Enum TestConditionEnum As Integer
      '=
      '<>
      '<
      '<=
      '>
      '>=
      'Like
      'Not Like
      'Between(Inclusive)

      None = 0
      Equals = 1
      NotEquals = 2
      LessThan = 3
      LessThanOrEqualTo = 4
      GreaterThan = 5
      GreaterThanOrEqualTo = 6
      LikeString = 7
      NotLikeString = 8
      Between = 9
      NotBetween = 10

    End Enum

    Public Function GetMatchingInstrumentIDs(ByVal pCustomFieldID As Integer, ByVal pGroupID As Integer, ByVal pSelectCondition As String, ByVal SelectValue As String, ByVal SelectValue2 As String) As Integer()
      ' ***************************************************************************
      ' Return an Integer array of Instrument IDs from the Custom Data Cache for the
      '  Given Custom Field / Group ID which match the given criteria.
      '
      ' note all the data type used, both in the cache and the selection values must
      ' imlement the IComparable interface. Generally not a problem as only Boolean,
      ' double, Date and String types are used.
      ' 
      ' ***************************************************************************

      Dim RVal(-1) As Integer
      Dim TempRVal() As Integer
      Dim ResultsCounter As Integer
      Dim InstrumentCounter As Integer
      Dim ThisCacheObject As FieldDataCacheClass = Nothing
      Dim SimpleFieldDataType As RenaissanceDataType = RenaissanceDataType.None

      ' Comparison variables.

      Dim DataValue As Object
      Dim Value1 As Object
      Dim Value2 As Object
      Dim CompareValue1 As IComparable = Nothing
      Dim CompareValue2 As IComparable = Nothing

      Dim SelectCondition As TestConditionEnum
      SelectCondition = TestConditionEnum.None

      Dim StartSearchFlag As Boolean = True

      Try
        CacheLockObject.AcquireReaderLock(LockDelayTime)

        ' Ensure that the Custom Field Data is fully loaded.
        ' Get Cache Data if necessary.

        If (GetCustomFieldDataStatus(pCustomFieldID) <> CacheStatus.FullyPopulated) Then
          LoadCustomFieldData(pCustomFieldID) ' Group Zero
        End If
        If (GetCustomFieldDataStatus(pCustomFieldID, pGroupID) <> CacheStatus.FullyPopulated) Then
          LoadCustomFieldData(pCustomFieldID, pGroupID) ' Specific Group (Group Zero should have been done above).
        End If

        ' Is there any data loaded...

        If (NumeratorInitialised) AndAlso (_IdNormalisationDictionary.Count > 0) AndAlso (_DataCacheDictionary.ContainsKey(pCustomFieldID)) Then
          ' OK, Initialise.

          ThisCacheObject = _DataCacheDictionary(pCustomFieldID)

          ' Set Comparison identifier

          Select Case pSelectCondition.Trim.ToUpper
            Case "="
              SelectCondition = TestConditionEnum.Equals

            Case "<>", "!="
              SelectCondition = TestConditionEnum.NotEquals

            Case "<"
              SelectCondition = TestConditionEnum.LessThan

            Case "<="
              SelectCondition = TestConditionEnum.LessThanOrEqualTo

            Case ">"
              SelectCondition = TestConditionEnum.GreaterThan

            Case ">="
              SelectCondition = TestConditionEnum.GreaterThanOrEqualTo

            Case "LIKE"
              SelectCondition = TestConditionEnum.LikeString
              SelectValue = SelectValue.Replace(CChar("%"), CChar("*")) ' Replace SQS '%' wildcard with '*' wildcard

            Case "NOT LIKE"
              SelectCondition = TestConditionEnum.NotLikeString
              SelectValue = SelectValue.Replace(CChar("%"), CChar("*"))

            Case Else
              If (pSelectCondition.Trim.ToUpper.StartsWith("BETWEEN")) Then
                SelectCondition = TestConditionEnum.Between
              ElseIf (pSelectCondition.Trim.ToUpper.StartsWith("NOT BETWEEN")) Then
                SelectCondition = TestConditionEnum.NotBetween
              Else
                StartSearchFlag = False
              End If

          End Select

          ' Validate : Only Strings can use Like / Not Like.

          If Not (ThisCacheObject.FieldDataType And RenaissanceDataType.TextType) Then

            If (SelectCondition = TestConditionEnum.LikeString) Then
              SelectCondition = TestConditionEnum.Equals
            End If
            If (SelectCondition = TestConditionEnum.NotLikeString) Then
              SelectCondition = TestConditionEnum.NotEquals
            End If

          End If

          ' Validate values

          If (ThisCacheObject.FieldDataType And RenaissanceDataType.BooleanType) Then
            SimpleFieldDataType = RenaissanceDataType.BooleanType

            If (SelectCondition = TestConditionEnum.Between) Then
              StartSearchFlag = False
            End If

            Try
              Value1 = CBool(SelectValue)
            Catch ex As Exception
              Value1 = CBool(False)
            End Try
            CompareValue1 = Value1

          ElseIf (ThisCacheObject.FieldDataType And RenaissanceDataType.DateType) Then
            SimpleFieldDataType = RenaissanceDataType.DateType

            Try
              If (IsDate(SelectValue)) Then
                Value1 = CDate(SelectValue)
                CompareValue1 = Value1
              Else
                StartSearchFlag = False
              End If

              If (SelectCondition = TestConditionEnum.Between) Then
                If (IsDate(SelectValue2)) Then
                  Value2 = CDate(SelectValue2)
                  CompareValue2 = Value2
                Else
                  StartSearchFlag = False
                End If
              End If

            Catch ex As Exception
              StartSearchFlag = False
            End Try

          ElseIf (ThisCacheObject.FieldDataType And RenaissanceDataType.NumericType) OrElse (ThisCacheObject.FieldDataType And RenaissanceDataType.PercentageType) Then
            SimpleFieldDataType = RenaissanceDataType.NumericType

            Try
              If (ConvertIsNumeric(SelectValue)) Then
                Value1 = ConvertValue(SelectValue, GetType(Double))
                CompareValue1 = Value1
              Else
                StartSearchFlag = False
              End If

              If (SelectCondition = TestConditionEnum.Between) Then
                If (ConvertIsNumeric(SelectValue2)) Then
                  Value2 = ConvertValue(SelectValue2, GetType(Double))
                  CompareValue2 = Value2
                Else
                  StartSearchFlag = False
                End If
              End If

            Catch ex As Exception
              StartSearchFlag = False
            End Try

          ElseIf (ThisCacheObject.FieldDataType And RenaissanceDataType.TextType) Then
            SimpleFieldDataType = RenaissanceDataType.TextType

            Try
              Value1 = SelectValue.ToString
              CompareValue1 = Value1

              If (SelectCondition = TestConditionEnum.Between) Then
                Value2 = SelectValue2.ToString
                CompareValue2 = Value2
              End If

            Catch ex As Exception
              StartSearchFlag = False
            End Try

          Else

            StartSearchFlag = False

          End If

          ' Start Search 

          If (StartSearchFlag) Then

            ResultsCounter = 0
            ReDim TempRVal(_IdNormalisationDictionary.Count - 1)

            For InstrumentCounter = 0 To (_IdNormalisationDictionary.Count - 1)

              DataValue = ThisCacheObject.GetDataPoint(InstrumentCounter, pGroupID)

              If (CompareValues(SelectCondition, SimpleFieldDataType, CompareValue1, CompareValue2, DataValue)) Then
                TempRVal(ResultsCounter) = _IdLookupArray(InstrumentCounter)
                ResultsCounter += 1
              End If
            Next

            ' Set RVal, Dimension RVal and copy in selection results.

            If (ResultsCounter > 0) Then
              ReDim RVal(ResultsCounter - 1)
              Array.Copy(TempRVal, RVal, ResultsCounter)
            End If

          End If

        End If

      Catch ex As Exception
      Finally
        If (CacheLockObject.IsReaderLockHeld) Then
          CacheLockObject.ReleaseReaderLock()
        End If
      End Try

      Return RVal

    End Function

    Private Function CompareValues(ByVal SelectCondition As TestConditionEnum, ByVal FieldDataType As RenaissanceDataType, ByVal CompareValue1 As IComparable, ByVal CompareValue2 As IComparable, ByVal DataValue As Object) As Boolean
      ' ***************************************************************************
      ' Check the given test criteria on the given data item.
      '
      ' Return True / False.
      '
      ' ***************************************************************************

      Dim RVal As Boolean = False
      Dim ComparisonResult As Integer

      Try

        If (CompareValue1 IsNot Nothing) Then

          ComparisonResult = CType(DataValue, IComparable).CompareTo(CompareValue1)

          If (FieldDataType And RenaissanceDataType.NumericType) OrElse (FieldDataType And RenaissanceDataType.PercentageType) Then
            If (Math.Abs(CDbl(CompareValue1) - CDbl(DataValue)) < EPSILON) Then
              ComparisonResult = 0
            End If
          End If

          Select Case SelectCondition
            Case TestConditionEnum.Equals, TestConditionEnum.NotEquals

              If (ComparisonResult = 0) Then
                RVal = True
              End If

              If (SelectCondition = TestConditionEnum.NotEquals) Then
                RVal = Not RVal
              End If

            Case TestConditionEnum.LessThan, TestConditionEnum.LessThanOrEqualTo
              If (ComparisonResult < 0) Then
                RVal = True
              ElseIf (ComparisonResult = 0) AndAlso (SelectCondition = TestConditionEnum.LessThanOrEqualTo) Then
                RVal = True
              End If

            Case TestConditionEnum.GreaterThan, TestConditionEnum.GreaterThanOrEqualTo
              If (ComparisonResult > 0) Then
                RVal = True
              ElseIf (ComparisonResult = 0) AndAlso (SelectCondition = TestConditionEnum.GreaterThanOrEqualTo) Then
                RVal = True
              End If

            Case TestConditionEnum.LikeString, TestConditionEnum.NotLikeString

              If (CStr(CompareValue1) Like CStr(DataValue)) Then
                RVal = True
              End If

              If (SelectCondition = TestConditionEnum.NotLikeString) Then
                RVal = Not RVal
              End If

            Case TestConditionEnum.Between, TestConditionEnum.NotBetween

              If (CompareValue2 IsNot Nothing) Then
                If (CompareValues(TestConditionEnum.GreaterThanOrEqualTo, FieldDataType, CompareValue1, Nothing, DataValue) AndAlso CompareValues(TestConditionEnum.LessThanOrEqualTo, FieldDataType, CompareValue2, Nothing, DataValue)) Then
                  RVal = True
                End If

                If (SelectCondition = TestConditionEnum.NotBetween) Then
                  RVal = Not RVal
                End If
              End If

          End Select ' Select Case SelectCondition

        End If ' CompareValue1 IsNot Nothing

      Catch ex As Exception
        RVal = False
      End Try

      Return RVal

    End Function

#End Region


  End Class


End Module
