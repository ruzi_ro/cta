Option Strict On

Imports RenaissanceGlobals
Imports RenaissanceGlobals.Globals
Imports RenaissanceStatFunctions
Imports Dundas.Charting.WinControl
Imports RenaissanceUtilities.DatePeriodFunctions
Imports CTA.MaxFunctions

Module ChartFunctions

  ' Apply Normalise Volatility for Backfilled series.
  ' At the moment, a backfill series is not used for any of the charts, so this is really just an 
  ' identifier for areas that might need to be updated later.

  Private CONST_ChartsBackfillVolatility As Boolean = False

  Public Function GetScalingFactor(ByRef pMainForm As CTAMain, ByVal pDynamicScalingFactor As Boolean, ByVal pScalingFactor As Double, ByVal pReferenceScalingFactor As Double, ByVal InstrumentID As ULong, ByVal CompareSeriesID As ULong) As Double
    ' *********************************************************************************
    ' Return the Scaling factor appropriate for the given Instrument ID according to
    ' what form options have been chosen.
    ' Default & Error return value of 1#.
    ' *********************************************************************************

    Dim RVal As Double = 1.0#

    Try
      If (Not pDynamicScalingFactor) Then
        If (pScalingFactor > 0) Then
          RVal = pScalingFactor
        End If
      ElseIf (InstrumentID > 0UL) AndAlso (CompareSeriesID > 0) Then ' Dynamic
        Dim ComparisonStdDev As Double
        Dim InstrumentStdDev As Double

        ComparisonStdDev = pMainForm.StatFunctions.GetSimpleStats(pMainForm.PertracData.GetPertracDataPeriod(InstrumentID, CompareSeriesID), CompareSeriesID, True, 0, pReferenceScalingFactor).StandardDeviation.M60
        InstrumentStdDev = pMainForm.StatFunctions.GetSimpleStats(pMainForm.PertracData.GetPertracDataPeriod(InstrumentID, CompareSeriesID), InstrumentID, True, 0, 1.0#).StandardDeviation.M60

        If (ComparisonStdDev > 0) AndAlso (InstrumentStdDev > 0) Then
          RVal = (ComparisonStdDev / InstrumentStdDev)
        End If

      End If

    Catch ex As Exception
      RVal = 1.0#
    End Try

    Return RVal

  End Function

#Region " Chart Code"

  Public Sub Set_Chart_FromList(ByRef pMainForm As CTAMain, ByVal InstrumentIDs() As Integer, ByVal pChartType As CTAChartTypes, ByRef pChart As Dundas.Charting.WinControl.Chart, ByRef pReferenceIndex As Object, ByVal pCondition As StatFunctions.ContingentSelect, ByVal pStartDate As Date, ByVal pEndDate As Date, ByVal pPeriod As Integer, ByVal pLamda As Double, ByVal pDynamicScalingFactor As Boolean, ByVal pScalingFactor As Double, ByVal pReferenceScalingFactor As Double, ByVal pShowAsPercentage As Boolean)  '  ByVal pStartDate As Date = Renaissance_BaseDate, Optional ByVal pEndDate As Date = Renaissance_EndDate_Data, Optional ByVal pPeriod As Double = 12, Optional ByVal pLamda As Double = 1, Optional ByVal pScalingFactor As Double = 1.0#, Optional ByVal pReferenceScalingFactor As Double = 1.0#)
    ' *************************************************************************************
    ' Routine to Draw the given chart, using the supplied parameters.
    ' Instrument(s) will be charted according to the Selected Items on the
    ' given List.
    ' Effectively the List Items must be DataRows or DataRowViews (DataView Rows) which
    ' return a value with reference to the List's DisplayMember and ValueMember properties.
    ' 
    ' Chart Type selection is contingent upon the CTAChartTypes enumeration.
    '
    ' pMainForm							:	Reference to Main From. Allows acces to Data functions and Error reporting.
    ' pList									: List from which to draw Instrument IDs
    ' pChartType						:	Chart type to draw.
    ' pChart								: Chart object to populate
    ' pReferenceIndex				: Reference (Comparison) object. 
    ' pCondition						: Condition (For Correlation, Alpha & Beta) - All, Up or Down Returns only.
    ' pStartDate						: Chart display Start Date
    ' pEndDate							: Chart display End Date
    ' pPeriod								: Number of calculation periods to use.
    ' pLamda								: Used to Exponentially weight Std Dev, Correlation, Alpha & Beta calculations.
    ' pDynamicScalingFactor	:	True if each Instruments Scaling factor is dynamically calculated
    '													to match Std Deviation with the reference instrument.
    '	pScalingFactor				: For Non-Dynamic Scaling, specify the Scaling factor to apply to Instruments.
    '	pReferenceScalingFactor	: Specify the Scaling factor to apply to the Reference Instrument.
    ' 
    ' Note that if Dynamic Scaling is specified for the Chart Instruments, they will be scaled to match the Std Dev for the reference
    ' instrument INCLUDING any modification resulting from pReferenceScalingFactor.
    ' 
    ' *************************************************************************************

    Dim InstrumentCounter As Integer
    Dim PertracID As Integer
    Dim PertracName As String
    Dim ReferenceID As Integer = 0

    ' Reference Index

    Try
      If (pReferenceIndex IsNot Nothing) AndAlso (IsNumeric(pReferenceIndex)) Then
        ReferenceID = Math.Max(CInt(pReferenceIndex), 0)
      End If
    Catch ex As Exception
      ReferenceID = 0
    End Try

    ' Ensure the correct number of chart series exist.

    Try
      While (pChart.Series.Count > InstrumentIDs.Count)
        pChart.Series.RemoveAt(pChart.Series.Count - 1)
      End While

      While (pChart.Series.Count < InstrumentIDs.Count)
        pChart.Series.Add("PS" & pChart.Series.Count.ToString)
      End While
    Catch ex As Exception
      pMainForm.LogError("Set_Chart_FromList()", LOG_LEVELS.Error, ex.Message, "Error Adding/Removing Chart series.", ex.StackTrace, True)

      Try
        pChart.Series.Clear()
      Catch Inner_Ex As Exception
      End Try
      Exit Sub
    End Try

    ' Loop through the selected List Items, setting the chart series as appropriate.

    For InstrumentCounter = 0 To (InstrumentIDs.Count - 1)
      Try
        PertracID = InstrumentIDs(InstrumentCounter)
        PertracName = pMainForm.PertracData.GetInformationValue(PertracID, PertracInformationFields.FundName).ToString
      Catch ex As Exception
        PertracID = 0
        PertracName = "<Error>"
      End Try

      Try
        Select Case pChartType
          Case CTAChartTypes.VAMI
            Set_LineChart(pMainForm, PertracID, pChart, InstrumentCounter, PertracName, pPeriod, pLamda, GetScalingFactor(pMainForm, pDynamicScalingFactor, pScalingFactor, pReferenceScalingFactor, CULng(PertracID), CULng(ReferenceID)), pStartDate, pEndDate)

          Case CTAChartTypes.Omega
            Set_OmegaChart(pMainForm, PertracID, pChart, InstrumentCounter, PertracName, pPeriod, pLamda, GetScalingFactor(pMainForm, pDynamicScalingFactor, pScalingFactor, pReferenceScalingFactor, CULng(PertracID), CULng(ReferenceID)), pStartDate, pEndDate, pShowAsPercentage)

          Case CTAChartTypes.MonthlyReturns
            Set_ReturnsBarChart(pMainForm, PertracID, pChart, InstrumentCounter, PertracName, pPeriod, pLamda, GetScalingFactor(pMainForm, pDynamicScalingFactor, pScalingFactor, pReferenceScalingFactor, CULng(PertracID), CULng(ReferenceID)), pStartDate, pEndDate)

          Case CTAChartTypes.RollingReturn
            Set_RollingReturnChart(pMainForm, PertracID, pChart, InstrumentCounter, PertracName, pPeriod, pLamda, GetScalingFactor(pMainForm, pDynamicScalingFactor, pScalingFactor, pReferenceScalingFactor, CULng(PertracID), CULng(ReferenceID)), pStartDate, pEndDate)

          Case CTAChartTypes.ReturnScatter
            Set_ReturnScatterChart(pMainForm, PertracID, ReferenceID, pChart, InstrumentCounter, PertracName, pPeriod, pLamda, GetScalingFactor(pMainForm, pDynamicScalingFactor, pScalingFactor, pReferenceScalingFactor, CULng(PertracID), CULng(ReferenceID)), pReferenceScalingFactor, pStartDate, pEndDate)

          Case CTAChartTypes.StdDev
            Set_StdDevChart(pMainForm, PertracID, pChart, InstrumentCounter, PertracName, pPeriod, pLamda, GetScalingFactor(pMainForm, pDynamicScalingFactor, pScalingFactor, pReferenceScalingFactor, CULng(PertracID), CULng(ReferenceID)), pStartDate, pEndDate)

          Case CTAChartTypes.DrawDown
            Set_DrawDownChart(pMainForm, PertracID, pChart, InstrumentCounter, PertracName, pPeriod, pLamda, GetScalingFactor(pMainForm, pDynamicScalingFactor, pScalingFactor, pReferenceScalingFactor, CULng(PertracID), CULng(ReferenceID)), pStartDate, pEndDate)

        End Select
      Catch ex As Exception
      End Try
    Next

  End Sub

  Public Sub Set_CandleChart_FromList(ByRef pMainForm As CTAMain, ByVal InstrumentIDs() As Integer, ByVal pChartType As CTAChartTypes, ByRef pChart As Dundas.Charting.WinControl.Chart, ByRef pReferenceIndex As Object, ByRef pHighlightIndex As Object, ByVal pQuartileDisplayData As CTAQuartileDisplayData, ByVal pStartDate As Date, ByVal pEndDate As Date, ByVal pPeriod As Integer, ByVal pLamda As Double, ByVal pDynamicScalingFactor As Boolean, ByVal pScalingFactor As Double, ByVal pReferenceScalingFactor As Double, ByVal OmitOutlierCount As Integer)  ' , Optional ByVal pEndDate As Date = Renaissance_EndDate_Data, Optional ByVal pPeriod As Double = 12, Optional ByVal pLamda As Double = 1)
    ' *************************************************************************************
    ' Routine to Draw the given chart, using the supplied parameters.
    ' Instrument(s) will be charted according to the Selected Items on the
    ' given List.
    ' Effectively the List Items must be DataRows or DataRowViews (DataView Rows) which
    ' return a value with reference to the List's DisplayMember and ValueMember properties.
    ' 
    ' Chart Type selection is contingent upon the CTAChartTypes enumeration.
    ' *************************************************************************************

    Dim InstrumentCounter As Integer
    Dim DateCounter As Integer
    Dim PertracID As Integer
    Dim PertracName As String
    Dim ReferenceID As Integer = 0
    Dim HighlightID As Integer = 0
    Dim StatDatePeriod As DealingPeriod = DealingPeriod.Daily

    Dim Chart_Series_StartDates() As Date = Nothing
    Dim Chart_Series_EndDates() As Date = Nothing
    Dim ChartDate_Series() As Date = Nothing
    Dim Chart_Min_Series(-1) As Double
    Dim Chart_Max_Series(-1) As Double
    Dim Chart_Quartile1_Series(-1) As Double
    Dim Chart_Quartile4_Series(-1) As Double
    Dim Chart_Highlight_Series() As Double = Nothing
    Dim Chart_Ranking_Series() As Double = Nothing
    Dim DataSeries(-1) As Array

    Dim HighlightSeriesLegendText As String = ""
    Dim RequiredSeriesCount As Integer = 1

    ' Reference Index

    Try
      If (pReferenceIndex IsNot Nothing) AndAlso (IsNumeric(pReferenceIndex)) Then
        ReferenceID = CInt(pReferenceIndex)
      End If
    Catch ex As Exception
      ReferenceID = 0
    End Try

    ' Highlight Index

    Try
      If (pHighlightIndex IsNot Nothing) AndAlso (IsNumeric(pHighlightIndex)) Then
        HighlightID = CInt(pHighlightIndex)
      End If
    Catch ex As Exception
      HighlightID = 0
    End Try

    ' Ensure the correct number of chart series exist.

    If (pChartType = CTAChartTypes.Quartile) AndAlso (HighlightID > 0) Then
      RequiredSeriesCount = 2
    End If

    Try
      While (pChart.Series.Count > RequiredSeriesCount)
        pChart.Series.RemoveAt(pChart.Series.Count - 1)
      End While

      While (pChart.Series.Count < RequiredSeriesCount)
        pChart.Series.Add("PS" & pChart.Series.Count.ToString)
      End While

      If pChart.Series.Count > 0 Then
        pChart.Series(0).Points.Clear()
      End If
      If pChart.Series.Count > 1 Then
        pChart.Series(1).Points.Clear()
      End If

    Catch ex As Exception
      pMainForm.LogError("Set_Chart_FromList()", LOG_LEVELS.Error, ex.Message, "Error Adding/Removing Chart series.", ex.StackTrace, True)

      Try
        pChart.Series.Clear()
      Catch Inner_Ex As Exception
      End Try
      Exit Sub
    End Try

    ' Check ReferenceID is present for Alpha and Beta charts (Having cleared the chart series).

    If (ReferenceID <= 0) Then
      If (pQuartileDisplayData = CTAQuartileDisplayData.Alpha) Or (pQuartileDisplayData = CTAQuartileDisplayData.Beta) Then
        If pChart.Series.Count > 0 Then
          If pChart.Series(0).YValuesPerPoint = 4 Then
            pChart.Series(0).Points.AddXY(Now.Date, New Object() {0, 0, 0, 0})
          Else
            pChart.Series(0).Points.AddXY(Now.Date, 0)
          End If
        End If
        If pChart.Series.Count > 1 Then
          pChart.Series(1).Points.AddXY(Now.Date, 0)
        End If

        GoTo Set_CandleChart_FromList_End

        Exit Sub
      End If
    End If

    If (HighlightID <= 0) Then
      If (pChartType = CTAChartTypes.Ranking) Then
        ' Exit if Ranking chart is required and no highlight series is chosen.
        If pChart.Series.Count > 0 Then
          If pChart.Series(0).YValuesPerPoint = 4 Then
            pChart.Series(0).Points.AddXY(Now.Date, New Object() {0, 0, 0, 0})
          Else
            pChart.Series(0).Points.AddXY(Now.Date, 0)
          End If
        End If
        If pChart.Series.Count > 1 Then
          pChart.Series(1).Points.AddXY(Now.Date, 0)
        End If

        GoTo Set_CandleChart_FromList_End

        Exit Sub
      End If
    End If

    ' Validate Acceptable Chart types

    If Not ((pChartType = CTAChartTypes.Quartile) Or (pChartType = CTAChartTypes.Ranking)) Then
      If pChart.Series.Count > 0 Then
        If pChart.Series(0).YValuesPerPoint = 4 Then
          pChart.Series(0).Points.AddXY(Now.Date, New Object() {0, 0, 0, 0})
        Else
          pChart.Series(0).Points.AddXY(Now.Date, 0)
        End If
      End If
      If pChart.Series.Count > 1 Then
        pChart.Series(1).Points.AddXY(Now.Date, 0)
      End If

      GoTo Set_CandleChart_FromList_End

      Exit Sub
    End If

    ' Get Best Possible Date Period

    Dim thisDatePeriod As DealingPeriod

    If (ReferenceID <= 0) Then
      thisDatePeriod = pMainForm.PertracData.GetPertracDataPeriod(ReferenceID)

      If pMainForm.PertracData.AnnualPeriodCount(thisDatePeriod) < pMainForm.PertracData.AnnualPeriodCount(StatDatePeriod) Then
        StatDatePeriod = thisDatePeriod
      End If
    End If

    If (HighlightID > 0) Then
      thisDatePeriod = pMainForm.PertracData.GetPertracDataPeriod(HighlightID)

      If pMainForm.PertracData.AnnualPeriodCount(thisDatePeriod) < pMainForm.PertracData.AnnualPeriodCount(StatDatePeriod) Then
        StatDatePeriod = thisDatePeriod
      End If
    End If

    For InstrumentCounter = 0 To (InstrumentIDs.Count - 1)
      Try
        PertracID = InstrumentIDs(InstrumentCounter)

        thisDatePeriod = pMainForm.PertracData.GetPertracDataPeriod(PertracID)

        If pMainForm.PertracData.AnnualPeriodCount(thisDatePeriod) < pMainForm.PertracData.AnnualPeriodCount(StatDatePeriod) Then
          StatDatePeriod = thisDatePeriod
        End If

      Catch ex As Exception
      End Try
    Next

    ' Establish Dates To use

    Dim WorkingStartDate As Date = pStartDate
    Dim WorkingEndDate As Date = pEndDate

    If (HighlightID > 0) Then
      ChartDate_Series = pMainForm.StatFunctions.DateSeries(StatDatePeriod, CULng(HighlightID), CONST_ChartsBackfillVolatility, pPeriod, pLamda, False, pStartDate, pEndDate)

      If (ChartDate_Series IsNot Nothing) AndAlso (ChartDate_Series.Length > 0) Then
        WorkingStartDate = ChartDate_Series(0).Date
        WorkingEndDate = ChartDate_Series(ChartDate_Series.Length - 1).Date
      End If
    ElseIf (ReferenceID > 0) Then
      ChartDate_Series = pMainForm.StatFunctions.DateSeries(StatDatePeriod, CULng(ReferenceID), CONST_ChartsBackfillVolatility, pPeriod, pLamda, False, pStartDate, pEndDate)

      If (ChartDate_Series IsNot Nothing) AndAlso (ChartDate_Series.Length > 0) Then
        WorkingStartDate = ChartDate_Series(0).Date
        WorkingEndDate = ChartDate_Series(ChartDate_Series.Length - 1).Date
      End If
    End If

    If (ChartDate_Series Is Nothing) OrElse (ChartDate_Series.Length <= 0) Then
      ' Build Default Date series, if one does has not already been selected.

      Dim DateCount As Integer

      If (WorkingStartDate < #1/1/1980#) Then
        WorkingStartDate = FitDateToPeriod(StatDatePeriod, #1/1/1980#, True)
      End If
      If (WorkingEndDate.CompareTo(Now.Date) > 0) Then
        WorkingEndDate = FitDateToPeriod(StatDatePeriod, Now.Date, True)
      End If

      DateCount = GetPeriodCount(StatDatePeriod, WorkingStartDate, WorkingEndDate)
      ChartDate_Series = CType(Array.CreateInstance(GetType(Date), DateCount), Date())
      If (DateCount > 0) Then
        ChartDate_Series(0) = WorkingStartDate

        For DateCount = 1 To (ChartDate_Series.Length - 1)
          ChartDate_Series(DateCount) = AddPeriodToDate(StatDatePeriod, ChartDate_Series(DateCount - 1), 1)
        Next
      End If

    End If

    ' Size Data Arrays

    Chart_Min_Series = CType(Array.CreateInstance(GetType(Double), ChartDate_Series.Length), Double())
    Chart_Max_Series = CType(Array.CreateInstance(GetType(Double), ChartDate_Series.Length), Double())
    Chart_Quartile1_Series = CType(Array.CreateInstance(GetType(Double), ChartDate_Series.Length), Double())
    Chart_Quartile4_Series = CType(Array.CreateInstance(GetType(Double), ChartDate_Series.Length), Double())
    Chart_Ranking_Series = CType(Array.CreateInstance(GetType(Double), ChartDate_Series.Length), Double())

    ReDim Chart_Series_StartDates(InstrumentIDs.Count - 1)
    ReDim Chart_Series_EndDates(InstrumentIDs.Count - 1)
    ReDim DataSeries(InstrumentIDs.Count - 1)

    ' Loop through the selected List Items, Getting the chart data as appropriate.

    Dim thisDateSeries() As Date
    Dim WholePertracDateSeries() As Date
    Dim WholeReferenceDateSeries() As Date
    Dim thisDataSeries() As Double
    Dim NAV_Series() As Double
    Dim NavCounter As Integer
    Dim IndexOffset As Integer
    Dim DataItemStartIndex As Integer
    Dim DataItemOverlap As Integer

    Dim Highlight_Series_InstrumentCounter As Integer = 0

    Chart_Highlight_Series = Nothing

    For InstrumentCounter = 0 To (InstrumentIDs.Count - 1)
      Try
        PertracID = InstrumentIDs(InstrumentCounter)
        PertracName = pMainForm.PertracData.GetInformationValue(PertracID, PertracInformationFields.FundName).ToString
      Catch ex As Exception
        PertracID = 0
        PertracName = "<Error>"
      End Try

      Try
        thisDateSeries = Nothing
        thisDataSeries = Nothing

        Chart_Series_StartDates(InstrumentCounter) = Renaissance_BaseDate
        Chart_Series_EndDates(InstrumentCounter) = Renaissance_BaseDate

        Select Case pQuartileDisplayData

          Case CTAQuartileDisplayData.MonthlyReturns
            thisDataSeries = pMainForm.StatFunctions.ReturnSeries(StatDatePeriod, CULng(PertracID), CONST_ChartsBackfillVolatility, pPeriod, pLamda, False, WorkingStartDate, WorkingEndDate, GetScalingFactor(pMainForm, pDynamicScalingFactor, pScalingFactor, pReferenceScalingFactor, CULng(PertracID), CULng(ReferenceID)))
            thisDateSeries = pMainForm.StatFunctions.DateSeries(StatDatePeriod, CULng(PertracID), CONST_ChartsBackfillVolatility, pPeriod, pLamda, False, WorkingStartDate, WorkingEndDate)

            If (thisDateSeries IsNot Nothing) AndAlso (thisDateSeries.Length > 0) Then
              Chart_Series_StartDates(InstrumentCounter) = thisDateSeries(0)
              Chart_Series_EndDates(InstrumentCounter) = thisDateSeries(thisDateSeries.Length - 1)
            End If

          Case CTAQuartileDisplayData.RollingReturns

            NAV_Series = pMainForm.StatFunctions.NAVSeries(StatDatePeriod, CULng(PertracID), CONST_ChartsBackfillVolatility, pPeriod, pLamda, False, WorkingStartDate, WorkingEndDate, GetScalingFactor(pMainForm, pDynamicScalingFactor, pScalingFactor, pReferenceScalingFactor, CULng(PertracID), CULng(ReferenceID)))
            If (NAV_Series IsNot Nothing) AndAlso (NAV_Series.Length > 0) Then
              thisDataSeries = CType(Array.CreateInstance(GetType(Double), NAV_Series.Length), Double())
              For NavCounter = (pPeriod) To (NAV_Series.Length - 1)
                If (NAV_Series(NavCounter - pPeriod) > 0) Then
                  thisDataSeries(NavCounter) = (NAV_Series(NavCounter) / NAV_Series(NavCounter - pPeriod)) - 1.0
                End If
              Next
            End If

            thisDateSeries = pMainForm.StatFunctions.DateSeries(StatDatePeriod, CULng(PertracID), CONST_ChartsBackfillVolatility, pPeriod, pLamda, False, WorkingStartDate, WorkingEndDate)

            If (thisDateSeries IsNot Nothing) AndAlso (thisDateSeries.Length > 0) Then
              Chart_Series_StartDates(InstrumentCounter) = thisDateSeries(0)
              Chart_Series_EndDates(InstrumentCounter) = thisDateSeries(thisDateSeries.Length - 1)
            End If

            ' Adjust DataSeries Valid Start Date by the pPeriod Count so that Null data at the start of the derived series
            ' is left off.
            Chart_Series_StartDates(InstrumentCounter) = AddPeriodToDate(StatDatePeriod, Chart_Series_StartDates(InstrumentCounter), pPeriod - 1)

          Case CTAQuartileDisplayData.Volatility
            thisDataSeries = pMainForm.StatFunctions.StdDevSeries(StatDatePeriod, CULng(PertracID), CONST_ChartsBackfillVolatility, pPeriod, pLamda, True, WorkingStartDate, WorkingEndDate, GetScalingFactor(pMainForm, pDynamicScalingFactor, pScalingFactor, pReferenceScalingFactor, CULng(PertracID), CULng(ReferenceID)))
            thisDateSeries = pMainForm.StatFunctions.DateSeries(StatDatePeriod, CULng(PertracID), CONST_ChartsBackfillVolatility, pPeriod, pLamda, False, WorkingStartDate, WorkingEndDate)

            If (thisDateSeries IsNot Nothing) AndAlso (thisDateSeries.Length > 0) Then
              Chart_Series_StartDates(InstrumentCounter) = thisDateSeries(0)
              Chart_Series_EndDates(InstrumentCounter) = thisDateSeries(thisDateSeries.Length - 1)
            End If

            Chart_Series_StartDates(InstrumentCounter) = AddPeriodToDate(StatDatePeriod, Chart_Series_StartDates(InstrumentCounter), pPeriod - 1)

          Case CTAQuartileDisplayData.Correlation
            thisDataSeries = pMainForm.StatFunctions.CorrelationSeries_Contingent(StatDatePeriod, CULng(ReferenceID), CULng(PertracID), CONST_ChartsBackfillVolatility, StatFunctions.ContingentSelect.ConditionAll, pPeriod, pLamda, WorkingStartDate, WorkingEndDate, pReferenceScalingFactor, GetScalingFactor(pMainForm, pDynamicScalingFactor, pScalingFactor, pReferenceScalingFactor, CULng(PertracID), CULng(ReferenceID)))
            thisDateSeries = pMainForm.StatFunctions.DateSeries(StatDatePeriod, CULng(ReferenceID), CONST_ChartsBackfillVolatility, pPeriod, pLamda, False, WorkingStartDate, WorkingEndDate)

            WholePertracDateSeries = pMainForm.StatFunctions.DateSeries(StatDatePeriod, CULng(PertracID), CONST_ChartsBackfillVolatility, pPeriod, pLamda, False, Renaissance_BaseDate, WorkingEndDate)
            If (ReferenceID > 0) Then
              WholeReferenceDateSeries = pMainForm.StatFunctions.DateSeries(StatDatePeriod, CULng(ReferenceID), CONST_ChartsBackfillVolatility, pPeriod, pLamda, False, Renaissance_BaseDate, WorkingEndDate)
            Else
              WholeReferenceDateSeries = pMainForm.StatFunctions.DateSeries(StatDatePeriod, CULng(PertracID), CONST_ChartsBackfillVolatility, pPeriod, pLamda, False, Renaissance_BaseDate, WorkingEndDate)
            End If

            If (WholePertracDateSeries IsNot Nothing) AndAlso (WholePertracDateSeries.Length > 0) AndAlso (WholeReferenceDateSeries IsNot Nothing) AndAlso (WholeReferenceDateSeries.Length > 0) Then
              Chart_Series_StartDates(InstrumentCounter) = MAX(WholePertracDateSeries(0), WholeReferenceDateSeries(0))
              Chart_Series_EndDates(InstrumentCounter) = MIN(WholePertracDateSeries(WholePertracDateSeries.Length - 1), WholeReferenceDateSeries(WholeReferenceDateSeries.Length - 1))
            End If

            Chart_Series_StartDates(InstrumentCounter) = AddPeriodToDate(StatDatePeriod, Chart_Series_StartDates(InstrumentCounter), pPeriod - 1)

          Case CTAQuartileDisplayData.Alpha
            thisDataSeries = pMainForm.StatFunctions.Alpha_Contingent(StatDatePeriod, CULng(ReferenceID), CULng(PertracID), CONST_ChartsBackfillVolatility, StatFunctions.ContingentSelect.ConditionAll, pPeriod, pLamda, WorkingStartDate, WorkingEndDate, pReferenceScalingFactor, GetScalingFactor(pMainForm, pDynamicScalingFactor, pScalingFactor, pReferenceScalingFactor, CULng(PertracID), CULng(ReferenceID)))
            thisDateSeries = pMainForm.StatFunctions.DateSeries(StatDatePeriod, CULng(ReferenceID), CONST_ChartsBackfillVolatility, pPeriod, pLamda, False, WorkingStartDate, WorkingEndDate)

            WholePertracDateSeries = pMainForm.StatFunctions.DateSeries(StatDatePeriod, CULng(PertracID), CONST_ChartsBackfillVolatility, pPeriod, pLamda, False, Renaissance_BaseDate, WorkingEndDate)
            If (ReferenceID > 0) Then
              WholeReferenceDateSeries = pMainForm.StatFunctions.DateSeries(StatDatePeriod, CULng(ReferenceID), CONST_ChartsBackfillVolatility, pPeriod, pLamda, False, Renaissance_BaseDate, WorkingEndDate)
            Else
              WholeReferenceDateSeries = pMainForm.StatFunctions.DateSeries(StatDatePeriod, CULng(PertracID), CONST_ChartsBackfillVolatility, pPeriod, pLamda, False, Renaissance_BaseDate, WorkingEndDate)
            End If

            If (WholePertracDateSeries IsNot Nothing) AndAlso (WholePertracDateSeries.Length > 0) AndAlso (WholeReferenceDateSeries IsNot Nothing) AndAlso (WholeReferenceDateSeries.Length > 0) Then
              Chart_Series_StartDates(InstrumentCounter) = MAX(WholePertracDateSeries(0), WholeReferenceDateSeries(0))
              Chart_Series_EndDates(InstrumentCounter) = MIN(WholePertracDateSeries(WholePertracDateSeries.Length - 1), WholeReferenceDateSeries(WholeReferenceDateSeries.Length - 1))
            End If

            Chart_Series_StartDates(InstrumentCounter) = AddPeriodToDate(StatDatePeriod, Chart_Series_StartDates(InstrumentCounter), pPeriod - 1)

          Case CTAQuartileDisplayData.Beta
            thisDataSeries = pMainForm.StatFunctions.Beta_Contingent(StatDatePeriod, CULng(ReferenceID), CULng(PertracID), CONST_ChartsBackfillVolatility, StatFunctions.ContingentSelect.ConditionAll, pPeriod, pLamda, WorkingStartDate, WorkingEndDate, pReferenceScalingFactor, GetScalingFactor(pMainForm, pDynamicScalingFactor, pScalingFactor, pReferenceScalingFactor, CULng(PertracID), CULng(ReferenceID)))
            thisDateSeries = pMainForm.StatFunctions.DateSeries(StatDatePeriod, CULng(ReferenceID), CONST_ChartsBackfillVolatility, pPeriod, pLamda, False, WorkingStartDate, WorkingEndDate)

            WholePertracDateSeries = pMainForm.StatFunctions.DateSeries(StatDatePeriod, CULng(PertracID), CONST_ChartsBackfillVolatility, pPeriod, pLamda, False, Renaissance_BaseDate, WorkingEndDate)
            If (ReferenceID > 0) Then
              WholeReferenceDateSeries = pMainForm.StatFunctions.DateSeries(StatDatePeriod, CULng(ReferenceID), CONST_ChartsBackfillVolatility, pPeriod, pLamda, False, Renaissance_BaseDate, WorkingEndDate)
            Else
              WholeReferenceDateSeries = pMainForm.StatFunctions.DateSeries(StatDatePeriod, CULng(PertracID), CONST_ChartsBackfillVolatility, pPeriod, pLamda, False, Renaissance_BaseDate, WorkingEndDate)
            End If

            If (WholePertracDateSeries IsNot Nothing) AndAlso (WholePertracDateSeries.Length > 0) AndAlso (WholeReferenceDateSeries IsNot Nothing) AndAlso (WholeReferenceDateSeries.Length > 0) Then
              Chart_Series_StartDates(InstrumentCounter) = MAX(WholePertracDateSeries(0), WholeReferenceDateSeries(0))
              Chart_Series_EndDates(InstrumentCounter) = MIN(WholePertracDateSeries(WholePertracDateSeries.Length - 1), WholeReferenceDateSeries(WholeReferenceDateSeries.Length - 1))
            End If

            Chart_Series_StartDates(InstrumentCounter) = AddPeriodToDate(StatDatePeriod, Chart_Series_StartDates(InstrumentCounter), pPeriod - 1)

        End Select

        ' Save Data Series
        ' DataSeries(InstrumentCounter)

        DataSeries(InstrumentCounter) = Array.CreateInstance(GetType(Double), ChartDate_Series.Length)

        If (thisDataSeries IsNot Nothing) AndAlso (thisDateSeries IsNot Nothing) AndAlso (thisDateSeries.Length > 0) Then

          DataItemStartIndex = 0
          IndexOffset = GetPriceIndex(StatDatePeriod, ChartDate_Series(0), thisDateSeries(0))
          If (IndexOffset < 0) Then
            DataItemStartIndex = Math.Abs(IndexOffset)
          End If

          DataItemOverlap = (thisDataSeries.Length - DataItemStartIndex)

          If (DataItemStartIndex + DataItemOverlap + IndexOffset) > ChartDate_Series.Length Then
            DataItemOverlap = (ChartDate_Series.Length - (IndexOffset + DataItemStartIndex))
          End If

          Array.ConstrainedCopy(thisDataSeries, DataItemStartIndex, DataSeries(InstrumentCounter), (DataItemStartIndex + IndexOffset), DataItemOverlap)

          If (PertracID = HighlightID) Then
            Chart_Highlight_Series = CType(DataSeries(InstrumentCounter), Double())
            Highlight_Series_InstrumentCounter = InstrumentCounter
            HighlightSeriesLegendText = PertracName
          End If
        End If

      Catch ex As Exception
      End Try
    Next

    ' OK, we have now collected all the series data, Now create the Max, Min etc charts.

    Dim MinIndex As Integer = 0
    Dim MaxIndex As Integer = (DataSeries.Length - 1)
    Dim Q4Index As Integer = CInt(Math.Round(DataSeries.Length * 0.25, 0))
    If (Q4Index < 0) Then Q4Index = 0
    If (Q4Index >= DataSeries.Length) Then Q4Index = DataSeries.Length - 1

    ' Ensure Q1 & Q4 segments are the same size. Obviously the fewer funds, the more imprecise the segment size is.
    Dim Q1Index As Integer = MaxIndex - Q4Index
    If (Q1Index < 0) Then Q1Index = 0

    Dim RankingIndex As Integer
    Dim FirstNonZeroIndex As Integer = (-1)

    Dim SortArray(DataSeries.Length - 1) As Double
    Dim TempSortArray(DataSeries.Length - 1) As Double
    Dim LastValidDateCounter As Integer = (-1)

    Try
      Dim ValidElementCount As Integer

      For DateCounter = 0 To (ChartDate_Series.Length - 1)
        ValidElementCount = 0

        For InstrumentCounter = 0 To (DataSeries.Length - 1)
          thisDataSeries = CType(DataSeries(InstrumentCounter), Double())

          If (ThisDataSeries IsNot Nothing) Then
            If ((ChartDate_Series(DateCounter) > Chart_Series_StartDates(InstrumentCounter)) Or ((ChartDate_Series(DateCounter) = Chart_Series_StartDates(InstrumentCounter)) And (ThisDataSeries(DateCounter) <> 0))) AndAlso (ChartDate_Series(DateCounter) <= Chart_Series_EndDates(InstrumentCounter)) Then
              SortArray(ValidElementCount) = ThisDataSeries(DateCounter)
              ValidElementCount += 1
            End If
          End If
        Next

        ' Omit outliers = Whole data set ?

        If (OmitOutlierCount * 2) >= ValidElementCount Then
          ValidElementCount = 0
        End If

        ' Set Quartile values.

        If (ValidElementCount > 0) Then
          LastValidDateCounter = DateCounter

          ' Sort available Data

          Array.Sort(SortArray, 0, ValidElementCount)

          ' Eliminate Outliers ?

          If (OmitOutlierCount > 0) Then
            ' ValidElementCount
            ' SortArray

            ValidElementCount -= (OmitOutlierCount * 2)
            Array.Copy(SortArray, 0, TempSortArray, 0, SortArray.Length)
            Array.Copy(TempSortArray, OmitOutlierCount, SortArray, 0, ValidElementCount)

          End If

          ' Set Data Indices

          MinIndex = 0
          MaxIndex = (ValidElementCount - 1)
          Q4Index = CInt(Math.Round(ValidElementCount * 0.25, 0))
          If (Q4Index < 0) Then Q4Index = 0
          If (Q4Index >= ValidElementCount) Then Q4Index = ValidElementCount - 1
          Q1Index = MaxIndex - Q4Index
          If (Q1Index < 0) Then Q1Index = 0

          ' Set Series Values

          Chart_Min_Series(DateCounter) = SortArray(MinIndex) * 100.0
          Chart_Max_Series(DateCounter) = SortArray(MaxIndex) * 100.0
          Chart_Quartile1_Series(DateCounter) = SortArray(Q1Index) * 100.0
          Chart_Quartile4_Series(DateCounter) = SortArray(Q4Index) * 100.0

          If (FirstNonZeroIndex < 0) Then
            If (SortArray(MinIndex) <> 0) OrElse (SortArray(MaxIndex) <> 0) Then
              FirstNonZeroIndex = DateCounter
            End If
          End If

          If (Chart_Highlight_Series IsNot Nothing) Then
            For RankingIndex = MaxIndex To MinIndex Step -1
              If (Chart_Highlight_Series(DateCounter) >= SortArray(RankingIndex)) Then
                If (ValidElementCount <= 1) Then
                  Chart_Ranking_Series(DateCounter) = 1
                Else
                  Chart_Ranking_Series(DateCounter) = Math.Round(RankingIndex / (ValidElementCount - 1), 4)
                End If
                Exit For
              End If
            Next
          End If

        End If

      Next

    Catch ex As Exception
    End Try

    ' Now (finally !) paint the chart.

    Try
      Dim BarSeries As Dundas.Charting.WinControl.Series = Nothing
      Dim HighlightSeries As Dundas.Charting.WinControl.Series = Nothing

      If (pChartType = CTAChartTypes.Quartile) Then
        BarSeries = pChart.Series(0)

        If (HighlightID > 0) AndAlso (pChart.Series.Count > 1) Then
          HighlightSeries = pChart.Series(1)

          HighlightSeries = pChart.Series(1)
          HighlightSeries.Type = SeriesChartType.Point
          HighlightSeries.XValueType = ChartValueTypes.Date
          HighlightSeries.YValuesPerPoint = 1
          HighlightSeries.YValueType = ChartValueTypes.Double
          HighlightSeries.MarkerColor = Color.Green
          HighlightSeries.MarkerStyle = MarkerStyle.Cross
          HighlightSeries.MarkerSize = 12

          HighlightSeries.Font = New Font("Arial", 8)

          Try
            HighlightSeries.CustomAttributes = "LabelStyle=TopLeft,EmptyPointValue=Zero"
          Catch ex As Exception
            HighlightSeries.CustomAttributes = "LabelStyle=TopLeft,EmptyPointValue=Zero"
          End Try

          HighlightSeries.LegendText = "" ' HighlightSeriesLegendText

        End If

      ElseIf (pChartType = CTAChartTypes.Ranking) Then
        HighlightSeries = pChart.Series(0)
        Chart_Highlight_Series = Chart_Ranking_Series
        BarSeries = Nothing

        HighlightSeries.Type = SeriesChartType.Line
        HighlightSeries.XValueType = ChartValueTypes.Date
        HighlightSeries.YValuesPerPoint = 1
        HighlightSeries.YValueType = ChartValueTypes.Double

        HighlightSeries.Font = New Font("Arial", 8)

        Try
          HighlightSeries.CustomAttributes = "LabelStyle=TopLeft,EmptyPointValue=Zero"
        Catch ex As Exception
          HighlightSeries.CustomAttributes = "LabelStyle=TopLeft,EmptyPointValue=Zero"
        End Try

        HighlightSeries.ChartType = "Line"
        HighlightSeries.ShadowOffset = 1

        HighlightSeries.LegendText = "" ' HighlightSeriesLegendText

      End If

      If (FirstNonZeroIndex < 0) Then
        FirstNonZeroIndex = 0
      End If

      For DateCounter = FirstNonZeroIndex To LastValidDateCounter ' (ChartDate_Series.Length - 1)
        ' High, Low, Open, Close
        If (BarSeries IsNot Nothing) Then
          BarSeries.Points.AddXY(ChartDate_Series(DateCounter), New Object() {Chart_Max_Series(DateCounter), Chart_Min_Series(DateCounter), Chart_Quartile4_Series(DateCounter), Chart_Quartile1_Series(DateCounter)})
        End If

        If (HighlightSeries IsNot Nothing) AndAlso (Chart_Highlight_Series IsNot Nothing) Then
          ' Sometimes the returns dates contrive to allow the first highlight element to be Zero.
          ' In this case, eliminate that data point.

          If (Chart_Series_StartDates(Highlight_Series_InstrumentCounter) < ChartDate_Series(DateCounter)) Then
            ' Constrain the Highlight series to be within the MIN and MAX values, only a consideration
            ' if outliers have been eliminated.

            If (OmitOutlierCount > 0) Then
              HighlightSeries.Points.AddXY(ChartDate_Series(DateCounter), Math.Max(Math.Min(Chart_Max_Series(DateCounter), Chart_Highlight_Series(DateCounter) * 100), Chart_Min_Series(DateCounter)))
            Else
              HighlightSeries.Points.AddXY(ChartDate_Series(DateCounter), Chart_Highlight_Series(DateCounter) * 100)
            End If

          End If

        End If
      Next

      ' Set Label Text
      If (HighlightSeries IsNot Nothing) Then
        If (HighlightSeries.Points.Count > 0) Then
          HighlightSeries.Points(HighlightSeries.Points.Count - 1).Label = HighlightSeriesLegendText
          HighlightSeries.ChartArea = pChart.ChartAreas(0).Name
        Else
          HighlightSeries.ChartArea = ""
        End If
      End If

    Catch ex As Exception
    End Try

Set_CandleChart_FromList_End:

    pChart.ChartAreas(0).CursorX.UserEnabled = True
    pChart.ChartAreas(0).CursorX.UserSelection = True
    pChart.ChartAreas(0).AxisX.View.Zoomable = True
    pChart.ChartAreas(0).AxisX.IntervalType = DateTimeIntervalType.Auto
    pChart.ChartAreas(0).AxisX.View.ZoomReset(0)


  End Sub

  Public Function Set_LineChart_RateReturnsLine(ByVal pMainForm As CTAMain, _
  ByVal pChart As Chart, _
  ByVal pSeriesNumber As Integer, _
  ByVal pCopySeriesNumber As Integer, _
  ByVal pSeriesAnnualReturn As Double, _
  ByVal pSeriesAnnualPeriodCount As Integer, _
  ByVal pLegendText As String) As Boolean

    ' ***********************************************************************************
    '
    '
    '
    ' ***********************************************************************************

    Dim SourceSeries As Dundas.Charting.WinControl.Series
    Dim DSeries As Dundas.Charting.WinControl.Series
    Dim PointCounter As Integer
    Dim ReturnNAV As Double

    Dim pSeriesReturn As Double

    pSeriesReturn = ((1.0 + pSeriesAnnualReturn) ^ (1 / pSeriesAnnualPeriodCount)) - 1.0


    Try
      ' Get Chart Series.

      If (pSeriesNumber < 0) OrElse (pSeriesNumber >= pChart.Series.Count) Then
        DSeries = pChart.Series.Add("PS" & pChart.Series.Count.ToString)
        DSeries.ChartArea = pChart.ChartAreas(0).Name
      Else
        DSeries = pChart.Series(pSeriesNumber)
        If (DSeries.ChartArea = "") Then DSeries.ChartArea = pChart.ChartAreas(0).Name
      End If

      DSeries.Points.Clear()

      SourceSeries = pChart.Series(pCopySeriesNumber)

      If (SourceSeries.Points.Count > 0) Then
        ' set first point
        ReturnNAV = SourceSeries.Points(0).YValues(0)

        DSeries.Points.Add(SourceSeries.Points(0))

        ' Set Subsequent points

        For PointCounter = 1 To (SourceSeries.Points.Count - 1)
          ReturnNAV *= (pSeriesReturn + 1.0)

          DSeries.Points.AddXY(SourceSeries.Points(PointCounter).XValue, ReturnNAV)
        Next
      End If

      ' Format series

      DSeries.Font = New Font("Arial", 8)

      DSeries.CustomAttributes = "LabelStyle=TopLeft,EmptyPointValue=Zero"

      DSeries.ChartType = "Line"
      DSeries.ShadowOffset = 1

      DSeries.LegendText = pLegendText

      ' Set Label Text
      If (DSeries.Points.Count > 0) Then
        DSeries.Points(DSeries.Points.Count - 1).Label = pLegendText
        DSeries.ChartArea = pChart.ChartAreas(0).Name
      Else
        DSeries.ChartArea = ""
      End If

    Catch ex As Exception
    End Try

    Return True

  End Function

  Public Function Set_LineChart_StaticReturnsLine(ByVal pMainForm As CTAMain, _
   ByVal pChart As Chart, _
   ByVal pSeriesNumber As Integer, _
   ByVal pCopySeriesNumber As Integer, _
   ByVal pSeriesAnnualReturn As Double, _
   ByVal pLegendText As String) As Boolean

    Dim SourceSeries As Dundas.Charting.WinControl.Series
    Dim DSeries As Dundas.Charting.WinControl.Series
    Dim PointCounter As Integer

    Try
      ' Get Chart Series.

      If (pSeriesNumber < 0) OrElse (pSeriesNumber >= pChart.Series.Count) Then
        DSeries = pChart.Series.Add("PS" & pChart.Series.Count.ToString)
        DSeries.ChartArea = pChart.ChartAreas(0).Name
      Else
        DSeries = pChart.Series(pSeriesNumber)
        If (DSeries.ChartArea = "") Then DSeries.ChartArea = pChart.ChartAreas(0).Name
      End If

      SourceSeries = pChart.Series(pCopySeriesNumber)

      DSeries.Points.Clear()
      DSeries.Type = SeriesChartType.Line
      DSeries.YValuesPerPoint = 1
      DSeries.YValueType = ChartValueTypes.Double
      DSeries.XValueType = ChartValueTypes.Date

      If (SourceSeries.Points.Count > 0) Then
        ' Set points

        For PointCounter = 0 To (SourceSeries.Points.Count - 1)
          DSeries.Points.AddXY(SourceSeries.Points(PointCounter).XValue, pSeriesAnnualReturn)
        Next
      End If

      ' Format series

      DSeries.Font = New Font("Arial", 8)

      DSeries.CustomAttributes = "LabelStyle=TopLeft,EmptyPointValue=Zero"

      DSeries.ChartType = "Line"
      DSeries.ShadowOffset = 1

      DSeries.LegendText = pLegendText

      ' Set Label Text
      If (DSeries.Points.Count > 0) Then
        DSeries.Points(DSeries.Points.Count - 1).Label = pLegendText
        DSeries.ChartArea = pChart.ChartAreas(0).Name
      Else
        DSeries.ChartArea = ""
      End If

    Catch ex As Exception
    End Try

    Return True

  End Function

  Public Function Set_LineChart(ByVal pMainForm As CTAMain, _
   ByVal pPertracID As Integer, _
   ByVal pChart As Dundas.Charting.WinControl.Chart, _
   ByVal pSeriesNumber As Integer, _
   ByVal pLegendText As String, _
   ByVal pRollingPeriod As Integer, _
   ByVal Lamda As Double, _
   ByVal ScalingFactor As Double, _
   ByVal ChartStartDate As Date, _
   ByVal ChartEndDate As Date, _
   Optional ByVal pLabelStyle As String = "TopLeft", _
   Optional ByVal pZoomReset As Boolean = True) As Boolean

    ' **********************************************************************
    ' Gets Pertrac Index data and sets the given series on the relative performance chart
    ' **********************************************************************

    Dim Date_Series(-1) As Date
    Dim NAV_Series(-1) As Double

    ' Validate
    Try
      If (pRollingPeriod < 1) Then
        pRollingPeriod = 1
      End If
    Catch ex As Exception
    End Try

    Try

      ' Get Underlying Data.

      If (pPertracID <= 0) Then
        Date_Series = Nothing
        NAV_Series = Nothing
      Else
        Try
          Date_Series = pMainForm.StatFunctions.DateSeries(pMainForm.PertracData.GetPertracDataPeriod(pPertracID), CULng(pPertracID), CONST_ChartsBackfillVolatility, pRollingPeriod, Lamda, False, ChartStartDate, ChartEndDate)
          NAV_Series = pMainForm.StatFunctions.NAVSeries(pMainForm.PertracData.GetPertracDataPeriod(pPertracID), CULng(pPertracID), CONST_ChartsBackfillVolatility, pRollingPeriod, Lamda, False, ChartStartDate, ChartEndDate, ScalingFactor)
        Catch ex As Exception
          Date_Series = Nothing
          NAV_Series = Nothing
        End Try
      End If

    Catch ex As Exception
    End Try

    Return Set_LineChart(pMainForm, pMainForm.PertracData.GetPertracDataPeriod(pPertracID), pChart, pSeriesNumber, pLegendText, Date_Series, NAV_Series, ChartStartDate, ChartEndDate, pLabelStyle, pZoomReset)

  End Function

  Public Function Set_LineChart(ByVal pMainForm As CTAMain, _
    ByVal StatsDatePeriod As RenaissanceGlobals.DealingPeriod, _
    ByVal pChart As Dundas.Charting.WinControl.Chart, _
    ByVal pSeriesNumber As Integer, _
    ByVal pLegendText As String, _
    ByVal Date_Series() As Date, _
    ByVal NAV_Series() As Double, _
    ByVal ChartStartDate As Date, _
    ByVal ChartEndDate As Date, _
    ByVal pLabelStyle As String, _
    ByVal pZoomReset As Boolean) As Boolean

    ' **********************************************************************
    ' Gets Pertrac Index data and sets the given series on the relative performance chart
    ' **********************************************************************

    Dim ThisDate As Date
    Dim ThisValue As Double

    ' Build the Chart series.

    Dim DSeries As Dundas.Charting.WinControl.Series
    Dim RowCounter As Integer

    Try
      ' Get Chart Series.

      If (pSeriesNumber < 0) OrElse (pSeriesNumber >= pChart.Series.Count) Then
        DSeries = pChart.Series.Add("PS" & pChart.Series.Count.ToString)
        DSeries.ChartArea = pChart.ChartAreas(0).Name
      Else
        DSeries = pChart.Series(pSeriesNumber)
        If (DSeries.ChartArea = "") Then DSeries.ChartArea = pChart.ChartAreas(0).Name
      End If

      DSeries.Points.Clear()

      ' Underlying Data.

      If (Date_Series Is Nothing) OrElse (NAV_Series Is Nothing) OrElse (Date_Series.Length <= 0) Then
        ' Default Chart if no data.

        DSeries.Points.AddXY(ChartStartDate, 100)

      Else
        ' Draw chart Data.
        ' The chart data will already have been date trimmed by the StatFunction functions.

        For RowCounter = 0 To (Date_Series.Length - 1)
          ' Chart date to Start of period - Looks better on the chart
          ' It can be inconsistent whether the date is at the start or end of the period.

          ThisDate = RenaissanceUtilities.DatePeriodFunctions.FitDateToPeriod(StatsDatePeriod, Date_Series(RowCounter), False)
          ThisValue = NAV_Series(RowCounter)

          If (ThisDate >= ChartStartDate) And (ThisDate <= ChartEndDate) Then

            ' Add this datapoint.

            DSeries.Points.AddXY(ThisDate, ThisValue)
          End If

        Next

      End If

      ' Format series

      DSeries.Font = New Font("Arial", 8)

      Try
        DSeries.CustomAttributes = "LabelStyle=" & pLabelStyle & ",EmptyPointValue=Zero"
      Catch ex As Exception
        DSeries.CustomAttributes = "LabelStyle=TopLeft,EmptyPointValue=Zero"
      End Try

      DSeries.ChartType = "Line"
      DSeries.ShadowOffset = 1

      DSeries.LegendText = pLegendText

      ' Set Label Text

      If (Date_Series Is Nothing) OrElse (NAV_Series Is Nothing) OrElse (Date_Series.Length <= 0) Then
        DSeries.ChartArea = ""
      Else
        If (DSeries.Points.Count > 0) Then
          DSeries.Points(DSeries.Points.Count - 1).Label = pLegendText
          DSeries.ChartArea = pChart.ChartAreas(0).Name
        Else
          DSeries.ChartArea = ""
        End If
      End If

    Catch ex As Exception
    End Try

    pChart.ChartAreas(0).CursorX.UserEnabled = True
    pChart.ChartAreas(0).CursorX.UserSelection = True
    pChart.ChartAreas(0).AxisX.View.Zoomable = True
    pChart.ChartAreas(0).AxisX.IntervalType = DateTimeIntervalType.Auto

    pChart.ChartAreas(0).CursorY.UserEnabled = True
    pChart.ChartAreas(0).CursorY.UserSelection = True
    pChart.ChartAreas(0).AxisY.View.Zoomable = True
    pChart.ChartAreas(0).AxisY.IntervalType = DateTimeIntervalType.Auto

    If (pZoomReset) Then
      pChart.ChartAreas(0).AxisX.View.ZoomReset(0)
      pChart.ChartAreas(0).AxisY.View.ZoomReset(0)
    End If

    Return True
  End Function

  Public Function Set_OmegaChart(ByVal pMainForm As CTAMain, _
   ByVal pPertracID As Integer, _
   ByVal pChart As Chart, _
   ByVal pSeriesNumber As Integer, _
   ByVal pLegendText As String, _
   ByVal pRollingPeriod As Integer, _
   ByVal Lamda As Double, _
   ByVal ScalingFactor As Double, _
   ByVal ChartStartDate As Date, _
   ByVal ChartEndDate As Date, _
   ByVal ShowAsPercentage As Boolean, _
   Optional ByVal pLabelStyle As String = "TopLeft", _
   Optional ByVal pZoomReset As Boolean = True) As Boolean

    ' **********************************************************************
    ' Gets Pertrac Index data and sets the given series on the Omega chart
    ' **********************************************************************

    Dim Date_Series() As Date = Nothing
    Dim NAV_Series() As Double = Nothing

    Try

      ' Get Underlying Data.

      If (pPertracID <= 0) OrElse (pRollingPeriod < 1) Then
        Date_Series = Nothing
        NAV_Series = Nothing
      Else
        Try
          Date_Series = pMainForm.StatFunctions.DateSeries(pMainForm.PertracData.GetPertracDataPeriod(pPertracID), CULng(pPertracID), CONST_ChartsBackfillVolatility, pRollingPeriod, Lamda, False, ChartStartDate, ChartEndDate)
          NAV_Series = pMainForm.StatFunctions.NAVSeries(pMainForm.PertracData.GetPertracDataPeriod(pPertracID), CULng(pPertracID), CONST_ChartsBackfillVolatility, pRollingPeriod, Lamda, False, ChartStartDate, ChartEndDate, ScalingFactor)
        Catch ex As Exception
          Date_Series = Nothing
          NAV_Series = Nothing
        End Try
      End If

    Catch ex As Exception
    End Try

    Return Set_OmegaChart(pMainForm, pMainForm.PertracData.GetPertracDataPeriod(pPertracID), pChart, pSeriesNumber, pLegendText, pRollingPeriod, Date_Series, NAV_Series, ChartStartDate, ChartEndDate, ShowAsPercentage, pLabelStyle, pZoomReset)

    Return True
  End Function

  Public Function Set_OmegaChart(ByVal pMainForm As CTAMain, _
    ByVal StatsDatePeriod As RenaissanceGlobals.DealingPeriod, _
    ByVal pChart As Chart, _
    ByVal pSeriesNumber As Integer, _
    ByVal pLegendText As String, _
    ByVal pRollingPeriod As Integer, _
    ByVal Date_Series() As Date, _
    ByVal NAV_Series() As Double, _
    ByVal ChartStartDate As Date, _
    ByVal ChartEndDate As Date, _
    ByVal ShowAsPercentage As Boolean, _
    ByVal pLabelStyle As String, _
    ByVal pZoomReset As Boolean) As Boolean

    ' **********************************************************************
    ' Gets Pertrac Index data and sets the given series on the Omega chart
    ' **********************************************************************

    Dim ThisReturn As Double
    Dim ThisOmega As Double

    ' Build the Chart series.

    Dim DSeries As Dundas.Charting.WinControl.Series
    ' Dim Return_Series() As Double
    Dim SortedReturns() As Double
    Dim ReturnCounter As Integer


    Try
      ' Get Chart Series.

      If (pSeriesNumber < 0) OrElse (pSeriesNumber >= pChart.Series.Count) Then
        DSeries = pChart.Series.Add("PS" & pChart.Series.Count.ToString)
        DSeries.ChartArea = pChart.ChartAreas(0).Name
      Else
        DSeries = pChart.Series(pSeriesNumber)
        If (DSeries.ChartArea = "") Then DSeries.ChartArea = pChart.ChartAreas(0).Name
      End If

      DSeries.Points.Clear()

      If (Date_Series Is Nothing) OrElse (NAV_Series Is Nothing) OrElse (Date_Series.Length <= (pRollingPeriod + 1)) Then
        ' Default Chart if no data.

        DSeries.Points.AddXY(ChartStartDate, 0)

      Else
        ' Draw chart Data.
        ' The chart data will already have been date trimmed by the StatFunction functions.

        ' First copy the return data so we can sort it without affecting the cached data.

        Dim StartingIndex As Integer
        StartingIndex = pRollingPeriod


        SortedReturns = CType(Array.CreateInstance(GetType(Double), NAV_Series.Length - StartingIndex), Double())

        For Index As Integer = 0 To (NAV_Series.Length - (pRollingPeriod + 1))
          SortedReturns(Index) = (NAV_Series(Index + pRollingPeriod) / NAV_Series(Index)) - 1.0#
        Next

        Array.Sort(SortedReturns)

        For ReturnCounter = 0 To (SortedReturns.Length - 1) ' Start at 1 to avoid first point Divide-By-Zero.

          If (ReturnCounter > 0) OrElse (ShowAsPercentage) Then

            ThisReturn = SortedReturns(ReturnCounter)
            If (ReturnCounter <= 0) Then
              ThisOmega = 100 ' Infinity (Divide by Zero)
            Else
              If (ShowAsPercentage) Then
                ThisOmega = ((SortedReturns.Length - ReturnCounter) / SortedReturns.Length) * 100.0#
              Else
                ThisOmega = (SortedReturns.Length - ReturnCounter) / ReturnCounter
              End If
            End If

            ' Add this datapoint.

            If (ThisOmega <= 100.0#) Then
              DSeries.Points.AddXY(ThisReturn * 100.0#, ThisOmega)
            End If

          End If

        Next

      End If

      ' Format series

      DSeries.Font = New Font("Arial", 8)

      Try
        DSeries.CustomAttributes = "LabelStyle=" & pLabelStyle & ",EmptyPointValue=Zero"
      Catch ex As Exception
        DSeries.CustomAttributes = "LabelStyle=TopLeft,EmptyPointValue=Zero"
      End Try

      DSeries.ChartType = "Line"
      DSeries.ShadowOffset = 1

      DSeries.LegendText = pLegendText

      ' Set Label Text
      If (DSeries.Points.Count > 0) Then
        ' DSeries.Points(DSeries.Points.Count - 1).Label = pLegendText
        DSeries.ChartArea = pChart.ChartAreas(0).Name
      Else
        DSeries.ChartArea = ""
      End If
    Catch ex As Exception
    End Try

    pChart.ChartAreas(0).CursorX.UserEnabled = True
    pChart.ChartAreas(0).CursorX.UserSelection = True
    pChart.ChartAreas(0).AxisX.View.Zoomable = True
    pChart.ChartAreas(0).AxisX.IntervalType = DateTimeIntervalType.Auto

    pChart.ChartAreas(0).CursorY.UserEnabled = True
    pChart.ChartAreas(0).CursorY.UserSelection = True
    pChart.ChartAreas(0).AxisY.View.Zoomable = True
    pChart.ChartAreas(0).AxisY.IntervalType = DateTimeIntervalType.Auto

    If (pZoomReset) Then
      pChart.ChartAreas(0).AxisX.View.ZoomReset(0)
      pChart.ChartAreas(0).AxisY.View.ZoomReset(0)
    End If

    Return True
  End Function

  Public Function Set_ReturnsBarChart(ByVal pMainForm As CTAMain, _
    ByVal pPertracID As Integer, _
    ByVal pChart As Chart, _
    ByVal pSeriesNumber As Integer, _
    ByVal pLegendText As String, _
    ByVal pRollingPeriod As Integer, _
    ByVal Lamda As Double, _
    ByVal ScalingFactor As Double, _
    ByVal ChartStartDate As Date, _
    ByVal ChartEndDate As Date, _
    Optional ByVal BarPositiveColouring As Boolean = False, _
    Optional ByVal pZoomReset As Boolean = True) As Boolean

    ' **********************************************************************
    ' Gets Pertrac Index data and sets the given series on the relative performance chart
    ' **********************************************************************

    Dim pLabelStyle As String = "TopLeft"

    Dim Date_Series(-1) As Date
    Dim Returns_Series(-1) As Double

    ' Build the Chart series, using only data that matches the timescale 
    ' of the F&C Partners Fund Price series.

    Try

      ' Get Underlying Data.

      If (pPertracID <= 0) Then
        Date_Series = Nothing
        Returns_Series = Nothing
      Else
        Date_Series = pMainForm.StatFunctions.DateSeries(pMainForm.PertracData.GetPertracDataPeriod(pPertracID), CULng(pPertracID), CONST_ChartsBackfillVolatility, pRollingPeriod, Lamda, False, ChartStartDate, ChartEndDate)
        Returns_Series = pMainForm.StatFunctions.ReturnSeries(pMainForm.PertracData.GetPertracDataPeriod(pPertracID), CULng(pPertracID), CONST_ChartsBackfillVolatility, pRollingPeriod, Lamda, False, ChartStartDate, ChartEndDate, ScalingFactor)
      End If

    Catch ex As Exception
    End Try

    Return Set_ReturnsBarChart(pMainForm, pChart, pSeriesNumber, pLegendText, Date_Series, Returns_Series, ChartStartDate, ChartEndDate, BarPositiveColouring, pZoomReset)

    Return True

  End Function

  Public Function Set_ReturnsBarChart(ByVal pMainForm As CTAMain, _
    ByVal pChart As Chart, _
    ByVal pSeriesNumber As Integer, _
    ByVal pLegendText As String, _
    ByVal Date_Series() As Date, _
    ByVal Returns_Series() As Double, _
    ByVal ChartStartDate As Date, _
    ByVal ChartEndDate As Date, _
    ByVal BarPositiveColouring As Boolean, _
    ByVal pZoomReset As Boolean) As Boolean

    ' **********************************************************************
    ' Gets Pertrac Index data and sets the given series on the relative performance chart
    ' **********************************************************************

    Dim pLabelStyle As String = "TopLeft"

    Dim ThisDate As Date
    Dim ThisValue As Double

    ' Build the Chart series, using only data that matches the timescale 
    ' of the F&C Partners Fund Price series.

    Dim DSeries As Dundas.Charting.WinControl.Series

    Try
      If (pSeriesNumber < 0) OrElse (pSeriesNumber >= pChart.Series.Count) Then
        DSeries = pChart.Series.Add("PS" & pChart.Series.Count.ToString)
        DSeries.ChartArea = pChart.ChartAreas(0).Name
      Else
        DSeries = pChart.Series(pSeriesNumber)
        If (DSeries.ChartArea = "") Then DSeries.ChartArea = pChart.ChartAreas(0).Name
      End If

      DSeries.Points.Clear()

      Dim RowCounter As Integer

      If (Date_Series Is Nothing) OrElse (Returns_Series Is Nothing) OrElse (Date_Series.Length <= 0) Then
        ' Default Chart if no data.

        DSeries.Points.AddXY(ChartStartDate, 0)

      Else
        ' Draw chart Data.
        ' The chart data will already have been date trimmed by the StatFunction functions.
        Dim PointIndex As Integer

        For RowCounter = 0 To (Date_Series.Length - 1)

          ThisDate = Date_Series(RowCounter)
          ThisValue = Returns_Series(RowCounter)

          If (ThisDate >= ChartStartDate) And (ThisDate <= ChartEndDate) Then

            ' Add this datapoint.

            PointIndex = DSeries.Points.AddXY(ThisDate, ThisValue * 100.0)

            If (BarPositiveColouring) Then
              If (ThisValue >= 0) Then
                DSeries.Points(PointIndex).Color = Color.Green
              Else
                DSeries.Points(PointIndex).Color = Color.Red
              End If
            End If
          End If

        Next

      End If

      ' Format series

      DSeries.Font = New Font("Arial", 8)

      Try
        DSeries.CustomAttributes = "LabelStyle=" & pLabelStyle & ",EmptyPointValue=Zero"
      Catch ex As Exception
        DSeries.CustomAttributes = "LabelStyle=TopLeft,EmptyPointValue=Zero"
      End Try

      DSeries.ChartType = "Column"
      DSeries.ShadowOffset = 1

      If (BarPositiveColouring) Then
        ' Try to create a legend with just the Legend text and no series identifier, since the identifier
        ' colour will be wrong - as we custom coloured all the data points.

        Try
          Dim TempLegendItem As New LegendItem()
          TempLegendItem.BorderColor = Color.Transparent
          TempLegendItem.Color = Color.Transparent
          TempLegendItem.MarkerStyle = MarkerStyle.None
          TempLegendItem.Style = LegendImageStyle.Marker
          TempLegendItem.Name = pLegendText

          If (pChart.Legends.Count <= 0) Then
            Dim NLegend As New Legend

            NLegend.BackColor = System.Drawing.Color.Transparent
            NLegend.BorderColor = System.Drawing.Color.Transparent
            NLegend.Docking = Dundas.Charting.WinControl.LegendDocking.Left
            NLegend.Alignment = StringAlignment.Near
            NLegend.DockInsideChartArea = True
            NLegend.DockToChartArea = "Default"
            NLegend.Name = "Custom"

            pChart.Legends.Add(NLegend)
          End If
          pChart.Legends(0).CustomItems.Add(TempLegendItem)

        Catch ex As Exception
        End Try
      Else
        DSeries.LegendText = pLegendText
      End If

    Catch ex As Exception
    End Try

    pChart.ChartAreas(0).CursorX.UserEnabled = True
    pChart.ChartAreas(0).CursorX.UserSelection = True
    pChart.ChartAreas(0).AxisX.View.Zoomable = True
    pChart.ChartAreas(0).AxisX.IntervalType = DateTimeIntervalType.Auto

    If (pZoomReset) Then
      pChart.ChartAreas(0).AxisX.View.ZoomReset(0)
    End If

    Return True

  End Function

  Public Function Set_RollingReturnChart(ByVal pMainForm As CTAMain, _
   ByVal pPertracID As Integer, _
   ByVal pChart As Chart, _
   ByVal pSeriesNumber As Integer, _
   ByVal pLegendText As String, _
   ByVal pRollingPeriod As Integer, _
   ByVal Lamda As Double, _
   ByVal ScalingFactor As Double, _
   ByVal ChartStartDate As Date, _
   ByVal ChartEndDate As Date, _
   Optional ByVal pLabelStyle As String = "TopLeft", _
   Optional ByVal pZoomReset As Boolean = True) As Boolean

    ' **********************************************************************
    ' Gets Pertrac Index data and sets the given series on the chart
    ' **********************************************************************

    Dim Date_Series() As Date = Nothing
    Dim NAV_Series() As Double = Nothing

    ' Validate
    Try
      If (pRollingPeriod < 1) Then
        pRollingPeriod = 1
      End If
    Catch ex As Exception
    End Try

    Try

      ' Get Underlying Data.

      If (pPertracID <= 0) Then
        Date_Series = Nothing
        NAV_Series = Nothing
      Else
        ' Note, all data is returned. Date selection occurs later.
        ' This is so that date restrictions do no affect rolling return calculations.

        Date_Series = pMainForm.StatFunctions.DateSeries(pMainForm.PertracData.GetPertracDataPeriod(pPertracID), CULng(pPertracID), CONST_ChartsBackfillVolatility, pRollingPeriod, Lamda, False, ChartStartDate, ChartEndDate)
        NAV_Series = pMainForm.StatFunctions.NAVSeries(pMainForm.PertracData.GetPertracDataPeriod(pPertracID), CULng(pPertracID), CONST_ChartsBackfillVolatility, pRollingPeriod, Lamda, False, ChartStartDate, ChartEndDate, ScalingFactor)
      End If

    Catch ex As Exception
    End Try

    Return Set_RollingReturnChart(pMainForm, pChart, pSeriesNumber, pLegendText, Date_Series, NAV_Series, pRollingPeriod, ChartStartDate, ChartEndDate, pLabelStyle, pZoomReset)

    Return True
  End Function

  Public Function Set_RollingReturnChart(ByVal pMainForm As CTAMain, _
    ByVal pChart As Dundas.Charting.WinControl.Chart, _
    ByVal pSeriesNumber As Integer, _
    ByVal pLegendText As String, _
    ByVal Date_Series() As Date, _
    ByVal NAV_Series() As Double, _
    ByVal pRollingPeriod As Integer, _
    ByVal ChartStartDate As Date, _
    ByVal ChartEndDate As Date, _
    ByVal pLabelStyle As String, _
    ByVal pZoomReset As Boolean) As Boolean

    ' **********************************************************************
    ' Gets Pertrac Index data and sets the given series on the chart
    ' **********************************************************************

    Try
      If (pRollingPeriod < 1) Then
        pRollingPeriod = 1
      End If
    Catch ex As Exception
    End Try

    ' Get Chart Series.

    Dim DSeries As Dundas.Charting.WinControl.Series
    Dim RowCounter As Integer

    Try
      If (pSeriesNumber < 0) OrElse (pSeriesNumber >= pChart.Series.Count) Then
        DSeries = pChart.Series.Add("PS" & pChart.Series.Count.ToString)
        DSeries.ChartArea = pChart.ChartAreas(0).Name
      Else
        DSeries = pChart.Series(pSeriesNumber)
        If (DSeries.ChartArea = "") Then DSeries.ChartArea = pChart.ChartAreas(0).Name
      End If

      DSeries.Points.Clear()

      Try

        If (Date_Series Is Nothing) OrElse (NAV_Series Is Nothing) OrElse (Date_Series.Length <= 0) Then
          ' Default Chart if no data.

          DSeries.Points.AddXY(ChartStartDate, 0)

        Else
          ' Draw chart Data.

          Dim ThisDate As Date
          Dim ThisValue As Double

          ' Now Build Chart series

          Dim FirstNAV As Double = NAV_Series(0)
          Dim SecondNAV As Double = NAV_Series(0)

          For RowCounter = 0 To (NAV_Series.Length - 1)

            ThisDate = Date_Series(RowCounter)
            SecondNAV = NAV_Series(RowCounter)

            If (RowCounter >= pRollingPeriod) Then
              FirstNAV = NAV_Series(RowCounter - pRollingPeriod)
            End If

            If (FirstNAV = 0.0#) Then
              ThisValue = 0
            Else
              ThisValue = (SecondNAV / FirstNAV) - 1.0#
            End If

            If (ThisDate >= ChartStartDate) And (ThisDate <= ChartEndDate) Then

              ' Add this datapoint.

              DSeries.Points.AddXY(ThisDate, ThisValue * 100)
            End If

            If (ThisDate > ChartEndDate) Then
              Exit For
            End If
          Next

        End If

      Catch ex As Exception
      End Try

      ' Format series

      DSeries.Font = New Font("Arial", 8)

      Try
        DSeries.CustomAttributes = "LabelStyle=" & pLabelStyle & ",EmptyPointValue=Zero"
      Catch ex As Exception
        DSeries.CustomAttributes = "LabelStyle=TopLeft,EmptyPointValue=Zero"
      End Try

      DSeries.ChartType = "Line"
      DSeries.ShadowOffset = 1

      DSeries.LegendText = pLegendText

      ' Set Label Text

      If (DSeries.Points.Count > 0) Then
        DSeries.Points(DSeries.Points.Count - 1).Label = pLegendText
        DSeries.ChartArea = pChart.ChartAreas(0).Name
      Else
        DSeries.ChartArea = ""
      End If
    Catch ex As Exception
    End Try

    pChart.ChartAreas(0).CursorX.UserEnabled = True
    pChart.ChartAreas(0).CursorX.UserSelection = True
    pChart.ChartAreas(0).AxisX.View.Zoomable = True
    pChart.ChartAreas(0).AxisX.IntervalType = DateTimeIntervalType.Auto

    pChart.ChartAreas(0).CursorY.UserEnabled = True
    pChart.ChartAreas(0).CursorY.UserSelection = True
    pChart.ChartAreas(0).AxisY.View.Zoomable = True
    pChart.ChartAreas(0).AxisY.IntervalType = DateTimeIntervalType.Auto

    If (pZoomReset) Then
      pChart.ChartAreas(0).AxisX.View.ZoomReset(0)
      pChart.ChartAreas(0).AxisY.View.ZoomReset(0)
    End If

    Return True
  End Function

  Public Function Set_ReturnScatterChart(ByVal pMainForm As CTAMain, _
   ByVal pPertracID As Integer, _
   ByVal pReferenceID As Integer, _
   ByVal pChart As Chart, _
   ByVal pSeriesNumber As Integer, _
   ByVal pLegendText As String, _
   ByVal pRollingPeriod As Integer, _
   ByVal Lamda As Double, _
   ByVal ScalingFactor As Double, _
   ByVal ReferenceScalingFactor As Double, _
   ByVal ChartStartDate As Date, _
   ByVal ChartEndDate As Date, _
   Optional ByVal pLabelStyle As String = "TopLeft") As Boolean

    ' **********************************************************************
    ' Gets Pertrac Index data and sets the given series on the chart
    ' **********************************************************************

    Dim StartDate As Date
    Dim EndDate As Date

    ' Validate
    Try
      If (pRollingPeriod < 1) Then
        pRollingPeriod = 1
      End If
    Catch ex As Exception
    End Try

    Try
      ' Base Series Start and End dates on the Fund series

      StartDate = ChartStartDate
      EndDate = ChartEndDate

    Catch ex As Exception
    End Try

    ' Get Chart Series.

    Dim DSeries As Dundas.Charting.WinControl.Series

    Try
      If (pSeriesNumber < 0) OrElse (pSeriesNumber >= pChart.Series.Count) Then
        DSeries = pChart.Series.Add("PS" & pChart.Series.Count.ToString)
        DSeries.ChartArea = pChart.ChartAreas(0).Name
      Else
        DSeries = pChart.Series(pSeriesNumber)
        If (DSeries.ChartArea = "") Then DSeries.ChartArea = pChart.ChartAreas(0).Name
      End If

      DSeries.Points.Clear()
      DSeries.Type = SeriesChartType.Point
      DSeries.YValuesPerPoint = 1
      DSeries.YValueType = ChartValueTypes.Double
      DSeries.XValueType = ChartValueTypes.Double

      Try
        Dim Reference_Date_Series() As Date
        Dim Reference_NAV_Series() As Double
        Dim Instrument_Date_Series() As Date
        Dim Instrument_NAV_Series() As Double
        Dim RowCounter As Integer
        Dim ValidData As Boolean = True

        ' Get Reference Data
        If (pReferenceID <= 0) Then
          Reference_Date_Series = Nothing
          Reference_NAV_Series = Nothing
        Else
          ' Note, all data is returned. Date selection occurs later.
          ' This is so that date restrictions do no affect rolling return calculations.

          Reference_Date_Series = pMainForm.StatFunctions.DateSeries(pMainForm.PertracData.GetPertracDataPeriod(pPertracID, pReferenceID), CULng(pReferenceID), CONST_ChartsBackfillVolatility, pRollingPeriod, Lamda, False, Renaissance_BaseDate, Renaissance_EndDate_Data)
          Reference_NAV_Series = pMainForm.StatFunctions.NAVSeries(pMainForm.PertracData.GetPertracDataPeriod(pPertracID, pReferenceID), CULng(pReferenceID), CONST_ChartsBackfillVolatility, pRollingPeriod, Lamda, False, Renaissance_BaseDate, Renaissance_EndDate_Data, ReferenceScalingFactor)
        End If

        ' Get Instrument Data.

        If (pPertracID <= 0) Then
          Instrument_Date_Series = Nothing
          Instrument_NAV_Series = Nothing
        Else
          ' Note, all data is returned. Date selection occurs later.
          ' This is so that date restrictions do no affect rolling return calculations.

          Instrument_Date_Series = pMainForm.StatFunctions.DateSeries(pMainForm.PertracData.GetPertracDataPeriod(pPertracID, pReferenceID), CULng(pPertracID), CONST_ChartsBackfillVolatility, pRollingPeriod, Lamda, False, Renaissance_BaseDate, Renaissance_EndDate_Data)
          Instrument_NAV_Series = pMainForm.StatFunctions.NAVSeries(pMainForm.PertracData.GetPertracDataPeriod(pPertracID, pReferenceID), CULng(pPertracID), CONST_ChartsBackfillVolatility, pRollingPeriod, Lamda, False, Renaissance_BaseDate, Renaissance_EndDate_Data, ScalingFactor)
        End If

        If (Reference_Date_Series Is Nothing) OrElse (Reference_NAV_Series Is Nothing) OrElse (Reference_Date_Series.Length <= 0) Then
          ValidData = False
        End If
        If (Instrument_Date_Series Is Nothing) OrElse (Instrument_NAV_Series Is Nothing) OrElse (Instrument_Date_Series.Length <= 0) Then
          ValidData = False
        End If

        ' Calculate Overlap and process on that basis

        Dim Instrument_Start_Index As Integer
        Dim Reference_Start_Index As Integer
        Dim OverlapLength As Integer

        If (ValidData) Then
          If (Instrument_Date_Series(0) = Reference_Date_Series(0)) Then
            Instrument_Start_Index = 1
            Reference_Start_Index = 1

            If (Instrument_Date_Series.Length <= Reference_Date_Series.Length) Then
              OverlapLength = (Instrument_Date_Series.Length - Instrument_Start_Index)
            Else
              OverlapLength = (Reference_Date_Series.Length - Reference_Start_Index)
            End If

          ElseIf (Instrument_Date_Series(0) < Reference_Date_Series(0)) Then

            Instrument_Start_Index = GetPriceIndex(pMainForm.PertracData.GetPertracDataPeriod(pPertracID, pReferenceID), Instrument_Date_Series(0), Reference_Date_Series(1))
            Reference_Start_Index = 1

            OverlapLength = (Instrument_Date_Series.Length - Instrument_Start_Index)
            If (OverlapLength > (Reference_Date_Series.Length - Reference_Start_Index)) Then
              OverlapLength = (Reference_Date_Series.Length - Reference_Start_Index)
            End If

          ElseIf (Instrument_Date_Series(0) > Reference_Date_Series(0)) Then

            Reference_Start_Index = GetPriceIndex(pMainForm.PertracData.GetPertracDataPeriod(pPertracID, pReferenceID), Reference_Date_Series(0), Instrument_Date_Series(1))
            Instrument_Start_Index = 1

            OverlapLength = (Instrument_Date_Series.Length - Instrument_Start_Index)
            If (OverlapLength > (Reference_Date_Series.Length - Reference_Start_Index)) Then
              OverlapLength = (Reference_Date_Series.Length - Reference_Start_Index)
            End If

          Else
            ' ???
            ValidData = False
          End If
        End If

        If (OverlapLength <= 0) Then
          ValidData = False
        End If

        If (ValidData = False) Then
          ' Default Chart if no data.

          DSeries.Points.AddXY(0, 0)

        Else
          ' Draw chart Data.
          Dim Instrument_Index As Integer
          Dim Reference_Index As Integer
          'Dim Instrument_Start_Index As Integer
          'Dim Reference_Start_Index As Integer

          Dim ThisDate As Date
          Dim ThisInstrumentValue As Double
          Dim ThisReferenceValue As Double

          ' Now Build Chart series

          Dim FirstInstrumentNAV As Double = Instrument_NAV_Series(Instrument_Start_Index)
          Dim SecondInstrumentNAV As Double = Instrument_NAV_Series(Instrument_Start_Index)
          Dim FirstReferenceNAV As Double = Reference_NAV_Series(Reference_Start_Index)
          Dim SecondReferenceNAV As Double = Reference_NAV_Series(Reference_Start_Index)

          For RowCounter = pRollingPeriod To (OverlapLength - 1)  ' (Instrument_NAV_Series.Length - 1)
            Instrument_Index = Instrument_Start_Index + RowCounter
            Reference_Index = Reference_Start_Index + RowCounter

            ThisDate = Instrument_Date_Series(Instrument_Index)
            SecondInstrumentNAV = Instrument_NAV_Series(Instrument_Index)
            SecondReferenceNAV = Reference_NAV_Series(Reference_Index)

            If (RowCounter >= pRollingPeriod) Then
              FirstInstrumentNAV = Instrument_NAV_Series(Instrument_Index - pRollingPeriod)
              FirstReferenceNAV = Reference_NAV_Series(Reference_Index - pRollingPeriod)
            End If
            ThisInstrumentValue = (SecondInstrumentNAV / FirstInstrumentNAV) - 1.0
            ThisReferenceValue = (SecondReferenceNAV / FirstReferenceNAV) - 1.0

            If (ThisDate >= StartDate) And (ThisDate <= EndDate) Then
              Dim DP As New Dundas.Charting.WinControl.DataPoint(ThisReferenceValue * 100, ThisInstrumentValue * 100)
              DP.ToolTip = ThisDate.ToString(DISPLAYMEMBER_DATEFORMAT)

              ' Add this datapoint.
              DSeries.Points.Add(DP)

              'DSeries.Points.AddXY(ThisReferenceValue * 100, ThisInstrumentValue * 100)
            End If

            If (ThisDate > EndDate) Then
              Exit For
            End If
          Next

        End If

      Catch ex As Exception
      End Try

      ' Format series

      DSeries.Font = New Font("Arial", 8)

      Try
        DSeries.CustomAttributes = "LabelStyle=" & pLabelStyle & ",EmptyPointValue=Zero"
      Catch ex As Exception
        DSeries.CustomAttributes = "LabelStyle=TopLeft,EmptyPointValue=Zero"
      End Try

      DSeries.ChartType = "Point"
      DSeries.ShadowOffset = 1
      DSeries.LegendText = pLegendText

      ' Set Label Text

      If (DSeries.Points.Count > 0) Then
        ' DSeries.Points(DSeries.Points.Count - 1).Label = pLegendText
        DSeries.ChartArea = pChart.ChartAreas(0).Name
      Else
        DSeries.ChartArea = ""
      End If
    Catch ex As Exception
    End Try

    pChart.ChartAreas(0).CursorX.UserEnabled = True
    pChart.ChartAreas(0).CursorX.UserSelection = True
    pChart.ChartAreas(0).AxisX.View.Zoomable = True
    pChart.ChartAreas(0).AxisX.IntervalType = DateTimeIntervalType.Auto
    pChart.ChartAreas(0).AxisX.View.ZoomReset(0)

    Return True
  End Function

  Public Function Set_StdDevChart(ByVal pMainForm As CTAMain, _
   ByVal pPertracID As Integer, _
   ByVal pChart As Chart, _
   ByVal pSeriesNumber As Integer, _
   ByVal pLegendText As String, _
   ByVal pRollingPeriod As Integer, _
   ByVal Lamda As Double, _
   ByVal ScalingFactor As Double, _
   ByVal ChartStartDate As Date, _
   ByVal ChartEndDate As Date, _
   Optional ByVal pLabelStyle As String = "TopLeft", _
   Optional ByVal pZoomReset As Boolean = True) As Boolean

    ' **********************************************************************
    ' Gets Pertrac Index data and sets the given series on the chart
    ' **********************************************************************

    ' Validate
    If (pRollingPeriod < 1) Then
      pRollingPeriod = 1
    End If

    Try

      Dim Date_Series() As Date
      Dim StdDev_Series() As Double

      ' Now get StdDev Series.

      If (pPertracID <= 0) Then
        Date_Series = Nothing
        StdDev_Series = Nothing
      Else
        ' Note, all data is returned. Date selection occurs later.
        ' This is so that date restrictions do no affect Standard Deviation calculations.

        Date_Series = pMainForm.StatFunctions.DateSeries(pMainForm.PertracData.GetPertracDataPeriod(pPertracID), CULng(pPertracID), CONST_ChartsBackfillVolatility, pRollingPeriod, Lamda, False, Renaissance_BaseDate, Renaissance_EndDate_Data)
        StdDev_Series = pMainForm.StatFunctions.StdDevSeries(pMainForm.PertracData.GetPertracDataPeriod(pPertracID), CULng(pPertracID), CONST_ChartsBackfillVolatility, pRollingPeriod, Lamda, True, Renaissance_BaseDate, Renaissance_EndDate_Data, ScalingFactor)
      End If

      Set_StdDevChart(pMainForm, pMainForm.PertracData.GetPertracDataPeriod(pPertracID), pChart, pSeriesNumber, pLegendText, pRollingPeriod, Date_Series, StdDev_Series, ChartStartDate, ChartEndDate, pLabelStyle, pZoomReset)
    Catch ex As Exception
    End Try

    Return True
  End Function

  Public Function Set_StdDevChart(ByVal pMainForm As CTAMain, _
    ByVal StatsDatePeriod As RenaissanceGlobals.DealingPeriod, _
    ByVal pChart As Chart, _
    ByVal pSeriesNumber As Integer, _
    ByVal pLegendText As String, _
    ByVal pRollingPeriod As Integer, _
    ByVal Date_Series() As Date, _
    ByVal StdDev_Series() As Double, _
    ByVal ChartStartDate As Date, _
    ByVal ChartEndDate As Date, _
    ByVal pLabelStyle As String, _
    ByVal pZoomReset As Boolean) As Boolean

    ' **********************************************************************
    ' Gets Pertrac Index data and sets the given series on the chart
    ' **********************************************************************

    ' Build the Chart series.

    Dim DSeries As Dundas.Charting.WinControl.Series

    Try
      If (pSeriesNumber < 0) OrElse (pSeriesNumber >= pChart.Series.Count) Then
        DSeries = pChart.Series.Add("PS" & pChart.Series.Count.ToString)
        DSeries.ChartArea = pChart.ChartAreas(0).Name
      Else
        DSeries = pChart.Series(pSeriesNumber)
        If (DSeries.ChartArea = "") Then DSeries.ChartArea = pChart.ChartAreas(0).Name
      End If

      DSeries.Points.Clear()


      Try
        Dim ThisDate As Date
        Dim RowCounter As Integer

        If (Date_Series Is Nothing) OrElse (StdDev_Series Is Nothing) OrElse (Date_Series.Length <= 0) Then
          ' Default Chart if no data.

          DSeries.Points.AddXY(ChartStartDate, 0)

        Else

          ' Start at 'pRollingPeriod' as StdDev to this point is Zero.

          For RowCounter = pRollingPeriod To (Date_Series.Length - 1)
            ThisDate = Date_Series(RowCounter)

            If (ThisDate >= ChartStartDate) And (ThisDate <= ChartEndDate) Then

              ' Add this datapoint.

              DSeries.Points.AddXY(ThisDate, StdDev_Series(RowCounter) * 100)
            End If

            If (ThisDate > ChartEndDate) Then
              Exit For
            End If
          Next

        End If
      Catch ex As Exception
      End Try

      ' Format series

      DSeries.Font = New Font("Arial", 8)

      Try
        DSeries.CustomAttributes = "LabelStyle=" & pLabelStyle & ",EmptyPointValue=Zero"
      Catch ex As Exception
        DSeries.CustomAttributes = "LabelStyle=TopLeft,EmptyPointValue=Zero"
      End Try

      DSeries.ChartType = "Line"
      DSeries.ShadowOffset = 1

      DSeries.LegendText = pLegendText

      ' Set Label Text

      If (DSeries.Points.Count > 0) Then
        DSeries.Points(DSeries.Points.Count - 1).Label = pLegendText
        DSeries.ChartArea = pChart.ChartAreas(0).Name
      Else
        DSeries.ChartArea = ""
      End If
    Catch ex As Exception
    End Try

    pChart.ChartAreas(0).CursorX.UserEnabled = True
    pChart.ChartAreas(0).CursorX.UserSelection = True
    pChart.ChartAreas(0).AxisX.View.Zoomable = True
    pChart.ChartAreas(0).AxisX.IntervalType = DateTimeIntervalType.Auto

    pChart.ChartAreas(0).CursorY.UserEnabled = True
    pChart.ChartAreas(0).CursorY.UserSelection = True
    pChart.ChartAreas(0).AxisY.View.Zoomable = True
    pChart.ChartAreas(0).AxisY.IntervalType = DateTimeIntervalType.Auto

    If (pZoomReset) Then
      pChart.ChartAreas(0).AxisX.View.ZoomReset(0)
      pChart.ChartAreas(0).AxisY.View.ZoomReset(0)
    End If

    Return True
  End Function

  Public Function Set_VARChart(ByVal pMainForm As CTAMain, _
   ByVal pPertracID As Integer, _
   ByVal pChart As Object, _
   ByVal pLegendText As String, _
   ByVal pVARConfidence As Double, _
   ByVal pRollingPeriod As Integer, _
   ByVal pVARPeriods As Integer, _
   ByVal Lamda As Double, _
   ByVal ScalingFactor As Double, _
   ByVal ChartStartDate As Date, _
   ByVal ChartEndDate As Date, _
   Optional ByVal pLabelStyle As String = "TopLeft", _
   Optional ByVal pZoomReset As Boolean = True) As Boolean

    ' **********************************************************************
    '
    ' **********************************************************************

    Try

      'If (TypeOf pChart Is C1.Win.C1Chart.C1Chart) Then
      '	' Return Set_VARChartC1(pMainForm, pPertracID, CType(pChart, C1.Win.C1Chart.C1Chart), pLegendText, pVARConfidence, pRollingPeriod, pVARPeriods, Lamda, ChartStartDate, ChartEndDate, pLabelStyle)
      'Else

      If (TypeOf pChart Is Dundas.Charting.WinControl.Chart) Then

        Return Set_VARChartDundas(pMainForm, pPertracID, CType(pChart, Dundas.Charting.WinControl.Chart), pLegendText, pVARConfidence, pRollingPeriod, pVARPeriods, Lamda, ScalingFactor, False, True, ChartStartDate, ChartEndDate, pLabelStyle, pZoomReset)

      End If

    Catch ex As Exception
    End Try

  End Function

  Public Function Set_VARChartDundas(ByVal pMainForm As CTAMain, _
    ByVal pPertracID As Integer, _
    ByVal pChart As Dundas.Charting.WinControl.Chart, _
    ByVal pLegendText As String, _
    ByVal pVARConfidence As Double, _
    ByVal pRollingPeriod As Integer, _
    ByVal pVARPeriods As Integer, _
    ByVal Lamda As Double, _
    ByVal ScalingFactor As Double, _
    ByVal pCenterAroundZero As Boolean, _
    ByVal pOffsetVarByRollingReturn As Boolean, _
    ByVal ChartStartDate As Date, _
    ByVal ChartEndDate As Date, _
    ByVal pLabelStyle As String, _
    ByVal pZoomReset As Boolean) As Boolean

    ' **********************************************************************
    '
    ' **********************************************************************

    'Dim PerformanceFundInstrument As Integer = 0
    'Dim VarConfidenceLevel As Double = pVARConfidence
    'Dim VarDurationPeriods As Integer = pVARPeriods
    Dim RollingPeriodCount As Integer = pVARPeriods
    Dim StDevCalcPeriods As Integer = pRollingPeriod
    'Dim CenterAroundZero As Boolean = pCenterAroundZero ' False
    'Dim OffsetVarByRollingReturn As Boolean = pOffsetVarByRollingReturn ' True
    Dim StatsDatePeriod As DealingPeriod

    Dim StartDate As Date = RenaissanceGlobals.Globals.Renaissance_BaseDate
    Dim EndDate As Date = RenaissanceGlobals.Globals.Renaissance_EndDate_Data

    Dim VarChart As Dundas.Charting.WinControl.Chart

    Try

      ' Resolve Chart reference

      If (pChart IsNot Nothing) Then
        VarChart = pChart
      Else
        Return False
      End If

      ' Resolve Instrument IDs, either Venice Instruments or Pertrac IDs.

      StatsDatePeriod = pMainForm.PertracData.GetPertracDataPeriod(pPertracID)

      ' Chart Parameters

      StartDate = ChartStartDate
      EndDate = ChartEndDate

      ' First, Get data sets

      Dim DateSeries() As Date
      Dim StDevSeries() As Double
      Dim RollingReturnSeries() As Double

      Dim ThisInstrumentID As ULong
      Dim FrontInstrumentID As ULong

      ' Derive Instrument ID

      ThisInstrumentID = pMainForm.StatFunctions.CombinedStatsID(pPertracID, False)
      FrontInstrumentID = ThisInstrumentID

      'Dim thisStatCache As StatFunctions.StatCacheClass
      'thisStatCache = pMainForm.StatFunctions.BuildNewStatSeries(ThisInstrumentID, False, StDevCalcPeriods, Lamda)

      ' Retrieve Data Series

      DateSeries = pMainForm.StatFunctions.DateSeries(StatsDatePeriod, ThisInstrumentID, False, StDevCalcPeriods, Lamda, True, RenaissanceGlobals.Globals.Renaissance_BaseDate, EndDate)
      StDevSeries = pMainForm.StatFunctions.StdDevSeries(StatsDatePeriod, ThisInstrumentID, False, StDevCalcPeriods, Lamda, True, RenaissanceGlobals.Globals.Renaissance_BaseDate, EndDate, ScalingFactor)
      RollingReturnSeries = pMainForm.StatFunctions.PeriodReturnSeries(StatsDatePeriod, ThisInstrumentID, False, RollingPeriodCount, Lamda, True, RenaissanceGlobals.Globals.Renaissance_BaseDate, EndDate, ScalingFactor)

      Set_VARChartDundas(pMainForm, StatsDatePeriod, pChart, pLegendText, DateSeries, StDevSeries, RollingReturnSeries, pVARConfidence, pRollingPeriod, pVARPeriods, pCenterAroundZero, pOffsetVarByRollingReturn, ChartStartDate, ChartEndDate, pLabelStyle, pZoomReset)

    Catch ex As Exception

    End Try

  End Function

  Public Function Set_VARChartDundas(ByVal pMainForm As CTAMain, _
    ByVal StatsDatePeriod As RenaissanceGlobals.DealingPeriod, _
    ByVal pChart As Dundas.Charting.WinControl.Chart, _
    ByVal pLegendText As String, _
    ByVal DateSeries() As Date, _
    ByVal StDevSeries() As Double, _
    ByVal RollingReturnSeries() As Double, _
    ByVal pVARConfidence As Double, _
    ByVal pRollingPeriod As Integer, _
    ByVal pVARPeriods As Integer, _
    ByVal pCenterAroundZero As Boolean, _
    ByVal pOffsetVarByRollingReturn As Boolean, _
    ByVal ChartStartDate As Date, _
    ByVal ChartEndDate As Date, _
    ByVal pLabelStyle As String, _
    ByVal pZoomReset As Boolean) As Boolean

    ' **********************************************************************
    '
    ' **********************************************************************

    Dim PerformanceFundInstrument As Integer = 0
    Dim VarConfidenceLevel As Double = pVARConfidence
    Dim VarDurationPeriods As Integer = pVARPeriods
    Dim RollingPeriodCount As Integer = pVARPeriods
    Dim StDevCalcPeriods As Integer = pRollingPeriod
    Dim CenterAroundZero As Boolean = pCenterAroundZero ' False
    Dim OffsetVarByRollingReturn As Boolean = pOffsetVarByRollingReturn ' True

    Dim VarChart As Dundas.Charting.WinControl.Chart

    Try

      ' Resolve Chart reference

      If (pChart IsNot Nothing) Then
        VarChart = pChart
      Else
        Return False
      End If

      ' Resolve Instrument IDs, either Venice Instruments or Pertrac IDs.

      ' First, Get data sets

      Dim FrontDateSeries() As Date = DateSeries
      Dim VarBaseRollingReturnSeries() As Double = RollingReturnSeries
      Dim VARSeries() As Double
      Dim VARMultiplier As Double
      Dim VarZScore As Double
      Dim ReturnCounter As Integer


      VARSeries = CType(Array.CreateInstance(GetType(Double), RollingReturnSeries.Length), Double())

      ' Get ZScore, the number of StDevs from mean representing the given confidence level.
      ' Note, for 95% confidence, you need the NormInv for 0.975 reflecting the fact that you need to omit both tails. i.e. 0.975+ and 0.025-

      VarZScore = NORMINV(0.5# + (VarConfidenceLevel / 2.0#), 0, 1)
      VARMultiplier = VarZScore * Math.Sqrt(CDbl(VarDurationPeriods)) / pMainForm.StatFunctions.Sqrt12(StatsDatePeriod)

      ' Set Var Series

      If OffsetVarByRollingReturn Then

        For ReturnCounter = RollingPeriodCount To (RollingReturnSeries.Length - 1) ' ' For ReturnCounter = 0 To (RollingReturnSeries.Length - 1)
          VARSeries(ReturnCounter) = StDevSeries(ReturnCounter - RollingPeriodCount) * VARMultiplier
        Next

      Else

        For ReturnCounter = 0 To (RollingReturnSeries.Length - 1)
          VARSeries(ReturnCounter) = StDevSeries(ReturnCounter) * VARMultiplier
        Next

      End If

      '	OK, we should have enough to paint the chart.

      ' VarChart

      While (VarChart.Series.Count > 3)
        VarChart.Series.RemoveAt(VarChart.Series.Count - 1)
      End While
      While (VarChart.Series.Count < 3)
        VarChart.Series.Add("S" & VarChart.Series.Count.ToString)
      End While

      VarChart.Series(0).Type = SeriesChartType.StackedArea
      VarChart.Series(0).Color = Color.Transparent
      VarChart.Series(0).BorderStyle = ChartDashStyle.NotSet
      VarChart.Series(0).BorderWidth = 0

      VarChart.Series(1).Type = SeriesChartType.StackedArea
      VarChart.Series(1).Color = Color.MistyRose
      VarChart.Series(1).BorderStyle = ChartDashStyle.NotSet
      VarChart.Series(1).BorderWidth = 0
      VarChart.Series(1).BackHatchStyle = ChartHatchStyle.Percent50

      VarChart.Series(2).Type = SeriesChartType.Line
      VarChart.Series(2).Color = Color.DarkBlue
      VarChart.Series(2).BorderStyle = ChartDashStyle.Solid
      VarChart.Series(2).BorderWidth = 2
      VarChart.Series(2).BackHatchStyle = ChartHatchStyle.Percent50

      VarChart.Series(0).Points.Clear()
      VarChart.Series(1).Points.Clear()
      VarChart.Series(2).Points.Clear()

      ' Calculate start of chart data.
      ' We do not want to display any of the Backfill return series, so work out where
      ' in the Combined series the Front Series data starts and use that as a backstop.

      Dim StartRollingPeriodIndex As Integer
      Dim StartStDevCalcIndex As Integer
      Dim StartOfFrontData As Integer
      Dim StartDateIndex As Integer

      StartDateIndex = GetPriceIndex(StatsDatePeriod, DateSeries(0), ChartStartDate)
      StartOfFrontData = Math.Max(StartDateIndex, GetPriceIndex(StatsDatePeriod, DateSeries(0), FrontDateSeries(1)))
      StartRollingPeriodIndex = Math.Max(RollingPeriodCount, StartOfFrontData)

      ' On reflection, it has been decided between ADC & NPP that the 'Current' VAR figure should reflect the StdDev
      ' from before the current Price-Moving-Average period. This means the chart will, for example, show the current
      ' 3 Month Return vs the VAR predicted at the start of those 3 months.
      ' Thus....

      If OffsetVarByRollingReturn Then

        StartStDevCalcIndex = Math.Max(StDevCalcPeriods + RollingPeriodCount, StartOfFrontData)

      Else

        StartStDevCalcIndex = Math.Max(StDevCalcPeriods, StartOfFrontData)

      End If

      ' Set Chart Data.

      If (DateSeries Is Nothing) OrElse (StDevSeries Is Nothing) OrElse (DateSeries.Length <= 0) Then

        VarChart.Series(0).Points.AddXY(ChartStartDate, 0)

      Else

        For ReturnCounter = Math.Min(StartRollingPeriodIndex, StartStDevCalcIndex) To (RollingReturnSeries.Length - 1)

          If (ReturnCounter >= StartRollingPeriodIndex) Then
            VarChart.Series(2).Points.AddXY(DateSeries(ReturnCounter), RollingReturnSeries(ReturnCounter) * 100.0#)
          Else
            VarChart.Series(2).Points.AddXY(DateSeries(ReturnCounter), 0)
          End If

          If (ReturnCounter >= StartStDevCalcIndex) Then
            ' StartStDevCalcIndex includes the RollingPeriod Adjustment
            ' VARSeries also includes the RollingPeriod adjustment.

            If (CenterAroundZero) Then

              VarChart.Series(0).Points.AddXY(DateSeries(ReturnCounter), (0.0# - VARSeries(ReturnCounter)) * 100.0#)
              VarChart.Series(1).Points.AddXY(DateSeries(ReturnCounter), (2.0# * VARSeries(ReturnCounter)) * 100.0#) ' This is the width of the band, not the top of the band.

            Else

              If OffsetVarByRollingReturn Then

                VarChart.Series(0).Points.AddXY(DateSeries(ReturnCounter), (VarBaseRollingReturnSeries(ReturnCounter - RollingPeriodCount) - VARSeries(ReturnCounter)) * 100.0#)
                VarChart.Series(1).Points.AddXY(DateSeries(ReturnCounter), (2.0# * VARSeries(ReturnCounter)) * 100.0#) ' This is the width of the band, not the top of the band.

              Else

                VarChart.Series(0).Points.AddXY(DateSeries(ReturnCounter), (VarBaseRollingReturnSeries(ReturnCounter) - VARSeries(ReturnCounter)) * 100.0#)
                VarChart.Series(1).Points.AddXY(DateSeries(ReturnCounter), (2.0# * VARSeries(ReturnCounter)) * 100.0#) ' This is the width of the band, not the top of the band.

              End If

            End If
          Else

            If (CenterAroundZero) Then

              VarChart.Series(0).Points.AddXY(DateSeries(ReturnCounter), (0.0# - VARSeries(StartStDevCalcIndex)) * 100.0#)
              VarChart.Series(1).Points.AddXY(DateSeries(ReturnCounter), (2.0# * VARSeries(StartStDevCalcIndex)) * 100.0#)

            Else

              If OffsetVarByRollingReturn Then

                VarChart.Series(0).Points.AddXY(DateSeries(ReturnCounter), (VarBaseRollingReturnSeries(StartStDevCalcIndex - RollingPeriodCount) - VARSeries(StartStDevCalcIndex)) * 100.0#)
                VarChart.Series(1).Points.AddXY(DateSeries(ReturnCounter), (2.0# * VARSeries(StartStDevCalcIndex)) * 100.0#)

              Else

                VarChart.Series(0).Points.AddXY(DateSeries(ReturnCounter), (VarBaseRollingReturnSeries(StartStDevCalcIndex) - VARSeries(StartStDevCalcIndex)) * 100.0#)
                VarChart.Series(1).Points.AddXY(DateSeries(ReturnCounter), (2.0# * VARSeries(StartStDevCalcIndex)) * 100.0#)

              End If

            End If

          End If

        Next

      End If

      pChart.ChartAreas(0).CursorX.UserEnabled = True
      pChart.ChartAreas(0).CursorX.UserSelection = True
      pChart.ChartAreas(0).AxisX.View.Zoomable = True
      pChart.ChartAreas(0).AxisX.IntervalType = DateTimeIntervalType.Auto

      pChart.ChartAreas(0).CursorY.UserEnabled = True
      pChart.ChartAreas(0).CursorY.UserSelection = True
      pChart.ChartAreas(0).AxisY.View.Zoomable = True
      pChart.ChartAreas(0).AxisY.IntervalType = DateTimeIntervalType.Auto

      If (pZoomReset) Then
        pChart.ChartAreas(0).AxisX.View.ZoomReset(0)
        pChart.ChartAreas(0).AxisY.View.ZoomReset(0)
      End If

    Catch ex As Exception

    End Try

  End Function

  Public Function Set_DrawDownChart(ByVal pMainForm As CTAMain, _
   ByVal pPertracID As Integer, _
   ByVal pChart As Chart, _
   ByVal pSeriesNumber As Integer, _
   ByVal pLegendText As String, _
   ByVal pRollingPeriod As Integer, _
   ByVal Lamda As Double, _
   ByVal ScalingFactor As Double, _
   ByVal ChartStartDate As Date, _
   ByVal ChartEndDate As Date, _
   Optional ByVal pLabelStyle As String = "TopLeft", _
   Optional ByVal pZoomReset As Boolean = True) As Boolean

    ' ***********************************************************************
    ' Gets Pertrac Index data and sets the given series on the Drawdown chart
    ' -----------------------------------------------------------------------
    ' 
    ' Remove Point Label. NPP 22 Feb 2008
    '
    ' ***********************************************************************

    Dim StartDate As Date
    Dim EndDate As Date

    ' Exit if no Index given.

    Try
      ' Base Series Start and End dates on the Fund series

      StartDate = CDate("1 Jan 1900")
      EndDate = CDate("1 Jan 3000")

    Catch ex As Exception
    End Try

    ' Build the Chart series.

    Dim DSeries As Dundas.Charting.WinControl.Series

    Dim Date_Series() As Date = Nothing
    Dim NAV_Series() As Double = Nothing

    Try
      If (pSeriesNumber < 0) OrElse (pSeriesNumber >= pChart.Series.Count) Then
        DSeries = pChart.Series.Add("PS" & pChart.Series.Count.ToString)
        DSeries.ChartArea = pChart.ChartAreas(0).Name
      Else
        DSeries = pChart.Series(pSeriesNumber)
        If (DSeries.ChartArea = "") Then DSeries.ChartArea = pChart.ChartAreas(0).Name
      End If

      DSeries.Points.Clear()

      ' Get Underlying Data.

      If (pPertracID <= 0) Then
        Date_Series = Nothing
        NAV_Series = Nothing
      Else
        Try
          Date_Series = pMainForm.StatFunctions.DateSeries(pMainForm.PertracData.GetPertracDataPeriod(pPertracID), CULng(pPertracID), CONST_ChartsBackfillVolatility, pRollingPeriod, Lamda, False, ChartStartDate, ChartEndDate)
          NAV_Series = pMainForm.StatFunctions.NAVSeries(pMainForm.PertracData.GetPertracDataPeriod(pPertracID), CULng(pPertracID), CONST_ChartsBackfillVolatility, pRollingPeriod, Lamda, False, ChartStartDate, ChartEndDate, ScalingFactor)
        Catch ex As Exception
          Date_Series = Nothing
          NAV_Series = Nothing
        End Try
      End If

    Catch ex As Exception
    End Try

    Return Set_DrawDownChart(pMainForm, pMainForm.PertracData.GetPertracDataPeriod(pPertracID), pChart, pSeriesNumber, pLegendText, Date_Series, NAV_Series, ChartStartDate, ChartEndDate, pLabelStyle, pZoomReset)

  End Function

  Public Function Set_DrawDownChart(ByVal pMainForm As CTAMain, _
   ByVal StatsDatePeriod As RenaissanceGlobals.DealingPeriod, _
   ByVal pChart As Chart, _
   ByVal pSeriesNumber As Integer, _
   ByVal pLegendText As String, _
   ByVal Date_Series() As Date, _
   ByVal NAV_Series() As Double, _
   ByVal ChartStartDate As Date, _
   ByVal ChartEndDate As Date, _
   ByVal pLabelStyle As String, _
   ByVal pZoomReset As Boolean) As Boolean

    ' ***********************************************************************
    ' Gets Pertrac Index data and sets the given series on the Drawdown chart
    ' -----------------------------------------------------------------------
    ' 
    ' Remove Point Label. NPP 22 Feb 2008
    '
    ' ***********************************************************************

    ' Build the Chart series.

    Dim DSeries As Dundas.Charting.WinControl.Series
    Dim RowCounter As Integer
    Dim WorstDrawDown As Double = 0
    Dim WorstDrawDownPoint As Integer

    Try
      If (pSeriesNumber < 0) OrElse (pSeriesNumber >= pChart.Series.Count) Then
        DSeries = pChart.Series.Add("PS" & pChart.Series.Count.ToString)
        DSeries.ChartArea = pChart.ChartAreas(0).Name
      Else
        DSeries = pChart.Series(pSeriesNumber)
        If (DSeries.ChartArea = "") Then DSeries.ChartArea = pChart.ChartAreas(0).Name
      End If

      DSeries.Points.Clear()

      If (Date_Series Is Nothing) OrElse (NAV_Series Is Nothing) OrElse (Date_Series.Length <= 0) Then
        ' Default Chart if no data.

        DSeries.Points.AddXY(ChartStartDate, 0)

      Else
        ' Draw chart Data.
        ' The chart data will already have been date trimmed by the StatFunction functions.

        Try
          Dim ThisDate As Date
          Dim ThisValue As Double
          Dim ThisIndex As Integer

          ' Now Build Chart series
          Dim HighNAV As Double = NAV_Series(0)
          Dim ThisNAV As Double

          For RowCounter = 0 To (NAV_Series.Length - 1)

            ThisDate = Date_Series(RowCounter)
            ThisNAV = NAV_Series(RowCounter)

            If (ThisNAV > HighNAV) Then
              HighNAV = ThisNAV
            End If

            If (ThisDate >= ChartStartDate) And (ThisDate <= ChartEndDate) Then

              ThisValue = (ThisNAV / HighNAV) - 1

              ' Add this datapoint.

              ThisIndex = DSeries.Points.AddXY(ThisDate, ThisValue * 100)

              ' Save Worst DrawDown point

              If (ThisValue < WorstDrawDown) Then
                WorstDrawDown = ThisValue
                WorstDrawDownPoint = ThisIndex
              End If

            End If

            If (ThisDate > ChartEndDate) Then
              Exit For
            End If
          Next
        Catch ex As Exception
        End Try

      End If

      ' Format series

      DSeries.Font = New Font("Arial", 8)

      Try
        DSeries.CustomAttributes = "LabelStyle=" & pLabelStyle & ",EmptyPointValue=Zero"
      Catch ex As Exception
        DSeries.CustomAttributes = "LabelStyle=TopLeft,EmptyPointValue=Zero"
      End Try

      DSeries.ChartType = "Line"
      DSeries.ShadowOffset = 1

      DSeries.LegendText = pLegendText

      ' Set Label Text

      If (DSeries.Points.Count > 0) Then
        '
        ' Remove Point Label. NPP 22 Feb 2008
        '

        'If (WorstDrawDown <= (-0.05)) AndAlso (pSeriesNumber < 5) Then
        '	DSeries.Points(WorstDrawDownPoint).Label = pLegendText
        'End If

        DSeries.ChartArea = pChart.ChartAreas(0).Name
      Else
        DSeries.ChartArea = ""
      End If
    Catch ex As Exception
    End Try

    pChart.ChartAreas(0).CursorX.UserEnabled = True
    pChart.ChartAreas(0).CursorX.UserSelection = True
    pChart.ChartAreas(0).AxisX.View.Zoomable = True
    pChart.ChartAreas(0).AxisX.IntervalType = DateTimeIntervalType.Auto

    pChart.ChartAreas(0).CursorY.UserEnabled = True
    pChart.ChartAreas(0).CursorY.UserSelection = True
    pChart.ChartAreas(0).AxisY.View.Zoomable = True
    pChart.ChartAreas(0).AxisY.IntervalType = DateTimeIntervalType.Auto

    If (pZoomReset) Then
      pChart.ChartAreas(0).AxisX.View.ZoomReset(0)
      pChart.ChartAreas(0).AxisY.View.ZoomReset(0)
    End If

    Return True
  End Function


#End Region


End Module
