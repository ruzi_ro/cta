Imports System.Data.SqlClient

Module ReturnsGridUtilities


	Friend Function Update_Grid_Percentages(ByRef UpdateGrid As C1.Win.C1FlexGrid.C1FlexGrid, ByRef pHoldingCol As Integer, ByRef pPercentagesCol As Integer, ByVal pFloorCol As Integer, ByVal pCapCol As Integer, ByVal pLowerLimitCol As Integer, ByVal pUpperLimitCol As Integer, ByVal UpdateCapsAndFloors As Boolean, Optional ByVal NoTitleRow As Boolean = False) As Double
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************

		Dim RowCounter As Integer
		Dim SumOfholding As Double = 0
		Dim StartRow As Integer = 1

		Try
			If (NoTitleRow) Then
				StartRow = 0
			End If

			For RowCounter = StartRow To (UpdateGrid.Rows.Count - 1)
				If IsNumeric(UpdateGrid.Item(RowCounter, pHoldingCol)) Then
					SumOfholding += CDbl(UpdateGrid.Item(RowCounter, pHoldingCol))
				End If
			Next

			For RowCounter = StartRow To (UpdateGrid.Rows.Count - 1)
				If IsNumeric(UpdateGrid.Item(RowCounter, pHoldingCol)) Then
					If (SumOfholding = 0) Then
						UpdateGrid.Item(RowCounter, pPercentagesCol) = 0

						If UpdateCapsAndFloors Then
							Try
								If CBool(UpdateGrid.Item(RowCounter, pCapCol)) AndAlso (CDbl(UpdateGrid.Item(RowCounter, pUpperLimitCol)) <> 0) Then
									UpdateGrid.Item(RowCounter, pUpperLimitCol) = 0
								End If
								If CBool(UpdateGrid.Item(RowCounter, pFloorCol)) AndAlso (CDbl(UpdateGrid.Item(RowCounter, pFloorCol)) <> 0) Then
									UpdateGrid.Item(RowCounter, pLowerLimitCol) = 0
								End If
							Catch ex As Exception
							End Try
						End If

					Else
						UpdateGrid.Item(RowCounter, pPercentagesCol) = CDbl(UpdateGrid.Item(RowCounter, pHoldingCol)) / SumOfholding

						If UpdateCapsAndFloors Then
							Try
								If CBool(UpdateGrid.Item(RowCounter, pCapCol)) AndAlso (CDbl(UpdateGrid.Item(RowCounter, pUpperLimitCol)) <> CDbl(UpdateGrid.Item(RowCounter, pPercentagesCol))) Then
									UpdateGrid.Item(RowCounter, pUpperLimitCol) = UpdateGrid.Item(RowCounter, pPercentagesCol)
								End If
								If CBool(UpdateGrid.Item(RowCounter, pFloorCol)) AndAlso (CDbl(UpdateGrid.Item(RowCounter, pLowerLimitCol)) <> CDbl(UpdateGrid.Item(RowCounter, pPercentagesCol))) Then
									UpdateGrid.Item(RowCounter, pLowerLimitCol) = UpdateGrid.Item(RowCounter, pPercentagesCol)
								End If
							Catch ex As Exception
							End Try
						End If
					End If
				End If
			Next

		Catch ex As Exception
		End Try

		Return SumOfholding

	End Function

	Friend Function GetAggregatedFundHolding(ByRef ThisConnection As SqlConnection, ByVal FundID As Integer, ByVal PertracID As Integer, ByVal pValueDate As Date, ByVal Knowledgedate As Date) As Double
		' *************************************************************************************
		'
		'
		'
		' Select Sum(TransactionSignedUnits * dbo.fn_BestSinglePrice(TransactionInstrument, null, Null, Null))
		' from(fn_TblTransaction_SelectKD(Null))
		' WHERE (TransactionFund = 1) AND (TransactionInstrument IN (SELECT DISTINCT InstrumentID From fn_tblInstrument_SelectKD(Null) WHERE InstrumentParent IN (SELECT InstrumentParent FROM fn_tblInstrument_SelectKD(Null) WHERE InstrumentPertracCode = 1327)))
		' *************************************************************************************

		Dim RVal As Double = 0
		Dim ThisCommand As New SqlCommand
		Dim ValueDate As Date = RenaissanceGlobals.Globals.Renaissance_EndDate_Data

		If (pValueDate > RenaissanceGlobals.Globals.Renaissance_BaseDate) Then
			' Date not 'Live'
			ValueDate = pValueDate
		End If

		Try
			ThisCommand.CommandType = CommandType.Text
			ThisCommand.CommandText = _
			 "Select Sum(TransactionSignedUnits * dbo.fn_BestSinglePrice(TransactionInstrument, Null, Null, @KnowledgeDate)) " & _
			 "from fn_TblTransaction_SelectKD(@KnowledgeDate) " & _
			 "WHERE (TransactionFund = @FundID) AND (TransactionValueDate <= @ValueDate) AND (TransactionInstrument IN (SELECT DISTINCT InstrumentID From fn_tblInstrument_SelectKD(@KnowledgeDate) WHERE InstrumentParent IN (SELECT InstrumentParent FROM fn_tblInstrument_SelectKD(@KnowledgeDate) WHERE InstrumentPertracCode = @PertracCode)))"
			ThisCommand.Parameters.Add(New SqlParameter("@FundID", SqlDbType.Int)).Value = FundID
			ThisCommand.Parameters.Add(New SqlParameter("@PertracCode", SqlDbType.Int)).Value = PertracID
			ThisCommand.Parameters.Add(New SqlParameter("@ValueDate", SqlDbType.DateTime)).Value = ValueDate
			ThisCommand.Parameters.Add(New SqlParameter("@KnowledgeDate", SqlDbType.DateTime)).Value = Knowledgedate ' MainForm.Main_Knowledgedate
			ThisCommand.Connection = ThisConnection

			SyncLock ThisCommand.Connection
				RVal = CDbl(Nz(ThisCommand.ExecuteScalar, 0))
			End SyncLock

		Catch ex As Exception
		Finally
			ThisCommand.Connection = Nothing
		End Try

		Return RVal

	End Function

	Friend Function GetAggregatedFundHolding(ByRef ThisConnection As SqlConnection, ByRef tblVeniceHoldings As DataTable, ByVal PertracID As Integer, ByVal Knowledgedate As Date) As Double
		' *************************************************************************************
		'
		'
		' *************************************************************************************

		Dim RVal As Double = 0
		Dim ThisCommand As New SqlCommand
		Dim tblParentIDs As New DataTable

		Try
			ThisCommand.CommandType = CommandType.Text
			ThisCommand.CommandText = _
			 "SELECT DISTINCT InstrumentParent FROM fn_tblInstrument_SelectKD(@KnowledgeDate) WHERE InstrumentPertracCode = @PertracCode "

			ThisCommand.Parameters.Add(New SqlParameter("@PertracCode", SqlDbType.Int)).Value = PertracID
			ThisCommand.Parameters.Add(New SqlParameter("@KnowledgeDate", SqlDbType.DateTime)).Value = Knowledgedate ' MainForm.Main_Knowledgedate
			ThisCommand.Connection = ThisConnection

			SyncLock ThisCommand.Connection
				tblParentIDs.Load(ThisCommand.ExecuteReader)
			End SyncLock

			If (tblParentIDs IsNot Nothing) AndAlso (tblParentIDs.Rows.Count > 0) Then
				Dim SelectedHoldings() As DataRow
				Dim SelectString As String = ""

				For Each ParentRow As DataRow In tblParentIDs.Rows
					If (SelectString.Length > 0) Then
						SelectString &= ","
					End If

					SelectString &= CStr(ParentRow("InstrumentParent"))
				Next

				SelectedHoldings = tblVeniceHoldings.Select("InstrumentID IN (" & SelectString & ")")

				If (SelectedHoldings IsNot Nothing) AndAlso (SelectedHoldings.Length > 0) Then
					For Each HoldingRow As DataRow In SelectedHoldings
						RVal += CDbl(HoldingRow("USDValue"))
					Next
				End If
			End If

		Catch ex As Exception
			RVal = 0
		Finally
			ThisCommand.Connection = Nothing
		End Try

		Return RVal

	End Function

	Friend Function GetVeniceHoldingsData(ByRef ThisConnection As SqlConnection, ByVal FundID As Integer, ByVal pValueDate As Date, ByVal pLookthrough As Boolean, ByVal Knowledgedate As Date) As DataTable
		' *************************************************************************************
		'
		' Retrieve current holdings (in USD) in the given Fund, aggregated to Parent ID
		' *************************************************************************************

		Dim RVal As New DataTable
		Dim ThisCommand As New SqlCommand
		Dim ValueDate As Date = RenaissanceGlobals.Globals.Renaissance_EndDate_Data

		Try
			If (pValueDate > RenaissanceGlobals.Globals.Renaissance_BaseDate) Then
				' Date not 'Live'
				ValueDate = pValueDate
			End If

			ThisCommand.CommandType = CommandType.Text
			ThisCommand.Connection = ThisConnection
			ThisCommand.Parameters.Add(New SqlParameter("@FundID", SqlDbType.Int)).Value = FundID
			ThisCommand.Parameters.Add(New SqlParameter("@ValueDate", SqlDbType.DateTime)).Value = ValueDate
			ThisCommand.Parameters.Add(New SqlParameter("@KnowledgeDate", SqlDbType.DateTime)).Value = Knowledgedate ' MainForm.Main_Knowledgedate
			ThisCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT

			If (pLookthrough) Then
				'FUNCTION dbo.fn_ProfitAndLossLookthrough
				'	(
				'	@FundID int,
				'	@ValueDateFrom datetime,
				'	@ValueDateTo datetime,
				'	@UnitPriceVariant int,
				'	@DisregardWatermarkPoints int,
				'    @MaintainConsistentFundPnLs int,
				'	@KnowledgeDateFrom datetime, 
				'	@KnowledgeDateTo datetime,
				'	@KnowledgeDateLinkedTables datetime, 
				'	@AggregateToInstrumentParentID int
				'	)

				ThisCommand.CommandText = "SELECT Fund as FundID, InstrumentParent AS InstrumentID, USDValue FROM dbo.fn_ProfitAndLossLookthrough(@FundID, @ValueDate, @ValueDate, Null, Null, Null, Null, 0, @KnowledgeDate, @KnowledgeDate, @KnowledgeDate, 1)"

			Else
				' NOT Lookthrough :-

				'FUNCTION dbo.fn_Valuation
				'(
				'	@FundID int,
				'	@LegalEntity varchar(100),
				'	@ValueDate datetime,
				'	@UnitPriceVariant int,
				'	@DisregardWatermarkPoints int,
				'	@AggregateToInstrumentParentID int,
				'	@ShowDetailedTransactions int,
				'	@DetailDate datetime, 
				'	@KnowledgeDate datetime
				')

				ThisCommand.CommandText = "SELECT Fund as FundID, InstrumentParent AS InstrumentID, USDValue FROM dbo.fn_valuation(@FundID, Null, @ValueDate, Null, Null, Null, Null, 1, 0, Null, @KnowledgeDate)"

			End If

			SyncLock ThisCommand.Connection
				RVal.Load(ThisCommand.ExecuteReader)
			End SyncLock

		Catch ex As Exception
			RVal = Nothing
		Finally
			ThisCommand.Connection = Nothing
		End Try

		Return RVal

	End Function


End Module
