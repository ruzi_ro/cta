'Imports System
'Imports System.Collections

'Module LookupCollectionModule

'  ' Collection to implement a dictionary object, sorted by Value
'  '
'  ' Acknowledgements to The Code Project and Foulds.Net
'  '
'  ' Adjustments made to the original code to Minimise the amount of array sorting and to 
'  ' Speed the retrieval of individual values by adding a parallel HashTable.
'  '
'  ' This does of course double the memory required, but it does improve performance significantly.
'  '
'  '
'  Public Class Lookup
'    Implements IComparable

'    Private mKey As Object
'    Private mValue As Object

'    ' ---------------------------------------------------------------------
'    '  Overloaded Constructor
'    ' ---------------------------------------------------------------------
'    Public Sub New()

'    End Sub

'    Public Sub New(ByVal key As Object, ByVal value As Object)
'      Me.Key = key
'      Me.Value = value
'    End Sub

'    ' ---------------------------------------------------------------------
'    '  CompareTo
'    ' ---------------------------------------------------------------------
'    Public Function CompareTo(ByVal obj As Object) As Integer Implements System.IComparable.CompareTo
'      Dim result As Integer
'      result = 0

'      If (TypeOf obj Is Lookup) Then
'        result = Me.Value.CompareTo(CType(obj, Lookup).Value)
'      End If

'      Return result

'    End Function

'    ' ---------------------------------------------------------------------
'    '  ToDictionaryEntry
'    ' ---------------------------------------------------------------------
'    Public Function ToDictionaryEntry() As DictionaryEntry
'      Return New DictionaryEntry(Me.Key, Me.Value)
'    End Function


'    ' =====================================================================
'    '  PROPERTIES
'    ' =====================================================================
'    Public Property Key() As Object
'      Get
'        Return Me.mKey
'      End Get
'      Set(ByVal Value As Object)
'        Me.mKey = Value
'      End Set
'    End Property

'    Public Property Value() As Object
'      Get
'        Return Me.mValue
'      End Get
'      Set(ByVal Value As Object)
'        Me.mValue = Value
'      End Set
'    End Property

'  End Class

'  Public Class LookupEnumerator
'    Implements IDictionaryEnumerator

'    Private index As Integer
'    Private items As ArrayList

'    ' ---------------------------------------------------------------------
'    '  Constructor
'    ' ---------------------------------------------------------------------

'    Public Sub New(ByVal list As ArrayList)
'      Me.index = (-1)
'      Me.items = list
'    End Sub


'    ' ---------------------------------------------------------------------
'    '  MoveNext
'    ' ---------------------------------------------------------------------

'    Public Function MoveNext() As Boolean Implements System.Collections.IEnumerator.MoveNext
'      Me.index += 1

'      If (Me.index >= Me.items.Count) Then
'        Return False
'      End If

'      Return True
'    End Function


'    ' =====================================================================
'    '  PROPERTIES
'    ' =====================================================================
'    ' ---------------------------------------------------------------------
'    '  Reset
'    ' ---------------------------------------------------------------------

'    Public Sub Reset() Implements System.Collections.IEnumerator.Reset
'      Me.index = -1

'    End Sub


'    ' ---------------------------------------------------------------------
'    '  Current
'    ' ---------------------------------------------------------------------

'    Public ReadOnly Property Current() As Object Implements System.Collections.IEnumerator.Current
'      Get
'        If (Me.index < 0 Or index >= Me.items.Count) Then
'          Throw New InvalidOperationException
'        End If

'        Return Me.items(index)

'      End Get
'    End Property


'    ' ---------------------------------------------------------------------
'    '  Entry
'    ' ---------------------------------------------------------------------

'    Public ReadOnly Property Entry() As DictionaryEntry Implements System.Collections.IDictionaryEnumerator.Entry
'      Get
'        Return CType(Me.Current, Lookup).ToDictionaryEntry
'      End Get
'    End Property


'    ' ---------------------------------------------------------------------
'    '  Key
'    ' ---------------------------------------------------------------------

'    Public ReadOnly Property Key() As Object Implements System.Collections.IDictionaryEnumerator.Key
'      Get
'        Return Me.Entry.Key
'      End Get
'    End Property


'    ' ---------------------------------------------------------------------
'    '  Value
'    ' ---------------------------------------------------------------------
'    Public ReadOnly Property Value() As Object Implements System.Collections.IDictionaryEnumerator.Value
'      Get
'        Return Me.Entry.Value
'      End Get
'    End Property

'  End Class

'  Public Class LookupCollection
'    Implements ICollection, IDictionary, IEnumerable

'    Private mItems As ArrayList
'    Private hItems As Hashtable

'    Private SortFlag As Boolean
'    Private thisKeys As ArrayList

'    ' ---------------------------------------------------------------------
'    '  Constructor
'    ' ---------------------------------------------------------------------
'    Public Sub New()
'      mItems = New ArrayList
'      hItems = New Hashtable

'      SortFlag = True
'      thisKeys = Nothing

'    End Sub


'    ' ---------------------------------------------------------------------
'    '  Add
'    ' ---------------------------------------------------------------------
'    Public Sub Add(ByVal key As Object, ByVal value As Object) Implements System.Collections.IDictionary.Add

'      '  do some validation
'      If (key Is Nothing) Then
'        Throw New ArgumentNullException("key is a null reference")
'      ElseIf (Me.Contains(key)) Then
'        Throw New ArgumentException("An element with the same key already exists")
'      End If

'      '  add the new item
'      Dim newItem As Lookup = New Lookup

'      newItem.Key = key
'      newItem.Value = value

'      thisKeys = Nothing
'      If Not hItems.Contains(key) Then
'        Me.mItems.Add(newItem)
'        Me.hItems.Add(key, value)
'      End If
'      SortFlag = False

'      ' Me.mItems.Sort()

'    End Sub

'    Public Sub Sort()
'      Me.mItems.Sort()
'      Me.SortFlag = True
'      thisKeys = Nothing
'    End Sub

'    ' ---------------------------------------------------------------------
'    '  Clear
'    ' ---------------------------------------------------------------------
'    Public Sub Clear() Implements System.Collections.IDictionary.Clear
'      Me.mItems.Clear()
'      Me.mItems.Clear()
'      thisKeys = Nothing
'    End Sub


'    ' ---------------------------------------------------------------------
'    '  Contains
'    ' ---------------------------------------------------------------------
'    Public Function Contains(ByVal key As Object) As Boolean Implements System.Collections.IDictionary.Contains
'      'Return (Not (Me.GetByKey(key) Is Nothing))

'      Return Me.mItems.Contains(key)
'    End Function


'    ' ---------------------------------------------------------------------
'    '  CopyTo
'    ' ---------------------------------------------------------------------
'    Public Sub CopyTo(ByVal array As System.Array, ByVal index As Integer) Implements System.Collections.ICollection.CopyTo
'      If (SortFlag = False) Then
'        Me.Sort()
'        SortFlag = True
'      End If

'      Me.mItems.CopyTo(array, index)
'    End Sub


'    ' ---------------------------------------------------------------------
'    '  GetEnumerator (1)
'    ' ---------------------------------------------------------------------

'    Public Function GetEnumerator() As System.Collections.IDictionaryEnumerator Implements System.Collections.IDictionary.GetEnumerator
'      If (SortFlag = False) Then
'        Me.Sort()
'        SortFlag = True
'      End If

'      Return New LookupEnumerator(Me.mItems)
'    End Function


'    ' ---------------------------------------------------------------------
'    '  GetEnumerator (2)
'    ' ---------------------------------------------------------------------

'    Public Function GetEnumerator1() As System.Collections.IEnumerator Implements System.Collections.IEnumerable.GetEnumerator
'      If (SortFlag = False) Then
'        Me.Sort()
'        SortFlag = True
'      End If

'      Return New LookupEnumerator(Me.mItems)
'    End Function

'    ' ---------------------------------------------------------------------
'    '  Remove
'    ' ---------------------------------------------------------------------
'    Public Sub Remove(ByVal key As Object) Implements System.Collections.IDictionary.Remove

'      If (key Is Nothing) Then
'        Throw New ArgumentNullException("key is a null reference")
'      End If

'      Dim deleteItem As Lookup = Me.GetByKey(key)

'      If (Not (deleteItem Is Nothing)) Then
'        Me.mItems.Remove(deleteItem)
'        Me.hItems.Remove(key)

'        thisKeys = Nothing

'        SortFlag = False
'        '        Me.mItems.Sort()
'      End If

'    End Sub


'    ' =====================================================================
'    '  PRIVATE
'    ' =====================================================================
'    Private Function GetByKey(ByVal Key As Object) As Lookup
'      Dim result As Lookup
'      Dim keyIndex As Integer
'      Dim keys As ArrayList = Me.Keys
'      Dim Value As Object

'      keyIndex = (-1)
'      result = Nothing
'      'If (SortFlag = False) Then
'      '  Me.Sort()
'      'End If


'      ' mItems
'      If (Me.hItems.Count > 0) Then
'        Value = Me.hItems.Item(Key)
'        If (Not (Value Is Nothing)) Then
'          result = New Lookup(Key, Value)
'        End If
'      End If

'      'If (Me.mItems.Count > 0) Then
'      '  keyIndex = keys.IndexOf(Key)

'      '  If (keyIndex >= 0) Then
'      '    result = CType(Me.mItems(keyIndex), Lookup)
'      '  End If
'      'End If

'      Return (result)
'    End Function


'    ' =====================================================================
'    '  PROPERTIES
'    ' =====================================================================

'    Public ReadOnly Property Count() As Integer Implements System.Collections.ICollection.Count
'      Get
'        Return Me.mItems.Count
'      End Get
'    End Property

'    Public ReadOnly Property IsSynchronized() As Boolean Implements System.Collections.ICollection.IsSynchronized
'      Get
'        Return False
'      End Get
'    End Property

'    Public ReadOnly Property SyncRoot() As Object Implements System.Collections.ICollection.SyncRoot
'      Get
'        Return Me
'      End Get
'    End Property


'    Public ReadOnly Property IsFixedSize() As Boolean Implements System.Collections.IDictionary.IsFixedSize
'      Get
'        Return False
'      End Get
'    End Property

'    Public ReadOnly Property IsReadOnly() As Boolean Implements System.Collections.IDictionary.IsReadOnly
'      Get
'        Return False
'      End Get
'    End Property

'    Default Public Property Item(ByVal key As Object) As Object Implements System.Collections.IDictionary.Item
'      Get

'        'If (key Is Nothing) Then
'        '  Throw New ArgumentNullException("key is a null reference")
'        'End If

'        Return Me.hItems.Item(key)

'        'If (SortFlag = False) Then
'        '  Me.Sort()
'        '  SortFlag = True
'        'End If

'        'Dim result As Object
'        'result = Nothing

'        'Dim findItem As Lookup
'        'findItem = Me.GetByKey(key)

'        'If (Not (findItem Is Nothing)) Then
'        '  result = findItem.Value
'        'End If

'        'Return result

'      End Get
'      Set(ByVal Value As Object)

'      End Set
'    End Property


'    Public ReadOnly Property Keys() As System.Collections.ICollection Implements System.Collections.IDictionary.Keys
'      Get

'        If (Not (thisKeys Is Nothing)) Then
'          Return thisKeys
'        End If

'        Dim curItem As Lookup
'        thisKeys = New ArrayList

'        For Each curItem In Me.mItems
'          thisKeys.Add(curItem.Key)
'        Next

'        Return thisKeys

'      End Get
'    End Property

'    Public ReadOnly Property Values() As System.Collections.ICollection Implements System.Collections.IDictionary.Values
'      Get
'        Dim result As ArrayList
'        result = New ArrayList

'        Dim curItem As Lookup

'        If (SortFlag = False) Then
'          Me.Sort()
'          SortFlag = True
'        End If

'        For Each curItem In Me.mItems
'          result.Add(curItem.Value)
'        Next

'        Return result

'      End Get
'    End Property

'  End Class


'	Public Class CustomC1Combo
'		Inherits ComboBox
'		Implements C1.Win.C1FlexGrid.IC1EmbeddedEditor

'		Protected Overrides Sub RefreshItem(ByVal index As Integer)
'			MyBase.RefreshItem(index)
'		End Sub

'		Protected Overrides Sub SetItemsCore(ByVal items As System.Collections.IList)
'			MyBase.SetItemsCore(items)
'		End Sub

'		Public Function C1EditorFormat(ByVal value As Object, ByVal mask As String) As String Implements C1.Win.C1FlexGrid.IC1EmbeddedEditor.C1EditorFormat
'			Return value.ToString
'		End Function

'		Public Function C1EditorGetStyle() As System.Drawing.Design.UITypeEditorEditStyle Implements C1.Win.C1FlexGrid.IC1EmbeddedEditor.C1EditorGetStyle
'			Return System.Drawing.Design.UITypeEditorEditStyle.DropDown
'		End Function

'		Public Function C1EditorGetValue() As Object Implements C1.Win.C1FlexGrid.IC1EmbeddedEditor.C1EditorGetValue
'			Return Me.SelectedValue
'		End Function

'		Public Sub C1EditorInitialize(ByVal value As Object, ByVal editorAttributes As System.Collections.IDictionary) Implements C1.Win.C1FlexGrid.IC1EmbeddedEditor.C1EditorInitialize
'      Try
'        If (value IsNot Nothing) Then
'          Me.SelectedValue = value
'        End If
'      Catch ex As Exception
'      End Try
'		End Sub

'    Public Function C1EditorKeyDownFinishEdit(ByVal e As System.Windows.Forms.KeyEventArgs) As Boolean Implements C1.Win.C1FlexGrid.IC1EmbeddedEditor.C1EditorKeyDownFinishEdit

'      Select Case e.KeyCode
'        Case Keys.Enter, Keys.Tab
'          Return True
'      End Select

'      Return False
'    End Function

'		Public Sub C1EditorUpdateBounds(ByVal rc As System.Drawing.Rectangle) Implements C1.Win.C1FlexGrid.IC1EmbeddedEditor.C1EditorUpdateBounds
'			MyBase.FontHeight = rc.Height

'			Bounds = rc
'		End Sub

'		Public Function C1EditorValueIsValid() As Boolean Implements C1.Win.C1FlexGrid.IC1EmbeddedEditor.C1EditorValueIsValid
'			Return True
'		End Function
'	End Class


'End Module
