if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[TmpTblPertracCustomFieldData]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[TmpTblPertracCustomFieldData]

GO

SELECT *
INTO TmpTblPertracCustomFieldData
FROM fn_tblPertracCustomFieldData_SelectKD(Null)

GO 

INSERT INTO TmpTblPertracCustomFieldData
SELECT * 
FROM tblPertracCustomFieldData
WHERE CustomFieldID = 11

GO

TRUNCATE TABLE tblPertracCustomFieldData

GO 

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[tblPertracCustomFieldData]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[tblPertracCustomFieldData]

GO 

CREATE TABLE [dbo].[tblPertracCustomFieldData] (
	[RN] [int] IDENTITY (1, 1) NOT NULL ,
	[AuditID] AS ([FieldDataID]) ,
	[FieldDataID] [int] NOT NULL ,
	[CustomFieldID] [int] NOT NULL ,
	[PertracID] [int] NOT NULL ,
	[GroupID] [int] NOT NULL ,
	[FieldNumericData] [float] NOT NULL ,
	[FieldTextData] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL ,
	[FieldDateData] [datetime] NOT NULL ,
	[FieldBooleanData] [bit] NOT NULL ,
	[UpdateRequired] [bit] NOT NULL ,
	[LatestReturnDate] [datetime] NULL ,
	[UserEntered] [varchar] (20) COLLATE Latin1_General_CI_AS NOT NULL ,
	[DateEntered] [datetime] NOT NULL ,
	[DateDeleted] [datetime] NULL 
) ON [PRIMARY]

GO

INSERT INTO tblPertracCustomFieldData(
	FieldDataID ,
	CustomFieldID,
	PertracID,
	GroupID,
	FieldNumericData,
	FieldTextData,
	FieldDateData, 
	FieldBooleanData, 
	UpdateRequired,
	LatestReturnDate, 
	UserEntered, 
	DateEntered,
	DateDeleted
	)
SELECT 
	FieldDataID ,
	CustomFieldID,
	PertracID,
	GroupID,
	FieldNumericData,
	FieldTextData,
	FieldDateData, 
	FieldBooleanData, 
	UpdateRequired,
	LatestReturnDate, 
	UserEntered, 
	DateEntered,
	DateDeleted
FROM TmpTblPertracCustomFieldData

GO

ALTER TABLE [dbo].[tblPertracCustomFieldData] WITH NOCHECK ADD 
	CONSTRAINT [PK_tblPertracCustomFieldData] PRIMARY KEY  CLUSTERED 
	(
		[RN]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[tblPertracCustomFieldData] ADD 
	CONSTRAINT [DF_tblPertracCustomFieldData_GroupID] DEFAULT (0) FOR [GroupID],
	CONSTRAINT [DF_tblPertracCustomFieldData_FieldNumericData] DEFAULT (0) FOR [FieldNumericData],
	CONSTRAINT [DF_tblPertracCustomFieldData_FieldTextData] DEFAULT ('') FOR [FieldTextData],
	CONSTRAINT [DF_tblPertracCustomFieldData_FieldDateData] DEFAULT ('1/1/1900') FOR [FieldDateData],
	CONSTRAINT [DF_tblPertracCustomFieldData_FieldBooleandData] DEFAULT (0) FOR [FieldBooleanData],
	CONSTRAINT [DF_tblPertracCustomFieldData_UpdateRequired] DEFAULT (0) FOR [UpdateRequired],
	CONSTRAINT [DF_tblPertracCustomFieldData_UserEntered] DEFAULT (user_name()) FOR [UserEntered],
	CONSTRAINT [DF_tblPertracCustomFieldData_DateEntered] DEFAULT (getdate()) FOR [DateEntered]
GO

 CREATE  INDEX [IX_tblPertracCustomFieldData] ON [dbo].[tblPertracCustomFieldData]([FieldDataID]) ON [PRIMARY]
GO

 CREATE  INDEX [IX_tblPertracCustomFieldData_1] ON [dbo].[tblPertracCustomFieldData]([CustomFieldID], [PertracID], [GroupID]) ON [PRIMARY]
GO

 CREATE  INDEX [IX_tblPertracCustomFieldData_2] ON [dbo].[tblPertracCustomFieldData]([GroupID], [DateEntered], [DateDeleted]) ON [PRIMARY]
GO

GRANT  SELECT  ON [dbo].[tblPertracCustomFieldData]  TO [InvestMaster_Sales]
GO

GRANT  SELECT  ON [dbo].[tblPertracCustomFieldData]  TO [Genoa_Admin]
GO

GRANT  SELECT  ON [dbo].[tblPertracCustomFieldData]  TO [Genoa_Manager]
GO

GRANT  SELECT  ON [dbo].[tblPertracCustomFieldData]  TO [Genoa_Read]
GO

TRUNCATE TABLE TmpTblPertracCustomFieldData

GO


DROP TABLE TmpTblPertracCustomFieldData

GO 

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[fn_tblPertracCustomFieldDataByGroup_SelectKD]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[fn_tblPertracCustomFieldDataByGroup_SelectKD]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[fn_tblPertracCustomFieldData_CacheSelect]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[fn_tblPertracCustomFieldData_CacheSelect]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[fn_tblPertracCustomFieldData_CacheSelectGroupBackfill]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[fn_tblPertracCustomFieldData_CacheSelectGroupBackfill]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[fn_tblPertracCustomFieldData_SelectKD]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[fn_tblPertracCustomFieldData_SelectKD]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[fn_tblPertracCustomFieldData_SinglePertracID]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[fn_tblPertracCustomFieldData_SinglePertracID]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[fn_tblPertracCustomFieldData_Status]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[fn_tblPertracCustomFieldData_Status]
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

CREATE FUNCTION dbo.fn_tblPertracCustomFieldDataByGroup_SelectKD
	(
	@GroupID int, 
	@CustomFieldID int, 
	@KnowledgeDate datetime
	)
RETURNS 
	@RVal_DataFields TABLE 
		(
		[RN] [int] ,
		[AuditID] [int]  ,
		[FieldDataID] [int] ,
		[CustomFieldID] [int] ,
		[PertracID] [int] ,
		[GroupID] [int] ,
		[FieldNumericData] [float] ,
		[FieldTextData] [varchar] (50),
		[FieldDateData] [datetime], 
		[FieldBooleanData] [bit], 
		[UpdateRequired] [bit] ,
		[LatestReturnDate] [datetime] ,
		[UserEntered] [varchar] (20),
		[DateEntered] [datetime] ,
		[DateDeleted] [datetime] ,
		PRIMARY KEY (CustomFieldID, PertracID)
		)
AS
BEGIN

	DECLARE @KDLive as bit
	
	SELECT @GroupID = ISNULL(@GroupID,0) 
	SELECT @CustomFieldID = ISNULL(@CustomFieldID,0) 
	SELECT @KnowledgeDate = ISNULL(@KnowledgeDate, '1 Jan 1900')

	IF @KnowledgeDate <= '1 Jan 1900'
		SELECT @KDLive = 1
	ELSE
		SELECT @KDLive = 0

	IF (@GroupID = 0)
	BEGIN
		INSERT INTO @RVal_DataFields
						(
					 	RN ,
						AuditID ,
						FieldDataID ,
						CustomFieldID,
						PertracID,
						GroupID,
						FieldNumericData,
						FieldTextData,
						FieldDateData, 
						FieldBooleanData, 
						UpdateRequired,
						LatestReturnDate, 
						UserEntered, 
						DateEntered,
						DateDeleted
						)
		SELECT 	RN ,
						AuditID ,
						FieldDataID ,
						CustomFieldID,
						PertracID,
						GroupID,
						FieldNumericData,
						FieldTextData,
						FieldDateData, 
						FieldBooleanData, 
						UpdateRequired,
						LatestReturnDate, 
						UserEntered, 
						DateEntered,
						DateDeleted
		FROM tblPertracCustomFieldData
		WHERE (GroupID = 0) AND 
					((@CustomFieldID = 0) OR (@CustomFieldID = CustomFieldID))
		
		RETURN
	END
	

	DECLARE @DataKeys TABLE
		(
		[CustomFieldID] [int] ,
		[PertracID] [int] ,
		[GroupID] [int] ,
		[RN] [int] 
		)

	DECLARE @BestDataKeys TABLE
		(
		[CustomFieldID] [int] ,
		[PertracID] [int] ,
		[GroupID] [int] 
		)

	DECLARE @GroupPertracIDs TABLE
		(
		[PertracID] [int] PRIMARY KEY
		)

	DECLARE @SelectedRNs TABLE
		(
		[RN] [int] PRIMARY KEY
		)
	
	-- Get Instrument IDs associated with the chosen group.
	
	INSERT INTO @GroupPertracIDs(PertracID)
	SELECT DISTINCT GroupPertracCode
	FROM fn_tblGroupItems_SelectKD(@KnowledgeDate)
	WHERE GroupID = @GroupID

	-- Get the 'Best' RN for each Data Item matching the given Group, or Group Zero.	
	
	INSERT INTO @DataKeys(CustomFieldID, PertracID, GroupID, RN)
	SELECT CustomFieldID, PertracID, GroupID, MAX(RN)
	FROM tblPertracCustomFieldData
  WHERE ((GroupID = 0) OR (GroupID = @GroupID)) AND 
				(PertracID IN (SELECT PertracID FROM @GroupPertracIDs)) AND 
				((@CustomFieldID = 0) OR (@CustomFieldID = CustomFieldID)) AND 
				((DateEntered <= @KnowledgeDate) OR (@KDLive = 1)) AND 
        (((DateDeleted > @KnowledgeDate) OR (DateDeleted IS NULL)) AND (NOT ((@KDLive = 1) AND (DateDeleted IS NOT NULL))))
	GROUP BY CustomFieldID, PertracID, GroupID

--	INSERT INTO @DataKeys(CustomFieldID, PertracID, GroupID, RN)
--	SELECT CustomFieldID, PertracID, GroupID, MAX(RN)
--	FROM tblPertracCustomFieldData
--  WHERE (GroupID IN (0, ISNULL(@GroupID, 0))) AND 
--				(PertracID IN (SELECT PertracID FROM @GroupPertracIDs)) AND 
--				(ISNULL(@CustomFieldID, 0) IN (0, CustomFieldID)) AND 
--				(((DateEntered <= @KnowledgeDate) or (DateEntered IS NULL)) OR (ISNULL(@KnowledgeDate, '1 Jan 1900') <= '1 Jan 1900')) AND 
--	      (((DateDeleted > @KnowledgeDate) or (DateDeleted IS NULL)) AND (NOT ((ISNULL(@KnowledgeDate, '1 Jan 1900') <= '1 Jan 1900') AND (NOT (DateDeleted is NULL)))))
--	GROUP BY CustomFieldID, PertracID, GroupID

	-- Get the 'Best' CustomFieldID, PertracID, GroupID combination.
	-- Gives preference to higher GroupIDs
	
	INSERT INTO @BestDataKeys(CustomFieldID, PertracID, GroupID)
	SELECT CustomFieldID, PertracID, MAX(GroupID)
	FROM @DataKeys
	GROUP BY CustomFieldID, PertracID

	-- Link that selection back to the @BestDataKeys table to get the RN for the 'Best' Data Row.
	
	INSERT INTO @SelectedRNs(RN)
	SELECT DISTINCT DataKeys.RN
	FROM 
		@BestDataKeys BestKeys LEFT JOIN
		@DataKeys DataKeys ON (DataKeys.CustomFieldID = BestKeys.CustomFieldID) AND (DataKeys.PertracID = BestKeys.PertracID) AND (DataKeys.GroupID = BestKeys.GroupID)
		
	-- Link Best RN selection back to 'tblPertracCustomFieldData' to return the Best Data
	-- Rows.
		
	INSERT INTO @RVal_DataFields
					(
				 	RN ,
					AuditID ,
					FieldDataID ,
					CustomFieldID,
					PertracID,
					GroupID,
					FieldNumericData,
					FieldTextData,
					FieldDateData, 
					FieldBooleanData, 
					UpdateRequired,
					LatestReturnDate, 
					UserEntered, 
					DateEntered,
					DateDeleted
					)
	SELECT 	Data.RN ,
					Data.AuditID ,
					Data.FieldDataID ,
					Data.CustomFieldID,
					Data.PertracID,
					Data.GroupID,
					Data.FieldNumericData,
					Data.FieldTextData,
					Data.FieldDateData, 
					Data.FieldBooleanData, 
					Data.UpdateRequired,
					Data.LatestReturnDate, 
					Data.UserEntered, 
					Data.DateEntered,
					Data.DateDeleted
	FROM	@SelectedRNs SelectedRNs LEFT JOIN 
				tblPertracCustomFieldData Data ON (SelectedRNs.RN = Data.RN)
					
RETURN
END



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  SELECT  ON [dbo].[fn_tblPertracCustomFieldDataByGroup_SelectKD]  TO [InvestMaster_Sales]
GO

GRANT  SELECT  ON [dbo].[fn_tblPertracCustomFieldDataByGroup_SelectKD]  TO [Genoa_Admin]
GO

GRANT  SELECT  ON [dbo].[fn_tblPertracCustomFieldDataByGroup_SelectKD]  TO [Genoa_Manager]
GO

GRANT  SELECT  ON [dbo].[fn_tblPertracCustomFieldDataByGroup_SelectKD]  TO [Genoa_Read]
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE FUNCTION dbo.fn_tblPertracCustomFieldData_CacheSelect
	(
	@GroupID int, 
	@CustomFieldID int, 
	@KnowledgeDate datetime
	)
RETURNS TABLE
AS
	--
	-- Function to return Custon Field Data relating to the given Group.
	-- Note, this function will NOT backfill data for Non-Zero Group IDs with Data from Group Zero.
	--
	-- NPP, Apr 2008.
	-- NPP, Oct 2008.
	--
	RETURN 

	SELECT 	CustomFieldID,
					PertracID,
					GroupID,
					FieldNumericData,
					FieldTextData,
					FieldDateData, 
					FieldBooleanData
	FROM tblPertracCustomFieldData
	WHERE (GroupID = @GroupID) AND 
				((@CustomFieldID = 0) OR (CustomFieldID = @CustomFieldID)) 



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  SELECT  ON [dbo].[fn_tblPertracCustomFieldData_CacheSelect]  TO [InvestMaster_Sales]
GO

GRANT  SELECT  ON [dbo].[fn_tblPertracCustomFieldData_CacheSelect]  TO [Genoa_Admin]
GO

GRANT  SELECT  ON [dbo].[fn_tblPertracCustomFieldData_CacheSelect]  TO [Genoa_Manager]
GO

GRANT  SELECT  ON [dbo].[fn_tblPertracCustomFieldData_CacheSelect]  TO [Genoa_Read]
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO



CREATE FUNCTION dbo.fn_tblPertracCustomFieldData_CacheSelectGroupBackfill
	(
	@GroupID int, 
	@CustomFieldID int, 
	@KnowledgeDate datetime
	)
RETURNS TABLE
AS
	--
	-- Function to return Custom Field Data relating to the given Group.
	-- Note, this function WILL return data for Group Zero AND the Requested Group.
	-- Note also that @KnowledgeDate MUST NOT BE NULL.
	--
	-- NPP, Apr 2008.
	--
	RETURN 

	SELECT 	CustomFieldID,
					PertracID,
					GroupID,
					FieldNumericData,
					FieldTextData,
					FieldDateData, 
					FieldBooleanData
	FROM tblPertracCustomFieldData
	WHERE ((GroupID = 0) OR (GroupID = @GroupID)) AND 
				((@CustomFieldID = 0) OR (CustomFieldID = @CustomFieldID)) AND 
				(PertracID IN (SELECT GroupPertracCode FROM fn_tblGroupItems_SelectKD(@KnowledgeDate) WHERE GroupID = @GroupID))



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  SELECT  ON [dbo].[fn_tblPertracCustomFieldData_CacheSelectGroupBackfill]  TO [InvestMaster_Sales]
GO

GRANT  SELECT  ON [dbo].[fn_tblPertracCustomFieldData_CacheSelectGroupBackfill]  TO [Genoa_Admin]
GO

GRANT  SELECT  ON [dbo].[fn_tblPertracCustomFieldData_CacheSelectGroupBackfill]  TO [Genoa_Manager]
GO

GRANT  SELECT  ON [dbo].[fn_tblPertracCustomFieldData_CacheSelectGroupBackfill]  TO [Genoa_Read]
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

CREATE FUNCTION dbo.fn_tblPertracCustomFieldData_SelectKD
(
@KnowledgeDate datetime
)
RETURNS @tblPertracCustomFieldData TABLE 
	(
	RN int,
	AuditID int ,
	FieldDataID int,
	CustomFieldID int,
	PertracID int,
	GroupID int,
	FieldNumericData float,
	FieldTextData varchar (50),
	FieldDateData datetime,
	FieldBooleanData bit,
	UpdateRequired bit,
	LatestReturnDate datetime,
	UserEntered varchar (20),
	DateEntered datetime,
	DateDeleted datetime, 
	PRIMARY KEY (CustomFieldID, PertracID)
	)
AS
	--
	-- No longer Knowledgedated as of Oct 2008. The number of Pertrac instruments has made this impractical.
	--
	BEGIN

		INSERT INTO @tblPertracCustomFieldData
						(
					 	RN ,
						AuditID ,
						FieldDataID ,
						CustomFieldID,
						PertracID,
						GroupID,
						FieldNumericData,
						FieldTextData,
						FieldDateData, 
						FieldBooleanData, 
						UpdateRequired,
						LatestReturnDate, 
						UserEntered, 
						DateEntered,
						DateDeleted
						)
		SELECT 	RN ,
						AuditID ,
						FieldDataID ,
						CustomFieldID,
						PertracID,
						GroupID,
						FieldNumericData,
						FieldTextData,
						FieldDateData, 
						FieldBooleanData, 
						UpdateRequired,
						LatestReturnDate, 
						UserEntered, 
						DateEntered,
						DateDeleted
		FROM tblPertracCustomFieldData
		WHERE (GroupID = 0) 
				
	RETURN
	END



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  SELECT  ON [dbo].[fn_tblPertracCustomFieldData_SelectKD]  TO [InvestMaster_Sales]
GO

GRANT  SELECT  ON [dbo].[fn_tblPertracCustomFieldData_SelectKD]  TO [Genoa_Admin]
GO

GRANT  SELECT  ON [dbo].[fn_tblPertracCustomFieldData_SelectKD]  TO [Genoa_Manager]
GO

GRANT  SELECT  ON [dbo].[fn_tblPertracCustomFieldData_SelectKD]  TO [Genoa_Read]
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO



CREATE FUNCTION dbo.fn_tblPertracCustomFieldData_SinglePertracID
(
@PertracID int, 
@KnowledgeDate datetime
)
RETURNS @tblPertracCustomFieldData TABLE 
	(
	RN int,
	AuditID int ,
	FieldDataID int,
	CustomFieldID int,
	PertracID int,
	GroupID int,
	FieldNumericData float,
	FieldTextData varchar (50),
	FieldDateData datetime,
	FieldBooleanData bit,
	UpdateRequired bit,
	LatestReturnDate datetime,
	UserEntered varchar (20),
	DateEntered datetime,
	DateDeleted datetime, 
	PRIMARY KEY (CustomFieldID, PertracID)
	)
AS
	BEGIN
		
		INSERT INTO @tblPertracCustomFieldData
						(
					 	RN ,
						AuditID ,
						FieldDataID ,
						CustomFieldID,
						PertracID,
						GroupID,
						FieldNumericData,
						FieldTextData,
						FieldDateData, 
						FieldBooleanData, 
						UpdateRequired,
						LatestReturnDate, 
						UserEntered, 
						DateEntered,
						DateDeleted
						)
		SELECT 	RN ,
						AuditID ,
						FieldDataID ,
						CustomFieldID,
						PertracID,
						GroupID,
						FieldNumericData,
						FieldTextData,
						FieldDateData, 
						FieldBooleanData, 
						UpdateRequired,
						LatestReturnDate, 
						UserEntered, 
						DateEntered,
						DateDeleted
		FROM tblPertracCustomFieldData
		WHERE (PertracID = @PertracID) AND (GroupID = 0) 

	-- Updated NPP 20 Oct 2008.
		
	RETURN
	END



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  SELECT  ON [dbo].[fn_tblPertracCustomFieldData_SinglePertracID]  TO [InvestMaster_Sales]
GO

GRANT  SELECT  ON [dbo].[fn_tblPertracCustomFieldData_SinglePertracID]  TO [Genoa_Admin]
GO

GRANT  SELECT  ON [dbo].[fn_tblPertracCustomFieldData_SinglePertracID]  TO [Genoa_Manager]
GO

GRANT  SELECT  ON [dbo].[fn_tblPertracCustomFieldData_SinglePertracID]  TO [Genoa_Read]
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

CREATE FUNCTION dbo.fn_tblPertracCustomFieldData_Status
	(
	@GroupID int, 
	@CustomFieldID int , 
	@PertracID int , 
	@KnowledgeDate datetime
	)
/*
	This function is designed to give the Updated status of any or all Custom Field
	data items.
	It is designed to return the status on the COMPLETE potential set of custom
	data items, whether or not a data item exists.
	If a data item is not present, then clearly the UpdateRequired status is True (1),
	If a Field is not volatile and a Data item exists, then UpdateRequired = False(0).
	If a new return has been entered (within the Custom Field Date range), or a data 
	return has been updated since the Data Item was entered then UpdateRequired status 
	is True (1).
	
	This function relies heavily on dbo.fn_CustomFieldUpdateRequired() which actually
	defines the True and False UpdateRequired conditions.

	NPP - 30 Apr 2007.
*/
RETURNS 
	@DataStatus 
	TABLE 
		(
		[FieldDataID] [int] ,
		[CustomFieldID] [int] ,
		[PertracID] [int] ,
		[GroupID] [int] ,
		[UpdateRequired] [bit] ,
		[FieldIsCalculated] [bit], 
		[LatestReturnDate] [datetime] ,
		[DateEntered] [datetime], 
		PRIMARY KEY ([CustomFieldID], [PertracID])
		)
AS
	BEGIN
	
	SELECT @PertracID = ISNULL(@PertracID, 0)
	SELECT @CustomFieldID = ISNULL(@CustomFieldID, 0)
	
	DECLARE @TmpPerformance TABLE
		(
		[ID] [int] ,
		[Date] [datetime] ,
		[Return] [float]  ,
		[NAV] [float]  ,
		[LastUpdated] [datetime], 
		PRIMARY KEY ([ID], [Date])
		)
		
	DECLARE @CustomDataSet TABLE
		(
		[CustomFieldID] [int] ,
		[PertracID] [int] ,
		[GroupID] [int] ,
		[FieldIsVolatile] bit, 
		[FieldIsCalculated] bit, 
		[CF_LastUpdated] [datetime], 
		PRIMARY KEY ([CustomFieldID], [PertracID])
		)

	DECLARE @PertracUpdateTimes TABLE
		(
		[CustomFieldID] [int] ,
		[PertracID] [int] ,
		[ReturnDate] [datetime] ,
		[LastUpdated] [datetime] , 
		PRIMARY KEY ([CustomFieldID], [PertracID])
		)
	
	-- 

	IF (@PertracID > 0)
	BEGIN
	
		IF (@PertracID >= 1048576)
		BEGIN
			INSERT @TmpPerformance([ID], [Date], [Return], [NAV], [LastUpdated])
			SELECT @PertracID, [ReturnDate], [ReturnPercent], [NAV], [DateEntered]
			FROM [MASTERSQL].[dbo].[tblInstrumentPerformance] Performance
			WHERE [InstrumentID] = (@PertracID - 1048576)
		END
		ELSE
		BEGIN
			INSERT @TmpPerformance([ID], [Date], [Return], [NAV], [LastUpdated])
			SELECT [ID], [Date], [Return], [NAV], [LastUpdated]
			FROM [MASTERSQL].[dbo].[Performance] Performance
			WHERE [ID] = @PertracID
		END
		
		INSERT @PertracUpdateTimes(CustomFieldID, PertracID, ReturnDate, LastUpdated)
		SELECT CF.FieldID, [Performance].[ID], MAX([Performance].[Date]), MAX([Performance].[LastUpdated]) 
		FROM
		fn_tblPertracCustomFields_SelectKD(@KnowledgeDate) CF CROSS JOIN
		@TmpPerformance Performance
		WHERE ((@CustomFieldID = 0) OR (@CustomFieldID = CF.FieldID)) AND 
				(CF.FieldIsVolatile = 1) AND ([Performance].[Date] <= ISNULL(CF.EndDate, '1 Jan 1900'))
		GROUP BY CF.FieldID, [Performance].[ID]
		ORDER BY CF.FieldID, [Performance].[ID]

		INSERT @CustomDataSet(CustomFieldID, PertracID, GroupID, FieldIsVolatile, FieldIsCalculated, CF_LastUpdated)
		SELECT
				CF.FieldID, 
				@PertracID, 
				0, 
				CF.FieldIsVolatile, 
				CF.FieldIsCalculated, 
				CF.DateEntered
		FROM 
				fn_tblPertracCustomFields_SelectKD(Null) CF 
		WHERE
				((ISNULL(@CustomFieldID, 0) = 0) OR (ISNULL(@CustomFieldID, 0) = CF.FieldID))
				
		INSERT @DataStatus(FieldDataID, CustomFieldID, PertracID, GroupID, UpdateRequired, FieldIsCalculated, LatestReturnDate, DateEntered)
		SELECT	
				ISNULL(CFD.FieldDataID, 0),
				DS.CustomFieldID, 
				DS.PertracID, 
				0, 
				dbo.fn_CustomFieldUpdateRequired(CFD.FieldDataID, CFD.UpdateRequired, DS.FieldIsVolatile, UpdateTimes.ReturnDate, UpdateTimes.LastUpdated, CFD.LatestReturnDate, CFD.DateEntered, DS.CF_LastUpdated ),
				DS.FieldIsCalculated, 
				ISNULL(CFD.LatestReturnDate, '1 Jan 1900'), 
				ISNULL(CFD.DateEntered, '1 Jan 1900')
		FROM @CustomDataSet DS LEFT JOIN 
				@PertracUpdateTimes UpdateTimes ON (DS.CustomFieldID = UpdateTimes.CustomFieldID) AND (DS.PertracID = UpdateTimes.PertracID) LEFT JOIN
				fn_tblPertracCustomFieldData_SinglePertracID(@PertracID, @KnowledgeDate) CFD ON (DS.CustomFieldID = CFD.CustomFieldID) AND (DS.PertracID = CFD.PertracID)  
		WHERE ((ISNULL(@CustomFieldID, 0) = 0) OR (DS.CustomFieldID = ISNULL(@CustomFieldID, 0)))
		ORDER BY DS.CustomFieldID, DS.PertracID

		RETURN				
	END

	IF (@PertracID <= 0) AND (ISNULL(@GroupID, 0) = 0)
	BEGIN

		-- External Data
		
		INSERT @PertracUpdateTimes(CustomFieldID, PertracID, ReturnDate, LastUpdated)
		SELECT CF.FieldID, [Performance].[ID], MAX([Performance].[Date]), MAX([Performance].[LastUpdated]) 
		FROM
		fn_tblPertracCustomFields_SelectKD(@KnowledgeDate) CF CROSS JOIN
		[MASTERSQL].[dbo].[Performance] Performance
		WHERE ((ISNULL(@CustomFieldID, 0) = 0) OR (ISNULL(@CustomFieldID, 0) = CF.FieldID)) AND 
				(CF.FieldIsVolatile = 1) AND ([Performance].[Date] <= ISNULL(CF.EndDate, '1 Jan 1900')) AND 
				((ISNULL(@PertracID, 0) = 0) OR ([Performance].[ID] = ISNULL(@PertracID, 0)))
		GROUP BY CF.FieldID, [Performance].[ID]
		ORDER BY CF.FieldID, [Performance].[ID]

		-- Internal Data 
		
		INSERT @PertracUpdateTimes(CustomFieldID, PertracID, ReturnDate, LastUpdated)
		SELECT CF.FieldID, ([Performance].[InstrumentID] + 1048576), MAX([Performance].[ReturnDate]), MAX([Performance].[DateEntered]) 
		FROM
		fn_tblPertracCustomFields_SelectKD(@KnowledgeDate) CF CROSS JOIN
		[MASTERSQL].[dbo].[tblInstrumentPerformance] Performance
		WHERE ((ISNULL(@CustomFieldID, 0) = 0) OR (ISNULL(@CustomFieldID, 0) = CF.FieldID)) AND 
				(CF.FieldIsVolatile = 1) AND ([Performance].[ReturnDate] <= ISNULL(CF.EndDate, '1 Jan 1900')) AND 
				((ISNULL(@PertracID, 0) = 0) OR ([Performance].[InstrumentID] = ISNULL(@PertracID, 0)))
		GROUP BY CF.FieldID, [Performance].[InstrumentID]
		ORDER BY CF.FieldID, [Performance].[InstrumentID]

		-- External Data

		INSERT @CustomDataSet(CustomFieldID, PertracID, GroupID, FieldIsVolatile, FieldIsCalculated, CF_LastUpdated)
		SELECT
				CF.FieldID, 
				MasterName.ID, 
				0, 
				CF.FieldIsVolatile, 
				CF.FieldIsCalculated, 
				CF.DateEntered
		FROM 
				(fn_tblPertracCustomFields_SelectKD(Null) CF CROSS JOIN
				[MASTERSQL].[dbo].[MasterName] MasterName)
		WHERE
				((ISNULL(@CustomFieldID, 0) = 0) OR (ISNULL(@CustomFieldID, 0) = CF.FieldID)) AND 
				((ISNULL(@PertracID, 0) = 0) OR ([MasterName].[ID] = ISNULL(@PertracID, 0)))

		-- Internal Data

		INSERT @CustomDataSet(CustomFieldID, PertracID, GroupID, FieldIsVolatile, FieldIsCalculated, CF_LastUpdated)
		SELECT
				CF.FieldID, 
				(MasterName.InstrumentID + 1048576), 
				0, 
				CF.FieldIsVolatile, 
				CF.FieldIsCalculated, 
				CF.DateEntered
		FROM 
				(fn_tblPertracCustomFields_SelectKD(Null) CF CROSS JOIN
				[MASTERSQL].[dbo].[tblInstrumentList] MasterName)
		WHERE
				((ISNULL(@CustomFieldID, 0) = 0) OR (ISNULL(@CustomFieldID, 0) = CF.FieldID)) AND 
				((ISNULL(@PertracID, 0) = 0) OR ([MasterName].[InstrumentID] = (ISNULL(@PertracID, 0) - 1048576)))

		INSERT @DataStatus(FieldDataID, CustomFieldID, PertracID, GroupID, UpdateRequired, FieldIsCalculated, LatestReturnDate, DateEntered)
		SELECT	
				ISNULL(CFD.FieldDataID, 0),
				DS.CustomFieldID, 
				DS.PertracID, 
				0, 
				dbo.fn_CustomFieldUpdateRequired(CFD.FieldDataID, CFD.UpdateRequired, DS.FieldIsVolatile, UpdateTimes.ReturnDate, UpdateTimes.LastUpdated, CFD.LatestReturnDate, CFD.DateEntered, DS.CF_LastUpdated ),
				DS.FieldIsCalculated, 
				ISNULL(CFD.LatestReturnDate, '1 Jan 1900'), 
				ISNULL(CFD.DateEntered, '1 Jan 1900')
		FROM @CustomDataSet DS LEFT JOIN 
				@PertracUpdateTimes UpdateTimes ON (DS.CustomFieldID = UpdateTimes.CustomFieldID) AND (DS.PertracID = UpdateTimes.PertracID) LEFT JOIN
				tblPertracCustomFieldData CFD ON (DS.CustomFieldID = CFD.CustomFieldID) AND (DS.PertracID = CFD.PertracID) AND (CFD.GroupID = 0)
		WHERE ((ISNULL(@CustomFieldID, 0) = 0) OR (DS.CustomFieldID = ISNULL(@CustomFieldID, 0)))
		ORDER BY DS.CustomFieldID, DS.PertracID

		RETURN		
	END
	ELSE
	BEGIN
		-- 	IF (@PertracID <= 0) AND (ISNULL(@GroupID, 0) > 0)

		-- External 
		
		INSERT @TmpPerformance([ID], [Date], [Return], [NAV], [LastUpdated])
		SELECT [ID], [Date], [Return], [NAV], [LastUpdated]
		FROM [MASTERSQL].[dbo].[Performance] Performance
		WHERE [ID] IN
			(SELECT Items.GroupPertracCode FROM fn_tblGroupItems_SelectKD(@KnowledgeDate) Items WHERE Items.GroupID = @GroupID)
		
		-- Internal
		
		INSERT @TmpPerformance([ID], [Date], [Return], [NAV], [LastUpdated])
		SELECT ([InstrumentID] + 1048576), [ReturnDate], [ReturnPercent], [NAV], [DateEntered]
		FROM [MASTERSQL].[dbo].[tblInstrumentPerformance] Performance
		WHERE [InstrumentID] IN 
			(SELECT (Items.GroupPertracCode - 1048576) FROM fn_tblGroupItems_SelectKD(@KnowledgeDate) Items WHERE Items.GroupID = @GroupID)
		
		
		INSERT @PertracUpdateTimes(CustomFieldID, PertracID, ReturnDate, LastUpdated)
		SELECT CF.FieldID, [Performance].[ID], MAX([Performance].[Date]), MAX([Performance].[LastUpdated]) 
		FROM
		fn_tblPertracCustomFields_SelectKD(@KnowledgeDate) CF CROSS JOIN
		@TmpPerformance Performance
		WHERE 
				((ISNULL(@CustomFieldID, 0) = 0) OR (ISNULL(@CustomFieldID, 0) = CF.FieldID)) AND 
				(CF.FieldIsVolatile = 1) AND ([Performance].[Date] <= ISNULL(CF.EndDate, '1 Jan 1900')) 
		GROUP BY CF.FieldID, [Performance].[ID]
		ORDER BY CF.FieldID, [Performance].[ID]


		INSERT @CustomDataSet(CustomFieldID, PertracID, GroupID, FieldIsVolatile, FieldIsCalculated, CF_LastUpdated)
		SELECT
				CF.FieldID, 
				Items.GroupPertracCode, 
				@GroupID, 
				CF.FieldIsVolatile, 
				CF.FieldIsCalculated, 
				CF.DateEntered
		FROM 
				(fn_tblPertracCustomFields_SelectKD(Null) CF CROSS JOIN
				fn_tblGroupItems_SelectKD(@KnowledgeDate) Items)
		WHERE (Items.GroupID = @GroupID) AND 
					((ISNULL(@PertracID, 0) = 0) OR (Items.GroupPertracCode = ISNULL(@PertracID, 0)))


		INSERT @DataStatus(FieldDataID, CustomFieldID, PertracID, GroupID, UpdateRequired, FieldIsCalculated, LatestReturnDate, DateEntered)
		SELECT	
				ISNULL(CFD.FieldDataID, 0),
				DS.CustomFieldID, 
				DS.PertracID, 
				ISNULL(CFD.GroupID, 0), 
				dbo.fn_CustomFieldUpdateRequired(CFD.FieldDataID, CFD.UpdateRequired, DS.FieldIsVolatile, UpdateTimes.ReturnDate, UpdateTimes.LastUpdated, CFD.LatestReturnDate, CFD.DateEntered, DS.CF_LastUpdated ),
				DS.FieldIsCalculated, 
				ISNULL(CFD.LatestReturnDate, '1 Jan 1900'), 
				ISNULL(CFD.DateEntered, '1 Jan 1900')
		FROM @CustomDataSet DS LEFT JOIN 
				fn_tblPertracCustomFieldDataByGroup_SelectKD(@GroupID, 0, @KnowledgeDate) CFD ON (DS.CustomFieldID = CFD.CustomFieldID) AND (DS.PertracID = CFD.PertracID) LEFT JOIN 
				@PertracUpdateTimes UpdateTimes ON (DS.CustomFieldID = UpdateTimes.CustomFieldID) AND (DS.PertracID = UpdateTimes.PertracID)
		WHERE ((ISNULL(@CustomFieldID, 0) = 0) OR (DS.CustomFieldID = ISNULL(@CustomFieldID, 0)))
		ORDER BY DS.CustomFieldID, DS.PertracID

		RETURN
	END

	RETURN
	END



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  SELECT  ON [dbo].[fn_tblPertracCustomFieldData_Status]  TO [InvestMaster_Sales]
GO

GRANT  SELECT  ON [dbo].[fn_tblPertracCustomFieldData_Status]  TO [Genoa_Admin]
GO

GRANT  SELECT  ON [dbo].[fn_tblPertracCustomFieldData_Status]  TO [Genoa_Manager]
GO

GRANT  SELECT  ON [dbo].[fn_tblPertracCustomFieldData_Status]  TO [Genoa_Read]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[adp_tblPertracCustomFieldData_DeleteCommand]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[adp_tblPertracCustomFieldData_DeleteCommand]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[adp_tblPertracCustomFieldData_InsertCommand]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[adp_tblPertracCustomFieldData_InsertCommand]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[adp_tblPertracCustomFieldData_SelectCommand]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[adp_tblPertracCustomFieldData_SelectCommand]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[adp_tblPertracCustomFieldData_UpdateCommand]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[adp_tblPertracCustomFieldData_UpdateCommand]
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO



CREATE PROCEDURE dbo.adp_tblPertracCustomFieldData_DeleteCommand
(
	@GroupID int = 0, 
	@CustomFieldID int, 
	@PertracID int 
)
AS

	SET NOCOUNT OFF
	
	DELETE FROM tblPertracCustomFieldData
	WHERE 
		(GroupID = ISNULL(@GroupID, 0)) AND 
		(CustomFieldID = ISNULL(@CustomFieldID, 0)) AND 
		(PertracID = ISNULL(@PertracID, 0)) AND 
		(DateDeleted IS NULL)

	IF @@ERROR = 0
	  BEGIN
	    RETURN -1
	  END
	ELSE
	  BEGIN
	    RETURN 0
	  END

	RETURN -1




GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[adp_tblPertracCustomFieldData_DeleteCommand]  TO [Genoa_Admin]
GO

GRANT  EXECUTE  ON [dbo].[adp_tblPertracCustomFieldData_DeleteCommand]  TO [Genoa_Manager]
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE PROCEDURE dbo.adp_tblPertracCustomFieldData_InsertCommand
	(
	@FieldDataID int,
	@CustomFieldID int,
	@PertracID int,
	@GroupID int,
	@FieldNumericData float,
	@FieldTextData varchar (50),
	@FieldDateData datetime, 
	@FieldBooleanData bit, 
	@UpdateRequired bit,
	@LatestReturnDate datetime, 
	@KnowledgeDate datetime
	)
--
-- (adp) Adaptor, tblPertracCustomFieldData procedure
--
-- Controlled Insert of a new Custom Field Data Item to the Venice (Renaissance) database
--
-- No longer Knowledgedated as of Oct 2008. The number of Pertrac instruments has made this impractical.
--
AS
	
	DECLARE @New_FieldDataID int 
	
	DECLARE @BestRN int
	
	SET NOCOUNT OFF

	SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
	
	-- Check for Data record matching the given CustomFieldID, PertracID and GroupID
	
	SELECT @BestRN = ISNULL(MAX(RN), 0)
  FROM tblPertracCustomFieldData 
  WHERE (GroupID = ISNULL(@GroupID, 0)) AND 
				(CustomFieldID = ISNULL(@CustomFieldID, 0)) AND 
				(PertracID = ISNULL(@PertracID, 0))

	-- If an existing row exists, check for any changes to the data.
		
	IF ISNULL(@BestRN, 0) > 0
	BEGIN
		 
		-- UPDATE.
		
		BEGIN TRANSACTION
					  
		-- Insert new record.
		
		UPDATE tblPertracCustomFieldData
		SET FieldNumericData = @FieldNumericData, FieldTextData = @FieldTextData,	FieldDateData = @FieldDateData, FieldBooleanData = @FieldBooleanData, UpdateRequired = @UpdateRequired, LatestReturnDate = @LatestReturnDate
		WHERE (RN = @BestRN)
				
		-- Select Newly created record

		SELECT 	RN ,
						AuditID ,
						FieldDataID ,
						CustomFieldID,
						PertracID,
						GroupID,
						FieldNumericData,
						FieldTextData,
						FieldDateData, 
						FieldBooleanData, 
						UpdateRequired,
						LatestReturnDate, 
						UserEntered, 
						DateEntered,
						DateDeleted
		FROM tblPertracCustomFieldData
		WHERE (RN = @BestRN)

		IF @@ERROR = 0
			BEGIN
				COMMIT TRANSACTION
				RETURN @New_FieldDataID
			END
		ELSE
			BEGIN
				ROLLBACK TRANSACTION
				RETURN 0
			END
	
		
		
	END
	ELSE
	BEGIN
		-- INSERT 
		
		SELECT @New_FieldDataID = 0
		
		BEGIN TRANSACTION
					  
		-- Insert new record.
		
		INSERT INTO tblPertracCustomFieldData(
			FieldDataID ,
			CustomFieldID,
			PertracID,
			GroupID,
			FieldNumericData,
			FieldTextData,
			FieldDateData, 
			FieldBooleanData, 
			UpdateRequired, 
			LatestReturnDate
			)
		VALUES (
			@New_FieldDataID, 
			@CustomFieldID,
			@PertracID,
			@GroupID,
			@FieldNumericData,
			@FieldTextData,
			@FieldDateData, 
			@FieldBooleanData, 
			@UpdateRequired, 
			@LatestReturnDate
			)
		
		-- Save RN of Newly added record.
		
		SELECT @BestRN = SCOPE_IDENTITY()		
		
		UPDATE tblPertracCustomFieldData
		SET FieldDataID = RN
		WHERE FieldDataID = 0
		
		SELECT 	@New_FieldDataID = FieldDataID
		FROM tblPertracCustomFieldData
		WHERE (RN = @BestRN)

		-- Select Newly created record

		SELECT 	RN ,
						AuditID ,
						FieldDataID ,
						CustomFieldID,
						PertracID,
						GroupID,
						FieldNumericData,
						FieldTextData,
						FieldDateData, 
						FieldBooleanData, 
						UpdateRequired,
						LatestReturnDate, 
						UserEntered, 
						DateEntered,
						DateDeleted
		FROM tblPertracCustomFieldData
		WHERE (RN = @BestRN)

		IF @@ERROR = 0
			BEGIN
				COMMIT TRANSACTION
				RETURN @New_FieldDataID
			END
		ELSE
			BEGIN
				ROLLBACK TRANSACTION
				RETURN 0
			END

	END
	
	RETURN 0




GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[adp_tblPertracCustomFieldData_InsertCommand]  TO [Genoa_Admin]
GO

GRANT  EXECUTE  ON [dbo].[adp_tblPertracCustomFieldData_InsertCommand]  TO [Genoa_Manager]
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO



CREATE PROCEDURE dbo.adp_tblPertracCustomFieldData_SelectCommand
(
@KnowledgeDate datetime = Null
)
AS

SELECT 	RN ,
				AuditID ,
				FieldDataID ,
				CustomFieldID,
				PertracID,
				GroupID,
				FieldNumericData,
				FieldTextData,
				FieldDateData, 
				FieldBooleanData, 
				UpdateRequired,
				LatestReturnDate, 
				UserEntered, 
				DateEntered,
				DateDeleted
FROM tblPertracCustomFieldData
WHERE (GroupID = 0) 

RETURN -1



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[adp_tblPertracCustomFieldData_SelectCommand]  TO [InvestMaster_Sales]
GO

GRANT  EXECUTE  ON [dbo].[adp_tblPertracCustomFieldData_SelectCommand]  TO [Genoa_Admin]
GO

GRANT  EXECUTE  ON [dbo].[adp_tblPertracCustomFieldData_SelectCommand]  TO [Genoa_Manager]
GO

GRANT  EXECUTE  ON [dbo].[adp_tblPertracCustomFieldData_SelectCommand]  TO [Genoa_Read]
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE PROCEDURE dbo.adp_tblPertracCustomFieldData_UpdateCommand
	(
	@FieldDataID int,
	@CustomFieldID int,
	@PertracID int,
	@GroupID int,
	@FieldNumericData float,
	@FieldTextData varchar (50),
	@FieldDateData datetime, 
	@FieldBooleanData bit, 
	@UpdateRequired bit,
	@LatestReturnDate datetime, 
	@KnowledgeDate datetime
	)
AS
	--
	-- (adp) Adaptor, tblPertracCustomFieldData procedure
	--
	-- Controlled Update of a new Custom Field Data Item to the Venice (Renaissance) database
	--
	-- No longer Knowledgedated as of Oct 2008. The number of Pertrac instruments has made this impractical.
	--
	
	SET NOCOUNT OFF

	DECLARE @RVal int 

	EXECUTE @RVal = [dbo].[adp_tblPertracCustomFieldData_InsertCommand] 
		@FieldDataID = @FieldDataID,
		@CustomFieldID = @CustomFieldID,
		@PertracID = @PertracID, 
		@GroupID = @GroupID, 
		@FieldNumericData = @FieldNumericData, 
		@FieldTextData = @FieldTextData, 
		@FieldDateData = @FieldDateData, 
		@FieldBooleanData = @FieldBooleanData, 
		@UpdateRequired = @UpdateRequired, 
		@LatestReturnDate = @LatestReturnDate,
		@KnowledgeDate = @KnowledgeDate
		
	RETURN @RVal






GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[adp_tblPertracCustomFieldData_UpdateCommand]  TO [Genoa_Admin]
GO

GRANT  EXECUTE  ON [dbo].[adp_tblPertracCustomFieldData_UpdateCommand]  TO [Genoa_Manager]
GO

DBCC SHRINKDATABASE (Renaissance , 0, TRUNCATEONLY)

GO
