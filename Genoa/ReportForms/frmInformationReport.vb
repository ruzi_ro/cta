Imports System.Data.SqlClient
Imports RenaissanceGlobals


Public Class frmInformationReport

	Inherits System.Windows.Forms.Form
  Implements StandardCTAForm

#Region " Windows Form Designer generated code "

  Private Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  Friend WithEvents btnRunReport As System.Windows.Forms.Button
  Friend WithEvents btnClose As System.Windows.Forms.Button
  Friend WithEvents Status1 As System.Windows.Forms.StatusStrip
  Friend WithEvents StatusLabel1 As System.Windows.Forms.ToolStripStatusLabel
  Friend WithEvents ToolStripProgressBar1 As System.Windows.Forms.ToolStripProgressBar
  Friend WithEvents Combo_SelectFrom As System.Windows.Forms.ComboBox
  Friend WithEvents Radio_Pertrac As System.Windows.Forms.RadioButton
  Friend WithEvents Radio_ExistingGroups As System.Windows.Forms.RadioButton
  Friend WithEvents List_SelectItems As System.Windows.Forms.ListBox
  Friend WithEvents Button_SelectNone As System.Windows.Forms.Button
  Friend WithEvents Button_SelectAll As System.Windows.Forms.Button
  Friend WithEvents Combo_CompareSeries As FCP_TelerikControls.FCP_RadComboBox
  Friend WithEvents Label29 As System.Windows.Forms.Label
  Friend WithEvents Radio_ExistingFunds As System.Windows.Forms.RadioButton
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.btnRunReport = New System.Windows.Forms.Button
    Me.btnClose = New System.Windows.Forms.Button
    Me.Status1 = New System.Windows.Forms.StatusStrip
    Me.ToolStripProgressBar1 = New System.Windows.Forms.ToolStripProgressBar
    Me.StatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel
    Me.Combo_SelectFrom = New System.Windows.Forms.ComboBox
    Me.Radio_Pertrac = New System.Windows.Forms.RadioButton
    Me.Radio_ExistingGroups = New System.Windows.Forms.RadioButton
    Me.Radio_ExistingFunds = New System.Windows.Forms.RadioButton
    Me.List_SelectItems = New System.Windows.Forms.ListBox
    Me.Button_SelectNone = New System.Windows.Forms.Button
    Me.Button_SelectAll = New System.Windows.Forms.Button
    Me.Combo_CompareSeries = New FCP_TelerikControls.FCP_RadComboBox
    Me.Label29 = New System.Windows.Forms.Label
    Me.Status1.SuspendLayout()
    Me.SuspendLayout()
    '
    'btnRunReport
    '
    Me.btnRunReport.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.btnRunReport.Location = New System.Drawing.Point(152, 318)
    Me.btnRunReport.Name = "btnRunReport"
    Me.btnRunReport.Size = New System.Drawing.Size(148, 28)
    Me.btnRunReport.TabIndex = 8
    Me.btnRunReport.Text = "Run Report"
    '
    'btnClose
    '
    Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.btnClose.Location = New System.Drawing.Point(368, 318)
    Me.btnClose.Name = "btnClose"
    Me.btnClose.Size = New System.Drawing.Size(75, 28)
    Me.btnClose.TabIndex = 9
    Me.btnClose.Text = "&Close"
    '
    'Status1
    '
    Me.Status1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripProgressBar1, Me.StatusLabel1})
    Me.Status1.Location = New System.Drawing.Point(0, 353)
    Me.Status1.Name = "Status1"
    Me.Status1.Size = New System.Drawing.Size(573, 22)
    Me.Status1.TabIndex = 69
    Me.Status1.Text = "StatusStrip1"
    '
    'ToolStripProgressBar1
    '
    Me.ToolStripProgressBar1.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
    Me.ToolStripProgressBar1.Maximum = 20
    Me.ToolStripProgressBar1.Name = "ToolStripProgressBar1"
    Me.ToolStripProgressBar1.Size = New System.Drawing.Size(150, 16)
    Me.ToolStripProgressBar1.Step = 1
    Me.ToolStripProgressBar1.Visible = False
    '
    'StatusLabel1
    '
    Me.StatusLabel1.Name = "StatusLabel1"
    Me.StatusLabel1.Size = New System.Drawing.Size(111, 17)
    Me.StatusLabel1.Text = "ToolStripStatusLabel1"
    '
    'Combo_SelectFrom
    '
    Me.Combo_SelectFrom.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
          Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_SelectFrom.Location = New System.Drawing.Point(152, 12)
    Me.Combo_SelectFrom.Name = "Combo_SelectFrom"
    Me.Combo_SelectFrom.Size = New System.Drawing.Size(409, 21)
    Me.Combo_SelectFrom.TabIndex = 3
    '
    'Radio_Pertrac
    '
    Me.Radio_Pertrac.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.Radio_Pertrac.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Radio_Pertrac.Location = New System.Drawing.Point(12, 12)
    Me.Radio_Pertrac.Name = "Radio_Pertrac"
    Me.Radio_Pertrac.Size = New System.Drawing.Size(124, 21)
    Me.Radio_Pertrac.TabIndex = 0
    Me.Radio_Pertrac.TabStop = True
    Me.Radio_Pertrac.Text = "Pertrac Instrument"
    Me.Radio_Pertrac.UseVisualStyleBackColor = True
    '
    'Radio_ExistingGroups
    '
    Me.Radio_ExistingGroups.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.Radio_ExistingGroups.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Radio_ExistingGroups.Location = New System.Drawing.Point(12, 39)
    Me.Radio_ExistingGroups.Name = "Radio_ExistingGroups"
    Me.Radio_ExistingGroups.Size = New System.Drawing.Size(124, 21)
    Me.Radio_ExistingGroups.TabIndex = 1
    Me.Radio_ExistingGroups.TabStop = True
    Me.Radio_ExistingGroups.Text = "CTA Group"
    Me.Radio_ExistingGroups.UseVisualStyleBackColor = True
    '
    'Radio_ExistingFunds
    '
    Me.Radio_ExistingFunds.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.Radio_ExistingFunds.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Radio_ExistingFunds.Location = New System.Drawing.Point(12, 66)
    Me.Radio_ExistingFunds.Name = "Radio_ExistingFunds"
    Me.Radio_ExistingFunds.Size = New System.Drawing.Size(124, 21)
    Me.Radio_ExistingFunds.TabIndex = 2
    Me.Radio_ExistingFunds.TabStop = True
    Me.Radio_ExistingFunds.Text = "Existing Position"
    Me.Radio_ExistingFunds.UseVisualStyleBackColor = True
    '
    'List_SelectItems
    '
    Me.List_SelectItems.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
          Or System.Windows.Forms.AnchorStyles.Left) _
          Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.List_SelectItems.FormattingEnabled = True
    Me.List_SelectItems.Location = New System.Drawing.Point(152, 40)
    Me.List_SelectItems.Name = "List_SelectItems"
    Me.List_SelectItems.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended
    Me.List_SelectItems.Size = New System.Drawing.Size(409, 238)
    Me.List_SelectItems.Sorted = True
    Me.List_SelectItems.TabIndex = 4
    '
    'Button_SelectNone
    '
    Me.Button_SelectNone.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Button_SelectNone.Location = New System.Drawing.Point(12, 183)
    Me.Button_SelectNone.Name = "Button_SelectNone"
    Me.Button_SelectNone.Size = New System.Drawing.Size(124, 25)
    Me.Button_SelectNone.TabIndex = 6
    Me.Button_SelectNone.Text = "Un-Select All"
    '
    'Button_SelectAll
    '
    Me.Button_SelectAll.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Button_SelectAll.Location = New System.Drawing.Point(12, 152)
    Me.Button_SelectAll.Name = "Button_SelectAll"
    Me.Button_SelectAll.Size = New System.Drawing.Size(124, 25)
    Me.Button_SelectAll.TabIndex = 5
    Me.Button_SelectAll.Text = "Select All"
    '
    'Combo_CompareSeries
    '
    Me.Combo_CompareSeries.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
          Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_CompareSeries.FormattingEnabled = True
    Me.Combo_CompareSeries.Location = New System.Drawing.Point(152, 287)
    Me.Combo_CompareSeries.Name = "Combo_CompareSeries"
    '
    '
    '
    Me.Combo_CompareSeries.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren
    Me.Combo_CompareSeries.SelectNoMatch = False
    Me.Combo_CompareSeries.Size = New System.Drawing.Size(409, 21)
    Me.Combo_CompareSeries.TabIndex = 7
    Me.Combo_CompareSeries.ThemeName = "ControlDefault"
    '
    'Label29
    '
    Me.Label29.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Label29.AutoSize = True
    Me.Label29.Location = New System.Drawing.Point(9, 290)
    Me.Label29.Name = "Label29"
    Me.Label29.Size = New System.Drawing.Size(109, 13)
    Me.Label29.TabIndex = 79
    Me.Label29.Text = "Chart Compare Series"
    '
    'frmInformationReport
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
    Me.ClientSize = New System.Drawing.Size(573, 375)
    Me.Controls.Add(Me.Combo_CompareSeries)
    Me.Controls.Add(Me.Label29)
    Me.Controls.Add(Me.Button_SelectAll)
    Me.Controls.Add(Me.Button_SelectNone)
    Me.Controls.Add(Me.List_SelectItems)
    Me.Controls.Add(Me.Radio_ExistingFunds)
    Me.Controls.Add(Me.Radio_ExistingGroups)
    Me.Controls.Add(Me.Radio_Pertrac)
    Me.Controls.Add(Me.Combo_SelectFrom)
    Me.Controls.Add(Me.Status1)
    Me.Controls.Add(Me.btnClose)
    Me.Controls.Add(Me.btnRunReport)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.MaximizeBox = False
    Me.MinimizeBox = False
    Me.MinimumSize = New System.Drawing.Size(450, 250)
    Me.Name = "frmInformationReport"
    Me.Text = "CTA Funds Information Report"
    Me.Status1.ResumeLayout(False)
    Me.Status1.PerformLayout()
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub

#End Region

#Region " Form Locals and Constants "


  ' Form 'Parent', the Main Venice form.
  ' Generally only accessed through the 'MainForm' property.
  Private WithEvents _MainForm As CTAMain

  ' Form ToolTip
  Private FormTooltip As New ToolTip()

  ' Form Constants, specific to the table being updated.

  Private ALWAYS_CLOSE_THIS_FORM As Boolean = True

  ' The standard ChangeID for this form. e.g. tblPerson
  Private THIS_FORM_ChangeID As RenaissanceGlobals.RenaissanceChangeID

  ' Form specific Permissioning variables
  Private THIS_FORM_PermissionArea As String
  Private THIS_FORM_PermissionType As RenaissanceGlobals.PermissionFeatureType

  ' Form specific Form type 
  Private THIS_FORM_FormID As CTAFormID

  ' Form Status Flags

  Private FormIsValid As Boolean
  Private FormChanged As Boolean
  Private _FormOpenFailed As Boolean

  ' User Permission Flags

  Private HasReadPermission As Boolean
  Private HasUpdatePermission As Boolean
  Private HasInsertPermission As Boolean
  Private HasDeletePermission As Boolean

  ' Data Structures
  Private PertracInstruments As DataView = Nothing ' Manages Select List for Pertrac Instruments.

#End Region

#Region " Form 'Properties' "

  Public ReadOnly Property MainForm() As CTAMain Implements StandardCTAForm.MainForm
    ' Public property to return handle to the 'Main' Venice form, where in reside most of the 
    ' data structures and many common utilities.
    Get
      Return _MainForm
    End Get
  End Property

  Public Property IsOverCancelButton() As Boolean Implements StandardCTAForm.IsOverCancelButton
    ' Public property maintaining a value indicating if the cursor is over the 'Cancel'
    ' Button on this form.
    ' This property is specifically designed for use by the field formating Event functions
    ' In order that they do not impose format restrictions if the user is about to click the 
    ' 'Cancel' button.
    '
    Get
      Return False
    End Get
    Set(ByVal Value As Boolean)
    End Set
  End Property

  Public ReadOnly Property IsInPaint() As Boolean Implements StandardCTAForm.IsInPaint
    Get
      Return False
    End Get
  End Property

  Public ReadOnly Property InUse() As Boolean Implements StandardCTAForm.InUse
    Get
      Return True
    End Get
  End Property

  Public ReadOnly Property FormOpenFailed() As Boolean Implements StandardCTAForm.FormOpenFailed
    Get
      Return _FormOpenFailed
    End Get
  End Property

#End Region

  Public Sub New(ByVal pMainForm As CTAMain)
    ' *************************************************************
    ' Custom 'New'. 
    ' Passes in the reference to the parent form.
    ' 
    ' Establishes form specific variables.
    ' Establishes Form specific Data connection / data structures.
    '
    ' *************************************************************

    Me.New()

    _MainForm = pMainForm
    AddHandler _MainForm.CTAAutoUpdate, AddressOf Me.AutoUpdate

    _FormOpenFailed = False

    ' ******************************************************
    ' Form Specific Settings :
    ' ******************************************************

    ' Form Permissioning :-

    THIS_FORM_PermissionArea = Me.Name
    THIS_FORM_PermissionType = RenaissanceGlobals.PermissionFeatureType.TypeForm

    ' 'This' form ID

    THIS_FORM_FormID = CTAFormID.frmInformationReport

    ' Format Event Handlers for form controls

    AddHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

    AddHandler Combo_SelectFrom.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_SelectFrom.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_SelectFrom.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_SelectFrom.KeyUp, AddressOf MainForm.ComboSelectAsYouType

    'AddHandler Combo_CompareSeries.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_CompareSeries.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    'AddHandler Combo_CompareSeries.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    'AddHandler Combo_CompareSeries.KeyUp, AddressOf MainForm.ComboSelectAsYouType

    Call SetPertracCombo()

    If (Combo_CompareSeries.Items.Count > 0) Then
      Combo_CompareSeries.SelectedIndex = 0
    End If

    ' Set up the ToolTip
    MainForm.SetFormToolTip(Me, FormTooltip)

    ' ******************************************************
    ' End Form Specific.
    ' ******************************************************

    THIS_FORM_ChangeID = RenaissanceChangeID.KnowledgeDate

  End Sub

#Region " This Form Event handlers : FormLoad / FormClose "

  ' Form Initialisation code.
  '
  Public Sub ResetForm() Implements StandardCTAForm.ResetForm

    Call Form_Load(Me, New System.EventArgs)
  End Sub

  Public Sub CloseForm() Implements StandardCTAForm.CloseForm
    ALWAYS_CLOSE_THIS_FORM = True
    Me.Close()
  End Sub

  Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    Me.MainForm.LogError(Me.Name, LOG_LEVELS.Audit, "", "Form Loaded", "", False)
    _FormOpenFailed = False

    If Not (MainForm Is Nothing) Then
      FormIsValid = True
    Else
      MessageBox.Show("'Main' form is not set. Closing form", "no Main Form", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
      FormIsValid = False
      _FormOpenFailed = True
      Exit Sub
    End If


    ' Check User permissions
    Call CheckPermissions()
    If (HasReadPermission = False) Then
      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User does not have Read permission for this Form..", "", True)

      FormIsValid = False
      _FormOpenFailed = True
      Exit Sub
    End If

    ' Build Combos

    Try

      MainForm.SetComboSelectionLengths(Me)

      Me.Radio_ExistingGroups.Checked = True

    Catch ex As Exception
    End Try

  End Sub

  Private Sub Form_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
    Dim HideForm As Boolean

    ' Hide or Close this form ?
    ' All depends on how many of this form type are Open or in Cache...


    If (ALWAYS_CLOSE_THIS_FORM = True) Or (FormIsValid = False) Then
      HideForm = False
    Else
      HideForm = True
      If MainForm.CTAForms.CountOf(THIS_FORM_FormID) > MainForm.ReportForm_CacheCount Then
        HideForm = False
      End If
    End If

    If HideForm = True Then
      MainForm.HideInFormsCollection(Me)
      Me.Hide() ' NPP Fix

      e.Cancel = True
    Else
      Try
        MainForm.RemoveFromFormsCollection(Me)
        RemoveHandler _MainForm.CTAAutoUpdate, AddressOf Me.AutoUpdate

        RemoveHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

        RemoveHandler Combo_SelectFrom.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_SelectFrom.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_SelectFrom.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_SelectFrom.KeyUp, AddressOf MainForm.ComboSelectAsYouType

        'RemoveHandler Combo_CompareSeries.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_CompareSeries.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        'RemoveHandler Combo_CompareSeries.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        'RemoveHandler Combo_CompareSeries.KeyUp, AddressOf MainForm.ComboSelectAsYouType

      Catch ex As Exception
      End Try
    End If

  End Sub


#End Region

  ' Routine to handle changes / updates to tables by this and other windows.
  ' If this, or any other, form posts a change to a table, then it will invoke an update event 
  ' detailing what tables have been altered.
  ' Each Form will receive this event through this 'AutoUpdate' routine whice is configured to handle
  ' the 'CTAAutoUpdate' event of the main Venice form.
  ' Each form may them react as appropriate to changes in any table that might impact it.
  '
  Private Sub AutoUpdate(ByVal sender As System.Object, ByVal e As RenaissanceGlobals.RenaissanceUpdateEventArgs)
    Dim KnowledgeDateChanged As Boolean
    Dim SetButtonStatus_Flag As Boolean

    If (Me.IsDisposed) OrElse (Me.Disposing) OrElse (Me.InUse = False) Then Exit Sub

    KnowledgeDateChanged = False
    SetButtonStatus_Flag = False

    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True) Or (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Connection) = True) Then
      KnowledgeDateChanged = True
    End If

    ' ****************************************************************
    ' Check for changes relevant to this form
    ' ****************************************************************

    ' Changes to the KnowledgeDate :-
    If e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True Then
      SetButtonStatus_Flag = True
    End If

    ' Changes to the tblGroupList table :-
    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblGroupList) = True) Or KnowledgeDateChanged Then

      ' Re-Set list.
      If Radio_ExistingGroups.Checked Then
        Call Radio_ExistingGroups_CheckedChanged(Radio_ExistingGroups, New EventArgs)
      End If
    End If

    '' Changes to the tblCTAItems table :-
    'If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblCTAItems) = True) Or KnowledgeDateChanged Then

    '	' Re-Set combo.
    '	Call SetEntityCombo()
    'End If


    ' Changes to the tblUserPermissions table :-
    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblUserPermissions) = True) Or KnowledgeDateChanged Then

      ' Check ongoing permissions.

      Call CheckPermissions()
      If (HasReadPermission = False) Then
        Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User has lost Read permission for this Form.", "", True)

        FormIsValid = False
        Me.Close()
        Exit Sub
      End If

      SetButtonStatus_Flag = True

    End If

    ' ****************************************************************
    '
    ' ****************************************************************


  End Sub

#Region " Workhorse functions : SetSortedRows / CheckPermission / ControlsChanged / Menu Events "

  ' Check User permissions
  Private Sub CheckPermissions()
    ' *****************************************************************************
    '
    ' *****************************************************************************

    Dim Permissions As Integer

    Permissions = MainForm.CheckPermissions(THIS_FORM_PermissionArea, THIS_FORM_PermissionType)

    HasReadPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermRead) > 0)
    HasUpdatePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermUpdate) > 0)
    HasInsertPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) > 0)
    HasDeletePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermDelete) > 0)

  End Sub


#End Region

#Region " Set Form Combos (Form Specific Code) "

  Private Sub Radio_ExistingGroups_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio_ExistingGroups.CheckedChanged
    ' ***********************************************************************************
    ' React to the ExistingGroups radio.
    '
    ' The Select Combo becomes a Combo to select an existing Group,
    ' The SelectList contains all Instruments in the selected Group(s).
    ' ***********************************************************************************

    If (Me.Created) AndAlso (Radio_ExistingGroups.Checked) Then

      Button_SelectAll.Enabled = True

      Try
        Me.Cursor = Cursors.WaitCursor

        ' Re-Configure Select Combo events.

        Try
          RemoveHandler Combo_SelectFrom.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        Catch ex As Exception
        End Try
        Try
          RemoveHandler Combo_SelectFrom.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch
        Catch ex As Exception
        End Try

        AddHandler Combo_SelectFrom.KeyUp, AddressOf MainForm.ComboSelectAsYouType

        ' Populate Select Combo.

        Call MainForm.SetTblGenericCombo( _
        Me.Combo_SelectFrom, _
        RenaissanceStandardDatasets.tblGroupList, _
        "GroupListName", _
        "GroupListID", _
        "", False, True, True, 0, "All Groups")   ' 

        ' Trigger  SelectList Rebuild.

        If Combo_SelectFrom.Items.Count > 0 Then
          Combo_SelectFrom.SelectedIndex = 0
        End If
        Call Combo_SelectFrom_SelectedIndexChanged(Combo_SelectFrom, New EventArgs)

      Catch ex As Exception
      Finally
        Me.Cursor = Cursors.Default
      End Try

    End If
  End Sub

  Private Sub Radio_Pertrac_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio_Pertrac.CheckedChanged
    ' ***********************************************************************************
    ' React to the Pertrac Radio button being selected.
    '
    ' The Select combo becomes a Select-As-You-Type edit box for the Select List control.
    ' The SelectList control contains All or Selected Pertrac Instruments.
    ' ***********************************************************************************

    If (Me.Created) AndAlso (Radio_Pertrac.Checked) Then

      Button_SelectAll.Enabled = False

      ' Re-Configure Select Combo events.

      Try
        RemoveHandler Combo_SelectFrom.KeyUp, AddressOf MainForm.ComboSelectAsYouType
      Catch ex As Exception
      End Try
      Try
        RemoveHandler Combo_SelectFrom.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch
      Catch ex As Exception
      End Try

      Try
        Me.Cursor = Cursors.WaitCursor

        AddHandler Combo_SelectFrom.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch

        Combo_SelectFrom.DataSource = Nothing
        Combo_SelectFrom.Items.Clear()
        Combo_SelectFrom.Text = ""

        ' Trigger Select List re-Build.

        Call Combo_SelectFrom_TextChanged(Combo_SelectFrom, New EventArgs)
      Catch ex As Exception
      Finally
        Me.Cursor = Cursors.Default
      End Try
    End If

  End Sub

  Private Sub Radio_ExistingFunds_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio_ExistingFunds.CheckedChanged
    ' ***********************************************************************************
    ' React to the ExistingFunds radio.
    '
    ' The Select Combo becomes a Combo to select an existing Venice Fund,
    ' The SelectList contains all Instruments, with associated Pertrac Instruments, in the selected Fund(s).
    ' ***********************************************************************************

    If (Me.Created) AndAlso (Radio_ExistingFunds.Checked) Then

      Button_SelectAll.Enabled = True

      Try
        Me.Cursor = Cursors.WaitCursor

        ' Re-Configure Select Combo events.

        Try
          RemoveHandler Combo_SelectFrom.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        Catch ex As Exception
        End Try
        Try
          RemoveHandler Combo_SelectFrom.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch
        Catch ex As Exception
        End Try

        AddHandler Combo_SelectFrom.KeyUp, AddressOf MainForm.ComboSelectAsYouType

        Call MainForm.PertracData.GetActivePertracInstruments(True, 0) '  GetActivePertracInstruments(MainForm, True, 0)

        ' Populate Select Combo.

        Call MainForm.SetTblGenericCombo( _
        Me.Combo_SelectFrom, _
        RenaissanceStandardDatasets.tblFund, _
        "FundCode", _
        "FundID", _
        "", False, True, True, 0, "All Funds")   ' 

        ' Trigger  SelectList Rebuild.

        If Combo_SelectFrom.Items.Count > 0 Then
          Combo_SelectFrom.SelectedIndex = 0
        Else
          Call Combo_SelectFrom_SelectedIndexChanged(Nothing, New EventArgs)
        End If

      Catch ex As Exception
      Finally
        Me.Cursor = Cursors.Default
      End Try
    End If
  End Sub

  Private Sub Combo_SelectFrom_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_SelectFrom.SelectedIndexChanged
    ' ***********************************************************************************
    '
    '
    '
    ' ***********************************************************************************

    If (Me.Created) Then

      If (Combo_SelectFrom.SelectedIndex >= 0) Then

        If Me.Radio_Pertrac.Checked Then
          ' 

          List_SelectItems.SuspendLayout()

          ' Ensure the DataView is populated.
          If (PertracInstruments Is Nothing) Then
            PertracInstruments = MainForm.PertracData.GetPertracInstruments()
          End If

          Try
            If (List_SelectItems.DisplayMember <> "ListDescription") Then
              List_SelectItems.DataSource = Nothing
              List_SelectItems.DisplayMember = "ListDescription"
            End If

            If (List_SelectItems.ValueMember <> "PertracCode") Then
              List_SelectItems.DataSource = Nothing
              List_SelectItems.ValueMember = "PertracCode"
            End If
          Catch ex As Exception
          End Try

          PertracInstruments.RowFilter = "Mastername LIKE '" & Combo_SelectFrom.SelectedText & "%'"

          If (List_SelectItems.DataSource IsNot PertracInstruments) Then
            List_SelectItems.DataSource = PertracInstruments
          End If

          List_SelectItems.ResumeLayout()

        ElseIf (Me.Radio_ExistingGroups.Checked) AndAlso (IsNumeric(Combo_SelectFrom.SelectedValue)) Then
          ' Populate Form with given data.

          Dim tmpCommand As New SqlCommand
          Dim ListTable As RenaissanceDataClass.DSGroupItems.tblGroupItemsDataTable

          Try

            tmpCommand.CommandType = CommandType.StoredProcedure
            tmpCommand.CommandText = "adp_tblGroupItems_SelectGroupPertracCodes"
            tmpCommand.Connection = MainForm.GetCTAConnection
            tmpCommand.Parameters.Add("@GroupID", SqlDbType.Int).Value = CInt(Combo_SelectFrom.SelectedValue)
            tmpCommand.Parameters.Add("@KnowledgeDate", SqlDbType.DateTime).Value = MainForm.Main_Knowledgedate
            tmpCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT

            List_SelectItems.SuspendLayout()

            If (List_SelectItems.DataSource Is Nothing) OrElse (Not (TypeOf List_SelectItems.DataSource Is RenaissanceDataClass.DSGroupItems.tblGroupItemsDataTable)) Then
              ListTable = New RenaissanceDataClass.DSGroupItems.tblGroupItemsDataTable
              ListTable.Columns.Add("ListDescription", GetType(String))
              ListTable.Columns.Add("PertracName", GetType(String))
              ListTable.Columns.Add("PertracProvider", GetType(String))
              ListTable.Columns.Add("IndexName", GetType(String))
              ListTable.Columns.Add("IndexProvider", GetType(String))

              List_SelectItems.DataSource = ListTable
            Else
              ListTable = CType(List_SelectItems.DataSource, RenaissanceDataClass.DSGroupItems.tblGroupItemsDataTable)
            End If

            List_SelectItems.DataSource = Nothing
            ListTable.Rows.Clear()
            ListTable.Load(tmpCommand.ExecuteReader)

            Try
              List_SelectItems.DisplayMember = "ListDescription"
              List_SelectItems.ValueMember = "GroupPertracCode"
            Catch ex As Exception
            End Try

            List_SelectItems.DataSource = ListTable
            List_SelectItems.ResumeLayout()

          Catch ex As Exception

            Call MainForm.LogError(Me.Name, 0, ex.Message, "Error building Select List", ex.StackTrace, True)

          Finally
            Try
              If (tmpCommand IsNot Nothing) AndAlso (tmpCommand.Connection IsNot Nothing) Then
                tmpCommand.Connection.Close()
                tmpCommand.Connection = Nothing
              End If
            Catch ex As Exception
            End Try
          End Try

        ElseIf Me.Radio_ExistingFunds.Checked Then

          List_SelectItems.SuspendLayout()

          Try
            If (List_SelectItems.DisplayMember <> "ListDescription") Then
              List_SelectItems.DataSource = Nothing
              List_SelectItems.DisplayMember = "ListDescription"
            End If

            If (List_SelectItems.ValueMember <> "PertracCode") Then
              List_SelectItems.DataSource = Nothing
              List_SelectItems.ValueMember = "PertracCode"
            End If
          Catch ex As Exception
          End Try

          ' List_SelectItems.DataSource = GetActivePertracInstruments(MainForm, False, CInt(Combo_SelectFrom.SelectedValue))
          List_SelectItems.DataSource = MainForm.PertracData.GetActivePertracInstruments(False, CInt(Combo_SelectFrom.SelectedValue))

          Try
            If (List_SelectItems.DisplayMember <> "ListDescription") Then
              List_SelectItems.DisplayMember = "ListDescription"
            End If

            If (List_SelectItems.ValueMember <> "PertracCode") Then
              List_SelectItems.ValueMember = "PertracCode"
            End If
          Catch ex As Exception
          End Try

          List_SelectItems.ResumeLayout()

        End If

      End If
    End If
  End Sub

  Private Sub Combo_SelectFrom_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Combo_SelectFrom.TextChanged
    ' ***********************************************************************************
    ' This Event handles the Select-As-You-Type functionality associated with the Pertrac Radio choice.
    '
    ' The List is managed by applying a select critera to the PertracInstruments DataView.
    ' ***********************************************************************************

    If (Me.Created) Then
      If (Combo_SelectFrom.SelectedIndex < 0) Then
        If Me.Radio_Pertrac.Checked Then

          List_SelectItems.SuspendLayout()

          ' Ensure the DataView is populated.
          If (PertracInstruments Is Nothing) Then
            PertracInstruments = MainForm.PertracData.GetPertracInstruments()
          End If

          ' Set Display and View Members

          Try
            If (List_SelectItems.DisplayMember <> "ListDescription") Then
              List_SelectItems.DataSource = Nothing
              List_SelectItems.DisplayMember = "ListDescription"
            End If

            If (List_SelectItems.ValueMember <> "PertracCode") Then
              List_SelectItems.DataSource = Nothing
              List_SelectItems.ValueMember = "PertracCode"
            End If
          Catch ex As Exception
          End Try

          ' Set Selection criteria.

          If (PertracInstruments.RowFilter <> "Mastername LIKE '" & Combo_SelectFrom.Text & "%'") Then
            PertracInstruments.RowFilter = "Mastername LIKE '" & Combo_SelectFrom.Text & "%'"
          End If

          ' Set DataSource

          If (List_SelectItems.DataSource IsNot PertracInstruments) Then
            List_SelectItems.DataSource = PertracInstruments
          End If

          ' Set Display and View Members (Double check).

          Try
            If (List_SelectItems.DisplayMember <> "ListDescription") Then
              List_SelectItems.DisplayMember = "ListDescription"
            End If

            If (List_SelectItems.ValueMember <> "PertracCode") Then
              List_SelectItems.ValueMember = "PertracCode"
            End If
          Catch ex As Exception
          End Try

          List_SelectItems.ResumeLayout()

        End If
      End If
    End If
  End Sub

  Private Sub Button_SelectAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_SelectAll.Click
    Dim IndexCounter As Integer

    Try
      List_SelectItems.SuspendLayout()

      For IndexCounter = (List_SelectItems.Items.Count - 1) To 0 Step -1
        If (List_SelectItems.SelectedIndices.Contains(IndexCounter) = False) Then
          List_SelectItems.SelectedIndices.Add(IndexCounter)
        End If
      Next
    Catch ex As Exception
    Finally
      List_SelectItems.ResumeLayout()
    End Try

  End Sub

  Private Sub Button_SelectNone_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_SelectNone.Click

    Try
      List_SelectItems.ClearSelected()
    Catch ex As Exception
    End Try

  End Sub

  Private Sub SetPertracCombo()

    Combo_CompareSeries.MasternameCollection = MainForm.MasternameDictionary
    Combo_CompareSeries.SelectNoMatch = False

  End Sub

#End Region

#Region " Buttons : Add / Delete / Cancel / Save and Close (Generic Code) "


	Private Sub btnRunReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRunReport.Click
		' *****************************************************************************
		' Run Report
		' *****************************************************************************

		Dim SelectedIDs(-1) As Integer
		Dim IndexCounter As Integer
		Dim FormControls As ArrayList = Nothing
		Dim ReferenceID As Integer = 0

		If (Me.List_SelectItems.SelectedItems.Count <= 0) Then
			Exit Sub
		End If

		If Me.Combo_CompareSeries.SelectedIndex > 0 Then
			Try
				ReferenceID = CInt(Combo_CompareSeries.SelectedValue)
			Catch ex As Exception
				ReferenceID = 0
			End Try
		End If

		Try
			FormControls = MainForm.DisableFormControls(Me)

			ReDim SelectedIDs(List_SelectItems.SelectedItems.Count - 1)

			For IndexCounter = 0 To (List_SelectItems.SelectedItems.Count - 1)
				SelectedIDs(IndexCounter) = List_SelectItems.SelectedItems(IndexCounter)(List_SelectItems.ValueMember)
			Next

			Me.StatusLabel1.Text = "Processing Report"
			Me.Refresh()
			Application.DoEvents()

			MainForm.MainReportHandler.InformationReport(SelectedIDs, ReferenceID, StatusLabel1)

		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error running the Information Report", ex.StackTrace, True)
		Finally
			MainForm.EnableFormControls(FormControls)
		End Try

		Me.StatusLabel1.Text = ""
		Me.btnRunReport.Enabled = True

	End Sub


	Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
		' *****************************************************************************
		' Close Form
		' *****************************************************************************

		Me.Close()

	End Sub

#End Region

#Region " Form Control Event Code"



#End Region




End Class
