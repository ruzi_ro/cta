if not exists (select * from dbo.syscolumns where (id = object_id(N'[dbo].[tblGroupItemData]')) and (OBJECTPROPERTY(id, N'IsUserTable') = 1) and (name='GroupScalingFactor'))
BEGIN
	ALTER TABLE [dbo].[tblGroupItemData] ADD 
	[GroupScalingFactor] [float] NOT NULL
	CONSTRAINT [DF_tblGroupItemData_GroupScalingFactor] DEFAULT (1)END
GO

if not exists (select * from dbo.syscolumns where (id = object_id(N'[dbo].[tblPertracCustomFields]')) and (OBJECTPROPERTY(id, N'IsUserTable') = 1) and (name='DefaultValue'))
BEGIN
	ALTER TABLE [dbo].[tblPertracCustomFields] ADD 
	[DefaultValue] [varchar](100) NOT NULL
	CONSTRAINT [DF_tblPertracCustomFields_DefaultValue] DEFAULT ('')END
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[fn_tblPertracCustomFieldData_CacheSelect]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[fn_tblPertracCustomFieldData_CacheSelect]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[fn_tblPertracCustomFieldData_CacheSelectGroupBackfill]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[fn_tblPertracCustomFieldData_CacheSelectGroupBackfill]
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE FUNCTION dbo.fn_tblPertracCustomFieldData_CacheSelect
	(
	@GroupID int, 
	@CustomFieldID int, 
	@KnowledgeDate datetime
	)
RETURNS TABLE
AS
	--
	-- Function to return Custon Field Data relating to the given Group.
	-- Note, this function will NOT backfill data for Non-Zero Group IDs with Data from Group Zero.
	-- Note also that @KnowledgeDate MUST NOT BE NULL.
	--
	-- NPP, Apr 2008.
	--
	RETURN 

	SELECT 	CustomFieldID,
		PertracID,
		GroupID,
		FieldNumericData,
		FieldTextData,
		FieldDateData, 
		FieldBooleanData
	FROM tblPertracCustomFieldData
	WHERE RN IN ( SELECT MAX(RN) 
		FROM tblPertracCustomFieldData 
		WHERE (GroupID = @GroupID) AND 
			((@CustomFieldID = 0) OR (CustomFieldID = @CustomFieldID)) AND
			((@KnowledgeDate <= '1 Jan 1900') OR (DateEntered <= @KnowledgeDate)) AND 
			(((DateDeleted IS NULL) OR (DateDeleted > @KnowledgeDate)) AND (NOT ((@KnowledgeDate <= '1 Jan 1900') AND (DateDeleted IS NOT NULL))))
		GROUP BY CustomFieldID, GroupID, PertracID)

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  SELECT  ON [dbo].[fn_tblPertracCustomFieldData_CacheSelect]  TO [public]
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE FUNCTION dbo.fn_tblPertracCustomFieldData_CacheSelectGroupBackfill
	(
	@GroupID int, 
	@CustomFieldID int, 
	@KnowledgeDate datetime
	)
RETURNS TABLE
AS
	--
	-- Function to return Custon Field Data relating to the given Group.
	-- Note, this function WILL return data for Group Zero AND the Requested Group.
	-- Note also that @KnowledgeDate MUST NOT BE NULL.
	--
	-- NPP, Apr 2008.
	--
	RETURN 

	SELECT 	CustomFieldID,
		PertracID,
		GroupID,
		FieldNumericData,
		FieldTextData,
		FieldDateData, 
		FieldBooleanData
	FROM tblPertracCustomFieldData
	WHERE RN IN ( SELECT MAX(RN) 
		FROM tblPertracCustomFieldData 
		WHERE ((GroupID = 0) OR (GroupID = @GroupID)) AND 
			((@CustomFieldID = 0) OR (CustomFieldID = @CustomFieldID)) AND
			(PertracID IN (SELECT GroupPertracCode FROM fn_tblGroupItems_SelectKD(@KnowledgeDate) WHERE GroupID = @GroupID)) AND 
			((@KnowledgeDate <= '1 Jan 1900') OR (DateEntered <= @KnowledgeDate)) AND 
			(((DateDeleted IS NULL) OR (DateDeleted > @KnowledgeDate)) AND (NOT ((@KnowledgeDate <= '1 Jan 1900') AND (DateDeleted IS NOT NULL))))
		GROUP BY CustomFieldID, GroupID, PertracID)

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  SELECT  ON [dbo].[fn_tblPertracCustomFieldData_CacheSelectGroupBackfill]  TO [public]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[fn_tblGroupItemData_SelectKD]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[fn_tblGroupItemData_SelectKD]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[fn_tblGroupsAggregated_SelectKD]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[fn_tblGroupsAggregated_SelectKD]
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

CREATE FUNCTION dbo.fn_tblGroupItemData_SelectKD 
(
@KnowledgeDate datetime
)
RETURNS @tblGroupItemData TABLE 
	(
 	RN int,
	AuditID int,
	GroupItemID int,
	GroupPertracCode int, 
	GroupLiquidity int,
	GroupHolding float,
	GroupNewHolding float,
	GroupPercent float,
	GroupExpectedReturn float,
	GroupUpperBound float, 
	GroupLowerBound float, 
	GroupFloor bit, 
	GroupCap bit, 
	GroupTradeSize float, 
	GroupBeta float, 
	GroupAlpha float, 
	GroupIndexE float, 
	GroupBetaE float, 
	GroupAlphaE float, 
	GroupStdErr float, 
	GroupScalingFactor float, 
	UserEntered varchar(20), 
	DateEntered datetime,
	DateDeleted datetime,
	PRIMARY KEY (GroupItemID)
	)
AS
	BEGIN
		INSERT INTO @tblGroupItemData
		SELECT 	[RN] ,
			[AuditID] ,
			[GroupItemID] ,
			[GroupPertracCode] , 
			[GroupLiquidity] ,
			[GroupHolding] ,
			[GroupNewHolding] ,
			[GroupPercent] ,
			[GroupExpectedReturn] ,
			[GroupUpperBound] , 
			[GroupLowerBound] , 
			[GroupFloor] , 
			[GroupCap] , 
			[GroupTradeSize] , 
			[GroupBeta] , 
			[GroupAlpha] , 
			[GroupIndexE] , 
			[GroupBetaE] , 
			[GroupAlphaE] , 
			[GroupStdErr] , 
			[GroupScalingFactor] , 
			[UserEntered] ,
			[DateEntered] ,
			[DateDeleted]
FROM tblGroupItemData
WHERE RN IN ( SELECT MAX(RN) 
              FROM tblGroupItemData 
              WHERE (((DateEntered <= @KnowledgeDate) or (DateEntered IS NULL)) OR (ISNULL(@KnowledgeDate, '1 Jan 1900') <= '1 Jan 1900')) AND 
                    (((DateDeleted > @KnowledgeDate) or (DateDeleted IS NULL)) AND (NOT ((ISNULL(@KnowledgeDate, '1 Jan 1900') <= '1 Jan 1900') AND (NOT (DateDeleted is NULL)))))
              GROUP BY GroupItemID )
		
	RETURN
	END


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  SELECT  ON [dbo].[fn_tblGroupItemData_SelectKD]  TO [public]
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


CREATE FUNCTION dbo.fn_tblGroupsAggregated_SelectKD 
(
	@GroupListID int = 0, 
	@KnowledgeDate datetime = Null
)
RETURNS TABLE
AS
RETURN 
	(
	SELECT     
		GList.GroupListID ,
		GList.GroupListName , 
		GList.GroupGroup , 
		GList.GroupDateFrom , 
		GList.GroupDateTo , 
		GItems.GroupItemID , 
		GItems.GroupPertracCode , 
		dbo.fn_GetMastername(GItems.GroupPertracCode) AS PertracName, 
		GItems.GroupIndexCode , 
		dbo.fn_GetMastername(GItems.GroupIndexCode) AS IndexName, 
		GItems.GroupSector , 
		ISNULL(GData.GroupLiquidity, 0) AS GroupLiquidity, 
		ISNULL(GData.GroupHolding,  0) AS GroupHolding, 
		ISNULL(GData.GroupNewHolding,  0) AS GroupNewHolding, 
		ISNULL(GData.GroupPercent,  0) AS GroupPercent, 
		ISNULL(GData.GroupExpectedReturn,  0) AS GroupExpectedReturn, 
		ISNULL(GData.GroupUpperBound,  0) AS GroupUpperBound, 
		ISNULL(GData.GroupLowerBound,  0) AS GroupLowerBound, 
		ISNULL(GData.GroupFloor,  0) AS GroupFloor, 
		ISNULL(GData.GroupCap,  0) AS GroupCap, 
		ISNULL(GData.GroupTradeSize,  0) AS GroupTradeSize, 
		ISNULL(GData.GroupBeta,  0) AS GroupBeta, 
		ISNULL(GData.GroupAlpha,  0) AS GroupAlpha, 
		ISNULL(GData.GroupIndexE,  0) AS GroupIndexE, 
		ISNULL(GData.GroupBetaE, 0) AS GroupBetaE, 
		ISNULL(GData.GroupAlphaE, 0) AS GroupAlphaE, 
		ISNULL(GData.GroupStdErr, 0) AS GroupStdErr, 
		ISNULL(GData.GroupScalingFactor, 1) As GroupScalingFactor
	FROM 
		fn_tblGroupItems_SelectKD(@KnowledgeDate) GItems LEFT JOIN
    fn_tblGroupItemData_SelectKD(@KnowledgeDate) GData ON (GItems.GroupItemID = GData.GroupItemID) AND (GItems.GroupPertracCode = GData.GroupPertracCode) LEFT JOIN
    fn_tblGroupList_SelectKD(@KnowledgeDate) GList ON GItems.GroupID = GList.GroupListID
  WHERE (GList.GroupListID = ISNULL(@GroupListID, 0)) OR (ISNULL(@GroupListID, 0) = 0)
	)                      	

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  SELECT  ON [dbo].[fn_tblGroupsAggregated_SelectKD]  TO [public]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[adp_tblGroupItemData_InsertCommand]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[adp_tblGroupItemData_InsertCommand]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[adp_tblGroupItemData_SelectCommand]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[adp_tblGroupItemData_SelectCommand]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[adp_tblGroupItemData_UpdateCommand]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[adp_tblGroupItemData_UpdateCommand]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[adp_tblGroupsAggregated_SelectCommand]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[adp_tblGroupsAggregated_SelectCommand]
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE PROCEDURE dbo.adp_tblGroupItemData_InsertCommand
	(
	@GroupItemID int,
	@GroupPertracCode int = 0,
	@GroupLiquidity int = 0,
	@GroupHolding float = 0,
	@GroupNewHolding float = 0,
	@GroupPercent float = 0,
	@GroupExpectedReturn float = 0,
	@GroupUpperBound float = 0, 
	@GroupLowerBound float = 0, 
	@GroupFloor bit = 0, 
	@GroupCap bit = 0, 
	@GroupTradeSize float = 0, 
	@GroupBeta float = 0, 
	@GroupAlpha float = 0, 
	@GroupIndexE float = 0, 
	@GroupBetaE float = 0, 
	@GroupAlphaE float = 0, 
	@GroupStdErr float = 0, 
	@GroupScalingFactor float = 1, 
	@KnowledgeDate datetime
	)
--
-- (adp) Adaptor, tblGroupItemData procedure
--
-- Controlled Insert of a new Genoa Group Data Item to the Venice (Renaissance) database
--
-- 
AS
	
	SET NOCOUNT OFF

	SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
	
	BEGIN TRANSACTION
		  
	-- Insert new record.
	INSERT INTO tblGroupItemData(
		[GroupItemID] ,
		[GroupPertracCode] , 
		[GroupLiquidity] ,
		[GroupHolding] ,
		[GroupNewHolding] ,
		[GroupPercent] ,
		[GroupExpectedReturn] ,
		[GroupUpperBound] , 
		[GroupLowerBound] , 
		[GroupFloor] , 
		[GroupCap] , 
		[GroupTradeSize] , 
		[GroupBeta] , 
		[GroupAlpha] , 
		[GroupIndexE] , 
		[GroupBetaE] , 
		[GroupAlphaE] , 
		[GroupStdErr] ,
		[GroupScalingFactor]
		)
	VALUES (
		@GroupItemID ,
		@GroupPertracCode , 
		@GroupLiquidity ,
		@GroupHolding ,
		@GroupNewHolding ,
		@GroupPercent ,
		@GroupExpectedReturn ,
		@GroupUpperBound , 
		@GroupLowerBound , 
		@GroupFloor , 
		@GroupCap , 
		@GroupTradeSize , 
		@GroupBeta , 
		@GroupAlpha , 
		@GroupIndexE , 
		@GroupBetaE , 
		@GroupAlphaE , 
		@GroupStdErr , 
		@GroupScalingFactor
		)
	
	-- Select Newly created record

	SELECT 	[RN] ,
		[AuditID] ,
		[GroupItemID] ,
		[GroupPertracCode] , 
		[GroupLiquidity] ,
		[GroupHolding] ,
		[GroupNewHolding] ,
		[GroupPercent] ,
		[GroupExpectedReturn] ,
		[GroupUpperBound] , 
		[GroupLowerBound] , 
		[GroupFloor] , 
		[GroupCap] , 
		[GroupTradeSize] , 
		[GroupBeta] , 
		[GroupAlpha] , 
		[GroupIndexE] , 
		[GroupBetaE] , 
		[GroupAlphaE] , 
		[GroupStdErr] , 
		[GroupScalingFactor],
		[UserEntered] ,
		[DateEntered] ,
		[DateDeleted] 
		FROM tblGroupItemData
	WHERE (RN = SCOPE_IDENTITY())


	IF @@ERROR = 0
	  BEGIN
	    COMMIT TRANSACTION
	    RETURN SCOPE_IDENTITY()
	  END
	ELSE
	  BEGIN
	    ROLLBACK TRANSACTION
	    RETURN 0
	  END

	RETURN 0


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[adp_tblGroupItemData_InsertCommand]  TO [InvestMaster_Manager]
GO

GRANT  EXECUTE  ON [dbo].[adp_tblGroupItemData_InsertCommand]  TO [InvestMaster_Admin]
GO

GRANT  EXECUTE  ON [dbo].[adp_tblGroupItemData_InsertCommand]  TO [Genoa_GroupEdit]
GO

GRANT  EXECUTE  ON [dbo].[adp_tblGroupItemData_InsertCommand]  TO [Genoa_Manager]
GO

GRANT  EXECUTE  ON [dbo].[adp_tblGroupItemData_InsertCommand]  TO [Genoa_Admin]
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE PROCEDURE dbo.adp_tblGroupItemData_SelectCommand
(
@KnowledgeDate datetime = Null
)
AS

SELECT 	[RN] ,
	[AuditID] ,
	[GroupItemID] ,
	[GroupPertracCode] ,
	[GroupLiquidity] ,
	[GroupHolding] ,
	[GroupNewHolding] ,
	[GroupPercent] ,
	[GroupExpectedReturn] ,
	[GroupUpperBound] , 
	[GroupLowerBound] , 
	[GroupFloor] , 
	[GroupCap] , 
	[GroupTradeSize] , 
	[GroupBeta] , 
	[GroupAlpha] , 
	[GroupIndexE] , 
	[GroupBetaE] , 
	[GroupAlphaE] , 
	[GroupStdErr] , 
	[GroupScalingFactor] , 
	[UserEntered] , 
	[DateEntered] ,
	[DateDeleted]
FROM fn_tblGroupItemData_SelectKD(@KnowledgeDate)
	
RETURN -1



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[adp_tblGroupItemData_SelectCommand]  TO [InvestMaster_Sales]
GO

GRANT  EXECUTE  ON [dbo].[adp_tblGroupItemData_SelectCommand]  TO [InvestMaster_Manager]
GO

GRANT  EXECUTE  ON [dbo].[adp_tblGroupItemData_SelectCommand]  TO [InvestMaster_Admin]
GO

GRANT  EXECUTE  ON [dbo].[adp_tblGroupItemData_SelectCommand]  TO [Genoa_Read]
GO

GRANT  EXECUTE  ON [dbo].[adp_tblGroupItemData_SelectCommand]  TO [Genoa_GroupEdit]
GO

GRANT  EXECUTE  ON [dbo].[adp_tblGroupItemData_SelectCommand]  TO [Genoa_Manager]
GO

GRANT  EXECUTE  ON [dbo].[adp_tblGroupItemData_SelectCommand]  TO [Genoa_Admin]
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE PROCEDURE dbo.adp_tblGroupItemData_UpdateCommand
	(
	@GroupItemID int,
	@GroupPertracCode int = 0,
	@GroupLiquidity int = 0,
	@GroupHolding float = 0,
	@GroupNewHolding float = 0,
	@GroupPercent float = 0,
	@GroupExpectedReturn float = 0,
	@GroupUpperBound float = 0, 
	@GroupLowerBound float = 0, 
	@GroupFloor bit = 0, 
	@GroupCap bit = 0, 
	@GroupTradeSize float = 0, 
	@GroupBeta float = 0, 
	@GroupAlpha float = 0, 
	@GroupIndexE float = 0, 
	@GroupBetaE float = 0, 
	@GroupAlphaE float = 0, 
	@GroupStdErr float = 0, 
	@GroupScalingFactor float = 1, 
	@KnowledgeDate datetime
	)
AS
	SET NOCOUNT OFF

	DECLARE @RVal int 

	IF ((@Knowledgedate >= (getdate())) OR (ISNULL(@Knowledgedate, '1 Jan 1900') <= '1 Jan 1900')) AND 
		(ISNULL(@GroupItemID, 0) > 0)
	  BEGIN
		EXECUTE @RVal = [dbo].[adp_tblGroupItemData_InsertCommand] 
			@GroupItemID = @GroupItemID,
			@GroupPertracCode = @GroupPertracCode, 
			@GroupLiquidity = @GroupLiquidity,
			@GroupHolding = @GroupHolding,
			@GroupNewHolding = @GroupNewHolding,
			@GroupPercent = @GroupPercent,
			@GroupExpectedReturn = @GroupExpectedReturn,
			@GroupUpperBound = @GroupUpperBound, 
			@GroupLowerBound = @GroupLowerBound, 
			@GroupFloor = @GroupFloor, 
			@GroupCap = @GroupCap, 
			@GroupTradeSize = @GroupTradeSize, 
			@GroupBeta = @GroupBeta, 
			@GroupAlpha = @GroupAlpha, 
			@GroupIndexE = @GroupIndexE, 
			@GroupBetaE = @GroupBetaE, 
			@GroupAlphaE = @GroupAlphaE, 
			@GroupStdErr = @GroupStdErr, 
			@GroupScalingFactor = @GroupScalingFactor, 
			@KnowledgeDate = @KnowledgeDate
			
		RETURN @RVal
	  END
	ELSE
	  BEGIN
		SELECT 	[RN] ,
			[AuditID] ,
			[GroupItemID] ,
			[GroupPertracCode] , 
			[GroupLiquidity] ,
			[GroupHolding] ,
			[GroupNewHolding] ,
			[GroupPercent] ,
			[GroupExpectedReturn] ,
			[GroupUpperBound] , 
			[GroupLowerBound] , 
			[GroupFloor] , 
			[GroupCap] , 
			[GroupTradeSize] , 
			[GroupBeta] , 
			[GroupAlpha] , 
			[GroupIndexE] , 
			[GroupBetaE] , 
			[GroupAlphaE] , 
			[GroupStdErr] , 
			[GroupScalingFactor] ,
			[UserEntered] , 
			[DateEntered] ,
			[DateDeleted]
		FROM fn_tblGroupItemData_SelectKD(@KnowledgeDate)
		WHERE (GroupItemID = @GroupItemID)
		
		SELECT @RVal = @GroupItemID
	  END
	  
	IF @@ERROR = 0
	  BEGIN
	    RETURN @RVal
	  END
	ELSE
	  BEGIN
	    RETURN 0
	  END

	RETURN 0




GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[adp_tblGroupItemData_UpdateCommand]  TO [InvestMaster_Manager]
GO

GRANT  EXECUTE  ON [dbo].[adp_tblGroupItemData_UpdateCommand]  TO [InvestMaster_Admin]
GO

GRANT  EXECUTE  ON [dbo].[adp_tblGroupItemData_UpdateCommand]  TO [Genoa_GroupEdit]
GO

GRANT  EXECUTE  ON [dbo].[adp_tblGroupItemData_UpdateCommand]  TO [Genoa_Manager]
GO

GRANT  EXECUTE  ON [dbo].[adp_tblGroupItemData_UpdateCommand]  TO [Genoa_Admin]
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


CREATE PROCEDURE dbo.adp_tblGroupsAggregated_SelectCommand
(
	@GroupListID int = 0, 
	@KnowledgeDate datetime = Null
)
AS
	SET NOCOUNT ON
	
	SELECT     
		GroupListID ,
		GroupListName , 
		GroupGroup , 
		GroupDateFrom , 
    GroupDateTo , 
    GroupItemID , 
    GroupPertracCode , 
    ISNULL(PertracName, '') AS PertracName, 
    GroupIndexCode , 
    ISNULL(IndexName, '') AS IndexName , 
    GroupSector , 
    GroupLiquidity, 
    GroupHolding, 
    0.0 AS GroupTrade, 
    GroupNewHolding, 
    GroupPercent, 
    GroupExpectedReturn, 
    GroupUpperBound, 
    GroupLowerBound, 
    GroupFloor, 
    GroupCap, 
    GroupTradeSize, 
    GroupBeta, 
    GroupAlpha, 
    GroupIndexE, 
    GroupBetaE,
    GroupAlphaE, 
    GroupStdErr, 
    GroupScalingFactor
	FROM 
		fn_tblGroupsAggregated_SelectKD(@GroupListID, @KnowledgeDate)
                      
	RETURN


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[adp_tblGroupsAggregated_SelectCommand]  TO [InvestMaster_Sales]
GO

GRANT  EXECUTE  ON [dbo].[adp_tblGroupsAggregated_SelectCommand]  TO [Genoa_Read]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[fn_tblPertracCustomFields_SelectKD]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[fn_tblPertracCustomFields_SelectKD]
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO



CREATE FUNCTION dbo.fn_tblPertracCustomFields_SelectKD
	(
	@KnowledgeDate datetime
	)
RETURNS TABLE
AS
RETURN
(
SELECT 	[RN] ,
				[AuditID] ,
				[FieldID],
				[FieldName],
				[CustomFieldType],
				[StartDate], 
				[EndDate], 
				[IsMaxValue], 
				[IsMinValue], 
				[IsFirstValue], 
				[IsLastValue], 
				[IsAverageValue], 
				[IsIRR], 
				[FieldPeriodCount], 
				[FieldIsVolatile],
				[FieldIsCalculated],
				[FieldIsSearchable],
				[FieldIsOptimisable],
				[FieldDataType],
				[FieldDetails],
				[DefaultValue], 
				[UserEntered], 
				[DateEntered],
				[DateDeleted]
FROM tblPertracCustomFields
WHERE RN IN ( SELECT MAX(RN) 
              FROM tblPertracCustomFields 
              WHERE (((DateEntered <= @KnowledgeDate) or (DateEntered IS NULL)) OR (ISNULL(@KnowledgeDate, '1 Jan 1900') <= '1 Jan 1900')) AND 
                    (((DateDeleted > @KnowledgeDate) or (DateDeleted IS NULL)) AND (NOT ((ISNULL(@KnowledgeDate, '1 Jan 1900') <= '1 Jan 1900') AND (NOT (DateDeleted is NULL)))))
              GROUP BY [FieldID] )
)



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  SELECT  ON [dbo].[fn_tblPertracCustomFields_SelectKD]  TO [InvestMaster_Sales]
GO

GRANT  SELECT  ON [dbo].[fn_tblPertracCustomFields_SelectKD]  TO [Naples_Role]
GO

GRANT  SELECT  ON [dbo].[fn_tblPertracCustomFields_SelectKD]  TO [Genoa_Admin]
GO

GRANT  SELECT  ON [dbo].[fn_tblPertracCustomFields_SelectKD]  TO [Genoa_Manager]
GO

GRANT  SELECT  ON [dbo].[fn_tblPertracCustomFields_SelectKD]  TO [Genoa_Read]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[adp_tblPertracCustomFields_InsertCommand]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[adp_tblPertracCustomFields_InsertCommand]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[adp_tblPertracCustomFields_SelectCommand]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[adp_tblPertracCustomFields_SelectCommand]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[adp_tblPertracCustomFields_UpdateCommand]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[adp_tblPertracCustomFields_UpdateCommand]
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO



CREATE PROCEDURE dbo.adp_tblPertracCustomFields_InsertCommand
	(
	@FieldID int,
	@FieldName varchar (100),
	@CustomFieldType int,
	@StartDate datetime, 
	@EndDate datetime, 
	@IsMaxValue bit, 
	@IsMinValue bit, 
	@IsFirstValue bit, 
	@IsLastValue bit, 
	@IsAverageValue bit, 
	@IsIRR bit = 0, 
	@FieldPeriodCount int, 
	@FieldIsVolatile bit,
	@FieldIsCalculated bit,
	@FieldIsSearchable bit,
	@FieldIsOptimisable bit,
	@FieldDataType int,
	@FieldDetails varchar (500),
	@DefaultValue varchar (100),
	@KnowledgeDate datetime
	)
--
-- (adp) Adaptor, tblPertracCustomFields procedure
--
-- Controlled Insert of a new Custom Field to the Venice (Renaissance) database
--
-- 
AS
	
	DECLARE @New_FieldID int 

	SET NOCOUNT OFF

	SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
	
	BEGIN TRANSACTION

	-- Establish FieldID to use. Existing or New one.
		
	IF ISNULL(@FieldID, 0) > 0 
	  BEGIN
	  SELECT @New_FieldID = @FieldID
	  END
	ELSE
	  BEGIN
	  SELECT @New_FieldID = (ISNULL(MAX(FieldID), 0)+1) FROM tblPertracCustomFields
	  END
		  
	-- Insert new record.
	INSERT INTO tblPertracCustomFields(
		FieldID ,
		FieldName,
		CustomFieldType,
		StartDate, 
		EndDate, 
		IsMaxValue, 
		IsMinValue, 
		IsFirstValue, 
		IsLastValue, 
		IsAverageValue, 
		IsIRR, 
		FieldPeriodCount, 
		FieldIsVolatile,
		FieldIsCalculated,
		FieldIsSearchable,
		FieldIsOptimisable,
		FieldDataType,
		FieldDetails,
		DefaultValue
		)
	VALUES (
		@New_FieldID, 
		@FieldName,
		@CustomFieldType,
		@StartDate, 
		@EndDate, 
		@IsMaxValue, 
		@IsMinValue, 
		@IsFirstValue, 
		@IsLastValue, 
		@IsAverageValue, 
		@IsIRR, 
		@FieldPeriodCount, 
		@FieldIsVolatile,
		@FieldIsCalculated,
		@FieldIsSearchable,
		@FieldIsOptimisable,
		@FieldDataType,
		@FieldDetails,
		ISNULL(@DefaultValue, '')
		)
	
	-- Select Newly created record

		SELECT 	[RN] ,
						[AuditID] ,
						[FieldID],
						[FieldName],
						[CustomFieldType],
						[StartDate], 
						[EndDate], 
						[IsMaxValue], 
						[IsMinValue], 
						[IsFirstValue], 
						[IsLastValue], 
						[IsAverageValue], 
						[IsIRR], 
						[FieldPeriodCount], 
						[FieldIsVolatile],
						[FieldIsCalculated],
						[FieldIsSearchable],
						[FieldIsOptimisable],
						[FieldDataType],
						[FieldDetails],
						[DefaultValue],
						[UserEntered], 
						[DateEntered],
						[DateDeleted]
	FROM tblPertracCustomFields
	WHERE (RN = SCOPE_IDENTITY())


	IF @@ERROR = 0
	  BEGIN
	    COMMIT TRANSACTION
	    RETURN @New_FieldID
	  END
	ELSE
	  BEGIN
	    ROLLBACK TRANSACTION
	    RETURN 0
	  END

	RETURN 0




GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[adp_tblPertracCustomFields_InsertCommand]  TO [Genoa_Admin]
GO

GRANT  EXECUTE  ON [dbo].[adp_tblPertracCustomFields_InsertCommand]  TO [Genoa_Manager]
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO



CREATE PROCEDURE dbo.adp_tblPertracCustomFields_SelectCommand
(
@KnowledgeDate datetime = Null
)
AS

SELECT 	[RN] ,
				[AuditID] ,
				[FieldID],
				[FieldName],
				[CustomFieldType],
				[StartDate], 
				[EndDate], 
				[IsMaxValue], 
				[IsMinValue], 
				[IsFirstValue], 
				[IsLastValue], 
				[IsAverageValue], 
				[IsIRR], 
				[FieldPeriodCount], 
				[FieldIsVolatile],
				[FieldIsCalculated],
				[FieldIsSearchable],
				[FieldIsOptimisable],
				[FieldDataType],
				[FieldDetails],
				[DefaultValue], 
				[UserEntered], 
				[DateEntered],
				[DateDeleted]
FROM fn_tblPertracCustomFields_SelectKD(@KnowledgeDate)
	
RETURN -1




GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[adp_tblPertracCustomFields_SelectCommand]  TO [InvestMaster_Sales]
GO

GRANT  EXECUTE  ON [dbo].[adp_tblPertracCustomFields_SelectCommand]  TO [Genoa_Admin]
GO

GRANT  EXECUTE  ON [dbo].[adp_tblPertracCustomFields_SelectCommand]  TO [Genoa_Manager]
GO

GRANT  EXECUTE  ON [dbo].[adp_tblPertracCustomFields_SelectCommand]  TO [Genoa_Read]
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO



CREATE PROCEDURE dbo.adp_tblPertracCustomFields_UpdateCommand
	(
	@FieldID int,
	@FieldName varchar (100),
	@CustomFieldType int,
	@StartDate datetime, 
	@EndDate datetime, 
	@IsMaxValue bit, 
	@IsMinValue bit, 
	@IsFirstValue bit, 
	@IsLastValue bit, 
	@IsAverageValue bit, 
	@IsIRR bit = 0, 
	@FieldPeriodCount int, 
	@FieldIsVolatile bit,
	@FieldIsCalculated bit,
	@FieldIsSearchable bit,
	@FieldIsOptimisable bit,
	@FieldDataType int,
	@FieldDetails varchar (500),
	@DefaultValue varchar (100),
	@KnowledgeDate datetime
	)
AS
	SET NOCOUNT OFF

	DECLARE @RVal int 

	IF ((@Knowledgedate >= (getdate())) OR (ISNULL(@Knowledgedate, '1 Jan 1900') <= '1 Jan 1900')) AND 
		(ISNULL(@FieldID, 0) > 0)
	  BEGIN
		EXECUTE @RVal = [dbo].[adp_tblPertracCustomFields_InsertCommand] 
			@FieldID = @FieldID,
			@FieldName = @FieldName,
			@CustomFieldType = @CustomFieldType,
			@StartDate = @StartDate, 
			@EndDate = @EndDate, 
			@IsMaxValue = @IsMaxValue, 
			@IsMinValue = @IsMinValue, 
			@IsFirstValue = @IsFirstValue, 
			@IsLastValue = @IsLastValue, 
			@IsAverageValue = @IsAverageValue, 
			@IsIRR = @IsIRR, 
			@FieldPeriodCount = @FieldPeriodCount, 
			@FieldIsVolatile = @FieldIsVolatile,
			@FieldIsCalculated = @FieldIsCalculated,
			@FieldIsSearchable = @FieldIsSearchable,
			@FieldIsOptimisable = @FieldIsOptimisable,
			@FieldDataType = @FieldDataType,
			@FieldDetails = @FieldDetails,
			@DefaultValue = @DefaultValue, 
			@KnowledgeDate = @KnowledgeDate
			
		RETURN @RVal
	  END
	ELSE
	  BEGIN
		SELECT 	[RN] ,
						[AuditID] ,
						[FieldID],
						[FieldName],
						[CustomFieldType],
						[StartDate], 
						[EndDate], 
						[IsMaxValue], 
						[IsMinValue], 
						[IsFirstValue], 
						[IsLastValue], 
						[IsAverageValue], 
						[IsIRR], 
						[FieldPeriodCount], 
						[FieldIsVolatile],
						[FieldIsCalculated],
						[FieldIsSearchable],
						[FieldIsOptimisable],
						[FieldDataType],
						[FieldDetails],
						[DefaultValue], 
						[UserEntered], 
						[DateEntered],
						[DateDeleted]
		FROM fn_tblPertracCustomFields_SelectKD(@KnowledgeDate)
		WHERE (FieldID = @FieldID)
		
		SELECT @RVal = @FieldID
	  END
	  
	IF @@ERROR = 0
	  BEGIN
	    RETURN @RVal
	  END
	ELSE
	  BEGIN
	    RETURN 0
	  END

	RETURN 0






GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[adp_tblPertracCustomFields_UpdateCommand]  TO [Genoa_Admin]
GO

GRANT  EXECUTE  ON [dbo].[adp_tblPertracCustomFields_UpdateCommand]  TO [Genoa_Manager]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[adp_tblGroupItems_SelectGroupPertracCodes]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[adp_tblGroupItems_SelectGroupPertracCodes]
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO



CREATE PROCEDURE dbo.adp_tblGroupItems_SelectGroupPertracCodes
(
@GroupID int = 0,
@KnowledgeDate datetime = Null
)
AS

DECLARE @PertracCodes TABLE
	(
	RN int IDENTITY (1, 1),
	PertracCode int PRIMARY KEY
	)

DECLARE @OldPertracCodes TABLE
	(
	RN int ,
	PertracCode int PRIMARY KEY
	)

DECLARE @NewPertracCodes TABLE
	(
	RN int ,
	PertracCode int PRIMARY KEY
	)

	
IF (@GroupID = 0)
	BEGIN
	
	INSERT INTO @PertracCodes(PertracCode)
	SELECT GroupPertracCode
	FROM fn_tblGroupItems_SelectKD(@KnowledgeDate)
	WHERE ISNULL(@GroupID, 0) IN (0, GroupID)
	GROUP BY GroupPertracCode
		
	INSERT INTO @OldPertracCodes(RN, PertracCode)
	SELECT RN, PertracCode
	FROM @PertracCodes
	WHERE PertracCode < 1048576

	INSERT INTO @NewPertracCodes(RN, PertracCode)
	SELECT RN, (PertracCode - 1048576)
	FROM @PertracCodes
	WHERE PertracCode >= 1048576
	
	SELECT 	Groups.RN AS RN,
			0 AS AuditID, 
			0 AS GroupID,
			0 AS GroupItemID,
			Groups.PertracCode AS GroupPertracCode,
			0 AS GroupIndexCode,
			Mastername.Mastername AS PertracName, 
			Information.DataVendorName AS PertracProvider, 
			(Mastername.Mastername + ' (' + Information.DataVendorName + ')') AS ListDescription, 
			'' AS GroupSector,
			'' AS UserEntered, 
			getdate() AS DateEntered,
			Null AS DateDeleted
	FROM	@OldPertracCodes Groups LEFT OUTER JOIN
			MASTERSQL.dbo.Mastername Mastername ON (Groups.PertracCode = Mastername.ID) LEFT OUTER JOIN
			MASTERSQL.dbo.Information Information ON (Groups.PertracCode = Information.ID)
	UNION
	SELECT 	Groups.RN AS RN,
			0 AS AuditID, 
			0 AS GroupID,
			0 AS GroupItemID,
			(Groups.PertracCode + 1048576) AS GroupPertracCode,
			0 AS GroupIndexCode,
			Mastername.InstrumentName AS PertracName, 
			Information.DataVendorName AS PertracProvider, 
			(Mastername.InstrumentName + ' (' + Information.DataVendorName + ')') AS ListDescription, 
			'' AS GroupSector,
			'' AS UserEntered, 
			getdate() AS DateEntered,
			Null AS DateDeleted
	FROM	@NewPertracCodes Groups LEFT OUTER JOIN
			[MASTERSQL].[dbo].[tblInstrumentList] Mastername ON (Groups.PertracCode = Mastername.InstrumentID) LEFT OUTER JOIN
			[MASTERSQL].[dbo].[tblInstrumentInformation] Information ON (Groups.PertracCode = Information.InstrumentID)
	ORDER BY PertracName
	
	END
ELSE
	BEGIN
	
	SELECT 	Groups.RN AS RN,
			Groups.AuditID, 
			Groups.GroupID AS GroupID,
			Groups.GroupItemID,
			Groups.GroupPertracCode AS GroupPertracCode,
			Groups.GroupIndexCode,
			Mastername.Mastername AS PertracName, 
			Information.DataVendorName AS PertracProvider, 
			(Mastername.Mastername + ' (' + Information.DataVendorName + ')') AS ListDescription, 
			Groups.GroupSector,
			Groups.UserEntered, 
			getdate() AS DateEntered,
			Null AS DateDeleted
	FROM	fn_tblGroupItems_SelectKD(@KnowledgeDate) Groups LEFT OUTER JOIN
			MASTERSQL.dbo.Mastername Mastername ON (Groups.GroupPertracCode = Mastername.ID) LEFT OUTER JOIN
			MASTERSQL.dbo.Information Information ON (Groups.GroupPertracCode = Information.ID)
	WHERE (@GroupID =  Groups.GroupID) AND (Groups.GroupPertracCode < 1048576)
	UNION
	SELECT 	Groups.RN AS RN,
			Groups.AuditID, 
			Groups.GroupID AS GroupID,
			Groups.GroupItemID,
			Groups.GroupPertracCode AS GroupPertracCode,
			Groups.GroupIndexCode,
			Mastername.InstrumentName AS PertracName, 
			Information.DataVendorName AS PertracProvider, 
			(Mastername.InstrumentName + ' (' + Information.DataVendorName + ')') AS ListDescription, 
			Groups.GroupSector,
			Groups.UserEntered, 
			getdate() AS DateEntered,
			Null AS DateDeleted
	FROM	fn_tblGroupItems_SelectKD(@KnowledgeDate) Groups LEFT OUTER JOIN
			[MASTERSQL].[dbo].[tblInstrumentList] Mastername ON ((Groups.GroupPertracCode - 1048576) = Mastername.InstrumentID) LEFT OUTER JOIN
			[MASTERSQL].[dbo].[tblInstrumentInformation] Information ON ((Groups.GroupPertracCode - 1048576) = Information.InstrumentID)
	WHERE (@GroupID =  Groups.GroupID) AND (Groups.GroupPertracCode >= 1048576)
	ORDER BY GroupID, PertracName
	
	END

RETURN -1




GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[adp_tblGroupItems_SelectGroupPertracCodes]  TO [InvestMaster_Sales]
GO

GRANT  EXECUTE  ON [dbo].[adp_tblGroupItems_SelectGroupPertracCodes]  TO [InvestMaster_Manager]
GO

GRANT  EXECUTE  ON [dbo].[adp_tblGroupItems_SelectGroupPertracCodes]  TO [InvestMaster_Admin]
GO

GRANT  EXECUTE  ON [dbo].[adp_tblGroupItems_SelectGroupPertracCodes]  TO [Genoa_Admin]
GO

GRANT  EXECUTE  ON [dbo].[adp_tblGroupItems_SelectGroupPertracCodes]  TO [Genoa_GroupEdit]
GO

GRANT  EXECUTE  ON [dbo].[adp_tblGroupItems_SelectGroupPertracCodes]  TO [Genoa_Manager]
GO

GRANT  EXECUTE  ON [dbo].[adp_tblGroupItems_SelectGroupPertracCodes]  TO [Genoa_Read]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[fn_ActiveFundPertracInstruments]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[fn_ActiveFundPertracInstruments]
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO



CREATE FUNCTION dbo.fn_ActiveFundPertracInstruments
(
	@FundID int,
	@ValueDate datetime,
	@KnowledgeDate datetime
)
RETURNS 
	@ReturnTable TABLE 
		(
		PertracCode int Primary Key, 
		Mastername varchar(255), 
		DataVendorName varchar(80)
		)
AS
BEGIN
	DECLARE @PertracCodes TABLE
		(
		PertracCode int Primary Key
		)

	INSERT INTO @PertracCodes
	SELECT tblInst.InstrumentPertracCode
	FROM fn_ActiveFundInstruments(@FundID, @ValueDate, @KnowledgeDate) ActiveInst LEFT JOIN
		fn_tblInstrument_SelectKD(@KnowledgeDate) tblInst ON (ActiveInst.Instrument = tblInst.InstrumentID)
	WHERE (tblInst.InstrumentPertracCode > 0)
	GROUP BY tblInst.InstrumentPertracCode
	
	
	INSERT INTO @ReturnTable(PertracCode, Mastername, DataVendorName)
	SELECT
		PertracCodes.PertracCode, 
		Mastername.Mastername, 
		Information.DataVendorName
	FROM @PertracCodes PertracCodes LEFT JOIN 
		[MASTERSQL].[dbo].[Mastername] Mastername ON (PertracCodes.PertracCode = Mastername.ID) LEFT JOIN 
		[MASTERSQL].[dbo].[Information] Information ON (PertracCodes.PertracCode = Information.ID)
	WHERE (PertracCodes.PertracCode < 1048576)
	
	INSERT INTO @ReturnTable(PertracCode, Mastername, DataVendorName)
	SELECT
		PertracCodes.PertracCode, 
		Mastername.InstrumentName, 
		Information.DataVendorName
	FROM @PertracCodes PertracCodes LEFT JOIN 
		[MASTERSQL].[dbo].[tblInstrumentList] Mastername ON ((PertracCodes.PertracCode - 1048576) = Mastername.InstrumentID) LEFT JOIN 
		[MASTERSQL].[dbo].[tblInstrumentInformation] Information ON ((PertracCodes.PertracCode - 1048576) = Information.InstrumentID)
	WHERE (PertracCodes.PertracCode >= 1048576)

RETURN
END



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[fn_GetMastername]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[fn_GetMastername]
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO



CREATE FUNCTION dbo.fn_GetMastername
	(
	@ID int
	)
RETURNS varchar(255)
AS
	BEGIN
	DECLARE @RVal as varchar(255)
	
	IF (@ID < 1048576)
		SELECT @RVal = (SELECT TOP 1 ISNULL(Mastername, '') FROM [MASTERSQL].[dbo].[Mastername] Mastername WHERE Mastername.ID = ISNULL(@ID, 0))
	ELSE
		SELECT @RVal = (SELECT TOP 1 ISNULL(InstrumentName, '') FROM [MASTERSQL].[dbo].[tblInstrumentList] Mastername WHERE Mastername.InstrumentID = (ISNULL(@ID, 0) - 1048576))

	RETURN @RVal
	END


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[fn_GetMastername]  TO [InvestMaster_Sales]
GO

GRANT  EXECUTE  ON [dbo].[fn_GetMastername]  TO [Naples_Role]
GO

GRANT  EXECUTE  ON [dbo].[fn_GetMastername]  TO [Florence_Read]
GO

GRANT  EXECUTE  ON [dbo].[fn_GetMastername]  TO [Genoa_Read]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[adp_tblGroupItems_SelectGroup]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[adp_tblGroupItems_SelectGroup]
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO



CREATE PROCEDURE dbo.adp_tblGroupItems_SelectGroup
(
@GroupID int = 0,
@KnowledgeDate datetime = Null
)
AS

	SELECT 	Groups.RN AS RN,
			Groups.AuditID, 
			Groups.GroupID AS GroupID,
			Groups.GroupItemID,
			Groups.GroupPertracCode,
			Groups.GroupIndexCode,
			Mastername.Mastername AS PertracName, 
			Information.DataVendorName AS PertracProvider, 
			(Mastername.Mastername + ' (' + Information.DataVendorName + ')') AS ListDescription, 
			Groups.GroupSector,
			Groups.UserEntered, 
			Groups.DateEntered,
			Null AS DateDeleted
	FROM	fn_tblGroupItems_SelectKD(@KnowledgeDate) Groups LEFT OUTER JOIN
			MASTERSQL.dbo.Mastername Mastername ON (Groups.GroupPertracCode = Mastername.ID) LEFT OUTER JOIN
			MASTERSQL.dbo.Information Information ON (Groups.GroupPertracCode = Information.ID)
	WHERE (ISNULL(@GroupID, 0) IN (Groups.GroupID , 0)) AND (Groups.GroupPertracCode < 1048576)
	UNION
	SELECT 	Groups.RN AS RN,
			Groups.AuditID, 
			Groups.GroupID AS GroupID,
			Groups.GroupItemID,
			Groups.GroupPertracCode,
			Groups.GroupIndexCode,
			Mastername.InstrumentName AS PertracName, 
			Information.DataVendorName AS PertracProvider, 
			(Mastername.InstrumentName + ' (' + Information.DataVendorName + ')') AS ListDescription, 
			Groups.GroupSector,
			Groups.UserEntered, 
			Groups.DateEntered,
			Null AS DateDeleted
	FROM	fn_tblGroupItems_SelectKD(@KnowledgeDate) Groups LEFT OUTER JOIN
			[MASTERSQL].[dbo].[tblInstrumentList] Mastername ON ((Groups.GroupPertracCode - 1048576) = Mastername.InstrumentID) LEFT OUTER JOIN
			[MASTERSQL].[dbo].[tblInstrumentInformation] Information ON ((Groups.GroupPertracCode - 1048576) = Information.InstrumentID)
	WHERE (ISNULL(@GroupID, 0) IN (Groups.GroupID , 0)) AND (Groups.GroupPertracCode >= 1048576)
	ORDER BY GroupID, PertracName
	
RETURN -1




GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[adp_tblGroupItems_SelectGroup]  TO [InvestMaster_Sales]
GO

GRANT  EXECUTE  ON [dbo].[adp_tblGroupItems_SelectGroup]  TO [InvestMaster_Manager]
GO

GRANT  EXECUTE  ON [dbo].[adp_tblGroupItems_SelectGroup]  TO [InvestMaster_Admin]
GO

GRANT  EXECUTE  ON [dbo].[adp_tblGroupItems_SelectGroup]  TO [Genoa_Admin]
GO

GRANT  EXECUTE  ON [dbo].[adp_tblGroupItems_SelectGroup]  TO [Genoa_GroupEdit]
GO

GRANT  EXECUTE  ON [dbo].[adp_tblGroupItems_SelectGroup]  TO [Genoa_Manager]
GO

GRANT  EXECUTE  ON [dbo].[adp_tblGroupItems_SelectGroup]  TO [Genoa_Read]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[spu_CovariancePertracDetails]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[spu_CovariancePertracDetails]
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO



CREATE PROCEDURE dbo.spu_CovariancePertracDetails 
	(
	@CovListID int , 
	@KnowledgeDate datetime
	)
AS
	SET NOCOUNT ON
	
	DECLARE @TmpTbl TABLE
	(
	PertracCode int
	)

	DECLARE @ReturnTbl TABLE
	(
	[RN] int IDENTITY (1, 1), 
	PertracCode int	
	)
	
	DECLARE @OldPertracCodes TABLE
		(
		RN int ,
		PertracCode int PRIMARY KEY
		)

	DECLARE @NewPertracCodes TABLE
		(
		RN int ,
		PertracCode int PRIMARY KEY
		)
	
	INSERT @TmpTbl(PertracCode)
	SELECT DISTINCT PertracID1
	FROM fn_tblCovariance_SelectKD(@KnowledgeDate)
	WHERE (ISNULL(@CovListID, 0) IN (0, CovListID))
	
	INSERT @TmpTbl(PertracCode)
	SELECT DISTINCT PertracID2
	FROM fn_tblCovariance_SelectKD(@KnowledgeDate)
	WHERE (ISNULL(@CovListID, 0) IN (0, CovListID))
	
	INSERT @ReturnTbl(PertracCode)
	SELECT DISTINCT PertracCode
	FROM @TmpTbl
	
	INSERT INTO @OldPertracCodes(RN, PertracCode)
	SELECT RN, PertracCode
	FROM @ReturnTbl
	WHERE PertracCode < 1048576

	INSERT INTO @NewPertracCodes(RN, PertracCode)
	SELECT RN, (PertracCode - 1048576)
	FROM @ReturnTbl
	WHERE PertracCode >= 1048576
	
	SELECT 	Groups.RN AS RN,
			0 AS AuditID, 
			0 AS GroupID,
			0 AS GroupItemID,
			Groups.PertracCode AS GroupPertracCode,
			0 AS GroupIndexCode,
			Mastername.Mastername AS PertracName, 
			Information.DataVendorName AS PertracProvider, 
			(Mastername.Mastername + ' (' + Information.DataVendorName + ')') AS ListDescription, 
			'' AS GroupSector,
			'' AS UserEntered, 
			getdate() AS DateEntered,
			Null AS DateDeleted
	FROM	@OldPertracCodes Groups LEFT OUTER JOIN
			MASTERSQL.dbo.Mastername Mastername ON (Groups.PertracCode = Mastername.ID) LEFT OUTER JOIN
			MASTERSQL.dbo.Information Information ON (Groups.PertracCode = Information.ID)
	UNION
	SELECT 	Groups.RN AS RN,
			0 AS AuditID, 
			0 AS GroupID,
			0 AS GroupItemID,
			(Groups.PertracCode + 1048576) AS GroupPertracCode,
			0 AS GroupIndexCode,
			Mastername.InstrumentName AS PertracName, 
			Information.DataVendorName AS PertracProvider, 
			(Mastername.InstrumentName + ' (' + Information.DataVendorName + ')') AS ListDescription, 
			'' AS GroupSector,
			'' AS UserEntered, 
			getdate() AS DateEntered,
			Null AS DateDeleted
	FROM	@NewPertracCodes Groups LEFT OUTER JOIN
			[MASTERSQL].[dbo].[tblInstrumentList] Mastername ON (Groups.PertracCode = Mastername.InstrumentID) LEFT OUTER JOIN
			[MASTERSQL].[dbo].[tblInstrumentInformation] Information ON (Groups.PertracCode = Information.InstrumentID)
	ORDER BY PertracName
	
	RETURN



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[spu_CovariancePertracDetails]  TO [InvestMaster_Sales]
GO

GRANT  EXECUTE  ON [dbo].[spu_CovariancePertracDetails]  TO [InvestMaster_Manager]
GO

GRANT  EXECUTE  ON [dbo].[spu_CovariancePertracDetails]  TO [Genoa_Read]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[fn_tblMastername]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[fn_tblMastername]
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

CREATE FUNCTION dbo.fn_tblMastername 
	(
	)
RETURNS 
	@tblMastername Table
			(
			[ID] int PRIMARY KEY, 
			[Mastername] varchar(255)
			)
AS
	BEGIN
	
	INSERT INTO @tblMastername([ID], [MASTERNAME])
	SELECT 
		[ID] AS [ID],
		[Mastername] AS [Mastername]
	FROM 
		[MASTERSQL].[dbo].[Mastername]
	WHERE
		[ID] < 1048576
		
	INSERT INTO @tblMastername([ID], [MASTERNAME])
	SELECT 
		([InstrumentID] + 1048576) AS [ID],
		[InstrumentName] AS [Mastername]
	FROM 
		[MASTERSQL].[dbo].[tblInstrumentList]
	ORDER BY [Mastername]
	
	RETURN
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  SELECT  ON [dbo].[fn_tblMastername]  TO [InvestMaster_Sales]
GO

GRANT  SELECT  ON [dbo].[fn_tblMastername]  TO [Genoa_Read]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[fn_tblPertracCustomFieldData_Status]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[fn_tblPertracCustomFieldData_Status]
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO



CREATE FUNCTION dbo.fn_tblPertracCustomFieldData_Status
	(
	@GroupID int, 
	@CustomFieldID int , 
	@PertracID int , 
	@KnowledgeDate datetime
	)
/*
	This function is designed to give the Updated status of any or all Custom Field
	data items.
	It is designed to return the status on the COMPLETE potential set of custom
	data items, whether or not a data item exists.
	If a data item is not present, then clearly the UpdateRequired status is True (1),
	If a Field is not volatile and a Data item exists, then UpdateRequired = False(0).
	If a new return has been entered (within the Custom Field Date range), or a data 
	return has been updated since the Data Item was entered then UpdateRequired status 
	is True (1).
	
	This function relies heavily on dbo.fn_CustomFieldUpdateRequired() which actually
	defines the True and Falser UpdateRequired conditions.

	NPP - 30 Apr 2007.
*/
RETURNS 
	@DataStatus 
	TABLE 
		(
		[FieldDataID] [int] ,
		[CustomFieldID] [int] ,
		[PertracID] [int] ,
		[GroupID] [int] ,
		[UpdateRequired] [bit] ,
		[FieldIsCalculated] [bit], 
		[LatestReturnDate] [datetime] ,
		[DateEntered] [datetime], 
		PRIMARY KEY ([CustomFieldID], [PertracID])
		)
AS
	BEGIN
	
	SELECT @PertracID = ISNULL(@PertracID, 0)
	SELECT @CustomFieldID = ISNULL(@CustomFieldID, 0)
	
	DECLARE @TmpPerformance TABLE
		(
		[ID] [int] ,
		[Date] [datetime] ,
		[Return] [float]  ,
		[NAV] [float]  ,
		[LastUpdated] [datetime], 
		PRIMARY KEY ([ID], [Date])
		)
		
	DECLARE @CustomDataSet TABLE
		(
		[CustomFieldID] [int] ,
		[PertracID] [int] ,
		[GroupID] [int] ,
		[FieldIsVolatile] bit, 
		[FieldIsCalculated] bit, 
		[CF_LastUpdated] [datetime], 
		PRIMARY KEY ([CustomFieldID], [PertracID])
		)

	DECLARE @PertracUpdateTimes TABLE
		(
		[CustomFieldID] [int] ,
		[PertracID] [int] ,
		[ReturnDate] [datetime] ,
		[LastUpdated] [datetime] , 
		PRIMARY KEY ([CustomFieldID], [PertracID])
		)
	
	-- 

	IF (@PertracID > 0)
	BEGIN
	
		IF (@PertracID >= 1048576)
		BEGIN
			INSERT @TmpPerformance([ID], [Date], [Return], [NAV], [LastUpdated])
			SELECT @PertracID, [ReturnDate], [ReturnPercent], [NAV], [DateEntered]
			FROM [MASTERSQL].[dbo].[tblInstrumentPerformance] Performance
			WHERE [InstrumentID] = (@PertracID - 1048576)
		END
		ELSE
		BEGIN
			INSERT @TmpPerformance([ID], [Date], [Return], [NAV], [LastUpdated])
			SELECT [ID], [Date], [Return], [NAV], [LastUpdated]
			FROM [MASTERSQL].[dbo].[Performance] Performance
			WHERE [ID] = @PertracID
		END
		

		INSERT @PertracUpdateTimes(CustomFieldID, PertracID, ReturnDate, LastUpdated)
		SELECT CF.FieldID, [Performance].[ID], MAX([Performance].[Date]), MAX([Performance].[LastUpdated]) 
		FROM
		fn_tblPertracCustomFields_SelectKD(@KnowledgeDate) CF CROSS JOIN
		@TmpPerformance Performance
		WHERE ((@CustomFieldID = 0) OR (@CustomFieldID = CF.FieldID)) AND 
				(CF.FieldIsVolatile = 1) AND ([Performance].[Date] <= ISNULL(CF.EndDate, '1 Jan 1900'))
		GROUP BY CF.FieldID, [Performance].[ID]
		ORDER BY CF.FieldID, [Performance].[ID]

		INSERT @CustomDataSet(CustomFieldID, PertracID, GroupID, FieldIsVolatile, FieldIsCalculated, CF_LastUpdated)
		SELECT
				CF.FieldID, 
				@PertracID, 
				0, 
				CF.FieldIsVolatile, 
				CF.FieldIsCalculated, 
				CF.DateEntered
		FROM 
				fn_tblPertracCustomFields_SelectKD(Null) CF 
		WHERE
				((ISNULL(@CustomFieldID, 0) = 0) OR (ISNULL(@CustomFieldID, 0) = CF.FieldID))
				
		INSERT @DataStatus(FieldDataID, CustomFieldID, PertracID, GroupID, UpdateRequired, FieldIsCalculated, LatestReturnDate, DateEntered)
		SELECT	
				ISNULL(CFD.FieldDataID, 0),
				DS.CustomFieldID, 
				DS.PertracID, 
				0, 
				dbo.fn_CustomFieldUpdateRequired(CFD.FieldDataID, CFD.UpdateRequired, DS.FieldIsVolatile, UpdateTimes.ReturnDate, UpdateTimes.LastUpdated, CFD.LatestReturnDate, CFD.DateEntered, DS.CF_LastUpdated ),
				DS.FieldIsCalculated, 
				ISNULL(CFD.LatestReturnDate, '1 Jan 1900'), 
				ISNULL(CFD.DateEntered, '1 Jan 1900')
		FROM @CustomDataSet DS LEFT JOIN 
				@PertracUpdateTimes UpdateTimes ON (DS.CustomFieldID = UpdateTimes.CustomFieldID) AND (DS.PertracID = UpdateTimes.PertracID) LEFT JOIN
				fn_tblPertracCustomFieldData_SinglePertracID(@PertracID, @KnowledgeDate) CFD ON (DS.CustomFieldID = CFD.CustomFieldID) AND (DS.PertracID = CFD.PertracID)  
		WHERE ((ISNULL(@CustomFieldID, 0) = 0) OR (DS.CustomFieldID = ISNULL(@CustomFieldID, 0)))
		ORDER BY DS.CustomFieldID, DS.PertracID

		RETURN				
	END

	IF (@PertracID <= 0) AND (ISNULL(@GroupID, 0) = 0)
	BEGIN

		-- External Data
		
		INSERT @PertracUpdateTimes(CustomFieldID, PertracID, ReturnDate, LastUpdated)
		SELECT CF.FieldID, [Performance].[ID], MAX([Performance].[Date]), MAX([Performance].[LastUpdated]) 
		FROM
		fn_tblPertracCustomFields_SelectKD(@KnowledgeDate) CF CROSS JOIN
		[MASTERSQL].[dbo].[Performance] Performance
		WHERE ((ISNULL(@CustomFieldID, 0) = 0) OR (ISNULL(@CustomFieldID, 0) = CF.FieldID)) AND 
				(CF.FieldIsVolatile = 1) AND ([Performance].[Date] <= ISNULL(CF.EndDate, '1 Jan 1900')) AND 
				((ISNULL(@PertracID, 0) = 0) OR ([Performance].[ID] = ISNULL(@PertracID, 0)))
		GROUP BY CF.FieldID, [Performance].[ID]
		ORDER BY CF.FieldID, [Performance].[ID]

		-- Internal Data 
		
		INSERT @PertracUpdateTimes(CustomFieldID, PertracID, ReturnDate, LastUpdated)
		SELECT CF.FieldID, ([Performance].[InstrumentID] + 1048576), MAX([Performance].[ReturnDate]), MAX([Performance].[DateEntered]) 
		FROM
		fn_tblPertracCustomFields_SelectKD(@KnowledgeDate) CF CROSS JOIN
		[MASTERSQL].[dbo].[tblInstrumentPerformance] Performance
		WHERE ((ISNULL(@CustomFieldID, 0) = 0) OR (ISNULL(@CustomFieldID, 0) = CF.FieldID)) AND 
				(CF.FieldIsVolatile = 1) AND ([Performance].[ReturnDate] <= ISNULL(CF.EndDate, '1 Jan 1900')) AND 
				((ISNULL(@PertracID, 0) = 0) OR ([Performance].[InstrumentID] = ISNULL(@PertracID, 0)))
		GROUP BY CF.FieldID, [Performance].[InstrumentID]
		ORDER BY CF.FieldID, [Performance].[InstrumentID]

		-- External Data

		INSERT @CustomDataSet(CustomFieldID, PertracID, GroupID, FieldIsVolatile, FieldIsCalculated, CF_LastUpdated)
		SELECT
				CF.FieldID, 
				MasterName.ID, 
				0, 
				CF.FieldIsVolatile, 
				CF.FieldIsCalculated, 
				CF.DateEntered
		FROM 
				(fn_tblPertracCustomFields_SelectKD(Null) CF CROSS JOIN
				[MASTERSQL].[dbo].[MasterName] MasterName)
		WHERE
				((ISNULL(@CustomFieldID, 0) = 0) OR (ISNULL(@CustomFieldID, 0) = CF.FieldID)) AND 
				((ISNULL(@PertracID, 0) = 0) OR ([MasterName].[ID] = ISNULL(@PertracID, 0)))

		-- Internal Data

		INSERT @CustomDataSet(CustomFieldID, PertracID, GroupID, FieldIsVolatile, FieldIsCalculated, CF_LastUpdated)
		SELECT
				CF.FieldID, 
				(MasterName.InstrumentID + 1048576), 
				0, 
				CF.FieldIsVolatile, 
				CF.FieldIsCalculated, 
				CF.DateEntered
		FROM 
				(fn_tblPertracCustomFields_SelectKD(Null) CF CROSS JOIN
				[MASTERSQL].[dbo].[tblInstrumentList] MasterName)
		WHERE
				((ISNULL(@CustomFieldID, 0) = 0) OR (ISNULL(@CustomFieldID, 0) = CF.FieldID)) AND 
				((ISNULL(@PertracID, 0) = 0) OR ([MasterName].[InstrumentID] = (ISNULL(@PertracID, 0) - 1048576)))

		INSERT @DataStatus(FieldDataID, CustomFieldID, PertracID, GroupID, UpdateRequired, FieldIsCalculated, LatestReturnDate, DateEntered)
		SELECT	
				ISNULL(CFD.FieldDataID, 0),
				DS.CustomFieldID, 
				DS.PertracID, 
				0, 
				dbo.fn_CustomFieldUpdateRequired(CFD.FieldDataID, CFD.UpdateRequired, DS.FieldIsVolatile, UpdateTimes.ReturnDate, UpdateTimes.LastUpdated, CFD.LatestReturnDate, CFD.DateEntered, DS.CF_LastUpdated ),
				DS.FieldIsCalculated, 
				ISNULL(CFD.LatestReturnDate, '1 Jan 1900'), 
				ISNULL(CFD.DateEntered, '1 Jan 1900')
		FROM @CustomDataSet DS LEFT JOIN 
				@PertracUpdateTimes UpdateTimes ON (DS.CustomFieldID = UpdateTimes.CustomFieldID) AND (DS.PertracID = UpdateTimes.PertracID) LEFT JOIN
				fn_tblPertracCustomFieldData_SelectKD(@KnowledgeDate) CFD ON (DS.CustomFieldID = CFD.CustomFieldID) AND (DS.PertracID = CFD.PertracID)  
		WHERE ((ISNULL(@CustomFieldID, 0) = 0) OR (DS.CustomFieldID = ISNULL(@CustomFieldID, 0)))
		ORDER BY DS.CustomFieldID, DS.PertracID

		RETURN		
	END
	ELSE
	BEGIN
		-- 	IF (@PertracID <= 0) AND (ISNULL(@GroupID, 0) > 0)

		-- External 
		
		INSERT @TmpPerformance([ID], [Date], [Return], [NAV], [LastUpdated])
		SELECT [ID], [Date], [Return], [NAV], [LastUpdated]
		FROM [MASTERSQL].[dbo].[Performance] Performance
		WHERE [ID] IN
			(SELECT Items.GroupPertracCode FROM fn_tblGroupItems_SelectKD(@KnowledgeDate) Items WHERE Items.GroupID = @GroupID)
		
		-- Internal
		
		INSERT @TmpPerformance([ID], [Date], [Return], [NAV], [LastUpdated])
		SELECT ([InstrumentID] + 1048576), [ReturnDate], [ReturnPercent], [NAV], [DateEntered]
		FROM [MASTERSQL].[dbo].[tblInstrumentPerformance] Performance
		WHERE [InstrumentID] IN 
			(SELECT (Items.GroupPertracCode - 1048576) FROM fn_tblGroupItems_SelectKD(@KnowledgeDate) Items WHERE Items.GroupID = @GroupID)
		
		
		INSERT @PertracUpdateTimes(CustomFieldID, PertracID, ReturnDate, LastUpdated)
		SELECT CF.FieldID, [Performance].[ID], MAX([Performance].[Date]), MAX([Performance].[LastUpdated]) 
		FROM
		fn_tblPertracCustomFields_SelectKD(@KnowledgeDate) CF CROSS JOIN
		@TmpPerformance Performance
		WHERE 
				((ISNULL(@CustomFieldID, 0) = 0) OR (ISNULL(@CustomFieldID, 0) = CF.FieldID)) AND 
				(CF.FieldIsVolatile = 1) AND ([Performance].[Date] <= ISNULL(CF.EndDate, '1 Jan 1900')) 
		GROUP BY CF.FieldID, [Performance].[ID]
		ORDER BY CF.FieldID, [Performance].[ID]


		INSERT @CustomDataSet(CustomFieldID, PertracID, GroupID, FieldIsVolatile, FieldIsCalculated, CF_LastUpdated)
		SELECT
				CF.FieldID, 
				Items.GroupPertracCode, 
				@GroupID, 
				CF.FieldIsVolatile, 
				CF.FieldIsCalculated, 
				CF.DateEntered
		FROM 
				(fn_tblPertracCustomFields_SelectKD(Null) CF CROSS JOIN
				fn_tblGroupItems_SelectKD(@KnowledgeDate) Items)
		WHERE (Items.GroupID = @GroupID) AND 
					((ISNULL(@PertracID, 0) = 0) OR (Items.GroupPertracCode = ISNULL(@PertracID, 0)))


		INSERT @DataStatus(FieldDataID, CustomFieldID, PertracID, GroupID, UpdateRequired, FieldIsCalculated, LatestReturnDate, DateEntered)
		SELECT	
				ISNULL(CFD.FieldDataID, 0),
				DS.CustomFieldID, 
				DS.PertracID, 
				ISNULL(CFD.GroupID, 0), 
				dbo.fn_CustomFieldUpdateRequired(CFD.FieldDataID, CFD.UpdateRequired, DS.FieldIsVolatile, UpdateTimes.ReturnDate, UpdateTimes.LastUpdated, CFD.LatestReturnDate, CFD.DateEntered, DS.CF_LastUpdated ),
				DS.FieldIsCalculated, 
				ISNULL(CFD.LatestReturnDate, '1 Jan 1900'), 
				ISNULL(CFD.DateEntered, '1 Jan 1900')
		FROM @CustomDataSet DS LEFT JOIN 
				fn_tblPertracCustomFieldDataByGroup_SelectKD(@GroupID, 0, @KnowledgeDate) CFD ON (DS.CustomFieldID = CFD.CustomFieldID) AND (DS.PertracID = CFD.PertracID) LEFT JOIN 
				@PertracUpdateTimes UpdateTimes ON (DS.CustomFieldID = UpdateTimes.CustomFieldID) AND (DS.PertracID = UpdateTimes.PertracID)
		WHERE ((ISNULL(@CustomFieldID, 0) = 0) OR (DS.CustomFieldID = ISNULL(@CustomFieldID, 0)))
		ORDER BY DS.CustomFieldID, DS.PertracID

		RETURN
	END

	RETURN
	END



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[fn_MasterFundAllocation]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[fn_MasterFundAllocation]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[fn_MasterFundAllocation_Combined]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[fn_MasterFundAllocation_Combined]
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO




CREATE FUNCTION dbo.fn_MasterFundAllocation
	(
	@MasterFundID int,
	@ReportDate datetime,
	@KnowledgeDateFrom datetime, 
	@KnowledgeDateTo datetime,
	@KnowledgeDateLinkedTables datetime
	)
-- Function to return data for the Master - Sub Fund allocation report
-- Designed to show the allocation of assets of a Master Fund between the Sub funds.
--
-- @MasterFundID	 	: Master Fund ID to report on.
-- @ReportDate			: Date indicating the month to report on.
--						: The given date will be used to derive the period start and end dates, being
--						: the last day of the given month and the last day of the previous month.
-- @KnowledgeDateFrom 	: Knowledgedate to apply to P&L From queries. Null or '1 Jan 1900' returns all rows.
-- @KnowledgeDateTo 	: Knowledgedate to apply to P&L To queries. Null or '1 Jan 1900' returns all rows.
-- @KnowledgeDateLinkedTables : Knowledgedate to apply to linked tables (eg tblInstrument). Null or '1 Jan 1900' returns all rows.
--	
RETURNS @AllocationResults
TABLE 
	(
	RN int IDENTITY (1, 1), 
	RowData varchar(100), 
	MainFund varchar(100) ,
	Fund1 varchar(100) ,
	Fund2 varchar(100) ,
	Fund3 varchar(100) 
	) 
AS
BEGIN
Declare @PnLResults TABLE
	(
	Fund int, 
	FundCode varchar(100), 
	InstrumentFundType int, 
	FundTypeDescription varchar(100), 
	Instrument int,
	InstrumentDescription varchar(100), 
	InstrumentTypeDescription varchar(100), 
	Value2 float, 
	FXRate2 float, 
	FundFXRate2 float, 
	USDValue float, 		
	Profit float
	)

DECLARE @ReportStartDate datetime
DECLARE @ReportEndDate datetime

Declare @MasterFund_Name varchar(100)
Declare @Fund1_Name varchar(100) 
Declare @Fund2_Name varchar(100) 
Declare @Fund3_Name varchar(100) 
Declare @Fund1_ID int
Declare @Fund2_ID int
Declare @Fund3_ID int
Declare @Fund1_CurrencyID int
Declare @Fund2_CurrencyID int
Declare @Fund3_CurrencyID int
Declare @Fund1_CounterpartyID int
Declare @Fund2_CounterpartyID int
Declare @Fund3_CounterpartyID int

Declare @MainFund_FundValue float
Declare @Fund1_FundValue float
Declare @Fund2_FundValue float
Declare @Fund3_FundValue float
Declare @MainFund_FundUnitCount float
Declare @MainFund_FundUnitPrice float

Declare @Fund1_StartMasterUnits float
Declare @Fund2_StartMasterUnits float
Declare @Fund3_StartMasterUnits float

Declare @MainFund_Value float
Declare @Fund1_Value float
Declare @Fund2_Value float
Declare @Fund3_Value float

Declare @MainFund_FX float
Declare @Fund1_FX float
Declare @Fund2_FX float
Declare @Fund3_FX float
Declare @Fund1_Profit float
Declare @Fund2_Profit float
Declare @Fund3_Profit float

DECLARE @thisCounterparty int
DECLARE @thisHolding float
DECLARE @thisFund float
DECLARE @thisValue float
DECLARE @thisProfit float

DECLARE @SubscribeTransactionType int
DECLARE @RedeemTransactionType int

SELECT @SubscribeTransactionType = TransactionTypeID
	FROM fn_tblTransactionType_SelectKD(@KnowledgeDateLinkedTables) WHERE TransactionType = 'SUBSCRIBE'
SELECT @RedeemTransactionType = TransactionTypeID
	FROM fn_tblTransactionType_SelectKD(@KnowledgeDateLinkedTables) WHERE TransactionType = 'REDEEM'

-- Establish Start and End dates, Being the last day of sequential months

SELECT @ReportStartDate = '1 Jan 2000'

SELECT @ReportStartDate = DATEADD(year, YEAR(@ReportDate) - YEAR(@ReportStartDate), @ReportStartDate)
SELECT @ReportStartDate = DATEADD(month, MONTH(@ReportDate) - MONTH(@ReportStartDate), @ReportStartDate)

SELECT @ReportEndDate = DATEADD(month, 1, @ReportStartDate)

SELECT @ReportStartDate = DATEADD(day, -1, @ReportStartDate)
SELECT @ReportEndDate = DATEADD(day, -1, @ReportEndDate)


-- First get a list of the 'Sub' Funds

Select @Fund1_ID = ISNULL(MIN(FundID), 0)
FROM fn_tblFund_SelectKD(@KnowledgeDateLinkedTables)
WHERE (FundParentFund = ISNULL(@MasterFundID, -1)) AND (FundID <> FundParentFund)

Select @Fund2_ID = ISNULL(MIN(FundID), 0)
FROM fn_tblFund_SelectKD(@KnowledgeDateLinkedTables)
WHERE (FundParentFund = ISNULL(@MasterFundID, -1)) AND 
	(FundID > @Fund1_ID) AND (FundID <> FundParentFund)

Select @Fund3_ID = ISNULL(MIN(FundID), 0)
FROM fn_tblFund_SelectKD(@KnowledgeDateLinkedTables)
WHERE (FundParentFund = ISNULL(@MasterFundID, -1)) AND 
	(FundID > @Fund2_ID) AND (FundID <> FundParentFund)

INSERT @AllocationResults(RowData, MainFund, Fund1, Fund2, Fund3)
SELECT 'FundID', @MasterFundID, @Fund1_ID, @Fund2_ID, @Fund3_ID

-- Fund Codes

SELECT @MasterFund_Name = FundCode
FROM fn_tblFund_SelectKD(@KnowledgeDateLinkedTables)
WHERE FundID = @MasterFundID

SELECT @Fund1_Name = FundCode
FROM fn_tblFund_SelectKD(@KnowledgeDateLinkedTables)
WHERE FundID = @Fund1_ID

SELECT @Fund2_Name = FundCode
FROM fn_tblFund_SelectKD(@KnowledgeDateLinkedTables)
WHERE FundID = @Fund2_ID

SELECT @Fund3_Name = FundCode
FROM fn_tblFund_SelectKD(@KnowledgeDateLinkedTables)
WHERE FundID = @Fund3_ID

INSERT @AllocationResults(RowData, MainFund, Fund1, Fund2, Fund3)
SELECT 'FundCode', @MasterFund_Name, @Fund1_Name, @Fund2_Name, @Fund3_Name


-- Get Counterparty

SELECT @Fund1_CounterpartyID = ISNULL(Max(CounterpartyID), 0)
FROM fn_tblCounterparty_SelectKD(@KnowledgeDateLinkedTables)
WHERE CounterpartyFundID = @Fund1_ID

SELECT @Fund2_CounterpartyID = ISNULL(Max(CounterpartyID), 0)
FROM fn_tblCounterparty_SelectKD(@KnowledgeDateLinkedTables)
WHERE CounterpartyFundID = @Fund2_ID

SELECT @Fund3_CounterpartyID = ISNULL(Max(CounterpartyID), 0)
FROM fn_tblCounterparty_SelectKD(@KnowledgeDateLinkedTables)
WHERE CounterpartyFundID = @Fund3_ID

-- Get Unit Counts

DECLARE tempCursor CURSOR
FOR 
SELECT TransactionCounterparty, Sum(TransactionSignedUnits)
FROM   fn_tblTransaction_SelectKD(@KnowledgeDateTo) Transactions 
WHERE (TransactionType IN (@SubscribeTransactionType, @RedeemTransactionType)) AND
      (TransactionFund = @MasterFundID) AND
      (TransactionValueDate <= @ReportEndDate) 
GROUP BY TransactionCounterparty

SELECT @MainFund_Value = 0
SELECT @Fund1_Value = 0
SELECT @Fund2_Value = 0
SELECT @Fund3_Value = 0

SELECT @Fund1_StartMasterUnits = 0
SELECT @Fund2_StartMasterUnits = 0
SELECT @Fund3_StartMasterUnits = 0

OPEN tempCursor

FETCH NEXT FROM tempCursor INTO @thisCounterparty, @thisHolding

WHILE @@fetch_status = 0
BEGIN
	SELECT @MainFund_Value = @MainFund_Value + @thisHolding
	
	IF @thisCounterparty = @Fund1_CounterpartyID
		SELECT @Fund1_StartMasterUnits = -@thisHolding

	IF @thisCounterparty = @Fund2_CounterpartyID
		SELECT @Fund2_StartMasterUnits = -@thisHolding

	IF @thisCounterparty = @Fund3_CounterpartyID
		SELECT @Fund3_StartMasterUnits = -@thisHolding
	
	FETCH NEXT FROM tempCursor INTO @thisCounterparty, @thisHolding
END
CLOSE tempCursor
DEALLOCATE tempCursor

SELECT @MainFund_Value = Abs(@MainFund_Value)

INSERT @AllocationResults(RowData, MainFund, Fund1, Fund2, Fund3)
SELECT 'StartMasterUnits', 
	CAST(@MainFund_Value AS decimal(38,2)), 
	CAST(@Fund1_StartMasterUnits AS decimal(38,2)), 
	CAST(@Fund2_StartMasterUnits AS decimal(38,2)), 
	CAST(@Fund3_StartMasterUnits AS decimal(38,2))


-- Get Current Fund Units in Issue
DECLARE tempCursor CURSOR
FOR 
SELECT Fund, UnitCount
FROM   fn_FundUnitCount(0, @ReportEndDate, @KnowledgeDateTo)  

SELECT @MainFund_Value = 0
SELECT @Fund1_Value = 0
SELECT @Fund2_Value = 0
SELECT @Fund3_Value = 0

OPEN tempCursor

FETCH NEXT FROM tempCursor INTO @thisFund, @thisHolding

WHILE @@fetch_status = 0
BEGIN
	IF @thisFund = @MasterFundID
		SELECT @MainFund_Value = Abs(@thisHolding)
	
	IF @thisFund = @Fund1_ID
		SELECT @Fund1_Value = Abs(@thisHolding)

	IF @thisFund = @Fund2_ID
		SELECT @Fund2_Value = Abs(@thisHolding)

	IF @thisFund = @Fund3_ID
		SELECT @Fund3_Value = Abs(@thisHolding)
	
	FETCH NEXT FROM tempCursor INTO @thisFund, @thisHolding
END
CLOSE tempCursor
DEALLOCATE tempCursor

SELECT @MainFund_FundUnitCount = @MainFund_Value

INSERT @AllocationResults(RowData, MainFund, Fund1, Fund2, Fund3)
SELECT 'FundUnitsInIssue', 
	CAST(@MainFund_Value AS decimal(38,2)), 
	CAST(@Fund1_Value AS decimal(38,2)), 
	CAST(@Fund2_Value AS decimal(38,2)), 
	CAST(@Fund3_Value AS decimal(38,2))


-- Get 'Sub' Fund Subscriptions / Redemptions this month
DECLARE tempCursor CURSOR
FOR 
SELECT TransactionFund, Sum(TransactionSignedUnits) As UnitCount
FROM fn_tblTransaction_SelectKD(@KnowledgeDateTo)
WHERE TransactionFund IN (@MasterFundID, @Fund1_ID, @Fund2_ID, @Fund3_ID) AND 
(TransactionValueDate > @ReportStartDate) AND 
(TransactionValueDate <= @ReportEndDate) AND 
(TransactionType in (@SubscribeTransactionType, @RedeemTransactionType))
GROUP BY TransactionFund

SELECT @MainFund_Value = 0
SELECT @Fund1_Value = 0
SELECT @Fund2_Value = 0
SELECT @Fund3_Value = 0

OPEN tempCursor

FETCH NEXT FROM tempCursor INTO @thisFund, @thisHolding

WHILE @@fetch_status = 0
BEGIN	
	IF @thisFund = @MasterFundID
		SELECT @MainFund_Value = (-@thisHolding)
	
	IF @thisFund = @Fund1_ID
		SELECT @Fund1_Value = (-@thisHolding)

	IF @thisFund = @Fund2_ID
		SELECT @Fund2_Value = (-@thisHolding)

	IF @thisFund = @Fund3_ID
		SELECT @Fund3_Value = (-@thisHolding)
	
	FETCH NEXT FROM tempCursor INTO @thisFund, @thisHolding
END
CLOSE tempCursor
DEALLOCATE tempCursor

INSERT @AllocationResults(RowData, MainFund, Fund1, Fund2, Fund3)
SELECT 'FundSubscriptionsThisMonth', 
	CAST(@MainFund_Value AS decimal(38,2)), 
	CAST(@Fund1_Value AS decimal(38,2)), 
	CAST(@Fund2_Value AS decimal(38,2)), 
	CAST(@Fund3_Value AS decimal(38,2))


-- ****

-- Get 'Sub' Fund Subscriptions / Redemptions this month
DECLARE tempCursor CURSOR
FOR 
SELECT TransactionFund, TransactionCounterparty, Sum(TransactionSignedUnits) As UnitCount
FROM fn_tblTransaction_SelectKD(@KnowledgeDateTo)
WHERE 
(TransactionFund = @MasterFundID) AND 
(TransactionCounterparty IN (@Fund1_CounterpartyID, @Fund2_CounterpartyID, @Fund3_CounterpartyID)) AND 
(TransactionValueDate > @ReportStartDate) AND 
(TransactionValueDate <= @ReportEndDate) AND 
(TransactionType in (@SubscribeTransactionType, @RedeemTransactionType))
GROUP BY TransactionFund, TransactionCounterparty

SELECT @Fund1_Value = 0
SELECT @Fund2_Value = 0
SELECT @Fund3_Value = 0

OPEN tempCursor

FETCH NEXT FROM tempCursor INTO @thisFund, @thisCounterparty, @thisHolding

WHILE @@fetch_status = 0
BEGIN		
	IF @thisCounterparty = @Fund1_CounterpartyID
		SELECT @Fund1_Value = (-@thisHolding)

	IF @thisCounterparty = @Fund2_CounterpartyID
		SELECT @Fund2_Value = (-@thisHolding)

	IF @thisCounterparty = @Fund3_CounterpartyID
		SELECT @Fund3_Value = (-@thisHolding)
	
	FETCH NEXT FROM tempCursor INTO @thisFund, @thisCounterparty, @thisHolding
END
CLOSE tempCursor
DEALLOCATE tempCursor

INSERT @AllocationResults(RowData, MainFund, Fund1, Fund2, Fund3)
SELECT 'MasterFundSubscriptionsThisMonth', 
	CAST(@MainFund_Value AS decimal(38,2)), 
	CAST(@Fund1_Value AS decimal(38,2)), 
	CAST(@Fund2_Value AS decimal(38,2)), 
	CAST(@Fund3_Value AS decimal(38,2))

-- ****

-- Get P&L and selected fund Valuations

SELECT @MainFund_Value = 0
SELECT @Fund1_Value = 0
SELECT @Fund2_Value = 0
SELECT @Fund3_Value = 0


INSERT @PnLResults(
	Fund, 
	FundCode, 
	InstrumentFundType, 
	FundTypeDescription, 
	Instrument, 
	InstrumentDescription, 
	InstrumentTypeDescription, 
	Value2, 
	FXRate2, 
	FundFXRate2, 
	USDValue, 
	Profit)
SELECT 
	Fund, 
	FundCode, 
	InstrumentFundType, 
	FundTypeDescription, 
	Instrument, 
	InstrumentDescription, 
	InstrumentTypeDescription, 
	Value2, 
	FXRate2, 
	FundFXRate2, 
	USDValue, 
	Profit
FROM fn_ProfitAndLoss
	(
	0, 
	@ReportStartDate, 
	@ReportEndDate, 
	Null, 
	Null, 
	@KnowledgeDateFrom, 
	@KnowledgeDateTo, 
	@KnowledgeDateLinkedTables, 
	Null
	)


-- Fund FX's
SELECT @Fund1_FX = 1
SELECT @Fund2_FX = 1
SELECT @Fund3_FX = 1

SELECT @Fund1_FX = ISNULL(MAX(FundFXRate2), 1)
FROM @PnLResults
WHERE Fund = @Fund1_ID

SELECT @Fund2_FX = ISNULL(MAX(FundFXRate2), 1)
FROM @PnLResults
WHERE Fund = @Fund2_ID

SELECT @Fund3_FX = ISNULL(MAX(FundFXRate2), 1)
FROM @PnLResults
WHERE Fund = @Fund3_ID

INSERT @AllocationResults(RowData, MainFund, Fund1, Fund2, Fund3)
SELECT 'FundFXRates', 
	CAST(0 AS decimal(32,8)), 
	CAST(@Fund1_FX AS decimal(38,6)), 
	CAST(@Fund2_FX AS decimal(38,6)), 
	CAST(@Fund3_FX AS decimal(38,6))


-- Fund Values
SELECT @MainFund_FundValue = 0
SELECT @Fund1_FundValue = 0
SELECT @Fund2_FundValue = 0
SELECT @Fund3_FundValue = 0


SELECT @MainFund_FundValue = ISNULL(Sum(Value2), 0)
FROM @PnLResults
WHERE Fund = ISNULL(@MasterFundID, 0)

SELECT @Fund1_FundValue = ISNULL(Sum(Value2), 0)
FROM @PnLResults
WHERE Fund = ISNULL(@Fund1_ID, 0)

SELECT @Fund2_FundValue = ISNULL(Sum(Value2), 0)
FROM @PnLResults
WHERE Fund = ISNULL(@Fund2_ID, 0)

SELECT @Fund3_FundValue = ISNULL(Sum(Value2), 0)
FROM @PnLResults
WHERE Fund = ISNULL(@Fund3_ID, 0)

INSERT @AllocationResults(RowData, MainFund, Fund1, Fund2, Fund3)
SELECT 'FundNetAssets', 
	CAST(@MainFund_FundValue AS decimal(36,4)), 
	CAST(@Fund1_FundValue * @Fund1_FX AS decimal(36,4)), 
	CAST(@Fund2_FundValue * @Fund2_FX AS decimal(36,4)), 
	CAST(@Fund3_FundValue * @Fund3_FX AS decimal(36,4))

-- Main Fund Unit Price

IF @MainFund_FundUnitCount > 0
	SELECT @MainFund_FundUnitPrice = @MainFund_FundValue / @MainFund_FundUnitCount

INSERT @AllocationResults(RowData, MainFund, Fund1, Fund2, Fund3)
SELECT 'FundUnitPrice', 
	CAST(@MainFund_FundUnitPrice AS decimal(32,8)), 
	CAST(@Fund1_Value AS decimal(38,2)), 
	CAST(@Fund2_Value AS decimal(38,2)), 
	CAST(@Fund3_Value AS decimal(38,2))

SELECT @MainFund_Value = 0
SELECT @Fund1_Value = 0
SELECT @Fund2_Value = 0
SELECT @Fund3_Value = 0

-- Share TransactionRequired

INSERT @AllocationResults(RowData, MainFund, Fund1, Fund2, Fund3)
SELECT 'ShareTransactionRequired', 
	CAST(0 AS decimal(32,8)), 
	CAST((((@Fund1_FundValue * @Fund1_FX) / @MainFund_FundUnitPrice) - @Fund1_StartMasterUnits) AS decimal(38,2)), 
	CAST((((@Fund2_FundValue * @Fund2_FX) / @MainFund_FundUnitPrice) - @Fund2_StartMasterUnits) AS decimal(38,2)), 
	CAST((((@Fund3_FundValue * @Fund3_FX) / @MainFund_FundUnitPrice) - @Fund3_StartMasterUnits) AS decimal(38,2))

-- EndMasterUnits

INSERT @AllocationResults(RowData, MainFund, Fund1, Fund2, Fund3)
SELECT 'EndMasterUnits', 
	CAST(0 AS decimal(32,8)), 
	CAST((((@Fund1_FundValue * @Fund1_FX) / @MainFund_FundUnitPrice)) AS decimal(38,2)), 
	CAST((((@Fund2_FundValue * @Fund2_FX) / @MainFund_FundUnitPrice)) AS decimal(38,2)), 
	CAST((((@Fund3_FundValue * @Fund3_FX) / @MainFund_FundUnitPrice)) AS decimal(38,2))

-- Class Specific Transactions

-- Administrator Fees

DECLARE tempCursor CURSOR
FOR 
SELECT Fund, ISNULL(Sum(Value2), 0), ISNULL(Sum(Profit), 0)
FROM   @PnLResults PnLResults
WHERE InstrumentDescription LIKE '%Administrator% Fees%'
GROUP BY  Fund

SELECT @Fund1_Value = 0
SELECT @Fund2_Value = 0
SELECT @Fund3_Value = 0
SELECT @Fund1_Profit = 0
SELECT @Fund2_Profit = 0
SELECT @Fund3_Profit = 0

OPEN tempCursor

FETCH NEXT FROM tempCursor INTO @thisFund, @thisValue, @thisProfit

WHILE @@fetch_status = 0
BEGIN
	
	IF @thisFund = @Fund1_ID
		SELECT @Fund1_Profit = @thisProfit

	IF @thisFund = @Fund2_ID
		SELECT @Fund2_Profit = @thisProfit

	IF @thisFund = @Fund3_ID
		SELECT @Fund3_Profit = @thisProfit
	
	FETCH NEXT FROM tempCursor INTO @thisFund, @thisValue, @thisProfit
END
CLOSE tempCursor
DEALLOCATE tempCursor

INSERT @AllocationResults(RowData, MainFund, Fund1, Fund2, Fund3)
SELECT 'Administrator Fees', 
	CAST(0 AS decimal(32,8)), 
	CAST(@Fund1_Profit * @Fund1_FX AS decimal(38,6)), 
	CAST(@Fund2_Profit * @Fund2_FX AS decimal(38,6)), 
	CAST(@Fund3_Profit * @Fund3_FX AS decimal(38,6))


-- Management Fees 

DECLARE tempCursor CURSOR
FOR 
SELECT Fund, ISNULL(Sum(Value2), 0), ISNULL(Sum(Profit), 0)
FROM   @PnLResults PnLResults
WHERE InstrumentDescription LIKE '%Management% Fee%'
GROUP BY  Fund

SELECT @Fund1_Value = 0
SELECT @Fund2_Value = 0
SELECT @Fund3_Value = 0
SELECT @Fund1_Profit = 0
SELECT @Fund2_Profit = 0
SELECT @Fund3_Profit = 0

OPEN tempCursor

FETCH NEXT FROM tempCursor INTO @thisFund, @thisValue, @thisProfit

WHILE @@fetch_status = 0
BEGIN
	
	IF @thisFund = @Fund1_ID
	BEGIN
		SELECT @Fund1_Value = @thisValue
		SELECT @Fund1_Profit = @thisProfit
	END

	IF @thisFund = @Fund2_ID
	BEGIN
		SELECT @Fund2_Value = @thisValue
		SELECT @Fund2_Profit = @thisProfit
	END

	IF @thisFund = @Fund3_ID
	BEGIN
		SELECT @Fund3_Value = @thisValue
		SELECT @Fund3_Profit = @thisProfit
	END
	
	FETCH NEXT FROM tempCursor INTO @thisFund, @thisValue, @thisProfit
END
CLOSE tempCursor
DEALLOCATE tempCursor

INSERT @AllocationResults(RowData, MainFund, Fund1, Fund2, Fund3)
SELECT 'Management Fees Profit', 
	CAST(0 AS decimal(32,8)), 
	CAST(@Fund1_Profit * @Fund1_FX AS decimal(38,6)), 
	CAST(@Fund2_Profit * @Fund2_FX AS decimal(38,6)), 
	CAST(@Fund3_Profit * @Fund3_FX AS decimal(38,6))

INSERT @AllocationResults(RowData, MainFund, Fund1, Fund2, Fund3)
SELECT 'Management Fees Value', 
	CAST(0 AS decimal(32,8)), 
	CAST(@Fund1_Value * @Fund1_FX AS decimal(38,6)), 
	CAST(@Fund2_Value * @Fund2_FX AS decimal(38,6)), 
	CAST(@Fund3_Value * @Fund3_FX AS decimal(38,6))


-- Distribution Fees 

DECLARE tempCursor CURSOR
FOR 
SELECT Fund, ISNULL(Sum(Value2), 0), ISNULL(Sum(Profit), 0)
FROM   @PnLResults PnLResults
WHERE InstrumentDescription LIKE '%Distribution% Fee%'
GROUP BY  Fund

SELECT @Fund1_Value = 0
SELECT @Fund2_Value = 0
SELECT @Fund3_Value = 0
SELECT @Fund1_Profit = 0
SELECT @Fund2_Profit = 0
SELECT @Fund3_Profit = 0

OPEN tempCursor

FETCH NEXT FROM tempCursor INTO @thisFund, @thisValue, @thisProfit

WHILE @@fetch_status = 0
BEGIN
	
	IF @thisFund = @Fund1_ID
	BEGIN
		SELECT @Fund1_Value = @thisValue
		SELECT @Fund1_Profit = @thisProfit
	END

	IF @thisFund = @Fund2_ID
	BEGIN
		SELECT @Fund2_Value = @thisValue
		SELECT @Fund2_Profit = @thisProfit
	END

	IF @thisFund = @Fund3_ID
	BEGIN
		SELECT @Fund3_Value = @thisValue
		SELECT @Fund3_Profit = @thisProfit
	END
	
	FETCH NEXT FROM tempCursor INTO @thisFund, @thisValue, @thisProfit
END
CLOSE tempCursor
DEALLOCATE tempCursor

INSERT @AllocationResults(RowData, MainFund, Fund1, Fund2, Fund3)
SELECT 'Distribution Fees Profit', 
	CAST(0 AS decimal(32,8)), 
	CAST(@Fund1_Profit * @Fund1_FX AS decimal(38,6)), 
	CAST(@Fund2_Profit * @Fund2_FX AS decimal(38,6)), 
	CAST(@Fund3_Profit * @Fund3_FX AS decimal(38,6))

INSERT @AllocationResults(RowData, MainFund, Fund1, Fund2, Fund3)
SELECT 'Distribution Fees Value', 
	CAST(0 AS decimal(32,8)), 
	CAST(@Fund1_Value * @Fund1_FX AS decimal(38,6)), 
	CAST(@Fund2_Value * @Fund2_FX AS decimal(38,6)), 
	CAST(@Fund3_Value * @Fund3_FX AS decimal(38,6))


-- Performance Fees Fees

DECLARE tempCursor CURSOR
FOR 
SELECT Fund, ISNULL(Sum(Value2), 0), ISNULL(Sum(Profit), 0)
FROM   @PnLResults PnLResults
WHERE InstrumentDescription LIKE '%Performance% Fee%'
GROUP BY  Fund

SELECT @Fund1_Value = 0
SELECT @Fund2_Value = 0
SELECT @Fund3_Value = 0
SELECT @Fund1_Profit = 0
SELECT @Fund2_Profit = 0
SELECT @Fund3_Profit = 0

OPEN tempCursor

FETCH NEXT FROM tempCursor INTO @thisFund, @thisValue, @thisProfit

WHILE @@fetch_status = 0
BEGIN
	
	IF @thisFund = @Fund1_ID
	BEGIN
		SELECT @Fund1_Value = @thisValue
		SELECT @Fund1_Profit = @thisProfit
	END

	IF @thisFund = @Fund2_ID
	BEGIN
		SELECT @Fund2_Value = @thisValue
		SELECT @Fund2_Profit = @thisProfit
	END

	IF @thisFund = @Fund3_ID
	BEGIN
		SELECT @Fund3_Value = @thisValue
		SELECT @Fund3_Profit = @thisProfit
	END
	
	FETCH NEXT FROM tempCursor INTO @thisFund, @thisValue, @thisProfit
END
CLOSE tempCursor
DEALLOCATE tempCursor

INSERT @AllocationResults(RowData, MainFund, Fund1, Fund2, Fund3)
SELECT 'Performance Fees Profit', 
	CAST(0 AS decimal(32,8)), 
	CAST(@Fund1_Profit * @Fund1_FX AS decimal(38,6)), 
	CAST(@Fund2_Profit * @Fund2_FX AS decimal(38,6)), 
	CAST(@Fund3_Profit * @Fund3_FX AS decimal(38,6))

INSERT @AllocationResults(RowData, MainFund, Fund1, Fund2, Fund3)
SELECT 'Performance Fees Value', 
	CAST(0 AS decimal(32,8)), 
	CAST(@Fund1_Value * @Fund1_FX AS decimal(38,6)), 
	CAST(@Fund2_Value * @Fund2_FX AS decimal(38,6)), 
	CAST(@Fund3_Value * @Fund3_FX AS decimal(38,6))


-- Equalisation Factors

-- DECLARE tempCursor CURSOR
-- FOR 
-- SELECT Fund, ISNULL(Sum(Value2), 0), ISNULL(Sum(Profit), 0)
-- FROM   @PnLResults PnLResults
-- WHERE InstrumentDescription LIKE '%Equali_ation% Factor%'
-- GROUP BY  Fund

DECLARE tempCursor CURSOR
FOR 
SELECT Fund, ISNULL(Sum(Value2), 0), ISNULL(Sum(Profit), 0)
FROM   @PnLResults PnLResults
WHERE Instrument IN (SELECT InstrumentID from fn_tblInstrument_SelectKD(@KnowledgeDateLinkedTables) WHERE InstrumentIsEqualisation <> 0)
GROUP BY  Fund


SELECT @Fund1_Value = 0
SELECT @Fund2_Value = 0
SELECT @Fund3_Value = 0
SELECT @Fund1_Profit = 0
SELECT @Fund2_Profit = 0
SELECT @Fund3_Profit = 0

OPEN tempCursor

FETCH NEXT FROM tempCursor INTO @thisFund, @thisValue, @thisProfit

WHILE @@fetch_status = 0
BEGIN
	
	IF @thisFund = @Fund1_ID
	BEGIN
		SELECT @Fund1_Value = @thisValue
		SELECT @Fund1_Profit = @thisProfit
	END

	IF @thisFund = @Fund2_ID
	BEGIN
		SELECT @Fund2_Value = @thisValue
		SELECT @Fund2_Profit = @thisProfit
	END

	IF @thisFund = @Fund3_ID
	BEGIN
		SELECT @Fund3_Value = @thisValue
		SELECT @Fund3_Profit = @thisProfit
	END
	
	FETCH NEXT FROM tempCursor INTO @thisFund, @thisValue, @thisProfit
END
CLOSE tempCursor
DEALLOCATE tempCursor

INSERT @AllocationResults(RowData, MainFund, Fund1, Fund2, Fund3)
SELECT 'Equalisation Factor Profit', 
	CAST(0 AS decimal(32,8)), 
	CAST(@Fund1_Profit * @Fund1_FX AS decimal(38,6)), 
	CAST(@Fund2_Profit * @Fund2_FX AS decimal(38,6)), 
	CAST(@Fund3_Profit * @Fund3_FX AS decimal(38,6))

INSERT @AllocationResults(RowData, MainFund, Fund1, Fund2, Fund3)
SELECT 'Equalisation Factor Value', 
	CAST(0 AS decimal(32,8)), 
	CAST(@Fund1_Value * @Fund1_FX AS decimal(38,6)), 
	CAST(@Fund2_Value * @Fund2_FX AS decimal(38,6)), 
	CAST(@Fund3_Value * @Fund3_FX AS decimal(38,6))

-- Write Off Acc.

DECLARE tempCursor CURSOR
FOR 
SELECT Fund, ISNULL(Sum(Value2), 0), ISNULL(Sum(Profit), 0)
FROM   @PnLResults PnLResults
WHERE InstrumentDescription LIKE '%Write off%'
GROUP BY  Fund

SELECT @Fund1_Value = 0
SELECT @Fund2_Value = 0
SELECT @Fund3_Value = 0
SELECT @Fund1_Profit = 0
SELECT @Fund2_Profit = 0
SELECT @Fund3_Profit = 0

OPEN tempCursor

FETCH NEXT FROM tempCursor INTO @thisFund, @thisValue, @thisProfit

WHILE @@fetch_status = 0
BEGIN
	
	IF @thisFund = @Fund1_ID
	BEGIN
		SELECT @Fund1_Value = @thisValue
		SELECT @Fund1_Profit = @thisProfit
	END

	IF @thisFund = @Fund2_ID
	BEGIN
		SELECT @Fund2_Value = @thisValue
		SELECT @Fund2_Profit = @thisProfit
	END

	IF @thisFund = @Fund3_ID
	BEGIN
		SELECT @Fund3_Value = @thisValue
		SELECT @Fund3_Profit = @thisProfit
	END
	
	FETCH NEXT FROM tempCursor INTO @thisFund, @thisValue, @thisProfit
END
CLOSE tempCursor
DEALLOCATE tempCursor

INSERT @AllocationResults(RowData, MainFund, Fund1, Fund2, Fund3)
SELECT 'Write off Profit', 
	CAST(0 AS decimal(32,8)), 
	CAST(@Fund1_Profit * @Fund1_FX AS decimal(38,6)), 
	CAST(@Fund2_Profit * @Fund2_FX AS decimal(38,6)), 
	CAST(@Fund3_Profit * @Fund3_FX AS decimal(38,6))

INSERT @AllocationResults(RowData, MainFund, Fund1, Fund2, Fund3)
SELECT 'Write off Value', 
	CAST(0 AS decimal(32,8)), 
	CAST(@Fund1_Value * @Fund1_FX AS decimal(38,6)), 
	CAST(@Fund2_Value * @Fund2_FX AS decimal(38,6)), 
	CAST(@Fund3_Value * @Fund3_FX AS decimal(38,6))


RETURN
END




GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO




CREATE FUNCTION dbo.fn_MasterFundAllocation_Combined
	(
	@MasterFundID int,
	@ReportDate datetime,
	@KnowledgeDateFrom datetime, 
	@KnowledgeDateTo datetime,
	@KnowledgeDateLinkedTables datetime
	)
RETURNS @AllocationResults
TABLE 
	(
	RN int IDENTITY (1, 1), 
	RowData varchar(100), 
	MainFund varchar(100) ,
	Fund1 varchar(100) ,
	Fund2 varchar(100) ,
	Fund3 varchar(100) 
	) 
AS
BEGIN
	DECLARE @ReportStartDate datetime
	DECLARE @ReportEndDate datetime

	-- Establish Start and End dates, Being the last day of sequential months

	SELECT @ReportStartDate = '1 Jan 2000'

	SELECT @ReportStartDate = DATEADD(year, YEAR(@ReportDate) - YEAR(@ReportStartDate), @ReportStartDate)
	SELECT @ReportStartDate = DATEADD(month, MONTH(@ReportDate) - MONTH(@ReportStartDate), @ReportStartDate)

	SELECT @ReportEndDate = DATEADD(month, 1, @ReportStartDate)

	SELECT @ReportStartDate = DATEADD(day, -1, @ReportStartDate)
	SELECT @ReportEndDate = DATEADD(day, -1, @ReportEndDate)


	
	INSERT INTO @AllocationResults(RowData, MainFund, Fund1, Fund2, Fund3)
	SELECT RowData, MainFund, Fund1, Fund2, Fund3
	FROM fn_MasterFundAllocation(@MasterFundID, @ReportEndDate, @KnowledgeDateFrom, @KnowledgeDateTo, @KnowledgeDateLinkedTables)

	INSERT INTO @AllocationResults(RowData, MainFund, Fund1, Fund2, Fund3)
	SELECT 'LastMonth_' + RowData, MainFund, Fund1, Fund2, Fund3
	FROM fn_MasterFundAllocation(@MasterFundID, @ReportStartDate, @KnowledgeDateFrom, @KnowledgeDateFrom, @KnowledgeDateLinkedTables)

	RETURN
END




GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  SELECT  ON [dbo].[fn_MasterFundAllocation_Combined]  TO [InvestMaster_Sales]
GO

GRANT  SELECT  ON [dbo].[fn_MasterFundAllocation_Combined]  TO [Reporter]
GO


if not exists (select * from dbo.syscolumns where (id = object_id(N'[dbo].[tblTransaction]')) and (OBJECTPROPERTY(id, N'IsUserTable') = 1) and (name='TransactionEffectivePrice'))
BEGIN
ALTER TABLE [dbo].[tblTransaction] ADD 
	[TransactionEffectivePrice] [float] NOT NULL CONSTRAINT [DF_tblTransaction_TransactionEffectivePrice] DEFAULT (0), 
	[TransactionEffectiveWatermark] [float] NOT NULL CONSTRAINT [DF_tblTransaction_TransactionEffectiveWatermark] DEFAULT (0), 
	[TransactionSpecificInitialEqualisationValue] [float] NOT NULL CONSTRAINT [DF_tblTransaction_TransactionSpecificInitialEqualisationValue] DEFAULT (0), 
	[TransactionEffectiveValueDate] [datetime] NULL, 
	[TransactionIsTransfer] [bit] NOT NULL CONSTRAINT [DF_tblTransaction_TransactionIsTransfer] DEFAULT (0), 
	[TransactionSpecificInitialEqualisationFlag] [bit] NOT NULL CONSTRAINT [DF_tblTransaction_TransactionSpecificInitialEqualisationFlag] DEFAULT (0)
END
GO

UPDATE tblTransaction
SET 	TransactionEffectivePrice = TransactionPrice, 
	TransactionEffectiveWatermark = 0, 
	TransactionSpecificInitialEqualisationValue = 0, 
	TransactionEffectiveValueDate = TransactionValueDate, 
	TransactionIsTransfer = 0, 
	TransactionSpecificInitialEqualisationFlag = 0
WHERE TransactionEffectiveValueDate IS Null

GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[fn_tblTransaction_SelectKD]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[fn_tblTransaction_SelectKD]
GO




SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE FUNCTION dbo.fn_tblTransaction_SelectKD
(
@KnowledgeDate datetime
)
RETURNS TABLE
AS
RETURN
(
SELECT 	RN ,
	AuditID ,
	TransactionID ,
	TransactionTicket ,
	TransactionFund ,
	TransactionSubFund ,
	TransactionSubFundValueFlag ,
	TransactionInstrument ,
	TransactionType ,
	TransactionType_Contra ,
	TransactionValueorAmount ,
	TransactionUnits ,
	TransactionSignedUnits ,
	TransactionPrice ,
	TransactionCosts ,
	TransactionCostPercent , 
	TransactionSettlement ,
	TransactionSignedSettlement ,
	TransactionMainFundPrice ,
	TransactionFXRate ,
	TransactionEffectivePrice , 
	TransactionEffectiveWatermark , 
	TransactionSpecificInitialEqualisationValue ,
	TransactionCounterparty ,
	TransactionInvestment ,
	TransactionExecution ,
	TransactionDataEntry ,
	TransactionDecisionDate ,
	TransactionValueDate ,
	TransactionFIFOValueDate ,
	TransactionEffectiveValueDate , 
	TransactionSettlementDate ,
	TransactionConfirmationDate ,
	TransactionEntryDate ,
	TransactionCostIsPercent , 
	TransactionExemptFromUpdate , 
	TransactionRedeemWholeHoldingFlag , 
	TransactionIsTransfer , 
	TransactionSpecificInitialEqualisationFlag , 
	TransactionFinalAmount ,
	TransactionFinalPrice ,
	TransactionFinalCosts ,
	TransactionFinalSettlement ,
	TransactionInstructionFlag ,
	TransactionBankConfirmation ,
	TransactionInitialDataEntry ,
	TransactionAmountConfirmed ,
	TransactionSettlementConfirmed ,
	TransactionFinalDataEntry ,
	TransactionComment ,
	TransactionCurrent ,
	TransactionParentID ,
	TransactionRedemptionID ,
	TransactionUser ,
	TransactionGroup ,
	TransactionLeg ,
	TransactionAdminStatus ,
	TransactionCTFLAGS ,
	TransactionCompoundRN ,
	TransactionDateEntered,
	TransactionDateDeleted
FROM tblTransaction
WHERE RN IN ( SELECT MAX(RN) 
              FROM tblTransaction 
              WHERE (((TransactionDateEntered <= @KnowledgeDate) or (TransactionDateEntered IS NULL)) OR (ISNULL(@KnowledgeDate, '1 Jan 1900') <= '1 Jan 1900')) AND 
                    (((TransactionDateDeleted > @KnowledgeDate) or (TransactionDateDeleted IS NULL)) AND (NOT ((ISNULL(@KnowledgeDate, '1 Jan 1900')  <= '1 Jan 1900') AND (NOT (TransactionDateDeleted is NULL))))) AND 
                    (TransactionParentID > 0)
              GROUP BY TransactionParentID, TransactionLeg )


)

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  SELECT  ON [dbo].[fn_tblTransaction_SelectKD]  TO [InvestMaster_Sales]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[fn_tblTransaction_FirstLeg_SelectKD]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[fn_tblTransaction_FirstLeg_SelectKD]
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE FUNCTION dbo.fn_tblTransaction_FirstLeg_SelectKD
(
@KnowledgeDate datetime
)
RETURNS TABLE
AS
RETURN
(
SELECT 	RN ,
	AuditID ,
	TransactionID ,
	TransactionTicket ,
	TransactionFund ,
	TransactionSubFund ,
	TransactionSubFundValueFlag ,
	TransactionInstrument ,
	TransactionType ,
	TransactionType_Contra ,
	TransactionValueorAmount ,
	TransactionUnits ,
	TransactionSignedUnits ,
	TransactionPrice ,
	TransactionCosts ,
	TransactionCostPercent , 
	TransactionSettlement ,
	TransactionSignedSettlement ,
	TransactionMainFundPrice ,
	TransactionFXRate ,
	TransactionEffectivePrice , 
	TransactionEffectiveWatermark , 
	TransactionSpecificInitialEqualisationValue ,
	TransactionCounterparty ,
	TransactionInvestment ,
	TransactionExecution ,
	TransactionDataEntry ,
	TransactionDecisionDate ,
	TransactionValueDate ,
	TransactionFIFOValueDate ,
	TransactionEffectiveValueDate , 
	TransactionSettlementDate ,
	TransactionConfirmationDate ,
	TransactionEntryDate ,
	TransactionCostIsPercent , 
	TransactionExemptFromUpdate , 
	TransactionRedeemWholeHoldingFlag , 
	TransactionIsTransfer , 
	TransactionSpecificInitialEqualisationFlag , 
	TransactionFinalAmount ,
	TransactionFinalPrice ,
	TransactionFinalCosts ,
	TransactionFinalSettlement ,
	TransactionInstructionFlag ,
	TransactionBankConfirmation ,
	TransactionInitialDataEntry ,
	TransactionAmountConfirmed ,
	TransactionSettlementConfirmed ,
	TransactionFinalDataEntry ,
	TransactionComment ,
	TransactionCurrent ,
	TransactionParentID ,
	TransactionRedemptionID ,
	TransactionUser ,
	TransactionGroup ,
	TransactionLeg ,
	TransactionAdminStatus ,
	TransactionCTFLAGS ,
	TransactionCompoundRN ,
	TransactionDateEntered,
	TransactionDateDeleted
FROM fn_tblTransaction_SelectKD (@KnowledgeDate)
WHERE (TransactionLeg = 1)

)

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  SELECT  ON [dbo].[fn_tblTransaction_FirstLeg_SelectKD]  TO [InvestMaster_Sales]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[adp_tblTransaction_FirstLeg_SelectCommand]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[adp_tblTransaction_FirstLeg_SelectCommand]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[adp_tblTransaction_InsertCommand]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[adp_tblTransaction_InsertCommand]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[adp_tblTransaction_SelectCommand]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[adp_tblTransaction_SelectCommand]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[adp_tblTransaction_UpdateCommand]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[adp_tblTransaction_UpdateCommand]
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE PROCEDURE dbo.adp_tblTransaction_FirstLeg_SelectCommand
(
@KnowledgeDate datetime = Null
)
AS
	SET NOCOUNT ON

	SELECT	[RN], 
			[AuditID], 
			TransactionID ,
			TransactionTicket ,
			TransactionFund ,
			TransactionSubFund ,
			TransactionSubFundValueFlag ,
			TransactionInstrument ,
			TransactionType ,
			TransactionType_Contra ,
			TransactionValueorAmount ,
			TransactionUnits ,
			TransactionSignedUnits ,
			TransactionPrice ,
			TransactionCosts ,
			TransactionCostPercent , 
			TransactionSettlement ,
			TransactionSignedSettlement ,
			TransactionMainFundPrice ,
			TransactionFXRate ,
			TransactionEffectivePrice , 
			TransactionEffectiveWatermark , 
			TransactionSpecificInitialEqualisationValue ,
			TransactionCounterparty ,
			TransactionInvestment ,
			TransactionExecution ,
			TransactionDataEntry ,
			TransactionDecisionDate ,
			TransactionValueDate ,
			TransactionFIFOValueDate ,
			TransactionEffectiveValueDate , 
			TransactionSettlementDate ,
			TransactionConfirmationDate ,
			TransactionEntryDate ,
			TransactionCostIsPercent , 
			TransactionExemptFromUpdate , 
			TransactionRedeemWholeHoldingFlag , 
			TransactionIsTransfer , 
			TransactionSpecificInitialEqualisationFlag , 
			TransactionFinalAmount ,
			TransactionFinalPrice ,
			TransactionFinalCosts ,
			TransactionFinalSettlement ,
			TransactionInstructionFlag ,
			TransactionBankConfirmation ,
			TransactionInitialDataEntry ,
			TransactionAmountConfirmed ,
			TransactionSettlementConfirmed ,
			TransactionFinalDataEntry ,
			TransactionComment ,
			TransactionCurrent ,
			TransactionParentID ,
			TransactionRedemptionID , 
			TransactionUser ,
			TransactionGroup ,
			TransactionLeg ,
			TransactionAdminStatus ,
			TransactionCTFLAGS ,
			TransactionCompoundRN ,
			TransactionDateEntered,
			TransactionDateDeleted
			
	FROM fn_tblTransaction_SelectKD(@Knowledgedate)
	WHERE TransactionLeg = 1
			
	RETURN -1



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[adp_tblTransaction_FirstLeg_SelectCommand]  TO [InvestMaster_Sales]
GO

GRANT  EXECUTE  ON [dbo].[adp_tblTransaction_FirstLeg_SelectCommand]  TO [InvestMaster_Manager]
GO

GRANT  EXECUTE  ON [dbo].[adp_tblTransaction_FirstLeg_SelectCommand]  TO [InvestMaster_Admin]
GO

GRANT  EXECUTE  ON [dbo].[adp_tblTransaction_FirstLeg_SelectCommand]  TO [InvestMaster_DataEntry]
GO

GRANT  EXECUTE  ON [dbo].[adp_tblTransaction_FirstLeg_SelectCommand]  TO [InvestMaster_AddTransaction]
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE PROCEDURE dbo.adp_tblTransaction_InsertCommand
	(
	@TransactionTicket nvarchar (50),
	@TransactionFund int,
	@TransactionSubFund int = Null, 
	@TransactionSubFundValueFlag smallint = 0,
	@TransactionInstrument int,
	@TransactionType int,
	@TransactionValueorAmount nvarchar (10),
	@TransactionUnits float = 0,
	@TransactionPrice float = 0,
	@TransactionCosts float = 0,
	@TransactionCostPercent  float = 0, 
	@TransactionMainFundPrice float = 0,
	@TransactionFXRate float = 0,
	@TransactionEffectivePrice float = NULL, 
	@TransactionEffectiveWatermark float = NULL, 
	@TransactionSpecificInitialEqualisationValue float = 0,
	@TransactionCounterparty int,
	@TransactionInvestment nvarchar (50),
	@TransactionExecution nvarchar (50),
	@TransactionDataEntry nvarchar (50),
	@TransactionDecisionDate datetime,
	@TransactionValueDate datetime,
	@TransactionFIFOValueDate datetime,
	@TransactionEffectiveValueDate datetime = NULL, 
	@TransactionSettlementDate datetime,
	@TransactionConfirmationDate datetime,
	@TransactionCostIsPercent bit = 0, 
	@TransactionExemptFromUpdate bit = 0, 
	@TransactionRedeemWholeHoldingFlag bit = 0,
	@TransactionIsTransfer bit = 0, 
	@TransactionSpecificInitialEqualisationFlag bit = 0, 
	@TransactionFinalAmount int = 0,
	@TransactionFinalPrice int = 0,
	@TransactionFinalCosts int = 0,
	@TransactionFinalSettlement int = 0,
	@TransactionInstructionFlag int = 0,
	@TransactionBankConfirmation int = 0,
	@TransactionInitialDataEntry int = 0,
	@TransactionAmountConfirmed int = 0,
	@TransactionSettlementConfirmed int = 0,
	@TransactionFinalDataEntry int = 0,
	@TransactionComment varchar (500),
	@TransactionParentID int = 0,
	@TransactionRedemptionID int = 0, 
	@TransactionGroup nvarchar (50),
	@TransactionAdminStatus int = 0,
	@TransactionCTFLAGS int = 0,
	@TransactionCompoundRN int = 0,
	@KnowledgeDate datetime, 
	@ValidationOnly int = 0,
	@rvStatusString varchar(200) OUTPUT	
	)
  --**************************************************************************************************
  -- Purpose: Post a transaction to the 'tblTransaction' table.
  --          This involves posting two records, The first describing the instrument transacted and the
  --          second describing the cash consideration. The consideration is posted in the currency of
  --          the instrument unless it is a FX transaction in which case the consideration is in USD.
  --
  -- Accepts:
  --
  -- Returns: ParentID of the new Transaction.
  --
  --**************************************************************************************************
AS
	SET NOCOUNT OFF

	-- **************************************************
	-- CONSTANTS.
	--
	-- These values used to be looked-up by reference to their
	-- various descriptions, but since the descriptions are as
	-- liable to change as the IDs it seemed easier just to 
	-- consider these IDs as constant.
	-- By and Large, changes to these items are restricted so that
	-- Under any presently imagined scenario, these values ARE
	-- constant.
	-- **************************************************

	DECLARE @TRANSACTIONTYPE_Consideration int
	DECLARE @TRANSACTIONTYPE_Subscribe int
	DECLARE @TRANSACTIONTYPE_Redeem int
	DECLARE @TRANSACTIONTYPE_Buy int
	DECLARE @TRANSACTIONTYPE_Sell int
	DECLARE @TRANSACTIONTYPE_BuyFX int
	DECLARE @TRANSACTIONTYPE_SellFX int
	DECLARE @TRANSACTIONTYPE_Fee int
	DECLARE @TRANSACTIONTYPE_InterestPayment int
	DECLARE @TRANSACTIONTYPE_InterestIncome int
	DECLARE @TRANSACTIONTYPE_Expense int
	
	DECLARE @COUNTERPARTY_MARKET int
	
	DECLARE @INSTRUMENTTYPE_Unit int
	DECLARE @INSTRUMENTTYPE_Cash int
	DECLARE @INSTRUMENTTYPE_Fee int
	DECLARE @INSTRUMENTTYPE_Interest int
	DECLARE @INSTRUMENTTYPE_Expense int
	DECLARE @INSTRUMENTTYPE_UnrealisedPnL int 
	
	DECLARE @INSTRUMENT_USD int
	
	SELECT @TRANSACTIONTYPE_Consideration = 1
	SELECT @TRANSACTIONTYPE_Subscribe = 2
	SELECT @TRANSACTIONTYPE_Redeem = 3
	SELECT @TRANSACTIONTYPE_Buy = 4
	SELECT @TRANSACTIONTYPE_Sell = 5	
	SELECT @TRANSACTIONTYPE_BuyFX = 6
	SELECT @TRANSACTIONTYPE_SellFX = 7	
	SELECT @TRANSACTIONTYPE_Fee = 8
	SELECT @TRANSACTIONTYPE_InterestPayment = 10
	SELECT @TRANSACTIONTYPE_InterestIncome = 11
	SELECT @TRANSACTIONTYPE_Expense = 13
	
	SELECT @COUNTERPARTY_MARKET = 1

	SELECT @INSTRUMENTTYPE_Unit = 2
	SELECT @INSTRUMENTTYPE_Cash = 3
	SELECT @INSTRUMENTTYPE_Interest = 5
	SELECT @INSTRUMENTTYPE_Fee = 7
	SELECT @INSTRUMENTTYPE_Expense = 10
	SELECT @INSTRUMENTTYPE_UnrealisedPnL = 11

	SELECT @INSTRUMENT_USD = 1 -- USD Instrument ID - Settlement Currency for FX transactions
	
	-- **************************************************

	DECLARE @New_TransactionID int
	DECLARE @New_TransactionParentID int
	DECLARE @New_TransactionTicket nvarchar (50)
	DECLARE @FXFlag int
	DECLARE @CashMultiplier float
	DECLARE @TransactionSettlement float
	DECLARE @ModifiedSettlement float
	DECLARE @SecondLeg_InstrumentID int
	DECLARE @SecondLeg_TransactionType int
	DECLARE @TransactionTypeName nvarchar(50)
	
	DECLARE @TransactionSignedSettlement float

	DECLARE @InstrumentType int
	DECLARE @InstrumentLastValidTradeDate datetime

			
	-- Validate KD, Don't save for non 'LIVE' Knowledgedates.
	IF (ISNULL(@Knowledgedate, '1 Jan 1900') < (getdate())) AND (ISNULL(@Knowledgedate, '1 Jan 1900') > '1 Jan 1900')
	  BEGIN
		SELECT @rvStatusString = 'KnowledgeDate is not LIVE.'
		GOTO ExitWithError
	  END
	  
	-- Validate Parameters
	IF ISNULL(@TransactionFund, 0) <= 0
	  BEGIN
		SELECT @rvStatusString = 'Invalid Fund ID'
		GOTO ExitWithError
	  END

	IF ISNULL(@TransactionInstrument, 0) <= 0
	  BEGIN
		SELECT @rvStatusString = 'Invalid Instrument ID'
		GOTO ExitWithError
	  END

	IF ISNULL(@TransactionType, 0) <= 0
	  BEGIN
		SELECT @rvStatusString = 'Invalid Transaction Type'
		GOTO ExitWithError
	  END
	  
	SELECT @TransactionTypeName = ISNULL(TransactionType, 'Missing Type')
	FROM fn_tblTransactionType_SelectKD(@KnowledgeDate)
	WHERE TransactionTypeID = @TransactionType
	
	IF ISNULL(@TransactionCounterparty, 0) <= 0
	  BEGIN
		SELECT @rvStatusString = 'Invalid Counterparty'
		GOTO ExitWithError
	  END

	IF ISNULL(@TransactionDecisionDate, '1 Jan 1900') <= '1 Jan 1900'
	  BEGIN
		SELECT @rvStatusString = 'DecisionDate not set'
		GOTO ExitWithError
	  END

	IF ISNULL(@TransactionValueDate, '1 Jan 1900') <= '1 Jan 1900'
	  BEGIN
		SELECT @rvStatusString = 'ValueDate not set'
		GOTO ExitWithError
	  END

	IF ISNULL(@TransactionSettlementDate, '1 Jan 1900') <= '1 Jan 1900'
	  BEGIN
		SELECT @rvStatusString = 'SettlementDate not set'
		GOTO ExitWithError
	  END

	IF ISNULL(@TransactionConfirmationDate, '1 Jan 1900') <= '1 Jan 1900'
	  BEGIN
		SELECT @rvStatusString = 'ConfirmationDate not set'
		GOTO ExitWithError
	  END

	-- Parameters are OK, continue...

	-- Get 'CashMove' appropriate to this TransactionType

	  --  Note :
	  --  TransactionSignedUnits = TransactionUnits * (-1 * CashMultiplier)
	  --  TransactionSignedSettlement = TransactionSettlement * (-1 * CashMultiplier)
	  --  Consideration_TransactionSettlement = TransactionSettlement
	  --  Consideration_TransactionSignedSettlement = -TransactionSignedSettlement
	  --
	  --  set TransactionSigned and TransactionSignedSettlement using the cash multiplier.
	  --  note that the cash multiplier is negated.  This is because it refers to the cash movement on
	  --  the other side of the transaction so for a BUY it is -1 becuase cash is going out.
	
	SELECT @CashMultiplier = ISNULL(Max(TransactionTypeCashmove), 0)
	FROM fn_tblTransactionType_SelectKD(@KnowledgeDate)
	WHERE TransactionTypeID = @TransactionType

	IF @CashMultiplier = 0
	  BEGIN
		SELECT @rvStatusString = 'Failed to resolve Cash Multiplier (or Zero.)'
		GOTO ExitWithError
	  END

	  
	-- Calculate Settlement Value
	
	IF (@TransactionCostIsPercent <> 0)
	  BEGIN
	    SELECT @TransactionCosts = @TransactionCostPercent * (@TransactionUnits * @TransactionPrice)
	  END
	ELSE
	  BEGIN
	    IF (@TransactionUnits * @TransactionPrice) = 0
	      BEGIN
	        SELECT @TransactionCostPercent = 0
	        SELECT @TransactionCostIsPercent = 0
	      END
	    ELSE
	      BEGIN
	        SELECT @TransactionCostPercent = @TransactionCosts / (@TransactionUnits * @TransactionPrice)
	      END
	  END
	  
	SELECT @TransactionSettlement = Round((((@TransactionUnits) * @TransactionPrice) - (@TransactionCosts * @CashMultiplier)), 5)

	-- Resolve the FX Status
	-- Resolve also the Modified Settlement for the First Transaction Leg. 
	-- Resolve also the Second-Leg TransactionType and InstrumentID

	SELECT @FXFlag = 0

	IF (@TransactionType = @TRANSACTIONTYPE_BuyFX) OR (@TransactionType = @TRANSACTIONTYPE_SellFX)
	  BEGIN
		SELECT @FXFlag = (-1)
		SELECT @ModifiedSettlement = (@TransactionUnits)
		SELECT @SecondLeg_TransactionType = @TransactionType
		SELECT @SecondLeg_InstrumentID = @INSTRUMENT_USD
	  END
	ELSE
	  BEGIN
	    -- NOT FX
		SELECT @ModifiedSettlement = (@TransactionSettlement)
		SELECT @SecondLeg_TransactionType = @TRANSACTIONTYPE_Consideration
		
		SELECT @SecondLeg_InstrumentID = ISNULL(InstrumentCurrencyID, 0)
		FROM fn_tblInstrument_SelectKD(@KnowledgeDate)
		WHERE InstrumentID = @TransactionInstrument
	  END
	  

	-- Validate Counterparty vs TransactionType
    
	IF @TransactionCounterparty = @COUNTERPARTY_MARKET 
	  BEGIN
		IF (@TransactionType IN (@TRANSACTIONTYPE_Subscribe, @TRANSACTIONTYPE_Redeem)) 
		  BEGIN
			SELECT @rvStatusString = 'Counterparty must not be `Market` for Transaction type of `Subscribe` or `Redeem`.'
			GOTO ExitWithError
		  END
	  END
	ELSE
	  BEGIN
		-- Counterparty is NOT 'MARKET'
		
		IF (@TransactionType IN (@TRANSACTIONTYPE_Subscribe, @TRANSACTIONTYPE_Redeem)) 
		  BEGIN
			-- Counterparty is not MARKET and TransactionType IS Subscribe or Redeem.
			-- Check that the transaction is in the Fund's Units. Subscriptions and Redemptions
			-- May only be done in the given Fund's Units.
			
			DECLARE @FundCount Int
			
			SELECT @FundCount = ISNULL(Count(RN), 0)
			FROM fn_tblFund_SelectKD(@KnowledgeDate)
			WHERE (FundID = @TransactionFund) AND (FundUnit = @TransactionInstrument)
			
			IF @FundCount = 0
			  BEGIN
				SELECT @rvStatusString = 'You can only Subscribe or Redeem this fund`s units.'
				GOTO ExitWithError
			  END
		  END
		ELSE
		  BEGIN
			-- Counterparty is Not Market and Transaction Type is NOT Subscribe ot Redeem.
			
			SELECT @rvStatusString = 'Counterparty must be `Market` unless Transaction type is `Subscribe` or `Redeem`.'
			GOTO ExitWithError
		  END
	  END
  
	-- Validate Transaction Type for Cash Instruments
	-- Get @InstrumentType AND @InstrumentLastValidTradeDate to save a second query later.
	
	SELECT @InstrumentType = ISNULL(Max(InstrumentType), 0), @InstrumentLastValidTradeDate = ISNULL(Max(InstrumentLastValidTradeDate), '1 Jan 1900')
	FROM fn_tblInstrument_SelectKD(@KnowledgeDate)
	WHERE InstrumentID = @TransactionInstrument
	
	IF @InstrumentType = @INSTRUMENTTYPE_Cash 
	  BEGIN
		IF NOT (@TransactionType IN (@TRANSACTIONTYPE_BuyFX, @TRANSACTIONTYPE_SellFX)) 
		  BEGIN
			SELECT @rvStatusString = 'Transaction type must be `BUY FX` or `SELL FX` if the Instrument is of type `Cash`.'
			GOTO ExitWithError
		  END
	  END
	  
	IF (@InstrumentType = @INSTRUMENTTYPE_Fee) OR (@TransactionType = @TRANSACTIONTYPE_Fee)
	  BEGIN
		IF (@InstrumentType <> @INSTRUMENTTYPE_Fee) OR (@TransactionType <> @TRANSACTIONTYPE_Fee)
		  BEGIN
			SELECT @rvStatusString = 'Transaction Type `FEE` must be used with FEE Instruments.'
			GOTO ExitWithError
		  END
	  END
	  		  
	IF (@InstrumentType = @INSTRUMENTTYPE_Interest) OR (@TransactionType IN (@TRANSACTIONTYPE_InterestPayment, @TRANSACTIONTYPE_InterestIncome))
	  BEGIN
		IF (@InstrumentType <> @INSTRUMENTTYPE_Interest) OR (NOT (@TransactionType IN (@TRANSACTIONTYPE_InterestPayment, @TRANSACTIONTYPE_InterestIncome)))
		  BEGIN
			SELECT @rvStatusString = 'Transaction Types `INTEREST INCOME` or `INTEREST PAYMENT` must be used with INTEREST Instruments.'
			GOTO ExitWithError
		  END
	  END
	  		  
	IF (@InstrumentType = @INSTRUMENTTYPE_Expense) OR (@TransactionType = @TRANSACTIONTYPE_Expense)
	  BEGIN
		IF (@InstrumentType <> @INSTRUMENTTYPE_Expense) OR (@TransactionType <> @TRANSACTIONTYPE_Expense)
		  BEGIN
			SELECT @rvStatusString = 'Transaction Type `EXPENSE` must be used with EXPENSE Instruments.'
			GOTO ExitWithError
		  END
	  END
	  		  
	-- Validate Latest Trade Date
	-- Always Allow Sells or Redemptions.

	IF (ISNULL(@InstrumentLastValidTradeDate, '1 Jan 1900') > '1 Jan 1900') AND (ISNULL(@InstrumentLastValidTradeDate, '1 Jan 1900') < ISNULL(@TransactionValueDate, '1 Jan 1900')) 
	  BEGIN
		IF NOT (@TransactionType IN (@TRANSACTIONTYPE_Sell, @TRANSACTIONTYPE_Redeem))
		  BEGIN
			SELECT @rvStatusString = 'The latest allowed Trade Date for this Instrument is ' + CONVERT(varchar(20), @InstrumentLastValidTradeDate, 106)
			GOTO ExitWithError
		  END
	  END


	-- **************************************************
	-- Start to Insert Transactions :-
	-- **************************************************

	-- Get tblTransaction Lock.
	DECLARE @sp_LockReturn int

	IF ISNULL(@ValidationOnly, 0) = 0
	  BEGIN
		EXEC @sp_LockReturn = sp_getapplock @Resource = 'tblTransaction', @LockMode  = 'exclusive', @LockOwner = 'session'
		IF @sp_LockReturn < 0
		  BEGIN
			SELECT @rvStatusString = 'Failed to get sp_getapplock on tblTransaction.'
			GOTO ExitWithError
		  END
	  END

	  
	SET TRANSACTION ISOLATION LEVEL SERIALIZABLE

	-- Resolve Transaction Ticket
	
	IF ISNULL(@TransactionTicket, '') = ''
	  BEGIN
	  
		-- FX Trade ?  (Ticket 'FX')
		IF @FXFlag <> 0
		  BEGIN
			SELECT @New_TransactionTicket = dbo.fn_NewTransactionTicket('FX', 0)
		  END
		
		
		IF (@TransactionType IN (@TRANSACTIONTYPE_Subscribe, @TRANSACTIONTYPE_Redeem)) 
		  BEGIN
			-- Is this Counterparty a Fund ? (Ticket 'IN' - Internal) 
			-- Else TicketType of 'SR' - Subscribe / Redeem
			
			IF ISNULL(@New_TransactionTicket, '') = ''
			  BEGIN
				DECLARE @CounterpartyFundID Int
			
				SELECT @CounterpartyFundID = ISNULL(MAX(CounterpartyFundID), 0)
				FROM fn_tblCounterparty_SelectKD(@KnowledgeDate)
				WHERE CounterpartyID = @TransactionCounterparty
			
				IF ISNULL(@CounterpartyFundID, 0) > 0
				  BEGIN
					SELECT @New_TransactionTicket = dbo.fn_NewTransactionTicket('IN', 0)
				  END
				ELSE
				  BEGIN
					SELECT @New_TransactionTicket = dbo.fn_NewTransactionTicket('SR', 0)
				  END
			  END
		  END -- TransactionType IN (Subscribe , Redeem)	
		
		IF (@TransactionType IN (@TRANSACTIONTYPE_Buy, @TRANSACTIONTYPE_Sell)) 
		  BEGIN
			IF ISNULL(@New_TransactionTicket, '') = ''
			  BEGIN
				-- If this Instrument is a Unit, then TicketType = 'IN' - i.e. We are buying one of our own Units.
				-- else TicketType = 'BS' - we are buying / Selling something else.
			  
				DECLARE @IsAnFandCFund int
				
				SELECT @IsAnFandCFund = ISNULL(Count(RN), 0)
				FROM fn_tblInstrument_SelectKD(@KnowledgeDate)
				WHERE (InstrumentType = @INSTRUMENTTYPE_Unit) AND (InstrumentID = @TransactionInstrument)
				
				IF @IsAnFandCFund > 0
				  BEGIN
					SELECT @New_TransactionTicket = dbo.fn_NewTransactionTicket('IN', 0)
				  END
				ELSE
				  BEGIN
					SELECT @New_TransactionTicket = dbo.fn_NewTransactionTicket('BS', 0)
				  END
			  END
		  END
		
		IF ISNULL(@New_TransactionTicket, '') = ''
		  BEGIN
			IF (@TransactionType = @TRANSACTIONTYPE_Fee) 
			  BEGIN
				SELECT @New_TransactionTicket = dbo.fn_NewTransactionTicket('EX', 0) -- 'Ex'pense
			  END
			ELSE
			IF NOT (@TransactionType IN (@TRANSACTIONTYPE_InterestPayment, @TRANSACTIONTYPE_InterestIncome))
			  BEGIN
				SELECT @New_TransactionTicket = dbo.fn_NewTransactionTicket('RE', 0) -- 'Revenue & Expenditure'
			  END
		  END
		
		IF ISNULL(@New_TransactionTicket, '') = ''
		  BEGIN
			SELECT @New_TransactionTicket = dbo.fn_NewTransactionTicket('OT', 0)  -- 'Ot'her
		  END
		
	  END
	ELSE
	  BEGIN
		SELECT @New_TransactionTicket = @TransactionTicket
	  END


	-- Validation Only

	IF ISNULL(@ValidationOnly, 0) <> 0
	  BEGIN
	    RETURN (1)
	  END
	  
	-- Start Transaction.	
		
	BEGIN TRANSACTION
	
	-- Parent ID
	
	IF ISNULL(@TransactionParentID, 0) > 0 
	  BEGIN
		SELECT @New_TransactionParentID = ISNULL(@TransactionParentID, 0)
		
		-- Clear 'Current' Status of Transactions being superceeded.
		UPDATE tblTransaction
		SET TransactionCurrent = 0
		WHERE TransactionParentID = ISNULL(@TransactionParentID, 0)
		
	  END
	ELSE
	  BEGIN
	    SELECT @New_TransactionParentID = (IDENT_CURRENT('tblTransaction') +1)
	  END
	
	-- Insert First Leg
	
	INSERT INTO tblTransaction(
		TransactionTicket, 
		TransactionFund, 
		TransactionInstrument,  
		TransactionType, 
		TransactionType_Contra, 
		TransactionValueorAmount, 
		TransactionUnits, 
		TransactionSignedUnits, 
		TransactionPrice, 
		TransactionCosts, 
		TransactionCostPercent, 
		TransactionSettlement, 
		TransactionSignedSettlement, 
		TransactionEffectivePrice , 
		TransactionEffectiveWatermark , 
		TransactionSpecificInitialEqualisationValue ,
		TransactionCounterparty, 
		TransactionInvestment, 
		TransactionExecution,  
		TransactionDataEntry, 
		TransactionDecisionDate, 
		TransactionValueDate, 
		TransactionFIFOValueDate, 
		TransactionEffectiveValueDate , 
		TransactionSettlementDate,  
		TransactionConfirmationDate, 
		TransactionCostIsPercent, 
		TransactionExemptFromUpdate, 
		TransactionRedeemWholeHoldingFlag, 
		TransactionIsTransfer , 
		TransactionSpecificInitialEqualisationFlag , 
		TransactionFinalAmount, 
		TransactionFinalPrice, 
		TransactionFinalCosts,  
		TransactionFinalSettlement, 
		TransactionInstructionFlag, 
		TransactionBankConfirmation, 
		TransactionInitialDataEntry, 
		TransactionAmountConfirmed,  
		TransactionSettlementConfirmed, 
		TransactionFinalDataEntry, 
		TransactionComment, 
		TransactionParentID, 
		TransactionRedemptionID, 
		TransactionGroup, 
		TransactionLeg, 
		TransactionCurrent, 
		TransactionAdminStatus, 
		TransactionCompoundRN, 
		TransactionCTFLAGS
		)
	VALUES (
		@New_TransactionTicket, 
		@TransactionFund, 
		@TransactionInstrument,  
		@TransactionType, 
		@TransactionType, 
		@TransactionValueorAmount, 
		(@TransactionUnits), 
		@TransactionUnits * ((-1.0) * @CashMultiplier), 
		@TransactionPrice, 
		@TransactionCosts,  
		@TransactionCostPercent, 
		(@ModifiedSettlement), 
		@ModifiedSettlement * ((-1.0) * @CashMultiplier), 
		ISNULL(@TransactionEffectivePrice, @TransactionPrice) , 
		ISNULL(@TransactionEffectiveWatermark, 0) , 
		ISNULL(@TransactionSpecificInitialEqualisationValue, 0) ,
		@TransactionCounterparty, 
		@TransactionInvestment, 
		@TransactionExecution,  
		@TransactionDataEntry, 
		ISNULL(@TransactionDecisionDate, @TransactionValueDate), 
		@TransactionValueDate, 
		ISNULL(@TransactionFIFOValueDate, @TransactionValueDate), 
		ISNULL(@TransactionEffectiveValueDate, @TransactionValueDate), 
		ISNULL(@TransactionSettlementDate, @TransactionValueDate), 
		ISNULL(@TransactionConfirmationDate, @TransactionValueDate), 
		@TransactionCostIsPercent, 
		@TransactionExemptFromUpdate, 
		ISNULL(@TransactionRedeemWholeHoldingFlag, 0), 
		ISNULL(@TransactionIsTransfer, 0), 
		ISNULL(@TransactionSpecificInitialEqualisationFlag, 0), 
		@TransactionFinalAmount, 
		@TransactionFinalPrice, 
		@TransactionFinalCosts,  
		@TransactionFinalSettlement, 
		@TransactionInstructionFlag, 
		@TransactionBankConfirmation, 
		@TransactionInitialDataEntry, 
		@TransactionAmountConfirmed,  
		@TransactionSettlementConfirmed, 
		@TransactionFinalDataEntry, 
		ISNULL(@TransactionComment, ''), 
		@New_TransactionParentID, 
		ISNULL(@TransactionRedemptionID, 0), 
		ISNULL(@TransactionGroup, ''), 
		1, 
		1, 
		@TransactionAdminStatus, 
		@TransactionCompoundRN, 
		@TransactionCTFLAGS
		)
	
	IF @@ERROR <> 0
	BEGIN
		ROLLBACK TRANSACTION
		EXEC sp_releaseapplock @Resource = 'tblTransaction', @LockOwner = 'session'
		SELECT @rvStatusString = 'Error inserting first Transaction leg.'
		GOTO ExitWithError
	END
	
	-- Verify that in the event of a New Transaction, that the ParentID was set to the
	-- correct value, i.e. The TransactionID. This was predicted using the IDENT_CURRENT()
	-- function, but this is not ABSOLUTELY reliable, thus a check is made.
	-- If Transactions are ONLY entered using this procedure and the User Table Locking is 
	-- applioed, then this prediction should be correct, but just in case....
	
	IF ISNULL(@TransactionParentID, 0) <= 0 
	BEGIN
		IF @New_TransactionParentID <> SCOPE_IDENTITY()
		BEGIN
			SELECT @New_TransactionParentID = SCOPE_IDENTITY()

			UPDATE tblTransaction
			SET TransactionParentID = @New_TransactionParentID
			WHERE TransactionID = @New_TransactionParentID
		END
	END

	-- Second Leg
	
	INSERT INTO tblTransaction(
		TransactionTicket, 
		TransactionFund, 
		TransactionInstrument,  
		TransactionType, 
		TransactionType_Contra, 
		TransactionValueorAmount, 
		TransactionUnits, 
		TransactionSignedUnits, 
		TransactionPrice, 
		TransactionCosts,  
		TransactionCostPercent, 
		TransactionSettlement, 
		TransactionSignedSettlement, 
		TransactionEffectivePrice , 
		TransactionEffectiveWatermark , 
		TransactionSpecificInitialEqualisationValue ,
		TransactionCounterparty, 
		TransactionInvestment, 
		TransactionExecution,  
		TransactionDataEntry, 
		TransactionDecisionDate, 
		TransactionValueDate, 
		TransactionFIFOValueDate, 
		TransactionEffectiveValueDate , 
		TransactionSettlementDate,  
		TransactionConfirmationDate, 
		TransactionCostIsPercent, 
		TransactionExemptFromUpdate, 
		TransactionRedeemWholeHoldingFlag, 
		TransactionIsTransfer , 
		TransactionSpecificInitialEqualisationFlag , 
		TransactionFinalAmount, 
		TransactionFinalPrice, 
		TransactionFinalCosts,  
		TransactionFinalSettlement, 
		TransactionInstructionFlag, 
		TransactionBankConfirmation, 
		TransactionInitialDataEntry, 
		TransactionAmountConfirmed,  
		TransactionSettlementConfirmed, 
		TransactionFinalDataEntry, 
		TransactionComment, 
		TransactionParentID, 
		TransactionRedemptionID, 
		TransactionGroup, 
		TransactionLeg, 
		TransactionCurrent, 
		TransactionAdminStatus, 
		TransactionCompoundRN, 
		TransactionCTFLAGS
		)
	VALUES (
		@New_TransactionTicket, 
		@TransactionFund, 
		@SecondLeg_InstrumentID,  
		@SecondLeg_TransactionType, 
		@TransactionType, 
		'Amount', 
		(@TransactionSettlement), 
		@TransactionSettlement * @CashMultiplier, 
		1, 
		0, 
		0,  
		(@TransactionSettlement), 
		@TransactionSettlement * @CashMultiplier, 
		ISNULL(@TransactionEffectivePrice, @TransactionPrice) , 
		ISNULL(@TransactionEffectiveWatermark, 0) , 
		ISNULL(@TransactionSpecificInitialEqualisationValue, 0) ,
		@TransactionCounterparty, 
		@TransactionInvestment, 
		@TransactionExecution,  
		@TransactionDataEntry, 
		ISNULL(@TransactionDecisionDate, @TransactionValueDate), 
		@TransactionValueDate, 
		ISNULL(@TransactionFIFOValueDate, @TransactionValueDate), 
		ISNULL(@TransactionEffectiveValueDate, @TransactionValueDate), 
		ISNULL(@TransactionSettlementDate, @TransactionValueDate), 
		ISNULL(@TransactionConfirmationDate, @TransactionValueDate), 
		0, 
		@TransactionExemptFromUpdate, 
		ISNULL(@TransactionRedeemWholeHoldingFlag, 0), 
		ISNULL(@TransactionIsTransfer, 0), 
		ISNULL(@TransactionSpecificInitialEqualisationFlag, 0), 
		@TransactionFinalAmount, 
		@TransactionFinalPrice, 
		@TransactionFinalCosts,  
		@TransactionFinalSettlement, 
		@TransactionInstructionFlag, 
		@TransactionBankConfirmation, 
		@TransactionInitialDataEntry, 
		@TransactionAmountConfirmed,  
		@TransactionSettlementConfirmed, 
		@TransactionFinalDataEntry, 
		ISNULL(@TransactionTypeName, ''), 
		@New_TransactionParentID, 
		ISNULL(@TransactionRedemptionID, 0), 
		ISNULL(@TransactionGroup, ''), 
		2, 
		1, 
		@TransactionAdminStatus, 
		@TransactionCompoundRN, 
		@TransactionCTFLAGS
		)
	
	IF @@ERROR <> 0
	BEGIN
		ROLLBACK TRANSACTION
		EXEC sp_releaseapplock @Resource = 'tblTransaction', @LockOwner = 'session'
		SELECT @rvStatusString = 'Error inserting Second Transaction leg.'
		GOTO ExitWithError
	END
	  		
	
	
	-- Return value and Commit / Rollback transaction accordingly.
	
	IF @@ERROR = 0
	  BEGIN
	    COMMIT TRANSACTION
		EXEC sp_releaseapplock @Resource = 'tblTransaction', @LockOwner = 'session'
		
		SELECT	[RN], 
			[AuditID], 
			TransactionID ,
			TransactionTicket ,
			TransactionFund ,
			TransactionSubFund ,
			TransactionSubFundValueFlag ,
			TransactionInstrument ,
			TransactionType ,
			TransactionType_Contra ,
			TransactionValueorAmount ,
			TransactionUnits ,
			TransactionSignedUnits ,
			TransactionPrice ,
			TransactionCosts ,
			TransactionCostPercent , 
			TransactionSettlement ,
			TransactionSignedSettlement ,
			TransactionMainFundPrice ,
			TransactionFXRate ,
			TransactionEffectivePrice , 
			TransactionEffectiveWatermark , 
			TransactionSpecificInitialEqualisationValue ,
			TransactionCounterparty ,
			TransactionInvestment ,
			TransactionExecution ,
			TransactionDataEntry ,
			TransactionDecisionDate ,
			TransactionValueDate ,
			TransactionFIFOValueDate ,
			TransactionEffectiveValueDate , 
			TransactionSettlementDate ,
			TransactionConfirmationDate ,
			TransactionEntryDate ,
			TransactionCostIsPercent , 
			TransactionExemptFromUpdate , 
			TransactionRedeemWholeHoldingFlag , 
			TransactionIsTransfer , 
			TransactionSpecificInitialEqualisationFlag , 
			TransactionFinalAmount ,
			TransactionFinalPrice ,
			TransactionFinalCosts ,
			TransactionFinalSettlement ,
			TransactionInstructionFlag ,
			TransactionBankConfirmation ,
			TransactionInitialDataEntry ,
			TransactionAmountConfirmed ,
			TransactionSettlementConfirmed ,
			TransactionFinalDataEntry ,
			TransactionComment ,
			TransactionCurrent ,
			TransactionParentID ,
			TransactionRedemptionID , 
			TransactionUser ,
			TransactionGroup ,
			TransactionLeg ,
			TransactionAdminStatus ,
			TransactionCTFLAGS ,
			TransactionCompoundRN ,
			TransactionDateEntered,
			TransactionDateDeleted
		FROM fn_tblTransaction_SelectKD(@KnowledgeDate)
		WHERE (TransactionParentID = @New_TransactionParentID) AND (TransactionLeg = 1)
		
		SELECT @rvStatusString = 'OK'
	    RETURN @New_TransactionParentID
	  END
	ELSE
	  BEGIN
	    ROLLBACK TRANSACTION
		EXEC sp_releaseapplock @Resource = 'tblTransaction', @LockOwner = 'session'
		SELECT @rvStatusString = 'Error Saving Transaction.'
		GOTO ExitWithError
	  END
	
ExitWithError:

	-- Return 'Current' Row.
	
	IF ISNULL(@TransactionParentID, 0) > 0
	    BEGIN
		SELECT	[RN], 
				[AuditID], 
				TransactionID ,
				TransactionTicket ,
				TransactionFund ,
				TransactionSubFund ,
				TransactionSubFundValueFlag ,
				TransactionInstrument ,
				TransactionType ,
				TransactionType_Contra ,
				TransactionValueorAmount ,
				TransactionUnits ,
				TransactionSignedUnits ,
				TransactionPrice ,
				TransactionCosts ,
				TransactionCostPercent , 
				TransactionSettlement ,
				TransactionSignedSettlement ,
				TransactionMainFundPrice ,
				TransactionFXRate ,
				TransactionEffectivePrice , 
				TransactionEffectiveWatermark , 
				TransactionSpecificInitialEqualisationValue ,
				TransactionCounterparty ,
				TransactionInvestment ,
				TransactionExecution ,
				TransactionDataEntry ,
				TransactionDecisionDate ,
				TransactionValueDate ,
				TransactionFIFOValueDate ,
				TransactionEffectiveValueDate , 
				TransactionSettlementDate ,
				TransactionConfirmationDate ,
				TransactionEntryDate ,
				TransactionCostIsPercent , 
				TransactionExemptFromUpdate , 
				TransactionRedeemWholeHoldingFlag , 
				TransactionIsTransfer , 
				TransactionSpecificInitialEqualisationFlag , 
				TransactionFinalAmount ,
				TransactionFinalPrice ,
				TransactionFinalCosts ,
				TransactionFinalSettlement ,
				TransactionInstructionFlag ,
				TransactionBankConfirmation ,
				TransactionInitialDataEntry ,
				TransactionAmountConfirmed ,
				TransactionSettlementConfirmed ,
				TransactionFinalDataEntry ,
				TransactionComment ,
				TransactionCurrent ,
				TransactionParentID ,
				TransactionRedemptionID ,
				TransactionUser ,
				TransactionGroup ,
				TransactionLeg ,
				TransactionAdminStatus ,
				TransactionCTFLAGS ,
				TransactionCompoundRN ,
				TransactionDateEntered,
				TransactionDateDeleted
		FROM fn_tblTransaction_SelectKD(@KnowledgeDate)
		WHERE (TransactionParentID = @TransactionParentID) AND (TransactionLeg = 1)
	    END

	RETURN 0
	


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[adp_tblTransaction_InsertCommand]  TO [InvestMaster_Manager]
GO

GRANT  EXECUTE  ON [dbo].[adp_tblTransaction_InsertCommand]  TO [InvestMaster_Admin]
GO

GRANT  EXECUTE  ON [dbo].[adp_tblTransaction_InsertCommand]  TO [InvestMaster_AddTransaction]
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE PROCEDURE dbo.adp_tblTransaction_SelectCommand
(
@KnowledgeDate datetime = Null
)
AS
	SET NOCOUNT ON

	SELECT	[RN], 
		[AuditID], 
		TransactionID ,
		TransactionTicket ,
		TransactionFund ,
		TransactionSubFund ,
		TransactionSubFundValueFlag ,
		TransactionInstrument ,
		TransactionType ,
		TransactionType_Contra ,
		TransactionValueorAmount ,
		TransactionUnits ,
		TransactionSignedUnits ,
		TransactionPrice ,
		TransactionCosts ,
		TransactionCostPercent , 
		TransactionSettlement ,
		TransactionSignedSettlement ,
		TransactionMainFundPrice ,
		TransactionFXRate ,
		TransactionEffectivePrice , 
		TransactionEffectiveWatermark , 
		TransactionSpecificInitialEqualisationValue ,
		TransactionCounterparty ,
		TransactionInvestment ,
		TransactionExecution ,
		TransactionDataEntry ,
		TransactionDecisionDate ,
		TransactionValueDate ,
		TransactionFIFOValueDate ,
		TransactionEffectiveValueDate , 
		TransactionSettlementDate ,
		TransactionConfirmationDate ,
		TransactionEntryDate ,
		TransactionCostIsPercent , 
		TransactionExemptFromUpdate , 
		TransactionRedeemWholeHoldingFlag ,
		TransactionIsTransfer , 
		TransactionSpecificInitialEqualisationFlag , 
		TransactionFinalAmount ,
		TransactionFinalPrice ,
		TransactionFinalCosts ,
		TransactionFinalSettlement ,
		TransactionInstructionFlag ,
		TransactionBankConfirmation ,
		TransactionInitialDataEntry ,
		TransactionAmountConfirmed ,
		TransactionSettlementConfirmed ,
		TransactionFinalDataEntry ,
		TransactionComment ,
		TransactionCurrent ,
		TransactionParentID ,
		TransactionRedemptionID ,
		TransactionUser ,
		TransactionGroup ,
		TransactionLeg ,
		TransactionAdminStatus ,
		TransactionCTFLAGS ,
		TransactionCompoundRN ,
		TransactionDateEntered,
		TransactionDateDeleted
			
	FROM fn_tblTransaction_SelectKD(@Knowledgedate)
			
	RETURN -1

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[adp_tblTransaction_SelectCommand]  TO [InvestMaster_Sales]
GO

GRANT  EXECUTE  ON [dbo].[adp_tblTransaction_SelectCommand]  TO [InvestMaster_Manager]
GO

GRANT  EXECUTE  ON [dbo].[adp_tblTransaction_SelectCommand]  TO [InvestMaster_Admin]
GO

GRANT  EXECUTE  ON [dbo].[adp_tblTransaction_SelectCommand]  TO [InvestMaster_DataEntry]
GO

GRANT  EXECUTE  ON [dbo].[adp_tblTransaction_SelectCommand]  TO [InvestMaster_AddTransaction]
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE PROCEDURE dbo.adp_tblTransaction_UpdateCommand
	(
	@TransactionTicket nvarchar (50),
	@TransactionFund int,
	@TransactionSubFund int = Null, 
	@TransactionSubFundValueFlag smallint = 0,
	@TransactionInstrument int,
	@TransactionType int,
	@TransactionValueorAmount nvarchar (10),
	@TransactionUnits float = 0,
	@TransactionPrice float = 0,
	@TransactionCosts float = 0,
	@TransactionCostPercent  float = 0, 
	@TransactionMainFundPrice float = 0,
	@TransactionFXRate float = 0,
	@TransactionEffectivePrice float = NULL, 
	@TransactionEffectiveWatermark float = NULL, 
	@TransactionSpecificInitialEqualisationValue float = 0,
	@TransactionCounterparty int,
	@TransactionInvestment nvarchar (50),
	@TransactionExecution nvarchar (50),
	@TransactionDataEntry nvarchar (50),
	@TransactionDecisionDate datetime,
	@TransactionValueDate datetime,
	@TransactionFIFOValueDate datetime,
	@TransactionEffectiveValueDate datetime = NULL, 
	@TransactionSettlementDate datetime,
	@TransactionConfirmationDate datetime,
	@TransactionCostIsPercent bit = 0, 
	@TransactionExemptFromUpdate bit = 0, 
	@TransactionRedeemWholeHoldingFlag bit = 0,
	@TransactionIsTransfer bit = 0, 
	@TransactionSpecificInitialEqualisationFlag bit = 0, 
	@TransactionFinalAmount int = 0,
	@TransactionFinalPrice int = 0,
	@TransactionFinalCosts int = 0,
	@TransactionFinalSettlement int = 0,
	@TransactionInstructionFlag int = 0,
	@TransactionBankConfirmation int = 0,
	@TransactionInitialDataEntry int = 0,
	@TransactionAmountConfirmed int = 0,
	@TransactionSettlementConfirmed int = 0,
	@TransactionFinalDataEntry int = 0,
	@TransactionComment varchar (500),
	@TransactionParentID int = 0,
	@TransactionRedemptionID int = 0, 
	@TransactionGroup nvarchar (50),
	@TransactionAdminStatus int = 0,
	@TransactionCTFLAGS int = 0,
	@TransactionCompoundRN int = 0,
	@KnowledgeDate datetime, 
	@ValidationOnly int = 0,
	@rvStatusString varchar(200) OUTPUT	
	)
AS
	SET NOCOUNT OFF;

	DECLARE @RVal int 

	IF ((@Knowledgedate >= (getdate())) OR (ISNULL(@Knowledgedate, '1 Jan 1900') <= '1 Jan 1900')) AND 
		(ISNULL(@TransactionParentID, 0) > 0)
	  BEGIN
		EXECUTE @RVal = [dbo].[adp_tblTransaction_InsertCommand] 
			@TransactionTicket = @TransactionTicket, 
			@TransactionFund = @TransactionFund, 
			@TransactionSubFund = @TransactionSubFund, 
			@TransactionSubFundValueFlag = @TransactionSubFundValueFlag, 
			@TransactionInstrument = @TransactionInstrument, 
			@TransactionType = @TransactionType, 
			@TransactionValueorAmount = @TransactionValueorAmount, 
			@TransactionUnits = @TransactionUnits, 
			@TransactionPrice = @TransactionPrice, 
			@TransactionCosts = @TransactionCosts, 
			@TransactionCostPercent = @TransactionCostPercent, 
			@TransactionMainFundPrice = @TransactionMainFundPrice, 
			@TransactionFXRate = @TransactionFXRate, 			
			@TransactionEffectivePrice = @TransactionEffectivePrice, 
			@TransactionEffectiveWatermark = @TransactionEffectiveWatermark, 
			@TransactionSpecificInitialEqualisationValue = @TransactionSpecificInitialEqualisationValue,			
			@TransactionCounterparty = @TransactionCounterparty, 
			@TransactionInvestment = @TransactionInvestment, 
			@TransactionExecution = @TransactionExecution, 
			@TransactionDataEntry = @TransactionDataEntry, 
			@TransactionDecisionDate = @TransactionDecisionDate, 
			@TransactionValueDate = @TransactionValueDate,
			@TransactionFIFOValueDate = @TransactionFIFOValueDate,
			@TransactionEffectiveValueDate = @TransactionEffectiveValueDate, 
			@TransactionSettlementDate = @TransactionSettlementDate, 
			@TransactionConfirmationDate = @TransactionConfirmationDate, 
			@TransactionCostIsPercent = @TransactionCostIsPercent, 
			@TransactionExemptFromUpdate = @TransactionExemptFromUpdate, 
			@TransactionRedeemWholeHoldingFlag = @TransactionRedeemWholeHoldingFlag, 
			@TransactionIsTransfer = @TransactionIsTransfer, 
			@TransactionSpecificInitialEqualisationFlag = @TransactionSpecificInitialEqualisationFlag, 
			@TransactionFinalAmount = @TransactionFinalAmount,
			@TransactionFinalPrice = @TransactionFinalPrice,
			@TransactionFinalCosts = @TransactionFinalCosts,
			@TransactionFinalSettlement = @TransactionFinalSettlement,
			@TransactionInstructionFlag = @TransactionInstructionFlag,
			@TransactionBankConfirmation = @TransactionBankConfirmation, 
			@TransactionInitialDataEntry = @TransactionInitialDataEntry,
			@TransactionAmountConfirmed = @TransactionAmountConfirmed,
			@TransactionSettlementConfirmed = @TransactionSettlementConfirmed,
			@TransactionFinalDataEntry= @TransactionFinalDataEntry,
			@TransactionComment = @TransactionComment,
			@TransactionParentID = @TransactionParentID,
			@TransactionRedemptionID = @TransactionRedemptionID, 
			@TransactionGroup = @TransactionGroup,
			@TransactionAdminStatus = @TransactionAdminStatus,
			@TransactionCTFLAGS = @TransactionCTFLAGS,
			@TransactionCompoundRN = @TransactionCompoundRN,
			@KnowledgeDate = @KnowledgeDate,
			@ValidationOnly = @ValidationOnly,
			@rvStatusString = @rvStatusString OUTPUT	
			
		RETURN @RVal
	  END
	ELSE
	  BEGIN
	SELECT	[RN], 
			[AuditID], 
			TransactionID ,
			TransactionTicket ,
			TransactionFund ,
			TransactionSubFund ,
			TransactionSubFundValueFlag ,
			TransactionInstrument ,
			TransactionType ,
			TransactionType_Contra ,
			TransactionValueorAmount ,
			TransactionUnits ,
			TransactionSignedUnits ,
			TransactionPrice ,
			TransactionCosts ,
			TransactionCostPercent , 
			TransactionSettlement ,
			TransactionSignedSettlement ,
			TransactionMainFundPrice ,
			TransactionFXRate ,
			TransactionEffectivePrice , 
			TransactionEffectiveWatermark , 
			TransactionSpecificInitialEqualisationValue ,
			TransactionCounterparty ,
			TransactionInvestment ,
			TransactionExecution ,
			TransactionDataEntry ,
			TransactionDecisionDate ,
			TransactionValueDate ,
			TransactionFIFOValueDate ,
			TransactionEffectiveValueDate , 
			TransactionSettlementDate ,
			TransactionConfirmationDate ,
			TransactionEntryDate ,
			TransactionCostIsPercent , 
			TransactionExemptFromUpdate , 
			TransactionRedeemWholeHoldingFlag , 
			TransactionIsTransfer , 
			TransactionSpecificInitialEqualisationFlag , 
			TransactionFinalAmount ,
			TransactionFinalPrice ,
			TransactionFinalCosts ,
			TransactionFinalSettlement ,
			TransactionInstructionFlag ,
			TransactionBankConfirmation ,
			TransactionInitialDataEntry ,
			TransactionAmountConfirmed ,
			TransactionSettlementConfirmed ,
			TransactionFinalDataEntry ,
			TransactionComment ,
			TransactionCurrent ,
			TransactionParentID ,
			TransactionRedemptionID , 
			TransactionUser ,
			TransactionGroup ,
			TransactionLeg ,
			TransactionAdminStatus ,
			TransactionCTFLAGS ,
			TransactionCompoundRN ,
			TransactionDateEntered,
			TransactionDateDeleted

		FROM fn_tblTransaction_SelectKD(@KnowledgeDate)
		WHERE (TransactionParentID = @TransactionParentID) AND (TransactionLeg = 1)
		
		SELECT @RVal = @TransactionParentID
	  END
	  
	IF @@ERROR = 0
	  BEGIN
	    RETURN @RVal
	  END
	ELSE
	  BEGIN
	    RETURN 0
	  END

	RETURN 0


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[adp_tblTransaction_UpdateCommand]  TO [InvestMaster_Manager]
GO

GRANT  EXECUTE  ON [dbo].[adp_tblTransaction_UpdateCommand]  TO [InvestMaster_Admin]
GO

GRANT  EXECUTE  ON [dbo].[adp_tblTransaction_UpdateCommand]  TO [InvestMaster_AddTransaction]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[fn_tblSelectTransaction_BlotterRecreate]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[fn_tblSelectTransaction_BlotterRecreate]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[fn_tblSelectTransaction_SelectKD]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[fn_tblSelectTransaction_SelectKD]
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO



CREATE FUNCTION dbo.fn_tblSelectTransaction_BlotterRecreate
(
	@ReportName varchar(30),
	@KeyField varchar(50),
	@KnowledgeDate datetime = Null
)
RETURNS TABLE
AS
	RETURN ( 

	SELECT	Trans.RN,
			Trans.AuditID, 
			Trans.TransactionID ,
			Trans.TransactionTicket ,
			Trans.TransactionFund ,
			Trans.TransactionInstrument ,
			Trans.TransactionType AS TransactionType ,
			Trans.TransactionValueorAmount ,
			Trans.TransactionUnits ,
			Trans.TransactionSignedUnits ,
			Trans.TransactionPrice ,
			Trans.TransactionCosts ,
			Trans.TransactionCostPercent ,
			Trans.TransactionSettlement ,
			Trans.TransactionSignedSettlement ,
			Trans.TransactionEffectivePrice , 
			Trans.TransactionEffectiveWatermark , 
			Trans.TransactionSpecificInitialEqualisationValue ,
			Trans.TransactionCounterparty ,
			Trans.TransactionInvestment ,
			Trans.TransactionExecution ,
			Trans.TransactionDataEntry ,
			Trans.TransactionDecisionDate ,
			Trans.TransactionValueDate ,
			Trans.TransactionFIFOValueDate ,
			Trans.TransactionEffectiveValueDate , 
			Trans.TransactionSettlementDate ,
			Trans.TransactionConfirmationDate ,
			Trans.TransactionEntryDate ,
			Trans.TransactionCostIsPercent ,
			Trans.TransactionExemptFromUpdate ,
			Trans.TransactionRedeemWholeHoldingFlag , 
			Trans.TransactionIsTransfer , 
			Trans.TransactionSpecificInitialEqualisationFlag , 
			Trans.TransactionFinalAmount ,
			Trans.TransactionFinalPrice ,
			Trans.TransactionFinalCosts ,
			Trans.TransactionFinalSettlement ,
			Trans.TransactionInstructionFlag ,
			Trans.TransactionBankConfirmation ,
			Trans.TransactionInitialDataEntry ,
			Trans.TransactionAmountConfirmed ,
			Trans.TransactionSettlementConfirmed ,
			Trans.TransactionFinalDataEntry ,
			Trans.TransactionComment ,
			Trans.TransactionParentID ,
			Trans.TransactionUser ,
			Trans.TransactionGroup ,
			Trans.TransactionAdminStatus ,
			Trans.TransactionCTFLAGS ,
			Trans.TransactionCompoundRN ,
			Trans.TransactionDateEntered,
			Funds.FundName AS FundName, 
			Funds.FundLegalEntity AS FundLegalEntity, 
			Insts.InstrumentDescription AS InstrumentDescription, 
			Insts.InstrumentCurrency AS InstrumentCurrency, 
			Insts.InstrumentPenaltyComment AS InstrumentPenaltyComment, 
			Insts.InstrumentClass AS InstrumentClass, 
			Insts.InstrumentType AS InstrumentType, 
			Insts.InstrumentNoticeDays AS InstrumentNoticeDays, 
			Insts.InstrumentPenalty AS InstrumentPenalty, 
			Insts.InstrumentUpfrontfees AS InstrumentUpfrontfees, 
			Insts.InstrumentParent AS InstrumentParent, 
			Insts.InstrumentParentEquivalentRatio AS InstrumentParentEquivalentRatio, 
			T_Type.TransactionType AS TransactionTypeName, 
			Counterparty.Counterpartyname, 
			dbo.fn_AmendedSettlement(Trans.TransactionType, Trans.TransactionSignedSettlement) as AmendedSettlement
			
	FROM	
			fn_tblReportDetailLog_SelectKD(@KnowledgeDate) DetailLog LEFT OUTER JOIN 
			-- fn_tblTransaction_SelectKD(@KnowledgeDate) Trans ON DetailLog.ValueField = Trans.TransactionID LEFT OUTER JOIN
			tblTransaction Trans ON DetailLog.ValueField = Trans.TransactionID LEFT OUTER JOIN
			fn_tblTransactionType_SelectKD(@KnowledgeDate) T_Type ON Trans.TransactionType = T_Type.TransactionTypeID LEFT OUTER JOIN
			fn_tblInstrument_SelectKD(@KnowledgeDate) Insts ON Trans.TransactionInstrument = Insts.InstrumentID LEFT OUTER JOIN
			fn_tblFund_SelectKD(@KnowledgeDate) Funds ON Trans.TransactionFund = Funds.FundID LEFT OUTER JOIN
			fn_tblCounterparty_SelectKD(@KnowledgeDate) Counterparty ON Trans.TransactionCounterparty = Counterparty.CounterpartyID	
	WHERE (DetailLog.ReportName = @ReportName) AND (DetailLog.KeyField = @KeyField) AND (Trans.TransactionLeg = 1)
	
	 )





GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  SELECT  ON [dbo].[fn_tblSelectTransaction_BlotterRecreate]  TO [InvestMaster_Sales]
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE FUNCTION dbo.fn_tblSelectTransaction_SelectKD
(
@BothLegs int = 0, 
@KnowledgeDate datetime = Null
)
RETURNS TABLE
AS
	RETURN ( 

	SELECT	Trans.RN,
			Trans.AuditID, 
			Trans.TransactionID ,
			Trans.TransactionTicket ,
			Trans.TransactionFund ,
			Trans.TransactionInstrument ,
			Trans.TransactionType AS TransactionType ,
			Trans.TransactionValueorAmount ,
			Trans.TransactionUnits ,
			Trans.TransactionSignedUnits ,
			Trans.TransactionPrice ,
			Trans.TransactionCosts ,
			Trans.TransactionCostPercent ,
			Trans.TransactionSettlement ,
			Trans.TransactionSignedSettlement ,
			Trans.TransactionEffectivePrice , 
			Trans.TransactionEffectiveWatermark , 
			Trans.TransactionSpecificInitialEqualisationValue ,
			Trans.TransactionCounterparty ,
			Trans.TransactionInvestment ,
			Trans.TransactionExecution ,
			Trans.TransactionDataEntry ,
			Trans.TransactionDecisionDate ,
			Trans.TransactionValueDate ,
			Trans.TransactionFIFOValueDate ,
			Trans.TransactionEffectiveValueDate , 
			Trans.TransactionSettlementDate ,
			Trans.TransactionConfirmationDate ,
			Trans.TransactionEntryDate ,
			Trans.TransactionCostIsPercent ,
			Trans.TransactionExemptFromUpdate ,
			Trans.TransactionRedeemWholeHoldingFlag , 
			Trans.TransactionIsTransfer , 
			Trans.TransactionSpecificInitialEqualisationFlag , 
			Trans.TransactionFinalAmount ,
			Trans.TransactionFinalPrice ,
			Trans.TransactionFinalCosts ,
			Trans.TransactionFinalSettlement ,
			Trans.TransactionInstructionFlag ,
			Trans.TransactionBankConfirmation ,
			Trans.TransactionInitialDataEntry ,
			Trans.TransactionAmountConfirmed ,
			Trans.TransactionSettlementConfirmed ,
			Trans.TransactionFinalDataEntry ,
			Trans.TransactionComment ,
			Trans.TransactionParentID ,
			Trans.TransactionUser ,
			Trans.TransactionGroup ,
			Trans.TransactionAdminStatus ,
			Trans.TransactionCTFLAGS ,
			Trans.TransactionCompoundRN ,
			Trans.TransactionDateEntered,
			Funds.FundName AS FundName, 
			Funds.FundLegalEntity AS FundLegalEntity, 
			Insts.InstrumentDescription AS InstrumentDescription, 
			Insts.InstrumentCurrency AS InstrumentCurrency, 
			Insts.InstrumentPenaltyComment AS InstrumentPenaltyComment, 
			Insts.InstrumentClass AS InstrumentClass, 
			Insts.InstrumentType AS InstrumentType, 
			Insts.InstrumentNoticeDays AS InstrumentNoticeDays, 
			Insts.InstrumentPenalty AS InstrumentPenalty, 
			Insts.InstrumentUpfrontfees AS InstrumentUpfrontfees, 
			Insts.InstrumentParent AS InstrumentParent, 
			Insts.InstrumentParentEquivalentRatio AS InstrumentParentEquivalentRatio, 
			T_Type.TransactionType AS TransactionTypeName, 
			Counterparty.Counterpartyname, 
			dbo.fn_AmendedSettlement(Trans.TransactionType, Trans.TransactionSignedSettlement) as AmendedSettlement
			
	FROM	fn_tblTransaction_SelectKD(@KnowledgeDate) Trans LEFT OUTER JOIN
			fn_tblTransactionType_SelectKD(@KnowledgeDate) T_Type ON 
				Trans.TransactionType = T_Type.TransactionTypeID LEFT OUTER JOIN
			fn_tblInstrument_SelectKD(@KnowledgeDate) Insts ON 
				Trans.TransactionInstrument = Insts.InstrumentID LEFT OUTER JOIN
			fn_tblFund_SelectKD(@KnowledgeDate) Funds ON 
				Trans.TransactionFund = Funds.FundID LEFT OUTER JOIN
			fn_tblCounterparty_SelectKD(@KnowledgeDate) Counterparty ON 
				Trans.TransactionCounterparty = Counterparty.CounterpartyID	
	WHERE (TransactionLeg = 1)
	
	 )




GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  SELECT  ON [dbo].[fn_tblSelectTransaction_SelectKD]  TO [InvestMaster_Sales]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[adp_tblSelectTransaction_BlotterRecreate]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[adp_tblSelectTransaction_BlotterRecreate]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[adp_tblSelectTransaction_SelectCommand]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[adp_tblSelectTransaction_SelectCommand]
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO




CREATE PROCEDURE dbo.adp_tblSelectTransaction_BlotterRecreate
(
	@ReportName varchar(30),
	@KeyField varchar(50),
	@KnowledgeDate datetime = Null
)
AS
	SET NOCOUNT ON
	
	SELECT	RN,
			AuditID, 
			TransactionID ,
			TransactionTicket ,
			TransactionFund ,
			TransactionInstrument ,
			TransactionType ,
			TransactionValueorAmount ,
			TransactionUnits ,
			TransactionSignedUnits ,
			TransactionPrice ,
			TransactionCosts ,
			TransactionCostPercent , 
			TransactionSettlement ,
			TransactionSignedSettlement ,
			TransactionEffectivePrice , 
			TransactionEffectiveWatermark , 
			TransactionSpecificInitialEqualisationValue ,
			TransactionCounterparty ,
			TransactionInvestment ,
			TransactionExecution ,
			TransactionDataEntry ,
			TransactionDecisionDate ,
			TransactionValueDate ,
			TransactionFIFOValueDate ,
			TransactionEffectiveValueDate , 
			TransactionSettlementDate ,
			TransactionConfirmationDate ,
			TransactionEntryDate ,
			TransactionCostIsPercent , 
			TransactionExemptFromUpdate , 
			TransactionRedeemWholeHoldingFlag , 
			TransactionIsTransfer , 
			TransactionSpecificInitialEqualisationFlag , 
			TransactionFinalAmount ,
			TransactionFinalPrice ,
			TransactionFinalCosts ,
			TransactionFinalSettlement ,
			TransactionInstructionFlag ,
			TransactionBankConfirmation ,
			TransactionInitialDataEntry ,
			TransactionAmountConfirmed ,
			TransactionSettlementConfirmed ,
			TransactionFinalDataEntry ,
			'TransactionComment' = CASE
			  WHEN [TransactionRedeemWholeHoldingFlag] = 0 THEN [TransactionComment]
			  ELSE 'REDEEM WHOLE HOLDING' + CHAR(13) + CHAR(10) + [TransactionComment]
			  END ,
			TransactionParentID ,
			TransactionUser ,
			TransactionGroup ,
			TransactionAdminStatus ,
			TransactionCTFLAGS ,
			TransactionCompoundRN ,
			TransactionDateEntered,
			FundName, 
			FundLegalEntity, 
			InstrumentDescription, 
			InstrumentCurrency, 
			InstrumentPenaltyComment, 
			InstrumentClass, 
			InstrumentType, 
			InstrumentNoticeDays, 
			InstrumentPenalty, 
			InstrumentUpfrontfees, 
			InstrumentParent, 
			InstrumentParentEquivalentRatio, 
			TransactionTypeName, 
			CounterpartyName, 
			AmendedSettlement
	FROM fn_tblSelectTransaction_BlotterRecreate(@ReportName, @KeyField, @Knowledgedate)
	
	RETURN -1
	





GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[adp_tblSelectTransaction_BlotterRecreate]  TO [InvestMaster_Sales]
GO

GRANT  EXECUTE  ON [dbo].[adp_tblSelectTransaction_BlotterRecreate]  TO [InvestMaster_Manager]
GO

GRANT  EXECUTE  ON [dbo].[adp_tblSelectTransaction_BlotterRecreate]  TO [InvestMaster_Admin]
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE PROCEDURE dbo.adp_tblSelectTransaction_SelectCommand
(
@BothLegs int = 0, 
@KnowledgeDate datetime = Null
)
AS
	SET NOCOUNT ON
	
	SELECT	RN,
			AuditID, 
			TransactionID ,
			TransactionTicket ,
			TransactionFund ,
			TransactionInstrument ,
			TransactionType ,
			TransactionValueorAmount ,
			TransactionUnits ,
			TransactionSignedUnits ,
			TransactionPrice ,
			TransactionCosts ,
			TransactionCostPercent , 
			TransactionSettlement ,
			TransactionSignedSettlement ,
			TransactionEffectivePrice , 
			TransactionEffectiveWatermark , 
			TransactionSpecificInitialEqualisationValue ,
			TransactionCounterparty ,
			TransactionInvestment ,
			TransactionExecution ,
			TransactionDataEntry ,
			TransactionDecisionDate ,
			TransactionValueDate ,
			TransactionFIFOValueDate ,
			TransactionEffectiveValueDate , 
			TransactionSettlementDate ,
			TransactionConfirmationDate ,
			TransactionEntryDate ,
			TransactionCostIsPercent , 
			TransactionExemptFromUpdate , 
			TransactionRedeemWholeHoldingFlag , 
			TransactionIsTransfer , 
			TransactionSpecificInitialEqualisationFlag , 
			TransactionFinalAmount ,
			TransactionFinalPrice ,
			TransactionFinalCosts ,
			TransactionFinalSettlement ,
			TransactionInstructionFlag ,
			TransactionBankConfirmation ,
			TransactionInitialDataEntry ,
			TransactionAmountConfirmed ,
			TransactionSettlementConfirmed ,
			TransactionFinalDataEntry ,
			'TransactionComment' = CASE
			  WHEN [TransactionRedeemWholeHoldingFlag] = 0 THEN [TransactionComment]
			  ELSE 'REDEEM WHOLE HOLDING' + CHAR(13) + CHAR(10) + [TransactionComment]
			  END ,
			TransactionParentID ,
			TransactionUser ,
			TransactionGroup ,
			TransactionAdminStatus ,
			TransactionCTFLAGS ,
			TransactionCompoundRN ,
			TransactionDateEntered,
			FundName, 
			FundLegalEntity, 
			InstrumentDescription, 
			InstrumentCurrency, 
			InstrumentPenaltyComment, 
			InstrumentClass, 
			InstrumentType, 
			InstrumentNoticeDays, 
			InstrumentPenalty, 
			InstrumentUpfrontfees, 
			InstrumentParent, 
			InstrumentParentEquivalentRatio, 
			TransactionTypeName, 
			CounterpartyName, 
			AmendedSettlement
	FROM fn_tblSelectTransaction_SelectKD(@BothLegs, @Knowledgedate)
	
	RETURN -1
	




GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[adp_tblSelectTransaction_SelectCommand]  TO [InvestMaster_Sales]
GO

GRANT  EXECUTE  ON [dbo].[adp_tblSelectTransaction_SelectCommand]  TO [InvestMaster_Manager]
GO

GRANT  EXECUTE  ON [dbo].[adp_tblSelectTransaction_SelectCommand]  TO [InvestMaster_Admin]
GO

if not exists (select * from dbo.syscolumns where (id = object_id(N'[dbo].[tblPendingTransactions]')) and (OBJECTPROPERTY(id, N'IsUserTable') = 1) and (name='TransactionEffectivePrice'))
BEGIN
ALTER TABLE [dbo].[tblPendingTransactions] ADD 
	[TransactionEffectivePrice] [float] NOT NULL CONSTRAINT [DF_tblPendingTransactions_TransactionEffectivePrice] DEFAULT (0), 
	[TransactionEffectiveWatermark] [float] NOT NULL CONSTRAINT [DF_tblPendingTransactions_TransactionEffectiveWatermark] DEFAULT (0), 
	[TransactionCostPercent] [float] NOT NULL CONSTRAINT [DF_tblPendingTransactions_TransactionCostPercent] DEFAULT (0), 
	[TransactionSpecificInitialEqualisationValue] [float] NOT NULL CONSTRAINT [DF_tblPendingTransactions_TransactionSpecificInitialEqualisationValue] DEFAULT (0), 
	[TransactionEffectiveValueDate] [datetime] NULL, 
	[TransactionIsTransfer] [bit] NOT NULL CONSTRAINT [DF_tblPendingTransactions_TransactionIsTransfer] DEFAULT (0), 
	[TransactionCostIsPercent] [bit] NOT NULL CONSTRAINT [DF_tblPendingTransactions_TransactionCostIsPercent] DEFAULT (0), 
	[TransactionExemptFromUpdate] [bit] NOT NULL CONSTRAINT [DF_tblPendingTransactions_TransactionExemptFromUpdate] DEFAULT (0), 
	[TransactionSpecificInitialEqualisationFlag] [bit] NOT NULL CONSTRAINT [DF_tblPendingTransactions_TransactionSpecificInitialEqualisationFlag] DEFAULT (0)

END
GO

UPDATE tblPendingTransactions
SET 	TransactionEffectivePrice = TransactionPrice, 
	TransactionEffectiveWatermark = 0, 
	TransactionSpecificInitialEqualisationValue = 0, 
	TransactionEffectiveValueDate = TransactionValueDate, 
	TransactionIsTransfer = 0, 
	TransactionSpecificInitialEqualisationFlag = 0
WHERE TransactionEffectiveValueDate IS Null

GO

if not exists (select * from dbo.syscolumns where (id = object_id(N'[dbo].[tblCompoundTransactionTemplate]')) and (OBJECTPROPERTY(id, N'IsUserTable') = 1) and (name='TransactionEffectivePrice'))
BEGIN
ALTER TABLE [dbo].[tblCompoundTransactionTemplate] ADD 
	[TransactionEffectivePrice] [varchar](400) NOT NULL CONSTRAINT [DF_tblCompoundTransactionTemplate_TransactionEffectivePrice] DEFAULT (''), 
	[TransactionEffectiveWatermark] [varchar](400) NOT NULL CONSTRAINT [DF_tblCompoundTransactionTemplate_TransactionEffectiveWatermark] DEFAULT (''), 
	[TransactionSpecificInitialEqualisationValue] [varchar](400) NOT NULL CONSTRAINT [DF_tblCompoundTransactionTemplate_TransactionSpecificInitialEqualisationValue] DEFAULT (''), 
	[TransactionEffectiveValueDate] [varchar](400) NOT NULL CONSTRAINT [DF_tblCompoundTransactionTemplate_TransactionEffectiveValueDate] DEFAULT (''), 
	[TransactionIsTransfer] [varchar](400) NOT NULL CONSTRAINT [DF_tblCompoundTransactionTemplate_TransactionIsTransfer] DEFAULT (''), 
	[TransactionCostIsPercent] [varchar](400) NOT NULL CONSTRAINT [DF_tblCompoundTransactionTemplate_TransactionCostIsPercent] DEFAULT (''), 
	[TransactionCostPercent] [varchar](400) NOT NULL CONSTRAINT [DF_tblCompoundTransactionTemplate_TransactionCostPercent] DEFAULT (''), 
	[TransactionSpecificInitialEqualisationFlag] [varchar](400) NOT NULL CONSTRAINT [DF_tblCompoundTransactionTemplate_TransactionSpecificInitialEqualisationFlag] DEFAULT ('')
END
GO

if not exists (select * from dbo.syscolumns where (id = object_id(N'[dbo].[tblCompoundTransaction]')) and (OBJECTPROPERTY(id, N'IsUserTable') = 1) and (name='TransactionEffectivePrice'))
BEGIN
ALTER TABLE [dbo].[tblCompoundTransaction] ADD 
	[TransactionEffectivePrice] [float] NOT NULL CONSTRAINT [DF_tblCompoundTransaction_TransactionEffectivePrice] DEFAULT (0), 
	[TransactionEffectiveWatermark] [float] NOT NULL CONSTRAINT [DF_tblCompoundTransaction_TransactionEffectiveWatermark] DEFAULT (0), 
	[TransactionCostPercent] [float] NOT NULL CONSTRAINT [DF_tblCompoundTransaction_TransactionCostPercent] DEFAULT (0), 
	[TransactionSpecificInitialEqualisationValue] [float] NOT NULL CONSTRAINT [DF_tblCompoundTransaction_TransactionSpecificInitialEqualisationValue] DEFAULT (0), 
	[TransactionEffectiveValueDate] [datetime] NULL, 
	[TransactionIsTransfer] [bit] NOT NULL CONSTRAINT [DF_tblCompoundTransaction_TransactionIsTransfer] DEFAULT (0), 
	[TransactionCostIsPercent] [bit] NOT NULL CONSTRAINT [DF_tblCompoundTransaction_TransactionCostIsPercent] DEFAULT (0), 
	[TransactionExemptFromUpdate] [bit] NOT NULL CONSTRAINT [DF_tblCompoundTransaction_TransactionExemptFromUpdate] DEFAULT (0), 
	[TransactionSpecificInitialEqualisationFlag] [bit] NOT NULL CONSTRAINT [DF_tblCompoundTransaction_TransactionSpecificInitialEqualisationFlag] DEFAULT (0)

END
GO

UPDATE tblCompoundTransaction
SET 	TransactionEffectivePrice = TransactionPrice, 
	TransactionEffectiveWatermark = 0, 
	TransactionSpecificInitialEqualisationValue = 0, 
	TransactionEffectiveValueDate = TransactionValueDate, 
	TransactionIsTransfer = 0, 
	TransactionSpecificInitialEqualisationFlag = 0
WHERE TransactionEffectiveValueDate IS Null

GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[fn_tblCompoundTransactionTemplate_SelectKD]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[fn_tblCompoundTransactionTemplate_SelectKD]
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE  FUNCTION fn_tblCompoundTransactionTemplate_SelectKD
(
@KnowledgeDate datetime
)
RETURNS TABLE
AS
RETURN
(
SELECT 	RN ,
	AuditID  ,
	TemplateID ,
	TemplateDescription ,
	TemplateStep ,
	TemplateTotalSteps ,
	TemplateFlags ,
	TransactionTicket ,
	TransactionFund ,
	TransactionInstrument ,
	TransactionType ,
	TransactionValueorAmount ,
	TransactionUnits ,
	TransactionPrice ,
	TransactionEffectivePrice , 
	TransactionCosts ,
	TransactionCostPercent ,
	TransactionSettlement ,
	TransactionCounterparty ,
	TransactionDecisionDate ,
	TransactionValueDate ,
	TransactionFIFOValueDate ,
	TransactionEffectiveValueDate ,
	TransactionSettlementDate ,
	TransactionConfirmationDate ,
	TransactionEffectiveWatermark ,
	TransactionSpecificInitialEqualisationValue , 
	TransactionCostIsPercent , 
	TransactionIsTransfer , 
	TransactionSpecificInitialEqualisationFlag , 
	TransactionComment ,
	TransactionGroup ,
	R1 ,
	R2 ,
	UserEntered ,
	DateEntered ,
	DateDeleted
FROM tblCompoundTransactionTemplate
WHERE RN IN ( SELECT MAX(RN) 
              FROM tblCompoundTransactionTemplate 
              WHERE (((DateEntered <= @KnowledgeDate) or (DateEntered IS NULL)) OR (ISNULL(@KnowledgeDate, '1 Jan 1900') <= '1 Jan 1900')) AND 
                    (((DateDeleted > @KnowledgeDate) or (DateDeleted IS NULL)) AND (NOT ((ISNULL(@KnowledgeDate, '1 Jan 1900') <= '1 Jan 1900') AND (NOT (DateDeleted is NULL)))))
              GROUP BY TemplateID, TemplateStep )
)


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  SELECT  ON [dbo].[fn_tblCompoundTransactionTemplate_SelectKD]  TO [InvestMaster_Sales]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[fn_tblCompoundTransaction_SelectKD]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[fn_tblCompoundTransaction_SelectKD]
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE FUNCTION dbo.fn_tblCompoundTransaction_SelectKD
(
@KnowledgeDate datetime
)
RETURNS TABLE
AS
RETURN
(
SELECT 	RN ,
	AuditID ,
	AdminStatus ,
	TransactionParentID ,
	TransactionCompoundID ,
	TransactionTemplateID ,
	TransactionTicket ,
	TransactionFund ,
	TransactionInstrument ,
	TransactionType ,
	TransactionValueorAmount ,
	TransactionUnits ,
	TransactionPrice ,
	TransactionCosts ,
	TransactionCostPercent ,
	TransactionEffectivePrice , 
	TransactionEffectiveWatermark , 
	TransactionSpecificInitialEqualisationValue , 
	TransactionCounterparty ,
	TransactionInvestment ,
	TransactionExecution ,
	TransactionDataEntry ,
	TransactionDecisionDate ,
	TransactionValueDate ,
	TransactionFIFOValueDate ,
	TransactionEffectiveValueDate , 
	TransactionSettlementDate ,
	TransactionConfirmationDate ,
	TransactionCostIsPercent ,
	TransactionExemptFromUpdate , 
	TransactionIsTransfer , 
	TransactionSpecificInitialEqualisationFlag , 
	TransactionFinalAmount ,
	TransactionFinalPrice ,
	TransactionFinalCosts ,
	TransactionFinalSettlement ,
	TransactionInstructionFlag ,
	TransactionBankConfirmation ,
	TransactionInitialDataEntry ,
	TransactionAmountConfirmed ,
	TransactionSettlementConfirmed ,
	TransactionFinalDataEntry ,
	TransactionComment  ,
	TransactionGroup ,
	ParameterCount ,
	Parameter1_Name ,
	Parameter1_Type ,
	Parameter1_Value ,
	Parameter2_Name ,
	Parameter2_Type ,
	Parameter2_Value ,
	Parameter3_Name ,
	Parameter3_Type ,
	Parameter3_Value ,
	Parameter4_Name ,
	Parameter4_Type ,
	Parameter4_Value ,
	Parameter5_Name ,
	Parameter5_Type ,
	Parameter5_Value ,
	CompoundTransactionGroup ,
	CompoundTransactionComment ,
	CompoundTransactionFlags ,
	CompoundTransactionPosted ,
	UserEntered ,
	DateEntered ,
	DateDeleted  
FROM tblCompoundTransaction
WHERE RN IN ( SELECT MAX(RN) 
              FROM tblCompoundTransaction 
              WHERE (CompoundTransactionPosted <> 0) AND 
					(((DateEntered <= @KnowledgeDate) or (DateEntered IS NULL)) OR (ISNULL(@KnowledgeDate, '1 Jan 1900') <= '1 Jan 1900')) AND 
                    (((DateDeleted > @KnowledgeDate) or (DateDeleted IS NULL)) AND (NOT ((ISNULL(@KnowledgeDate, '1 Jan 1900') <= '1 Jan 1900') AND (NOT (DateDeleted is NULL)))))
              GROUP BY TransactionCompoundID )
)


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  SELECT  ON [dbo].[fn_tblCompoundTransaction_SelectKD]  TO [InvestMaster_Sales]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[adp_tblCompoundTransaction_InsertCommand]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[adp_tblCompoundTransaction_InsertCommand]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[adp_tblCompoundTransaction_SelectCommand]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[adp_tblCompoundTransaction_SelectCommand]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[adp_tblCompoundTransaction_UpdateCommand]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[adp_tblCompoundTransaction_UpdateCommand]
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


CREATE PROCEDURE dbo.adp_tblCompoundTransaction_InsertCommand
(
	@AdminStatus int, 
	@TransactionParentID int = 0,
	@TransactionCompoundID int,
	@TransactionTemplateID int,
	@TransactionTicket varchar (50),
	@TransactionFund int,
	@TransactionInstrument int,
	@TransactionType int,
	@TransactionValueorAmount varchar (10),
	@TransactionUnits float = 0,
	@TransactionPrice float = 0,
	@TransactionCosts float = 0,
	@TransactionCostPercent float = 0,
	@TransactionEffectivePrice float = NULL,
	@TransactionEffectiveWatermark float = NULL,
	@TransactionSpecificInitialEqualisationValue float = 0,
	@TransactionCounterparty int,
	@TransactionInvestment varchar (50),
	@TransactionExecution varchar (50),
	@TransactionDataEntry varchar (50),
	@TransactionDecisionDate datetime,
	@TransactionValueDate datetime,
	@TransactionFIFOValueDate datetime,
	@TransactionEffectiveValueDate datetime = NULL,
	@TransactionSettlementDate datetime,
	@TransactionConfirmationDate datetime,
	@TransactionCostIsPercent bit = 0, 
	@TransactionExemptFromUpdate bit = 0, 
	@TransactionIsTransfer bit = 0, 
	@TransactionSpecificInitialEqualisationFlag bit = 0, 
	@TransactionFinalAmount int = 0,
	@TransactionFinalPrice int = 0,
	@TransactionFinalCosts int = 0,
	@TransactionFinalSettlement int = 0,
	@TransactionInstructionFlag int = 0,
	@TransactionBankConfirmation int = 0,
	@TransactionInitialDataEntry int = 0,
	@TransactionAmountConfirmed int = 0,
	@TransactionSettlementConfirmed int = 0,
	@TransactionFinalDataEntry int = 0,
	@TransactionComment varchar (500),
	@TransactionGroup varchar (50),
	@ParameterCount int = 0, 
	@Parameter1_Name varchar (50) = '',
	@Parameter1_Type int = 0, 
	@Parameter1_Value varchar (50) = '0',
	@Parameter2_Name varchar (50) = '',
	@Parameter2_Type int = 0, 
	@Parameter2_Value varchar (50) = '0',
	@Parameter3_Name varchar (50) = '',
	@Parameter3_Type int = 0, 
	@Parameter3_Value varchar (50) = '0',
	@Parameter4_Name varchar (50) = '',
	@Parameter4_Type int = 0, 
	@Parameter4_Value varchar (50) = '0',
	@Parameter5_Name varchar (50) = '',
	@Parameter5_Type int = 0, 
	@Parameter5_Value varchar (50) = '0',
	@CompoundTransactionGroup int = 0, 
	@CompoundTransactionComment varchar (50) = '',
	@CompoundTransactionFlags int = 0, 
	@CompoundTransactionPosted int = 0, 
	@KnowledgeDate datetime, 
	@rvStatusString varchar(200) OUTPUT	
)
--
-- (adp) Adaptor, tblCompoundTransaction Insert procedure
--
-- Controlled Insert of a new Compound Transaction to the Venice (Renaissance) database
--
AS

	DECLARE @New_CT_ID int 
	DECLARE @New_TransactionTicket varchar(50)
	

	SET NOCOUNT OFF

	SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
	
	BEGIN TRANSACTION

	-- Establish TransactionCompoundID to use. Existing or New one.
		
	IF ISNULL(@TransactionCompoundID, 0) > 0 
	  BEGIN
	  SELECT @New_CT_ID = @TransactionCompoundID
	  END
	ELSE
	  BEGIN
	  SELECT @New_CT_ID = (ISNULL(MAX(TransactionCompoundID), 0)+1) FROM tblCompoundTransaction
	  END
		  
		  
	-- Ticket
	
	IF ISNULL(@TransactionTicket, '') = ''
	BEGIN
		SELECT @New_TransactionTicket = dbo.fn_NewCompoundTransactionTicket(0)
	END
	ELSE
	BEGIN
		SELECT @New_TransactionTicket = @TransactionTicket
	END
	
	-- Insert new record.
	
	INSERT INTO tblCompoundTransaction(
			AdminStatus ,
			TransactionParentID ,
			TransactionCompoundID ,
			TransactionTemplateID ,
			TransactionTicket ,
			TransactionFund ,
			TransactionInstrument ,
			TransactionType ,
			TransactionValueorAmount ,
			TransactionUnits ,
			TransactionPrice ,
			TransactionCosts ,
			TransactionCostPercent ,
			TransactionEffectivePrice , 
			TransactionEffectiveWatermark , 
			TransactionSpecificInitialEqualisationValue , 
			TransactionCounterparty ,
			TransactionInvestment ,
			TransactionExecution ,
			TransactionDataEntry ,
			TransactionDecisionDate ,
			TransactionValueDate ,
			TransactionFIFOValueDate ,
			TransactionEffectiveValueDate , 
			TransactionSettlementDate ,
			TransactionConfirmationDate ,
			TransactionCostIsPercent ,
			TransactionExemptFromUpdate , 
			TransactionIsTransfer , 
			TransactionSpecificInitialEqualisationFlag , 
			TransactionFinalAmount ,
			TransactionFinalPrice ,
			TransactionFinalCosts ,
			TransactionFinalSettlement ,
			TransactionInstructionFlag ,
			TransactionBankConfirmation ,
			TransactionInitialDataEntry ,
			TransactionAmountConfirmed ,
			TransactionSettlementConfirmed ,
			TransactionFinalDataEntry ,
			TransactionComment  ,
			TransactionGroup ,
			ParameterCount ,
			Parameter1_Name ,
			Parameter1_Type ,
			Parameter1_Value ,
			Parameter2_Name ,
			Parameter2_Type ,
			Parameter2_Value ,
			Parameter3_Name ,
			Parameter3_Type ,
			Parameter3_Value ,
			Parameter4_Name ,
			Parameter4_Type ,
			Parameter4_Value ,
			Parameter5_Name ,
			Parameter5_Type ,
			Parameter5_Value ,
			CompoundTransactionGroup ,
			CompoundTransactionComment ,
			CompoundTransactionFlags ,
			CompoundTransactionPosted)
	VALUES (
			@AdminStatus ,
			@TransactionParentID ,
			@New_CT_ID ,
			@TransactionTemplateID ,
			@New_TransactionTicket ,
			@TransactionFund ,
			@TransactionInstrument ,
			@TransactionType ,
			@TransactionValueorAmount ,
			@TransactionUnits ,
			@TransactionPrice ,
			@TransactionCosts ,
			ISNULL(@TransactionCostPercent, 0) ,
			ISNULL(@TransactionEffectivePrice, @TransactionPrice) , 
			ISNULL(@TransactionEffectiveWatermark, 0) , 
			ISNULL(@TransactionSpecificInitialEqualisationValue, 0) , 
			@TransactionCounterparty ,
			@TransactionInvestment ,
			@TransactionExecution ,
			@TransactionDataEntry ,
			@TransactionDecisionDate ,
			@TransactionValueDate ,
			@TransactionFIFOValueDate ,
			ISNULL(@TransactionEffectiveValueDate, @TransactionValueDate) , 
			@TransactionSettlementDate ,
			@TransactionConfirmationDate ,
			ISNULL(@TransactionCostIsPercent, 0) ,
			ISNULL(@TransactionExemptFromUpdate, 0) , 
			ISNULL(@TransactionIsTransfer, 0) , 
			ISNULL(@TransactionSpecificInitialEqualisationFlag, 0) , 
			@TransactionFinalAmount ,
			@TransactionFinalPrice ,
			@TransactionFinalCosts ,
			@TransactionFinalSettlement ,
			@TransactionInstructionFlag ,
			@TransactionBankConfirmation ,
			@TransactionInitialDataEntry ,
			@TransactionAmountConfirmed ,
			@TransactionSettlementConfirmed ,
			@TransactionFinalDataEntry ,
			@TransactionComment  ,
			@TransactionGroup ,
			@ParameterCount ,
			@Parameter1_Name ,
			@Parameter1_Type ,
			@Parameter1_Value ,
			@Parameter2_Name ,
			@Parameter2_Type ,
			@Parameter2_Value ,
			@Parameter3_Name ,
			@Parameter3_Type ,
			@Parameter3_Value ,
			@Parameter4_Name ,
			@Parameter4_Type ,
			@Parameter4_Value ,
			@Parameter5_Name ,
			@Parameter5_Type ,
			@Parameter5_Value ,
			@CompoundTransactionGroup ,
			@CompoundTransactionComment ,
			@CompoundTransactionFlags ,
			0) -- @CompoundTransactionPosted)

	-- Select Newly created record
	
	SELECT	[RN], 
			[AuditID], 
			AdminStatus ,
			TransactionParentID ,
			TransactionCompoundID ,
			TransactionTemplateID ,
			TransactionTicket ,
			TransactionFund ,
			TransactionInstrument ,
			TransactionType ,
			TransactionValueorAmount ,
			TransactionUnits ,
			TransactionPrice ,
			TransactionCosts ,
			TransactionCostPercent ,
			TransactionEffectivePrice , 
			TransactionEffectiveWatermark , 
			TransactionSpecificInitialEqualisationValue , 
			TransactionCounterparty ,
			TransactionInvestment ,
			TransactionExecution ,
			TransactionDataEntry ,
			TransactionDecisionDate ,
			TransactionValueDate ,
			TransactionFIFOValueDate ,
			TransactionEffectiveValueDate , 
			TransactionSettlementDate ,
			TransactionConfirmationDate ,
			TransactionCostIsPercent ,
			TransactionExemptFromUpdate , 
			TransactionIsTransfer , 
			TransactionSpecificInitialEqualisationFlag , 
			TransactionFinalAmount ,
			TransactionFinalPrice ,
			TransactionFinalCosts ,
			TransactionFinalSettlement ,
			TransactionInstructionFlag ,
			TransactionBankConfirmation ,
			TransactionInitialDataEntry ,
			TransactionAmountConfirmed ,
			TransactionSettlementConfirmed ,
			TransactionFinalDataEntry ,
			TransactionComment  ,
			TransactionGroup ,
			ParameterCount ,
			Parameter1_Name ,
			Parameter1_Type ,
			Parameter1_Value ,
			Parameter2_Name ,
			Parameter2_Type ,
			Parameter2_Value ,
			Parameter3_Name ,
			Parameter3_Type ,
			Parameter3_Value ,
			Parameter4_Name ,
			Parameter4_Type ,
			Parameter4_Value ,
			Parameter5_Name ,
			Parameter5_Type ,
			Parameter5_Value ,
			CompoundTransactionGroup ,
			CompoundTransactionComment ,
			CompoundTransactionFlags ,
			CompoundTransactionPosted ,
			UserEntered ,
			DateEntered ,
			DateDeleted   
	FROM [dbo].[tblCompoundTransaction]
	WHERE (RN = SCOPE_IDENTITY())

	-- Return value and Commit / Rollback transaction accordingly.
	
	IF @@ERROR = 0
	  BEGIN
	    COMMIT TRANSACTION
	    RETURN @New_CT_ID
	  END
	ELSE
	  BEGIN
	    ROLLBACK TRANSACTION
	    RETURN 0
	  END
	
	




GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[adp_tblCompoundTransaction_InsertCommand]  TO [InvestMaster_Manager]
GO

GRANT  EXECUTE  ON [dbo].[adp_tblCompoundTransaction_InsertCommand]  TO [InvestMaster_Admin]
GO

GRANT  EXECUTE  ON [dbo].[adp_tblCompoundTransaction_InsertCommand]  TO [InvestMaster_AddTransaction]
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE PROCEDURE dbo.adp_tblCompoundTransaction_SelectCommand
(
@KnowledgeDate datetime = Null
)
AS
	SET NOCOUNT ON

	SELECT	[RN], 
			[AuditID], 
			AdminStatus ,
			TransactionParentID ,
			TransactionCompoundID ,
			TransactionTemplateID ,
			TransactionTicket ,
			TransactionFund ,
			TransactionInstrument ,
			TransactionType ,
			TransactionValueorAmount ,
			TransactionUnits ,
			TransactionPrice ,
			TransactionCosts ,
			TransactionCostPercent ,
			TransactionEffectivePrice , 
			TransactionEffectiveWatermark , 
			TransactionSpecificInitialEqualisationValue , 
			TransactionCounterparty ,
			TransactionInvestment ,
			TransactionExecution ,
			TransactionDataEntry ,
			TransactionDecisionDate ,
			TransactionValueDate ,
			TransactionFIFOValueDate ,
			TransactionEffectiveValueDate , 
			TransactionSettlementDate ,
			TransactionConfirmationDate ,
			TransactionCostIsPercent ,
			TransactionExemptFromUpdate , 
			TransactionIsTransfer , 
			TransactionSpecificInitialEqualisationFlag , 
			TransactionFinalAmount ,
			TransactionFinalPrice ,
			TransactionFinalCosts ,
			TransactionFinalSettlement ,
			TransactionInstructionFlag ,
			TransactionBankConfirmation ,
			TransactionInitialDataEntry ,
			TransactionAmountConfirmed ,
			TransactionSettlementConfirmed ,
			TransactionFinalDataEntry ,
			TransactionComment  ,
			TransactionGroup ,
			ParameterCount ,
			Parameter1_Name ,
			Parameter1_Type ,
			Parameter1_Value ,
			Parameter2_Name ,
			Parameter2_Type ,
			Parameter2_Value ,
			Parameter3_Name ,
			Parameter3_Type ,
			Parameter3_Value ,
			Parameter4_Name ,
			Parameter4_Type ,
			Parameter4_Value ,
			Parameter5_Name ,
			Parameter5_Type ,
			Parameter5_Value ,
			CompoundTransactionGroup ,
			CompoundTransactionComment ,
			CompoundTransactionFlags ,
			CompoundTransactionPosted ,
			UserEntered ,
			DateEntered ,
			DateDeleted  
			
	FROM fn_tblCompoundTransaction_SelectKD(@Knowledgedate)
			
	RETURN -1


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[adp_tblCompoundTransaction_SelectCommand]  TO [InvestMaster_Sales]
GO

GRANT  EXECUTE  ON [dbo].[adp_tblCompoundTransaction_SelectCommand]  TO [InvestMaster_Manager]
GO

GRANT  EXECUTE  ON [dbo].[adp_tblCompoundTransaction_SelectCommand]  TO [InvestMaster_Admin]
GO

GRANT  EXECUTE  ON [dbo].[adp_tblCompoundTransaction_SelectCommand]  TO [InvestMaster_AddTransaction]
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO




CREATE PROCEDURE dbo.adp_tblCompoundTransaction_UpdateCommand
(
	@AdminStatus int, 
	@TransactionParentID int = 0,
	@TransactionCompoundID int,
	@TransactionTemplateID int,
	@TransactionTicket varchar (50),
	@TransactionFund int,
	@TransactionInstrument int,
	@TransactionType int,
	@TransactionValueorAmount varchar (10),
	@TransactionUnits float = 0,
	@TransactionPrice float = 0,
	@TransactionCosts float = 0,
	@TransactionCostPercent float = 0,
	@TransactionEffectivePrice float = NULL,
	@TransactionEffectiveWatermark float = NULL,
	@TransactionSpecificInitialEqualisationValue float = 0,
	@TransactionCounterparty int,
	@TransactionInvestment varchar (50),
	@TransactionExecution varchar (50),
	@TransactionDataEntry varchar (50),
	@TransactionDecisionDate datetime,
	@TransactionValueDate datetime,
	@TransactionFIFOValueDate datetime,
	@TransactionEffectiveValueDate datetime = NULL,
	@TransactionSettlementDate datetime,
	@TransactionConfirmationDate datetime,
	@TransactionCostIsPercent bit = 0, 
	@TransactionExemptFromUpdate bit = 0, 
	@TransactionIsTransfer bit = 0, 
	@TransactionSpecificInitialEqualisationFlag bit = 0, 
	@TransactionFinalAmount int = 0,
	@TransactionFinalPrice int = 0,
	@TransactionFinalCosts int = 0,
	@TransactionFinalSettlement int = 0,
	@TransactionInstructionFlag int = 0,
	@TransactionBankConfirmation int = 0,
	@TransactionInitialDataEntry int = 0,
	@TransactionAmountConfirmed int = 0,
	@TransactionSettlementConfirmed int = 0,
	@TransactionFinalDataEntry int = 0,
	@TransactionComment varchar (500),
	@TransactionGroup varchar (50),
	@ParameterCount int = 0, 
	@Parameter1_Name varchar (50) = '',
	@Parameter1_Type int = 0, 
	@Parameter1_Value varchar (50) = '0',
	@Parameter2_Name varchar (50) = '',
	@Parameter2_Type int = 0, 
	@Parameter2_Value varchar (50) = '0',
	@Parameter3_Name varchar (50) = '',
	@Parameter3_Type int = 0, 
	@Parameter3_Value varchar (50) = '0',
	@Parameter4_Name varchar (50) = '',
	@Parameter4_Type int = 0, 
	@Parameter4_Value varchar (50) = '0',
	@Parameter5_Name varchar (50) = '',
	@Parameter5_Type int = 0, 
	@Parameter5_Value varchar (50) = '0',
	@CompoundTransactionGroup int = 0, 
	@CompoundTransactionComment varchar (50) = '',
	@CompoundTransactionFlags int = 0, 
	@CompoundTransactionPosted int = 0, 
	@KnowledgeDate datetime, 
	@rvStatusString varchar(200) OUTPUT	
)
AS
	SET NOCOUNT OFF;

	DECLARE @RVal int 

	IF (@Knowledgedate >= (getdate())) OR (ISNULL(@Knowledgedate, '1 Jan 1900') <= '1 Jan 1900')
	  BEGIN
		EXECUTE @RVal = [dbo].[adp_tblCompoundTransaction_InsertCommand] 
			@AdminStatus = @AdminStatus , 
			@TransactionParentID = @TransactionParentID ,
			@TransactionCompoundID = @TransactionCompoundID ,
			@TransactionTemplateID = @TransactionTemplateID ,
			@TransactionTicket = @TransactionTicket ,
			@TransactionFund = @TransactionFund ,
			@TransactionInstrument = @TransactionInstrument ,
			@TransactionType = @TransactionType ,
			@TransactionValueorAmount = @TransactionValueorAmount ,
			@TransactionUnits = @TransactionUnits ,
			@TransactionPrice = @TransactionPrice ,
			@TransactionCosts = @TransactionCosts ,
			@TransactionCostPercent = @TransactionCostPercent, 
			@TransactionEffectivePrice = @TransactionEffectivePrice, 
			@TransactionEffectiveWatermark = @TransactionEffectiveWatermark, 
			@TransactionSpecificInitialEqualisationValue = @TransactionSpecificInitialEqualisationValue, 
			@TransactionCounterparty = @TransactionCounterparty ,
			@TransactionInvestment = @TransactionInvestment ,
			@TransactionExecution = @TransactionExecution ,
			@TransactionDataEntry = @TransactionDataEntry ,
			@TransactionDecisionDate = @TransactionDecisionDate ,
			@TransactionValueDate = @TransactionValueDate ,
			@TransactionFIFOValueDate = @TransactionFIFOValueDate ,
			@TransactionEffectiveValueDate = @TransactionEffectiveValueDate, 
			@TransactionSettlementDate = @TransactionSettlementDate ,
			@TransactionConfirmationDate = @TransactionConfirmationDate ,
			@TransactionCostIsPercent = @TransactionCostIsPercent, 
			@TransactionExemptFromUpdate = @TransactionExemptFromUpdate, 
			@TransactionIsTransfer = @TransactionIsTransfer, 
			@TransactionSpecificInitialEqualisationFlag = @TransactionSpecificInitialEqualisationFlag, 
			@TransactionFinalAmount = @TransactionFinalAmount ,
			@TransactionFinalPrice = @TransactionFinalPrice ,
			@TransactionFinalCosts = @TransactionFinalCosts ,
			@TransactionFinalSettlement = @TransactionFinalSettlement ,
			@TransactionInstructionFlag = @TransactionInstructionFlag ,
			@TransactionBankConfirmation = @TransactionBankConfirmation ,
			@TransactionInitialDataEntry = @TransactionInitialDataEntry ,
			@TransactionAmountConfirmed = @TransactionAmountConfirmed ,
			@TransactionSettlementConfirmed = @TransactionSettlementConfirmed ,
			@TransactionFinalDataEntry = @TransactionFinalDataEntry ,
			@TransactionComment = @TransactionComment ,
			@TransactionGroup = @TransactionGroup ,
			@ParameterCount = @ParameterCount , 
			@Parameter1_Name = @Parameter1_Name ,
			@Parameter1_Type = @Parameter1_Type , 
			@Parameter1_Value = @Parameter1_Value ,
			@Parameter2_Name = @Parameter2_Name ,
			@Parameter2_Type = @Parameter2_Type , 
			@Parameter2_Value = @Parameter2_Value ,
			@Parameter3_Name = @Parameter3_Name ,
			@Parameter3_Type = @Parameter3_Type , 
			@Parameter3_Value = @Parameter3_Value ,
			@Parameter4_Name = @Parameter4_Name ,
			@Parameter4_Type = @Parameter4_Type , 
			@Parameter4_Value = @Parameter4_Value ,
			@Parameter5_Name = @Parameter5_Name ,
			@Parameter5_Type = @Parameter5_Type , 
			@Parameter5_Value = @Parameter5_Value ,
			@CompoundTransactionGroup = @CompoundTransactionGroup , 
			@CompoundTransactionComment = @CompoundTransactionComment ,
			@CompoundTransactionFlags = @CompoundTransactionFlags , 
			@CompoundTransactionPosted = 0, -- @CompoundTransactionPosted , 
			@KnowledgeDate = @KnowledgeDate , 
			@rvStatusString = @rvStatusString OUTPUT 

		RETURN @RVal
	  END
	ELSE
	  BEGIN

		SELECT	[RN], 
				[AuditID], 
			AdminStatus ,
			TransactionParentID ,
			TransactionCompoundID ,
			TransactionTemplateID ,
			TransactionTicket ,
			TransactionFund ,
			TransactionInstrument ,
			TransactionType ,
			TransactionValueorAmount ,
			TransactionUnits ,
			TransactionPrice ,
			TransactionCosts ,
			TransactionCostPercent ,
			TransactionEffectivePrice , 
			TransactionEffectiveWatermark , 
			TransactionSpecificInitialEqualisationValue , 
			TransactionCounterparty ,
			TransactionInvestment ,
			TransactionExecution ,
			TransactionDataEntry ,
			TransactionDecisionDate ,
			TransactionValueDate ,
			TransactionFIFOValueDate ,
			TransactionEffectiveValueDate , 
			TransactionSettlementDate ,
			TransactionConfirmationDate ,
			TransactionCostIsPercent ,
			TransactionExemptFromUpdate , 
			TransactionIsTransfer , 
			TransactionSpecificInitialEqualisationFlag , 
			TransactionFinalAmount ,
			TransactionFinalPrice ,
			TransactionFinalCosts ,
			TransactionFinalSettlement ,
			TransactionInstructionFlag ,
			TransactionBankConfirmation ,
			TransactionInitialDataEntry ,
			TransactionAmountConfirmed ,
			TransactionSettlementConfirmed ,
			TransactionFinalDataEntry ,
			TransactionComment  ,
			TransactionGroup ,
			ParameterCount ,
			Parameter1_Name ,
			Parameter1_Type ,
			Parameter1_Value ,
			Parameter2_Name ,
			Parameter2_Type ,
			Parameter2_Value ,
			Parameter3_Name ,
			Parameter3_Type ,
			Parameter3_Value ,
			Parameter4_Name ,
			Parameter4_Type ,
			Parameter4_Value ,
			Parameter5_Name ,
			Parameter5_Type ,
			Parameter5_Value ,
			CompoundTransactionGroup ,
			CompoundTransactionComment ,
			CompoundTransactionFlags ,
			CompoundTransactionPosted ,
			UserEntered ,
			DateEntered ,
			DateDeleted   
		FROM [dbo].[fn_tblCompoundTransaction_SelectKD](@KnowledgeDate) 
		WHERE (TransactionCompoundID = @TransactionCompoundID)

	  END
	 



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[adp_tblCompoundTransaction_UpdateCommand]  TO [InvestMaster_Manager]
GO

GRANT  EXECUTE  ON [dbo].[adp_tblCompoundTransaction_UpdateCommand]  TO [InvestMaster_Admin]
GO

GRANT  EXECUTE  ON [dbo].[adp_tblCompoundTransaction_UpdateCommand]  TO [InvestMaster_AddTransaction]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[adp_tblPendingTransactions_InsertCommand]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[adp_tblPendingTransactions_InsertCommand]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[adp_tblPendingTransactions_SelectCommand]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[adp_tblPendingTransactions_SelectCommand]
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE PROCEDURE dbo.adp_tblPendingTransactions_InsertCommand
	(
	@TransactionTicket nvarchar (50),
	@TransactionFund int,
	@TransactionSubFund int = Null, 
	@TransactionSubFundValueFlag smallint = 0,
	@TransactionInstrument int,
	@TransactionType int,
	@TransactionValueorAmount nvarchar (10),
	@TransactionUnits float = 0,
	@TransactionPrice float = 0,
	@TransactionCosts float = 0,
	@TransactionCostPercent float = 0,
	@TransactionMainFundPrice float = 0,
	@TransactionFXRate float = 0,
	@TransactionEffectivePrice float = NULL,
	@TransactionEffectiveWatermark float = NULL,
	@TransactionSpecificInitialEqualisationValue float = 0,
	@TransactionCounterparty int,
	@TransactionInvestment nvarchar (50),
	@TransactionExecution nvarchar (50),
	@TransactionDataEntry nvarchar (50),
	@TransactionDecisionDate datetime,
	@TransactionValueDate datetime,
	@TransactionFIFOValueDate datetime,
	@TransactionEffectiveValueDate datetime = NULL,
	@TransactionSettlementDate datetime,
	@TransactionConfirmationDate datetime,
	@TransactionCostIsPercent bit = 0, 
	@TransactionExemptFromUpdate bit = 0, 
	@TransactionIsTransfer bit = 0, 
	@TransactionSpecificInitialEqualisationFlag bit = 0, 
	@TransactionFinalAmount int = 0,
	@TransactionFinalPrice int = 0,
	@TransactionFinalCosts int = 0,
	@TransactionFinalSettlement int = 0,
	@TransactionInstructionFlag int = 0,
	@TransactionBankConfirmation int = 0,
	@TransactionInitialDataEntry int = 0,
	@TransactionAmountConfirmed int = 0,
	@TransactionSettlementConfirmed int = 0,
	@TransactionFinalDataEntry int = 0,
	@TransactionComment varchar (500),
	@TransactionParentID int = 0,
	@TransactionGroup nvarchar (50),
	@TransactionAdminStatus int = 0,
	@TransactionCTFLAGS int = 0,
	@TransactionCompoundRN int = 0,
	@KnowledgeDate datetime
	)
AS
	SET NOCOUNT OFF

	DECLARE @IntitalTransCount int 
	SELECT @IntitalTransCount = @@TRANCOUNT

	SET TRANSACTION ISOLATION LEVEL SERIALIZABLE

	BEGIN TRANSACTION

	INSERT INTO tblPendingTransactions(
			TransactionTicket, 
			TransactionFund, 
			TransactionSubFund, 
			TransactionSubFundValueFlag, 
			TransactionInstrument,  
			TransactionType, 
			TransactionValueorAmount, 
			TransactionUnits, 
			TransactionPrice, 
			TransactionCosts,  
			TransactionCostPercent, 
			TransactionEffectivePrice,
			TransactionEffectiveWatermark,
			TransactionSpecificInitialEqualisationValue,
			TransactionMainFundPrice, 
			TransactionFXRate, 
			TransactionCounterparty, 
			TransactionInvestment, 
			TransactionExecution,  
			TransactionDataEntry, 
			TransactionDecisionDate, 
			TransactionValueDate, 
			TransactionFIFOValueDate, 
			TransactionEffectiveValueDate,
			TransactionSettlementDate,  
			TransactionConfirmationDate, 
			TransactionCostIsPercent,
			TransactionExemptFromUpdate,
			TransactionIsTransfer,
			TransactionSpecificInitialEqualisationFlag,
			TransactionFinalAmount, 
			TransactionFinalPrice, 
			TransactionFinalCosts,  
			TransactionFinalSettlement, 
			TransactionInstructionFlag, 
			TransactionBankConfirmation, 
			TransactionInitialDataEntry, 
			TransactionAmountConfirmed,  
			TransactionSettlementConfirmed, 
			TransactionFinalDataEntry, 
			TransactionComment, 
			TransactionParentID, 
			TransactionGroup, 
			TransactionAdminStatus, 
			TransactionCompoundRN, 
			TransactionCTFLAGS
			)
		VALUES (
			@TransactionTicket, 
			@TransactionFund, 
			@TransactionSubFund, 
			@TransactionSubFundValueFlag, 
			@TransactionInstrument,  
			@TransactionType, 
			@TransactionValueorAmount, 
			@TransactionUnits, 
			@TransactionPrice, 
			@TransactionCosts,  
			ISNULL(@TransactionCostPercent, 0) ,
			ISNULL(@TransactionEffectivePrice, @TransactionPrice) , 
			ISNULL(@TransactionEffectiveWatermark, 0) , 
			ISNULL(@TransactionSpecificInitialEqualisationValue, 0) , 
			@TransactionMainFundPrice, 
			@TransactionFXRate, 
			@TransactionCounterparty, 
			@TransactionInvestment, 
			@TransactionExecution,  
			@TransactionDataEntry, 
			ISNULL(@TransactionDecisionDate, @TransactionValueDate), 
			@TransactionValueDate, 
			ISNULL(@TransactionFIFOValueDate, @TransactionValueDate), 
			ISNULL(@TransactionEffectiveValueDate, @TransactionValueDate) , 
			ISNULL(@TransactionSettlementDate, @TransactionValueDate), 
			ISNULL(@TransactionConfirmationDate, @TransactionValueDate), 
			ISNULL(@TransactionCostIsPercent, 0) ,
			ISNULL(@TransactionExemptFromUpdate, 0) , 
			ISNULL(@TransactionIsTransfer, 0) , 
			ISNULL(@TransactionSpecificInitialEqualisationFlag, 0) , 
			@TransactionFinalAmount, 
			@TransactionFinalPrice, 
			@TransactionFinalCosts,  
			@TransactionFinalSettlement, 
			@TransactionInstructionFlag, 
			@TransactionBankConfirmation, 
			@TransactionInitialDataEntry, 
			@TransactionAmountConfirmed,  
			@TransactionSettlementConfirmed, 
			@TransactionFinalDataEntry, 
			ISNULL(@TransactionComment, ''), 
			@TransactionParentID, 
			ISNULL(@TransactionGroup, ''), 
			@TransactionAdminStatus, 
			@TransactionCompoundRN, 
			@TransactionCTFLAGS
			)

	IF @@ERROR <> 0
	BEGIN
		ROLLBACK TRANSACTION
		RETURN 0
	END
	ELSE
	BEGIN
		WHILE @IntitalTransCount < @@TRANCOUNT
			COMMIT TRANSACTION

		RETURN SCOPE_IDENTITY()
	END
	



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[adp_tblPendingTransactions_InsertCommand]  TO [InvestMaster_Manager]
GO

GRANT  EXECUTE  ON [dbo].[adp_tblPendingTransactions_InsertCommand]  TO [InvestMaster_Admin]
GO

GRANT  EXECUTE  ON [dbo].[adp_tblPendingTransactions_InsertCommand]  TO [InvestMaster_AddTransaction]
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE PROCEDURE dbo.adp_tblPendingTransactions_SelectCommand
(
@KnowledgeDate datetime = Null
)
AS
	SET NOCOUNT ON

	SELECT	[RN], 
			[AuditID], 
			TransactionID ,
			TransactionTicket ,
			TransactionFund ,
			TransactionSubFund ,
			TransactionSubFundValueFlag ,
			TransactionInstrument ,
			TransactionType ,
			TransactionValueorAmount ,
			TransactionUnits ,
			TransactionPrice ,
			TransactionCosts ,
			TransactionCostPercent , 
			TransactionMainFundPrice ,
			TransactionFXRate ,
			TransactionEffectivePrice , 
			TransactionEffectiveWatermark , 
			TransactionSpecificInitialEqualisationValue , 
			TransactionCounterparty ,
			TransactionInvestment ,
			TransactionExecution ,
			TransactionDataEntry ,
			TransactionDecisionDate ,
			TransactionValueDate ,
			TransactionFIFOValueDate ,
			TransactionEffectiveValueDate, 
			TransactionSettlementDate ,
			TransactionConfirmationDate ,
			TransactionCostIsPercent ,
			TransactionExemptFromUpdate , 
			TransactionIsTransfer , 
			TransactionSpecificInitialEqualisationFlag, 
			TransactionFinalAmount ,
			TransactionFinalPrice ,
			TransactionFinalCosts ,
			TransactionFinalSettlement ,
			TransactionInstructionFlag ,
			TransactionBankConfirmation ,
			TransactionInitialDataEntry ,
			TransactionAmountConfirmed ,
			TransactionSettlementConfirmed ,
			TransactionFinalDataEntry ,
			TransactionComment ,
			TransactionParentID ,
			TransactionUser ,
			TransactionGroup ,
			TransactionAdminStatus ,
			TransactionCTFLAGS ,
			TransactionCompoundRN 			
	FROM tblPendingTransactions
			
	RETURN -1



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[adp_tblPendingTransactions_SelectCommand]  TO [InvestMaster_Sales]
GO

GRANT  EXECUTE  ON [dbo].[adp_tblPendingTransactions_SelectCommand]  TO [InvestMaster_Manager]
GO

GRANT  EXECUTE  ON [dbo].[adp_tblPendingTransactions_SelectCommand]  TO [InvestMaster_Admin]
GO

GRANT  EXECUTE  ON [dbo].[adp_tblPendingTransactions_SelectCommand]  TO [InvestMaster_AddTransaction]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CommitCompoundTransaction]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[CommitCompoundTransaction]
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


CREATE PROCEDURE dbo.CommitCompoundTransaction
	(
		@TransactionCompoundRN int
	)
AS

SET NOCOUNT ON

DECLARE @RVAL int
DECLARE @rvStatusString  varchar(200)

--
-- First Commit the Pending Transactions to the real Transactions Table.
--

DECLARE @TransactionTicket nvarchar (50)
DECLARE @TransactionFund int
DECLARE @TransactionSubFund int  
DECLARE @TransactionSubFundValueFlag smallint
DECLARE @TransactionInstrument int
DECLARE @TransactionType int
DECLARE @TransactionValueorAmount nvarchar (10)
DECLARE @TransactionUnits float 
DECLARE @TransactionPrice float 
DECLARE @TransactionCosts float 
DECLARE	@TransactionCostPercent float
DECLARE @TransactionMainFundPrice float 
DECLARE @TransactionFXRate float 
DECLARE	@TransactionEffectivePrice float
DECLARE	@TransactionEffectiveWatermark float 
DECLARE	@TransactionSpecificInitialEqualisationValue float
DECLARE @TransactionCounterparty int
DECLARE @TransactionInvestment nvarchar (50)
DECLARE @TransactionExecution nvarchar (50)
DECLARE @TransactionDataEntry nvarchar (50)
DECLARE @TransactionDecisionDate datetime
DECLARE @TransactionValueDate datetime
DECLARE @TransactionFIFOValueDate datetime
DECLARE	@TransactionEffectiveValueDate datetime
DECLARE @TransactionSettlementDate datetime
DECLARE @TransactionConfirmationDate datetime
DECLARE	@TransactionCostIsPercent bit
DECLARE	@TransactionExemptFromUpdate bit 
DECLARE	@TransactionIsTransfer bit 
DECLARE	@TransactionSpecificInitialEqualisationFlag bit
DECLARE @TransactionFinalAmount int 
DECLARE @TransactionFinalPrice int 
DECLARE @TransactionFinalCosts int 
DECLARE @TransactionFinalSettlement int 
DECLARE @TransactionInstructionFlag int 
DECLARE @TransactionBankConfirmation int 
DECLARE @TransactionInitialDataEntry int 
DECLARE @TransactionAmountConfirmed int
DECLARE @TransactionSettlementConfirmed int 
DECLARE @TransactionFinalDataEntry int 
DECLARE @TransactionComment varchar (500)
DECLARE @TransactionParentID int 
DECLARE @TransactionGroup nvarchar (50)
DECLARE @TransactionAdminStatus int 
DECLARE @TransactionCTFLAGS int 

DECLARE PendingTransactionsCursor CURSOR
FOR 
SELECT 		TransactionTicket, 
			TransactionFund, 
			TransactionSubFund, 
			TransactionSubFundValueFlag, 
			TransactionInstrument,  
			TransactionType, 
			TransactionValueorAmount, 
			TransactionUnits, 
			TransactionPrice, 
			TransactionCosts,  
			TransactionCostPercent, 
			TransactionMainFundPrice, 
			TransactionFXRate, 
			TransactionEffectivePrice,
			TransactionEffectiveWatermark,
			TransactionSpecificInitialEqualisationValue,			
			TransactionCounterparty, 
			TransactionInvestment, 
			TransactionExecution,  
			TransactionDataEntry, 
			TransactionDecisionDate, 
			TransactionValueDate, 
			TransactionFIFOValueDate, 
			TransactionEffectiveValueDate, 
			TransactionSettlementDate,  
			TransactionConfirmationDate, 
			TransactionCostIsPercent,
			TransactionExemptFromUpdate,
			TransactionIsTransfer,
			TransactionSpecificInitialEqualisationFlag,
			TransactionFinalAmount, 
			TransactionFinalPrice, 
			TransactionFinalCosts,  
			TransactionFinalSettlement, 
			TransactionInstructionFlag, 
			TransactionBankConfirmation, 
			TransactionInitialDataEntry, 
			TransactionAmountConfirmed,  
			TransactionSettlementConfirmed, 
			TransactionFinalDataEntry, 
			TransactionComment, 
			TransactionParentID, 
			TransactionGroup, 
			TransactionAdminStatus, 
			TransactionCTFLAGS
FROM   tblPendingTransactions
WHERE (TransactionCompoundRN = @TransactionCompoundRN) 
ORDER BY RN
	
OPEN PendingTransactionsCursor

FETCH NEXT FROM PendingTransactionsCursor 
INTO  		@TransactionTicket, 
			@TransactionFund, 
			@TransactionSubFund, 
			@TransactionSubFundValueFlag, 
			@TransactionInstrument,  
			@TransactionType, 
			@TransactionValueorAmount, 
			@TransactionUnits, 
			@TransactionPrice, 
			@TransactionCosts,  
			@TransactionCostPercent,
			@TransactionMainFundPrice, 
			@TransactionFXRate, 
			@TransactionEffectivePrice, 
			@TransactionEffectiveWatermark,
			@TransactionSpecificInitialEqualisationValue,
			@TransactionCounterparty, 
			@TransactionInvestment, 
			@TransactionExecution,  
			@TransactionDataEntry, 
			@TransactionDecisionDate, 
			@TransactionValueDate, 
			@TransactionFIFOValueDate, 
			@TransactionEffectiveValueDate, 
			@TransactionSettlementDate,  
			@TransactionConfirmationDate, 
			@TransactionCostIsPercent, 
			@TransactionExemptFromUpdate, 
			@TransactionIsTransfer, 
			@TransactionSpecificInitialEqualisationFlag, 
			@TransactionFinalAmount, 
			@TransactionFinalPrice, 
			@TransactionFinalCosts,  
			@TransactionFinalSettlement, 
			@TransactionInstructionFlag, 
			@TransactionBankConfirmation, 
			@TransactionInitialDataEntry, 
			@TransactionAmountConfirmed,  
			@TransactionSettlementConfirmed, 
			@TransactionFinalDataEntry, 
			@TransactionComment, 
			@TransactionParentID, 
			@TransactionGroup, 
			@TransactionAdminStatus, 
			@TransactionCTFLAGS

IF @@ERROR <> 0
BEGIN
	CLOSE PendingTransactionsCursor
	DEALLOCATE PendingTransactionsCursor
	ROLLBACK TRANSACTION
	RETURN 0
END

BEGIN TRANSACTION
	
WHILE @@fetch_status = 0
BEGIN

	EXEC @RVal =  adp_tblTransaction_InsertCommand
		@TransactionTicket = @TransactionTicket,
		@TransactionFund = @TransactionFund, 
		@TransactionInstrument = @TransactionInstrument, 
		@TransactionType = @TransactionType, 
		@TransactionValueorAmount = @TransactionValueorAmount,
		@TransactionUnits = @TransactionUnits,
		@TransactionPrice = @TransactionPrice,
		@TransactionCosts = @TransactionCosts,
		@TransactionCostPercent = @TransactionCostPercent, 
		@TransactionEffectivePrice = @TransactionEffectivePrice, 
		@TransactionEffectiveWatermark = @TransactionEffectiveWatermark, 
		@TransactionSpecificInitialEqualisationValue = @TransactionSpecificInitialEqualisationValue, 
		@TransactionCounterparty = @TransactionCounterparty,
		@TransactionInvestment = @TransactionInvestment,
		@TransactionExecution = @TransactionExecution,
		@TransactionDataEntry = @TransactionDataEntry,
		@TransactionDecisionDate = @TransactionDecisionDate,
		@TransactionValueDate = @TransactionValueDate,
		@TransactionFIFOValueDate = @TransactionFIFOValueDate,
		@TransactionEffectiveValueDate = @TransactionEffectiveValueDate, 
		@TransactionSettlementDate = @TransactionSettlementDate,
		@TransactionConfirmationDate = @TransactionConfirmationDate,
		@TransactionCostIsPercent = @TransactionCostIsPercent, 
		@TransactionExemptFromUpdate = @TransactionExemptFromUpdate, 
		@TransactionIsTransfer = @TransactionIsTransfer, 
		@TransactionSpecificInitialEqualisationFlag = @TransactionSpecificInitialEqualisationFlag, 
		@TransactionFinalAmount = @TransactionFinalAmount,
		@TransactionFinalPrice = @TransactionFinalPrice,
		@TransactionFinalCosts = @TransactionFinalCosts,
		@TransactionFinalSettlement = @TransactionFinalSettlement,
		@TransactionInstructionFlag = @TransactionInstructionFlag,
		@TransactionBankConfirmation = @TransactionBankConfirmation,
		@TransactionInitialDataEntry = @TransactionInitialDataEntry,
		@TransactionAmountConfirmed = @TransactionAmountConfirmed,
		@TransactionSettlementConfirmed = @TransactionSettlementConfirmed,
		@TransactionFinalDataEntry = @TransactionFinalDataEntry,
		@TransactionComment = @TransactionComment,
		@TransactionParentID = @TransactionParentID,
		@TransactionRedemptionID = 0, 
		@TransactionGroup = @TransactionGroup,
		@TransactionAdminStatus = @TransactionAdminStatus,
		@TransactionCTFLAGS = @TransactionCTFLAGS,
		@TransactionCompoundRN = @TransactionCompoundRN,
		@KnowledgeDate = Null, 
		@ValidationOnly = 0,
		@rvStatusString = @rvStatusString OUTPUT	

	IF @@ERROR <> 0
	BEGIN
		CLOSE PendingTransactionsCursor
		DEALLOCATE PendingTransactionsCursor
		ROLLBACK TRANSACTION
		RETURN 0
	END

	IF @RVAL <= 0
	BEGIN
		CLOSE PendingTransactionsCursor
		DEALLOCATE PendingTransactionsCursor
		ROLLBACK TRANSACTION
		RETURN 0
	END

	FETCH NEXT FROM PendingTransactionsCursor 
	INTO  	@TransactionTicket, 
			@TransactionFund, 
			@TransactionSubFund, 
			@TransactionSubFundValueFlag, 
			@TransactionInstrument,  
			@TransactionType, 
			@TransactionValueorAmount, 
			@TransactionUnits, 
			@TransactionPrice, 
			@TransactionCosts,  
			@TransactionCostPercent,
			@TransactionMainFundPrice, 
			@TransactionFXRate, 
			@TransactionEffectivePrice, 
			@TransactionEffectiveWatermark,
			@TransactionSpecificInitialEqualisationValue,
			@TransactionCounterparty, 
			@TransactionInvestment, 
			@TransactionExecution,  
			@TransactionDataEntry, 
			@TransactionDecisionDate, 
			@TransactionValueDate, 
			@TransactionFIFOValueDate, 
			@TransactionEffectiveValueDate, 
			@TransactionSettlementDate,  
			@TransactionConfirmationDate, 
			@TransactionCostIsPercent, 
			@TransactionExemptFromUpdate, 
			@TransactionIsTransfer, 
			@TransactionSpecificInitialEqualisationFlag, 
			@TransactionFinalAmount, 
			@TransactionFinalPrice, 
			@TransactionFinalCosts,  
			@TransactionFinalSettlement, 
			@TransactionInstructionFlag, 
			@TransactionBankConfirmation, 
			@TransactionInitialDataEntry, 
			@TransactionAmountConfirmed,  
			@TransactionSettlementConfirmed, 
			@TransactionFinalDataEntry, 
			@TransactionComment, 
			@TransactionParentID, 
			@TransactionGroup, 
			@TransactionAdminStatus, 
			@TransactionCTFLAGS

	IF @@ERROR <> 0
	BEGIN
		CLOSE PendingTransactionsCursor
		DEALLOCATE PendingTransactionsCursor
		ROLLBACK TRANSACTION
		RETURN 0
	END

END	

CLOSE PendingTransactionsCursor
DEALLOCATE PendingTransactionsCursor

DECLARE @TransactionCompoundID int

SELECT @TransactionCompoundID = ISNULL(MAX(TransactionCompoundID), 0)
FROM tblCompoundTransaction
WHERE RN = @TransactionCompoundRN

IF @@ERROR <> 0
BEGIN
	ROLLBACK TRANSACTION
	RETURN 0
END

UPDATE tblTransaction
SET TransactionDateDeleted = getdate()
WHERE	(TransactionCompoundRN > 0) AND 
		(TransactionCompoundRN <> @TransactionCompoundRN) AND 
		(RN IN (SELECT RN from fn_tblTransaction_SelectKD(Null) WHERE (TransactionCompoundRN IN (SELECT RN FROM tblCompoundTransaction WHERE TransactionCompoundID = @TransactionCompoundID))))

IF @@ERROR <> 0
BEGIN
	ROLLBACK TRANSACTION
	RETURN 0
END

-- Delete from tblPendingTransactions

DELETE
FROM tblPendingTransactions
WHERE TransactionCompoundRN = @TransactionCompoundRN

IF @@ERROR <> 0
BEGIN
	ROLLBACK TRANSACTION
	RETURN 0
END


-- Update tblCompoundTransaction

UPDATE tblCompoundTransaction
SET CompoundTransactionPosted = (-1)
WHERE RN = @TransactionCompoundRN

IF @@ERROR <> 0
BEGIN
	ROLLBACK TRANSACTION
	RETURN 0
END

-- Commit the Transactions

COMMIT TRANSACTION	
	
IF @@ERROR <> 0
BEGIN
	ROLLBACK TRANSACTION
	RETURN 0
END
		
RETURN (-1)




GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[CommitCompoundTransaction]  TO [InvestMaster_Manager]
GO

GRANT  EXECUTE  ON [dbo].[CommitCompoundTransaction]  TO [InvestMaster_Admin]
GO

GRANT  EXECUTE  ON [dbo].[CommitCompoundTransaction]  TO [InvestMaster_AddTransaction]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[fn_NewTransactionTicket]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[fn_NewTransactionTicket]
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


CREATE  FUNCTION dbo.fn_NewTransactionTicket
	(
	@Header varchar(50), 
	@Modifier int
	)
RETURNS varchar(50)
AS
	BEGIN
		DECLARE @CurrentTicket varchar(50)
		DECLARE @NewTicket varchar(50)
		DECLARE @CurrentCounter int
		DECLARE @NewCounter int
		Declare @ChrCount int

		SELECT @CurrentTicket = Null

		SELECT @CurrentTicket = ISNULL(MAX(TransactionTicket), @Header + '_000000')
		FROM tblTransaction
		WHERE TransactionTicket LIKE (@Header + '_%')

		SELECT @CurrentCounter = CONVERT(int, RIGHT(@CurrentTicket, LEN(@CurrentTicket) - (LEN(@Header) + 1 )))

		SELECT @NewCounter = (@CurrentCounter + 1 + ISNULL(@Modifier, 0))

		SELECT @ChrCount = CONVERT(Int, LOG10(@NewCounter)) + 1

		IF @NewCounter >= 900000
		BEGIN
			
			IF Right(STR(@NewCounter), 5) = '00000' 
			BEGIN
				IF Left(LTRIM(STR(@NewCounter)), @ChrCount-5) = REPLICATE('9', @ChrCount-5)
				BEGIN
					SELECT @NewCounter = (@NewCounter * 100)
					SELECT @ChrCount = @ChrCount + 2
				END
			END
		END

		IF @ChrCount < 6
			SELECT @ChrCount = 6
			
		SELECT @NewTicket = @Header + '_' + Right(REPLICATE('0', @ChrCount) + LTRIM(STR(@NewCounter)), @ChrCount)

		RETURN @NewTicket
	END


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[fn_NewTransactionTicket]  TO [InvestMaster_Sales]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[fn_tblSelectCompoundTransaction_SelectKD]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[fn_tblSelectCompoundTransaction_SelectKD]
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO




CREATE FUNCTION dbo.fn_tblSelectCompoundTransaction_SelectKD
(
@KnowledgeDate datetime = Null
)
RETURNS TABLE
AS
	RETURN ( 

	SELECT	Trans.RN,
			Trans.AuditID, 
			Trans.TransactionCompoundID, 
			Trans.TransactionTicket ,
			Trans.TransactionFund ,
			Trans.TransactionInstrument ,
			Trans.TransactionTemplateID ,
			Trans.TransactionValueorAmount ,
			Trans.TransactionUnits ,
			Trans.TransactionPrice ,
			Trans.TransactionCosts ,
			Trans.TransactionCostPercent ,
			Trans.TransactionEffectivePrice , 
			Trans.TransactionEffectiveWatermark , 
			Trans.TransactionSpecificInitialEqualisationValue ,
			Trans.TransactionCounterparty ,
			Trans.TransactionInvestment ,
			Trans.TransactionExecution ,
			Trans.TransactionDataEntry ,
			Trans.TransactionDecisionDate ,
			Trans.TransactionValueDate ,
			Trans.TransactionFIFOValueDate ,
			Trans.TransactionEffectiveValueDate , 
			Trans.TransactionSettlementDate ,
			Trans.TransactionConfirmationDate ,
			Trans.TransactionCostIsPercent ,
			Trans.TransactionExemptFromUpdate ,
			Trans.TransactionIsTransfer , 
			Trans.TransactionSpecificInitialEqualisationFlag , 
			Trans.TransactionFinalAmount ,
			Trans.TransactionFinalPrice ,
			Trans.TransactionFinalCosts ,
			Trans.TransactionFinalSettlement ,
			Trans.TransactionInstructionFlag ,
			Trans.TransactionBankConfirmation ,
			Trans.TransactionInitialDataEntry ,
			Trans.TransactionAmountConfirmed ,
			Trans.TransactionSettlementConfirmed ,
			Trans.TransactionFinalDataEntry ,
			Trans.TransactionParentID ,
			Trans.CompoundTransactionGroup ,
			Trans.CompoundTransactionComment ,
			Trans.CompoundTransactionFLAGS ,
			Trans.DateEntered,
			Funds.FundName AS FundName, 
			Insts.InstrumentDescription AS InstrumentDescription, 
			T_Type.TemplateDescription AS TransactionTypeName
			
	FROM	fn_tblCompoundTransaction_SelectKD(@KnowledgeDate) Trans LEFT OUTER JOIN
			fn_tblCompoundTransactionType_SelectKD(@KnowledgeDate) T_Type ON 
				Trans.TransactionTemplateID = T_Type.TemplateID LEFT OUTER JOIN
			fn_tblInstrument_SelectKD(@KnowledgeDate) Insts ON 
				Trans.TransactionInstrument = Insts.InstrumentID LEFT OUTER JOIN
			fn_tblFund_SelectKD(@KnowledgeDate) Funds ON 
				Trans.TransactionFund = Funds.FundID	
	
	 )




GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  SELECT  ON [dbo].[fn_tblSelectCompoundTransaction_SelectKD]  TO [InvestMaster_Sales]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[adp_tblSelectCompoundTransaction_SelectCommand]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[adp_tblSelectCompoundTransaction_SelectCommand]
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO




CREATE PROCEDURE dbo.adp_tblSelectCompoundTransaction_SelectCommand
(
@KnowledgeDate datetime = Null
)
AS
	SET NOCOUNT ON
	
	SELECT	RN,
			AuditID, 
			TransactionCompoundID, 
			TransactionTicket ,
			TransactionFund ,
			TransactionInstrument ,
			TransactionTemplateID ,
			TransactionValueorAmount ,
			TransactionUnits ,
			TransactionPrice ,
			TransactionCosts ,
			TransactionCostPercent , 
			TransactionCounterparty ,
			TransactionEffectivePrice , 
			TransactionEffectiveWatermark , 
			TransactionSpecificInitialEqualisationValue ,
			TransactionInvestment ,
			TransactionExecution ,
			TransactionDataEntry ,
			TransactionDecisionDate ,
			TransactionValueDate ,
			TransactionFIFOValueDate ,
			TransactionEffectiveValueDate , 
			TransactionSettlementDate ,
			TransactionConfirmationDate ,
			TransactionCostIsPercent , 
			TransactionExemptFromUpdate , 
			TransactionIsTransfer , 
			TransactionSpecificInitialEqualisationFlag , 
			TransactionFinalAmount ,
			TransactionFinalPrice ,
			TransactionFinalCosts ,
			TransactionFinalSettlement ,
			TransactionInstructionFlag ,
			TransactionBankConfirmation ,
			TransactionInitialDataEntry ,
			TransactionAmountConfirmed ,
			TransactionSettlementConfirmed ,
			TransactionFinalDataEntry ,
			TransactionParentID ,
			CompoundTransactionGroup ,
			CompoundTransactionComment ,
			CompoundTransactionFLAGS ,
			DateEntered,
			FundName, 
			InstrumentDescription, 
			TransactionTypeName

	FROM fn_tblSelectCompoundTransaction_SelectKD(@Knowledgedate)
	
	RETURN -1
	




GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[adp_tblSelectCompoundTransaction_SelectCommand]  TO [InvestMaster_Sales]
GO

GRANT  EXECUTE  ON [dbo].[adp_tblSelectCompoundTransaction_SelectCommand]  TO [InvestMaster_Manager]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[fn_BasicEqualisationCalcs]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[fn_BasicEqualisationCalcs]
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO



CREATE FUNCTION dbo.fn_BasicEqualisationCalcs
(
	@FundID int,
	@CounterpartyID int,
	@InvestorGroup int,
	@ValueDate datetime,
	@KnowledgeDate datetime
)

-- Function to Return BASIC Equalisation and Incentive Fee calculations.
-- Note This function also returns details of PENDING transactions, though they have no impact on any of the Totals
-- etc that are calculated.
--
-- This function was developed to feed data to VENICE2
-- The Complexities of the Equalisation calculations are such that it seems easier to do them
-- in Venice2 rather than in Transact SQL.

-- @FundID			: Limit the Funds to calculate for. NULL or 0 returns all Funds.
-- @CounterpartyID	: Limit the Counterparties to calculate for. NULL or 0 returns all Counterparties.
--					: FundID and CounterpartyID restrictions are cumulative (AND not OR).
-- @InvestorGroup	: InvestorGroup ID to select Counterparties by.
--					: As ever, 0 or Null indicate no selection. 
-- @Valuedate 		: Valuedate limit to apply to Transactions. NULL or '1 Jan 1900' returns all rows.
-- @Knowledgedate	: Knowledgedate to apply to source tables. Null or '1 Jan 1900' returns all rows.

RETURNS @EqualisationTransactions 
TABLE(RN int IDENTITY (1, 1), 
	PendingTransaction int DEFAULT 0, 	-- Pending Transaction Flag
	Counterparty int, 					-- Counterparty ID Identifier
	CounterpartyName varchar(100), 		-- Counterparty Name
	Fund int, 							-- Fund ID Identifier
	FundName varchar(100), 				-- Fund Name
	FundBaseCurrency int,				-- Fund Currency ID
	FundBaseCurrencyName varchar(100),	-- Fund Currency Name
	Instrument int, 					-- Instrument ID Identifier
	InstrumentDescription varchar(100), 	-- Instrument description
	InstrumentCurrencyID int, 
	TransactionType int, 				-- Transaction Type Identifier
	TransactionValueorAmount varchar(10),	-- Transaction based on Value or Amount
	TypeDescription varchar(100), 		-- Transaction type description
	ValueDate datetime, 				-- Transaction Value Date
	TransactionEffectiveValueDate datetime , -- ***
	ParentID int, 						-- Transaction Parent ID for this Transaction. Uniquely identifies pairs of Transactions. Also Indicative of original Entry Order.
	RedemptionID int DEFAULT 0,		-- If this is a Redemption, allows the user to specify a specific transaction against which to redeem. As opposed to standard FIFO.
	Price float, 						-- Transaction Price
	TransactionEffectivePrice float , -- ***
	SignedUnits float, 					-- Signed Transaction Unit amount
	SignedSettlement float, 			-- Signed Transaction Consideration amount
 	RunningTotalSubscriptions float, 	-- Running Total for Subscriptions up to this Transaction. (Ordered by ValueDate, ParentID)
 	RunningTotalRedemptions float, 		-- Running Total for Redemptions
	SpecificRedemptionSignedUnits float DEFAULT 0.0, 
	TotalSubscriptions float, 			-- Total Subscriptions for this Counterparty / Fund / Instrument
	TotalRedemptions float, 			-- Total Redemptions for this Counterparty / Fund / Instrument
	TotalSubscriptionConsideration float, 	-- Total Subscription Consideration for this Counterparty / Fund / Instrument
	TotalRedemptionProceeds float, 		-- Total Redemption proceeds for this Counterparty / Fund / Instrument
	GNAV_Price float, 					-- Gross NAV for this Fund
	NAV_Price float, 					-- NAV for this Fund
	Watermark float, 					-- Latest Watermark Price for this fund (Up to given @ValueDate)
	TransactionEffectiveWatermark float , -- ***
  WatermarkDate datetime,				-- Date of the latest Watermark (above)
  PerformanceFeesPercent float, 		-- Performance fee % levied by this Fund
	TransactionIsTransfer bit , -- ***
	TransactionSpecificInitialEqualisationFlag bit , -- ***
	TransactionSpecificInitialEqualisationValue float , -- ***
	RedemptionRatio float DEFAULT 1,	-- Percent of position remaining after Redemptions.
 	AccIncentiveFee float DEFAULT 0, 	-- Accrued Incentive Fee (Since the last Watermark)
	ContingentRedemptionFactor float DEFAULT 0,	-- Contingent Redemption due for this position
	InitialEqualisationFactor float DEFAULT 0, 	-- Equalisation amount due to this position at the time of Subscription
	CurrentEqualisationFactor float DEFAULT 0, 	-- Current Equalisation due on this position
	SumOfContingentRedemption float DEFAULT 0, 	-- Total Contingent Redemption Due to all Subscriptions, after Redemptions, for this Cpty / Fund / Instrument
	SumOFCurrentEqualisation float DEFAULT 0,	-- Total Current Equalisation Due to all Subscriptions, after Redemptions,  for this Cpty / Fund / Instrument
	SumOfContingentRedemptionExRedemption float DEFAULT 0, 	-- Total Contingent Redemption Due to all Subscriptions, ingoring Redemptions, for this Cpty / Fund / Instrument
	SumOFCurrentEqualisationExRedemption float DEFAULT 0)	-- Total Current Equalisation Due to all Subscriptions, ingoring Redemptions, for this Cpty / Fund / Instrument
AS
BEGIN

-- Declare TABLE Variables.

DECLARE @Transactions 		TABLE 	(RN int IDENTITY (1, 1), 
					PendingTransaction int DEFAULT 0, 
					Counterparty int DEFAULT 0, 
					Fund int DEFAULT 0, 
					Instrument int DEFAULT 0, 
					TransactionType int DEFAULT 0, 
					TransactionValueorAmount varchar(10) DEFAULT '',
					ValueDate datetime DEFAULT '1 Jan 1900', 
					TransactionEffectiveValueDate datetime DEFAULT '1 Jan 1900', -- ***
					ParentID int DEFAULT 0, 
					RedemptionID int DEFAULT 0, 
					Price float DEFAULT 0.0, 
					TransactionEffectivePrice float DEFAULT 0.0, -- ***
					TransactionEffectiveWatermark float DEFAULT 0.0, -- ***
					TransactionIsTransfer bit DEFAULT 0, -- ***
					TransactionSpecificInitialEqualisationFlag bit DEFAULT 0, -- ***
					TransactionSpecificInitialEqualisationValue float DEFAULT 0, -- ***
					SignedUnits float DEFAULT 0.0, 
					SpecificRedemptionSignedUnits float DEFAULT 0.0, 
					SignedSettlement float DEFAULT 0.0)

DECLARE @AggregatedTotals 	TABLE (Counterparty int, Fund int, Instrument int, TransactionType int, 
                                      SumSignedUnits float, SumSignedSettlement float)

DECLARE @TempEqualisation 	TABLE 	(RN int IDENTITY (1, 1), 
					TransactionRN int,
					PendingTransaction int DEFAULT 0,
					Counterparty int, Fund int, Instrument int, 
					TransactionType int, 
					TransactionValueorAmount varchar(10) DEFAULT '', 
					ValueDate datetime, 
					TransactionEffectiveValueDate datetime , -- ***
					ParentID int, 
					RedemptionID int, 
					Price float, 
					TransactionEffectivePrice float , -- ***
					TransactionEffectiveWatermark float , -- ***
					TransactionIsTransfer bit , -- ***
					TransactionSpecificInitialEqualisationFlag bit , -- ***
					TransactionSpecificInitialEqualisationValue float , -- ***
					SignedUnits float, 
					SignedSettlement float, 
 					TransactionSpecificRedemption float DEFAULT 0, 
				  RunningTotalSubscriptions float DEFAULT 0, 
 					RunningTotalRedemptions float DEFAULT 0, 
					TotalSubscriptions float, 
					TotalRedemptions float, 
					TotalSubscriptionConsideration float, 
					TotalRedemptionProceeds float)

--DECLARE @TempEqualisationTotals TABLE (TempET_Counterparty int, TempET_Fund int, TempET_Instrument int, 
--					TempET_SumOfContingentRed float, TempET_SumOFCurrentEqu float, 
--					TempET_SumOfContingentRedExRedemption float, TempET_SumOFCurrentEquExRedemption float, 
--					PRIMARY KEY (TempET_Counterparty, TempET_Fund, TempET_Instrument))

DECLARE @InvestorGroupCounterparties TABLE (CounterpartyID int)

DECLARE @SubscribeTransactionType int
DECLARE @RedeemTransactionType int

-- Venice calculates NAV and Subscriptions are entered at NAV. But this may change, so allow for this.

DECLARE @SubsPriceIsGNAV int		-- Is the price at which subscriptions to our funds are entered into venice the NAV or GNAV ?
DECLARE @FundPriceIsGNAV int		-- Is the calculated fund price the NAV or GNAV ? (i.e. Are the Performance fees entered as accruals into Venice ?)

SELECT @SubsPriceIsGNAV = 1		-- Indicates wether Subscription prices in Venice should be taken to be at GNAV
SELECT @FundPriceIsGNAV = 0		-- Specified in the call to fn_UnitPrice

-- Retrieve Subscribe and Redeem Transaction Types.

SELECT @SubscribeTransactionType = TransactionTypeID
	FROM fn_tblTransactionType_SelectKD(@Knowledgedate) WHERE TransactionType = 'SUBSCRIBE'
SELECT @RedeemTransactionType = TransactionTypeID
	FROM fn_tblTransactionType_SelectKD(@Knowledgedate) WHERE TransactionType = 'REDEEM'

-- Initialise @InvestorGroupCounterparties table if necessary

IF ISNULL(@InvestorGroup, '') = ''
  BEGIN
	INSERT @InvestorGroupCounterparties(CounterpartyID)
	SELECT 0
  END
ELSE
  BEGIN
	INSERT @InvestorGroupCounterparties(CounterpartyID)
	SELECT CounterpartyID
	FROM fn_tblCounterparty_SelectKD(@Knowledgedate)
	WHERE CounterpartyInvestorGroup = @InvestorGroup
  END


-- Retrieve Subscribe and Redeem Transactions :-
--	All Funds if @FundID = Null or 0, else Selected FundID
--	All Valuedates if @ValueDate = Null or <= '1 Jan 1900', else for Valuedates <= @ValueDate
-- FundID and ValueDate selection parameters are cumulative.

-- Gather 'Real' Transactions
INSERT @Transactions(
		PendingTransaction, 
		Counterparty, 
		Fund, 
		Instrument, 
		TransactionType, 
		TransactionValueorAmount,
		ValueDate, 
		TransactionEffectiveValueDate, 
		ParentID, 
		RedemptionID, 
		Price, 
		TransactionEffectivePrice, 
		TransactionEffectiveWatermark, 
		TransactionIsTransfer, 
		TransactionSpecificInitialEqualisationFlag, 
		TransactionSpecificInitialEqualisationValue, 
		SignedUnits, 
		SignedSettlement)
	SELECT     
		0, 
		TransactionCounterparty, 
		TransactionFund, 
		TransactionInstrument, 
		TransactionType, 
		TransactionValueorAmount, 
		TransactionValueDate, 
		TransactionEffectiveValueDate, 
		TransactionParentID, 
		TransactionRedemptionID, 
    TransactionPrice, 
		TransactionEffectivePrice, 
		TransactionEffectiveWatermark, 
		TransactionIsTransfer, 
		TransactionSpecificInitialEqualisationFlag, 
		TransactionSpecificInitialEqualisationValue, 
		-(TransactionSignedUnits) AS TransactionSignedUnits, 
    -(TransactionSignedSettlement) AS TransactionSignedSettlement
	FROM         fn_tblTransaction_SelectKD(@Knowledgedate)
	WHERE     
			(TransactionType IN (@SubscribeTransactionType, @RedeemTransactionType)) AND
		  ((TransactionFund = ISNULL(@FundID, 0)) OR (ISNULL(@FundID, 0) = 0)) AND
		  ((TransactionCounterparty = ISNULL(@CounterpartyID, 0)) OR (ISNULL(@CounterpartyID, 0) = 0)) AND
		  ((TransactionCounterparty  IN (SELECT CounterpartyID FROM @InvestorGroupCounterparties)) OR (ISNULL(@InvestorGroup, 0) = 0)) AND
		  ((TransactionValueDate <= ISNULL(@ValueDate, '1 Jan 1900')) OR (ISNULL(@ValueDate, '1 Jan 1900') <= '1 Jan 1900'))
	ORDER BY TransactionCounterparty, TransactionFund, TransactionInstrument, TransactionValueDate, TransactionParentID

-- Gather 'Pending' Transactions
INSERT @Transactions(
		PendingTransaction, 
		Counterparty, 
		Fund, 
		Instrument, 
		TransactionType, 
		TransactionValueorAmount, 
		ValueDate,
		TransactionEffectiveValueDate,  
		ParentID, 
		RedemptionID, 
		Price, 
		TransactionEffectivePrice, 
		TransactionEffectiveWatermark, 
		TransactionIsTransfer, 
		TransactionSpecificInitialEqualisationFlag, 
		TransactionSpecificInitialEqualisationValue, 
		SignedUnits, 
		SignedSettlement)
	SELECT     
		(-1), 
		TransactionCounterparty, 
		TransactionFund, 
		TransactionInstrument, 
		TransactionType, 
		TransactionValueorAmount, 
		TransactionValueDate,
		TransactionEffectiveValueDate,  
		TransactionParentID, 
		0, 
    TransactionPrice, 
		TransactionEffectivePrice, 
		TransactionEffectiveWatermark, 
		TransactionIsTransfer, 
		TransactionSpecificInitialEqualisationFlag, 
		TransactionSpecificInitialEqualisationValue, 
		-(TransactionSignedUnits) AS TransactionSignedUnits, 
    -(TransactionSignedSettlement) AS TransactionSignedSettlement
	FROM         fn_tblTransaction_SelectKD(@Knowledgedate)
	WHERE     (TransactionType IN (@SubscribeTransactionType, @RedeemTransactionType)) AND
		  ((TransactionFund = ISNULL(@FundID, 0)) OR (ISNULL(@FundID, 0) = 0)) AND
		  ((TransactionCounterparty = ISNULL(@CounterpartyID, 0)) OR (ISNULL(@CounterpartyID, 0) = 0)) AND
		  ((TransactionCounterparty  IN (SELECT CounterpartyID FROM @InvestorGroupCounterparties)) OR (ISNULL(@InvestorGroup, 0) = 0)) AND
		  ((TransactionValueDate > ISNULL(@ValueDate, '1 Jan 1900')) AND (ISNULL(@ValueDate, '1 Jan 1900') > '1 Jan 1900'))
	ORDER BY TransactionCounterparty, TransactionFund, TransactionInstrument, TransactionValueDate, TransactionParentID

-- Update @Transactions to account for Redemptions against specific Subscriptions.
-- For the affected Redemption Transactions, The 'SpecificRedemptionSignedUnits' field will be set and the 'SignedUnits' field cleared.
-- this is done so that the first pass population of the @TempEqualisation table includes the Specific Redemption transactions, but they will have had
-- no impact.
-- The Specific Redemption Transactions will then be added in through a second phase of processing

UPDATE @Transactions
SET SpecificRedemptionSignedUnits = SignedUnits, SignedUnits = 0
WHERE (RedemptionID > 0) AND (PendingTransaction = 0) AND (TransactionType = @RedeemTransactionType) 

-- Calculate Aggregated Subscription and redemption totals.
-- Not including Pending Transactions.

Insert @AggregatedTotals
	SELECT     Counterparty, Fund, Instrument, TransactionType, 
                   Sum(SignedUnits), Sum(SignedSettlement)
	FROM       @Transactions
	WHERE      (PendingTransaction = 0)
	GROUP BY   Counterparty, Fund, Instrument, TransactionType

-- Return values :-
-- Selected transactions with Running Subscription / Redemption totals and Total Redemption figures.
-- Ignores Pending transactions with regard to running totals etc.

INSERT @TempEqualisation(TransactionRN, PendingTransaction, 
		Counterparty, Fund, Instrument, TransactionType, TransactionValueorAmount, 
		ValueDate, 
		TransactionEffectiveValueDate, 
		ParentID, 
		RedemptionID, 
		Price, 
		TransactionEffectivePrice, 
		TransactionEffectiveWatermark, 
		TransactionIsTransfer, 
		TransactionSpecificInitialEqualisationFlag, 
		TransactionSpecificInitialEqualisationValue, 
		SignedUnits, 
		SignedSettlement, 
		RunningTotalSubscriptions, 
		RunningTotalRedemptions,
		TotalSubscriptions,
		TotalRedemptions,
		TotalSubscriptionConsideration,
		TotalRedemptionProceeds)

SELECT RN, PendingTransaction, Counterparty, Fund, Instrument, TransactionType, TransactionValueorAmount, 
		ValueDate, 
		TransactionEffectiveValueDate, 
		ParentID, 
		RedemptionID, 
		Price, 
		TransactionEffectivePrice, 
		TransactionEffectiveWatermark, 
		TransactionIsTransfer, 
		TransactionSpecificInitialEqualisationFlag, 
		TransactionSpecificInitialEqualisationValue, 
		SignedUnits, 
		SignedSettlement, 
	(SELECT ISNULL(SUM(b.SignedUnits), 0)
		FROM @Transactions as b 
		WHERE (b.Counterparty = a.Counterparty) AND 
                  (b.Fund = a.Fund) AND 
                  (b.Instrument = a.Instrument) AND 
                  (b.TransactionType = @SubscribeTransactionType) AND 
                  (b.PendingTransaction = 0) AND 
                  (b.RN <= a.RN) 
	) AS RunningTotalSubscriptions, 
	(SELECT ISNULL(SUM(b.SignedUnits), 0)
		FROM @Transactions as b 
		WHERE (b.Counterparty = a.Counterparty) AND 
                  (b.Fund = a.Fund) AND 
                  (b.Instrument = a.Instrument) AND 
                  (b.TransactionType = @RedeemTransactionType) AND 
                  (b.PendingTransaction = 0) AND 
                  (b.RN <= a.RN) 
	) AS RunningTotalRedemptions, 
	(SELECT ISNULL(SUM(b.SumSignedUnits) , 0)
		FROM @AggregatedTotals as b 
  		WHERE (b.Counterparty = a.Counterparty) AND 
                  (b.Fund = a.Fund) AND 
                  (b.Instrument = a.Instrument) AND 
                  (b.TransactionType = @SubscribeTransactionType)
	) AS TotalSubscriptions, 
	(SELECT ISNULL(SUM(b.SumSignedUnits) , 0)
   		FROM @AggregatedTotals as b 
  		WHERE (b.Counterparty = a.Counterparty) AND 
                  (b.Fund = a.Fund) AND 
                  (b.Instrument = a.Instrument) AND 
                  (b.TransactionType = @RedeemTransactionType)
	) AS TotalRedemptions, 
	(SELECT ISNULL(SUM(b.SumSignedSettlement) , 0)
		FROM @AggregatedTotals as b 
  		WHERE (b.Counterparty = a.Counterparty) AND 
                  (b.Fund = a.Fund) AND 
                  (b.Instrument = a.Instrument) AND 
                  (b.TransactionType = @SubscribeTransactionType)
	) AS TotalSubscriptionConsideration, 
	(SELECT ISNULL(SUM(b.SumSignedSettlement) , 0)
   		FROM @AggregatedTotals as b 
  		WHERE (b.Counterparty = a.Counterparty) AND 
                  (b.Fund = a.Fund) AND 
                  (b.Instrument = a.Instrument) AND 
                  (b.TransactionType = @RedeemTransactionType)
	) AS TotalRedemptionProceeds
FROM @Transactions as a
ORDER BY a.RN
--ORDER BY a.Counterparty, a.Fund, a.Instrument, a.ValueDate, a.RN

-- Second Pass processing to Account for specific Redemption transactions.

DECLARE @SpecificRedemptionRN int
DECLARE @SubscriptionRN int
DECLARE @SpecificRedemptionID int
DECLARE @SpecificCounterparty int
DECLARE @SpecificFund int
DECLARE @SpecificInstrument int
DECLARE @SpecificSignedUnits float

-- Declare a cursor to cycle through the Specific Redemption transactions.

DECLARE csrSpecificRedemptions CURSOR
FOR 
SELECT	RN, RedemptionID, Counterparty, Fund, Instrument, SpecificRedemptionSignedUnits
FROM   @Transactions
WHERE (RedemptionID > 0) AND (PendingTransaction = 0) AND (TransactionType = @RedeemTransactionType) 
ORDER BY RN

-- Cycle through....
	
OPEN csrSpecificRedemptions

FETCH NEXT FROM csrSpecificRedemptions 
INTO @SpecificRedemptionRN, @SpecificRedemptionID, @SpecificCounterparty, @SpecificFund, @SpecificInstrument, @SpecificSignedUnits

WHILE @@fetch_status = 0
BEGIN

	-- Get RN of Supscription to redeem from
	
	SELECT @SubscriptionRN = ISNULL(RN, 0)
	FROM @TempEqualisation
	WHERE ParentID = @SpecificRedemptionID
	
	IF ISNULL(@SubscriptionRN, 0) <= 0
		BEGIN
			-- No Matching Subscription Found, Patch this Redemption in just like a normal one...

			SELECT @SubscriptionRN = ISNULL(RN, 0)
			FROM @TempEqualisation
			WHERE TransactionRN = @SpecificRedemptionRN
			
			UPDATE @TempEqualisation
			SET RunningTotalRedemptions = RunningTotalRedemptions + @SpecificSignedUnits
			WHERE (RN >= @SubscriptionRN) AND (Counterparty = @SpecificCounterparty) AND (Fund = @SpecificFund) AND (Instrument = @SpecificInstrument)
			
			-- Update Totals
			UPDATE @TempEqualisation
			SET TotalRedemptions = TotalRedemptions + @SpecificSignedUnits
			WHERE (Counterparty = @SpecificCounterparty) AND (Fund = @SpecificFund) AND (Instrument = @SpecificInstrument)
			
		END
	ELSE
		BEGIN
			-- Process Specific Redemption.

			-- Update Specific Subscription Record
			UPDATE @TempEqualisation
			SET TransactionSpecificRedemption = @SpecificSignedUnits
			WHERE (RN = @SubscriptionRN)

			-- Update Running totals and Totals.
			-- Note, TotalRedemption column is no longer the same for all records relating to a given Fund/Instrument/Client.
			-- 
			UPDATE @TempEqualisation
			SET RunningTotalRedemptions = RunningTotalRedemptions + @SpecificSignedUnits, 
					TotalRedemptions = TotalRedemptions + @SpecificSignedUnits
			WHERE (RN >= @SubscriptionRN) AND (Counterparty = @SpecificCounterparty) AND (Fund = @SpecificFund) AND (Instrument = @SpecificInstrument)
	
		END
		
	-- Ensure Redemptions do not exceed Subscriptions
	-- This should not occur. (?)
	UPDATE @TempEqualisation
	SET RunningTotalRedemptions = (-RunningTotalSubscriptions)
	WHERE (ABS(RunningTotalRedemptions) > RunningTotalSubscriptions)
	
	-- Restore Units Field for Specific Redemption transactions
	UPDATE @TempEqualisation
	SET SignedUnits = @SpecificSignedUnits
	WHERE (TransactionRN = @SpecificRedemptionRN)

	FETCH NEXT FROM csrSpecificRedemptions 
	INTO @SpecificRedemptionRN, @SpecificRedemptionID, @SpecificCounterparty, @SpecificFund, @SpecificInstrument, @SpecificSignedUnits

END


-- Tag the unit price and watermark on to the Temporary Equalisation Table.

INSERT @EqualisationTransactions(PendingTransaction, Counterparty, CounterpartyName, Fund, FundName, FundBaseCurrency, FundBaseCurrencyName, 
				Instrument, InstrumentDescription, InstrumentCurrencyID, TransactionType, TransactionValueorAmount, TypeDescription, 
				ValueDate, 
				TransactionEffectiveValueDate, 
				ParentID, RedemptionID, 
				Price, 
				TransactionEffectivePrice, 
				SignedUnits, SpecificRedemptionSignedUnits, SignedSettlement, 
				RunningTotalSubscriptions, 
				RunningTotalRedemptions,
				TotalSubscriptions,
				TotalRedemptions,
				TotalSubscriptionConsideration,
				TotalRedemptionProceeds,
				GNAV_Price, NAV_Price, 
				Watermark, 
				TransactionEffectiveWatermark, 
				WatermarkDate, 
				PerformanceFeesPercent, 
				TransactionIsTransfer, 
				TransactionSpecificInitialEqualisationFlag, 
				TransactionSpecificInitialEqualisationValue
				)
SELECT  TempEq.PendingTransaction, TempEq.Counterparty, tblCounterparty.CounterpartyName, 
				TempEq.Fund, tblFund.FundName, tblFund.FundBaseCurrency, FundCcy.InstrumentDescription, 
				TempEq.Instrument, tblInstrument.InstrumentDescription,  tblInstrument.InstrumentCurrencyID, 
        TempEq.TransactionType, TempEq.TransactionValueorAmount, tblTransactionType.TransactionType, 
        TempEq.ValueDate, 
        TempEq.TransactionEffectiveValueDate, 
        TempEq.ParentID, TempEq.RedemptionID, 
        TempEq.Price, 
        TempEq.TransactionEffectivePrice, 
        TempEq.SignedUnits, TempEq.TransactionSpecificRedemption, TempEq.SignedSettlement, 
				ISNULL(TempEq.RunningTotalSubscriptions, 0) , 
				ISNULL(TempEq.RunningTotalRedemptions, 0) , 
				ISNULL(TempEq.TotalSubscriptions, 0) , 
				ISNULL(TempEq.TotalRedemptions, 0) , 
				ISNULL(TempEq.TotalSubscriptionConsideration, 0) , 
				ISNULL(TempEq.TotalRedemptionProceeds, 0) , 	
				ISNULL(GNAVPrice.UnitPrice, 0), 
				ISNULL(NAVPrice.UnitPrice, 0), 
				100, 
				TransactionEffectiveWatermark, 
				'1 Jan 1900', -- ISNULL(Watermark.PriceLevel, 100), ISNULL(Watermark.WatermarkDate, '1 Jan 1900'), 
				tblFund.FundPerformanceFees, 
				TempEq.TransactionIsTransfer, 
				TempEq.TransactionSpecificInitialEqualisationFlag, 
				TempEq.TransactionSpecificInitialEqualisationValue	
FROM 	@TempEqualisation TempEq LEFT OUTER JOIN
				fn_UnitPrice(@FundID, @ValueDate, 3, 0, @KnowledgeDate) GNAVPrice ON (TempEq.Fund = GNAVPrice.Fund) LEFT OUTER JOIN
				fn_UnitPrice(@FundID, @ValueDate, 4, 0, @KnowledgeDate) NAVPrice ON (TempEq.Fund = NAVPrice.Fund) LEFT OUTER JOIN
				-- fn_BestFundWatermark(@ValueDate, @KnowledgeDate) Watermark ON (TempEq.Fund = Watermark.Fund) LEFT OUTER JOIN
				fn_tblFund_SelectKD(@KnowledgeDate) tblFund ON (TempEq.Fund = tblFund.FundID) LEFT OUTER JOIN
				fn_tblTransactionType_SelectKD(@KnowledgeDate) tblTransactionType ON (TempEq.TransactionType = tblTransactionType.TransactionTypeID) LEFT OUTER JOIN
				fn_tblCounterparty_SelectKD(@KnowledgeDate) tblCounterparty ON (TempEq.Counterparty = tblCounterparty.CounterpartyID) LEFT OUTER JOIN
				fn_tblInstrument_SelectKD(@KnowledgeDate) tblInstrument ON (TempEq.Instrument = tblInstrument.InstrumentID) LEFT OUTER JOIN
				fn_tblInstrument_SelectKD(@KnowledgeDate) FundCcy ON (tblFund.FundBaseCurrency = FundCcy.InstrumentID)
ORDER BY TempEq.Counterparty, TempEq.Fund, TempEq.Instrument, TempEq.TransactionType, TempEq.ValueDate, TempEq.RN


-- Calculate Redemption Ratios

-- Calculate Partial redemptions
UPDATE @EqualisationTransactions
SET  RedemptionRatio = (Abs(RunningTotalSubscriptions) - Abs(TotalRedemptions)) / Abs(SignedUnits)
WHERE (Abs(TotalRedemptions) <= Abs(RunningTotalSubscriptions)) AND (Abs(RunningTotalSubscriptions) - Abs(TotalRedemptions)) < Abs(SignedUnits)

-- Update Redemption Ratios for Specific Redemptions.
UPDATE @EqualisationTransactions
SET  RedemptionRatio = (Abs(SignedUnits) - Abs(SpecificRedemptionSignedUnits)) / Abs(SignedUnits)
WHERE (Abs(SpecificRedemptionSignedUnits) > 0) AND (TransactionType = @SubscribeTransactionType) AND (RedemptionRatio > ((Abs(SignedUnits) - Abs(SpecificRedemptionSignedUnits)) / Abs(SignedUnits)))

-- Pending transactions marked as fully redeemed.
UPDATE @EqualisationTransactions
SET  RedemptionRatio = 0
WHERE (PendingTransaction <> 0)

-- Mark Fully Redeemed Transactions and REDEEM transactions.
UPDATE @EqualisationTransactions
SET  RedemptionRatio = 0
WHERE (Abs(TotalRedemptions) >= Abs(RunningTotalSubscriptions)) OR
	(TransactionType = @RedeemTransactionType)

-- Calculate Incentive Fees and Equalisation Factors

-- Update @EqualisationTransactions with Performance and Equalisation details
-- 

--UPDATE @EqualisationTransactions
----SET InitialEqualisationFactor = SignedUnits * [dbo].[fn_CalcInitialEqualisationFactor](Fund, Watermark, WatermarkDate, Price, ValueDate, @SubsPriceIsGNAV, GNAV_Price, @FundPriceIsGNAV, PerformanceFeesPercent), 
----    CurrentEqualisationFactor = SignedUnits * [dbo].[fn_CalcCurrentEqualisationFactor](Fund, Watermark, WatermarkDate, Price, ValueDate, @SubsPriceIsGNAV, GNAV_Price, @FundPriceIsGNAV, PerformanceFeesPercent)
--SET InitialEqualisationFactor = SignedUnits * [dbo].[fn_CalcInitialEqualisationFactor](Fund, Watermark, WatermarkDate, Price, ValueDate, @SubsPriceIsGNAV, NAV_Price, 0, PerformanceFeesPercent), 
--    CurrentEqualisationFactor = SignedUnits * [dbo].[fn_CalcCurrentEqualisationFactor](Fund, Watermark, WatermarkDate, Price, ValueDate, @SubsPriceIsGNAV, NAV_Price, 0, PerformanceFeesPercent)
--WHERE 	(TransactionType = @SubscribeTransactionType) AND
--	(PendingTransaction = 0)

--UPDATE @EqualisationTransactions
----SET AccIncentiveFee = SignedUnits * [dbo].[fn_CalcIncentiveFee](InitialEqualisationFactor, Fund, Watermark, WatermarkDate, Price, ValueDate, @SubsPriceIsGNAV, NAV_Price, @FundPriceIsGNAV, PerformanceFeesPercent), 
----    ContingentRedemptionFactor = SignedUnits * [dbo].[fn_CalcContingentRedemptionFactor](Fund, Watermark, WatermarkDate, Price, ValueDate, @SubsPriceIsGNAV, NAV_Price, @FundPriceIsGNAV, PerformanceFeesPercent)
--SET AccIncentiveFee = SignedUnits * [dbo].[fn_CalcIncentiveFee](InitialEqualisationFactor, Fund, Watermark, WatermarkDate, Price, ValueDate, @SubsPriceIsGNAV, NAV_Price, 0, PerformanceFeesPercent), 
--    ContingentRedemptionFactor = SignedUnits * [dbo].[fn_CalcContingentRedemptionFactor](Fund, Watermark, WatermarkDate, Price, ValueDate, @SubsPriceIsGNAV, NAV_Price, 0, PerformanceFeesPercent)
--WHERE 	(TransactionType = @SubscribeTransactionType) AND
--	(PendingTransaction = 0)

--INSERT @TempEqualisationTotals (TempET_Counterparty, TempET_Fund, TempET_Instrument, 
--	TempET_SumOfContingentRed, TempET_SumOFCurrentEqu, TempET_SumOfContingentRedExRedemption, TempET_SumOFCurrentEquExRedemption)
--SELECT Counterparty, Fund, Instrument, 
--	Sum(ContingentRedemptionFactor * RedemptionRatio), Sum(CurrentEqualisationFactor * RedemptionRatio),
--	Sum(ContingentRedemptionFactor), Sum(CurrentEqualisationFactor)
--FROM @EqualisationTransactions
--GROUP BY Counterparty, Fund, Instrument

--UPDATE @EqualisationTransactions
--SET SumOfContingentRedemption = (SELECT (b.TempET_SumOfContingentRed) FROM @TempEqualisationTotals b WHERE ((b.TempET_Fund = Fund) AND (b.TempET_Instrument = Instrument) AND (b.TempET_Counterparty = Counterparty))),
--    SumOFCurrentEqualisation = (SELECT (b.TempET_SumOFCurrentEqu) FROM @TempEqualisationTotals b WHERE ((b.TempET_Fund = Fund) AND (b.TempET_Instrument = Instrument) AND (b.TempET_Counterparty = Counterparty))),  
--    SumOfContingentRedemptionExRedemption = (SELECT (b.TempET_SumOfContingentRedExRedemption) FROM @TempEqualisationTotals b WHERE ((b.TempET_Fund = Fund) AND (b.TempET_Instrument = Instrument) AND (b.TempET_Counterparty = Counterparty))),
--    SumOFCurrentEqualisationExRedemption = (SELECT (b.TempET_SumOFCurrentEquExRedemption) FROM @TempEqualisationTotals b WHERE ((b.TempET_Fund = Fund) AND (b.TempET_Instrument = Instrument) AND (b.TempET_Counterparty = Counterparty)))

---- SumOfContingentRedemptionExRedemption


RETURN
END




GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  SELECT  ON [dbo].[fn_BasicEqualisationCalcs]  TO [InvestMaster_Sales]
GO

GRANT  SELECT  ON [dbo].[fn_BasicEqualisationCalcs]  TO [InvestMaster_Manager]
GO

GRANT  SELECT  ON [dbo].[fn_BasicEqualisationCalcs]  TO [InvestMaster_Admin]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[spu_BasicEqualisationCalcs]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[spu_BasicEqualisationCalcs]
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE PROCEDURE dbo.spu_BasicEqualisationCalcs
(
	@FundID int,
	@CounterpartyID int,
	@InvestorGroup int,
	@ValueDate datetime,
	@KnowledgeDate datetime

)
AS

SELECT    
						spu_EqualisationCalcs.PendingTransaction,
						spu_EqualisationCalcs.Counterparty,
						spu_EqualisationCalcs.CounterpartyName,
						spu_EqualisationCalcs.Fund,
						spu_EqualisationCalcs.FundName,
						spu_EqualisationCalcs.FundBaseCurrency,
						spu_EqualisationCalcs.FundBaseCurrencyName,
						spu_EqualisationCalcs.Instrument,
						spu_EqualisationCalcs.InstrumentDescription,
						spu_EqualisationCalcs.InstrumentCurrencyID,
						spu_EqualisationCalcs.TransactionType,
						spu_EqualisationCalcs.TransactionValueorAmount,
						spu_EqualisationCalcs.TypeDescription,
						spu_EqualisationCalcs.ValueDate,
						spu_EqualisationCalcs.TransactionEffectiveValueDate,
						spu_EqualisationCalcs.ParentID,
						spu_EqualisationCalcs.RedemptionID,
						spu_EqualisationCalcs.Price,
						spu_EqualisationCalcs.TransactionEffectivePrice,
						spu_EqualisationCalcs.SignedUnits,
						spu_EqualisationCalcs.SignedSettlement,
 						spu_EqualisationCalcs.RunningTotalSubscriptions,
 						spu_EqualisationCalcs.RunningTotalRedemptions,
						spu_EqualisationCalcs.SpecificRedemptionSignedUnits,
						spu_EqualisationCalcs.TotalSubscriptions,
						spu_EqualisationCalcs.TotalRedemptions,
						spu_EqualisationCalcs.TotalSubscriptionConsideration,
						spu_EqualisationCalcs.TotalRedemptionProceeds,
						spu_EqualisationCalcs.GNAV_Price,
						spu_EqualisationCalcs.NAV_Price,
						spu_EqualisationCalcs.Watermark,
						spu_EqualisationCalcs.TransactionEffectiveWatermark,
						spu_EqualisationCalcs.WatermarkDate,
						spu_EqualisationCalcs.PerformanceFeesPercent,
						spu_EqualisationCalcs.TransactionIsTransfer,
						spu_EqualisationCalcs.TransactionSpecificInitialEqualisationFlag,
						spu_EqualisationCalcs.TransactionSpecificInitialEqualisationValue,
						spu_EqualisationCalcs.RedemptionRatio,
 						spu_EqualisationCalcs.AccIncentiveFee,
						spu_EqualisationCalcs.ContingentRedemptionFactor,
						spu_EqualisationCalcs.InitialEqualisationFactor,
						spu_EqualisationCalcs.CurrentEqualisationFactor,
						spu_EqualisationCalcs.SumOfContingentRedemption,
						spu_EqualisationCalcs.SumOFCurrentEqualisation,
						spu_EqualisationCalcs.SumOfContingentRedemptionExRedemption,
						spu_EqualisationCalcs.SumOFCurrentEqualisationExRedemption,
						Counterparties.CounterpartyName, Counterparties.CounterpartyPFPCID, Counterparties.CounterpartyInvestorGroup, 
            Counterparties.CounterpartyManagementFee, Counterparties.CounterpartyPerformanceFee, Counterparties.CounterpartyPerformanceFeeThreshold, 
            Counterparties.CounterpartyFundID, Counterparties.CounterpartyLeverageProvider, Counterparties.CounterpartyAddress2, 
            Counterparties.CounterpartyAddress1, Counterparties.CounterpartyAddress3, Counterparties.CounterpartyAddress4, 
            Counterparties.CounterpartyAddress5, Counterparties.CounterpartyPostCode, Counterparties.CounterpartyContactName, 
            Counterparties.CounterpartyAccountName, Counterparties.CounterpartyEMail, Counterparties.CounterpartyPhoneNumber, 
            Counterparties.CounterpartyFaxNumber, Counterparties.CounterpartyUser, Counterparties.CounterpartyDateEntered, 
            Counterparties.CounterpartyDateDeleted, InvestorGroups.IGName, 
            BestFX.FXRate AS Instrument_FXtoUSD
FROM        fn_BasicEqualisationCalcs(@FundID, @CounterpartyID, @InvestorGroup, @ValueDate, @KnowledgeDate) spu_EqualisationCalcs LEFT OUTER JOIN
            fn_tblCounterparty_SelectKD(@KnowledgeDate) Counterparties ON 
            spu_EqualisationCalcs.Counterparty = Counterparties.CounterpartyID LEFT OUTER JOIN
            fn_tblInvestorGroup_SelectKD(@KnowledgeDate) InvestorGroups ON Counterparties.CounterpartyInvestorGroup = InvestorGroups.IGID LEFT OUTER JOIN
            fn_BestFX(@ValueDate, @KnowledgeDate) BestFX ON (BestFX.FXCurrencyCode = spu_EqualisationCalcs.InstrumentCurrencyID)
WHERE     (spu_EqualisationCalcs.SignedUnits <> 0)
ORDER BY spu_EqualisationCalcs.Counterparty, spu_EqualisationCalcs.Fund, spu_EqualisationCalcs.Instrument

RETURN




GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[spu_BasicEqualisationCalcs]  TO [InvestMaster_Sales]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[fn_tblInformation_Details]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[fn_tblInformation_Details]
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

CREATE FUNCTION dbo.fn_tblInformation_Details
	(
	)
RETURNS 
 @tblInformation TABLE 
			(
			[ID] int PRIMARY KEY, 
			[Mastername] varchar(80), 
			[DataVendorID] varchar(80)
			) 
AS
	BEGIN

		INSERT INTO @tblInformation([ID], [Mastername], [DataVendorID])
		SELECT 
			[ID] ,
			[DataVendorName] ,
			[DataVendorID] 
		FROM 
			[MASTERSQL].[dbo].[Information]
		UNION ALL
		SELECT 
			([InstrumentID] + 1048576) AS ID,
			[DataVendorName] ,
			[DataVendorID] 
		FROM 
			[MASTERSQL].[dbo].[tblInstrumentInformation]


	RETURN
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  SELECT  ON [dbo].[fn_tblInformation_Details]  TO [InvestMaster_Sales]
GO

GRANT  SELECT  ON [dbo].[fn_tblInformation_Details]  TO [Genoa_Read]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[fn_tblDataDictionary_TransactionFormLabels_SelectKD]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[fn_tblDataDictionary_TransactionFormLabels_SelectKD]
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO




CREATE FUNCTION dbo.fn_tblDataDictionary_TransactionFormLabels_SelectKD
(
@KnowledgeDate datetime -- Not Used
)
RETURNS TABLE
AS
RETURN 
(
SELECT [RN],
	[AuditID] ,
	[TransactionType], 
	[CounterpartyText], 
	[CompoundFlag], 
	[TransactionAmount], 
	[TransactionPrice], 
	[TransactionCosts], 
	[TransactionSettlement], 
	[DecisionDateText], 
	[TradeDateText], 
	[SettlementDateText], 
	[ConfirmationDateText], 
	[FieldText1], 
	[FieldText2], 
	[FieldText3], 
	[FieldText4], 
	[FieldText5], 
	[FieldText6]
FROM tblDataDictionary_TransactionFormLabels
)





GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  SELECT  ON [dbo].[fn_tblDataDictionary_TransactionFormLabels_SelectKD]  TO [InvestMaster_Sales]
GO

GRANT  SELECT  ON [dbo].[fn_tblDataDictionary_TransactionFormLabels_SelectKD]  TO [InvestMaster_Admin]
GO

