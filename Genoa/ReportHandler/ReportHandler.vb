Option Explicit On

Imports System.IO
Imports System.Reflection
Imports System.Data.SqlClient
Imports C1.C1Report
Imports RenaissanceGlobals
Imports RenaissanceGlobals.Globals
Imports RenaissanceStatFunctions
Imports RenaissanceUtilities.DatePeriodFunctions


Public Class classReportFieldValue
  Private _SystemString As String
  Private _ReportFieldName As String
  Private _ReportPropertyName As String
  Private _ReportFieldValue As String

  Private Sub New()
  End Sub

  Public Sub New(ByVal pFieldName As String, ByVal PFieldValue As String)
    Me._SystemString = ""
    Me._ReportFieldName = pFieldName
    Me._ReportFieldValue = PFieldValue
    Me._ReportPropertyName = "Text"
  End Sub

  Public Sub New(ByVal pFieldName As String, ByVal PFieldValue As String, ByVal pPropertyValue As String)
    Me._SystemString = ""
    Me._ReportFieldName = pFieldName
    Me._ReportFieldValue = PFieldValue
    Me._ReportPropertyName = pPropertyValue
  End Sub

  Public Sub New(ByVal pSystemString As String, ByVal pFieldName As String, ByVal PFieldValue As String, ByVal pPropertyValue As String)
    Me._SystemString = pSystemString
    Me._ReportFieldName = pFieldName
    Me._ReportFieldValue = PFieldValue
    Me._ReportPropertyName = pPropertyValue
  End Sub

  Public ReadOnly Property SystemString() As String
    Get
      Return _SystemString
    End Get
  End Property

  Public ReadOnly Property ReportFieldName() As String
    Get
      Return _ReportFieldName
    End Get
  End Property

  Public ReadOnly Property ReportFieldValue() As String
    Get
      Return _ReportFieldValue
    End Get
  End Property

  Public ReadOnly Property ReportPropertyName() As String
    Get
      Return _ReportPropertyName
    End Get
  End Property
End Class

Public Class ReportHandler
  Public Const REPORT_KnowledgeDate_FieldName As String = "Text_KnowledgeDate"
  Public Const REPORT_ValueDate_FieldName As String = "Text_ValueDate"

  ' Specialist table from which to run the 'Table of contents' report

  Private tblTableOfContents As DataTable = Nothing

#Region " Local Variable Declaration"

  Private WithEvents _MainForm As CTAMain

#End Region

#Region " Properties"

  Public ReadOnly Property MainForm() As CTAMain
    Get
      Return _MainForm
    End Get
  End Property


#End Region

#Region " Constructors"

  Private Sub New()

  End Sub

  Public Sub New(ByRef pMainForm As CTAMain)

    _MainForm = pMainForm

  End Sub

#End Region

#Region " Display Report / Get Report Definition functions"

  Sub DisplayReport(ByVal pFundID As Integer, ByVal pReportName As String, ByVal pRecordset As Object, ByRef pStatusBar As ToolStripProgressBar)
    ' ************************************************************************
    ' Display the given C1Report in using the given data source.
    '
    ' ************************************************************************

    Try
      DisplayReport(pReportName, pReportName, pFundID, pRecordset, pStatusBar)
    Catch ex As Exception
    End Try

  End Sub

  Sub DisplayReport(ByVal pReportName As String, ByVal pRecordset As Object, ByVal pFundID As Integer, ByVal pValueDate As Date, ByVal pKnowledgeDate As Date, ByRef pStatusBar As ToolStripProgressBar)
    ' ************************************************************************
    ' Display the given C1Report in using the given data source.
    ' Use the supplied Value Date and knowledgeDate
    ' ************************************************************************

    Try
      DisplayReport(pReportName, pReportName, pRecordset, pFundID, pValueDate, pKnowledgeDate, pStatusBar)
    Catch ex As Exception
    End Try

  End Sub

  Sub DisplayReport(ByVal pReportFile As String, ByVal pReportName As String, ByVal pFundID As Integer, ByRef pRecordset As Object, ByRef pStatusBar As ToolStripProgressBar)
    ' ************************************************************************
    ' Display the given C1Report in using the given data source.
    '
    ' ************************************************************************

    DisplayReport(pReportName, pReportName, pRecordset, pFundID, Now(), MainForm.Main_Knowledgedate, pStatusBar)
  End Sub

  Sub DisplayReport(ByVal pReportFile As String, ByVal pReportName As String, ByRef pRecordset As Object, ByVal pFundID As Integer, ByVal pValueDate As Date, ByVal pKnowledgeDate As Date, ByRef pStatusBar As ToolStripProgressBar)
    ' ************************************************************************
    ' Display the given C1Report in a frmViewReport form.
    '
    ' First, Get the report definition,
    ' Second, Apply the given data source to it.
    ' Third, Display in a preview window.
    '
    ' ************************************************************************

    Dim thisReport As C1Report

    ' Get report definition.

    thisReport = GetReportDefinition(pFundID, pReportFile, pReportName)

    If (thisReport Is Nothing) Then
      MainForm.LogError("ReportHandler", LOG_LEVELS.Error, "", "Failed to get definition for report '" & pReportName & "'", "", True)
      Exit Sub
    End If

    Call DisplayReport(thisReport, pRecordset, pValueDate, pKnowledgeDate, pStatusBar)

  End Sub

  Sub DisplayReport(ByVal pReportName As String, ByVal pRecordset As Object, ByVal pFundID As Integer, ByRef pFieldValues() As classReportFieldValue, ByRef pStatusBar As ToolStripProgressBar)
    ' ************************************************************************
    ' Display the given C1Report in a frmViewReport form.
    '
    ' First, Get the report definition,
    ' Second, Apply the given data source to it.
    ' Third, Display in a preview window.
    '
    ' ************************************************************************

    Dim thisReport As C1Report

    ' Get report definition.

    thisReport = GetReportDefinition(pFundID, pReportName, pReportName)

    If (thisReport Is Nothing) Then
      Exit Sub
    End If

    Call DisplayReport(thisReport, pRecordset, pFieldValues, pStatusBar)

  End Sub

  Public Sub DisplayReport(ByRef thisReport As C1Report, ByVal pRecordset As Object, ByVal pValueDate As Date, ByVal pKnowledgeDate As Date, ByRef pStatusBar As ToolStripProgressBar)
    ' ************************************************************************
    ' Display the given C1Report in a frmViewReport form.
    '
    ' First, Set ValueDate and KnowledgeDate fields.
    ' Second, Display in a preview window.
    '
    ' ************************************************************************


    If (thisReport Is Nothing) Then Exit Sub
    If (pRecordset Is Nothing) Then Exit Sub

    Dim KDString As String = ""
    Try
      If pKnowledgeDate.CompareTo(KNOWLEDGEDATE_NOW) <= 0 Then
        KDString = "Live"
      ElseIf pKnowledgeDate.TimeOfDay.TotalSeconds >= LAST_SECOND Then
        KDString = pKnowledgeDate.ToString(REPORT_DATEFORMAT)
      Else
        KDString = pKnowledgeDate.ToString(REPORT_LONGDATEFORMAT)
      End If
    Catch ex As Exception
    End Try

    Call DisplayReport(thisReport, pRecordset, New classReportFieldValue() {New classReportFieldValue(REPORT_ValueDate_FieldName, pValueDate.ToString(REPORT_DATEFORMAT)), New classReportFieldValue(REPORT_KnowledgeDate_FieldName, KDString)}, pStatusBar)

  End Sub

  Public Sub DisplayReport(ByRef thisReport As C1Report, ByRef pRecordset As Object, ByRef pFieldValues() As classReportFieldValue, ByRef pStatusBar As ToolStripProgressBar)
    ' ************************************************************************
    '
    '
    ' ************************************************************************

    Dim thisFieldValue As classReportFieldValue

    If (Not (pFieldValues Is Nothing)) AndAlso (pFieldValues.Length > 0) Then
      For Each thisFieldValue In pFieldValues
        Call SetFieldValue(thisReport, thisFieldValue)
      Next
    End If

    Try
      thisReport.DataSource.Recordset = pRecordset
    Catch ex As Exception
      MainForm.LogError("ReportHandler", LOG_LEVELS.Error, ex.Message, "Failed to set report datasource.", ex.StackTrace, True)
      Exit Sub
    End Try

    DisplayReport(thisReport, pStatusBar)

  End Sub

  Public Sub DisplayReport(ByRef pC1Report As C1Report, ByRef pStatusBar As ToolStripProgressBar)
    ' ************************************************************************
    ' Display the given C1Report in a frmViewReport form.
    '
    ' ************************************************************************

    Dim thisViewReportForm As frmViewReport

    ' Open new ViewForm object

    Try
      thisViewReportForm = CType(MainForm.New_CTAForm(CTAFormID.frmViewReport).Form, frmViewReport)
      thisViewReportForm.Opacity = 0
      thisViewReportForm.Text = pC1Report.ReportName
      MainForm.BuildWindowsMenu()
    Catch ex As Exception
      Exit Sub
    End Try

    If (thisViewReportForm Is Nothing) Then
      Exit Sub
    End If

    ' Render Report to Preview object

    Try
      Dim thisCursor As Cursor
      Dim OrgTitle As String

      OrgTitle = thisViewReportForm.Text
      thisViewReportForm.Text = "Rendering " & OrgTitle
      thisCursor = thisViewReportForm.Cursor
      thisViewReportForm.Cursor = Cursors.WaitCursor

      If (Not (pStatusBar Is Nothing)) Then
        AddHandler pC1Report.StartSection, AddressOf IncrementStatusBar
        pC1Report.Tag = pStatusBar
      End If

      thisViewReportForm.ReportPreview.Document = pC1Report.Document

      If (Not (pStatusBar Is Nothing)) Then
        pStatusBar.Visible = False
        pC1Report.Tag = Nothing
        RemoveHandler pC1Report.StartSection, AddressOf IncrementStatusBar
      End If

      thisViewReportForm.Cursor = thisCursor
      thisViewReportForm.Text = OrgTitle
    Catch ex As Exception
    End Try

    ' Show form (probably unnecessary as it is shown by default by the New_VeniceForm() routine)

    Try
      If Not (thisViewReportForm Is Nothing) Then
        thisViewReportForm.Show()
        'thisViewReportForm.Visible = True

        Dim OCount As Integer

        If (MainForm.EnableReportFadeIn) Then

          Try
            Dim TStart As Date = Now()
            Dim TEnd As Date

            For OCount = 0 To 100 Step 5
              thisViewReportForm.Opacity = OCount / 100
              Threading.Thread.Sleep(10)
            Next

            TEnd = Now()
            If (TEnd - TStart).TotalMilliseconds >= 1000 Then
              MainForm.DisableReportFadeIn()
            End If

          Catch ex As Exception
          End Try

        Else
          thisViewReportForm.Opacity = 1.0#
        End If

      End If
    Catch ex As Exception
    End Try

  End Sub

  Public Sub DisplayReport(ByVal pReportTitle As String, ByRef pPrintDocument As System.Drawing.Printing.PrintDocument, ByRef pStatusBar As ToolStripProgressBar)
    ' ************************************************************************
    ' Display the given PrintDocument in a frmViewReport form.
    '
    ' ************************************************************************

    Dim thisViewReportForm As frmViewReport

    ' Open new ViewForm object

    Try
      thisViewReportForm = CType(MainForm.New_CTAForm(CTAFormID.frmViewReport).Form, frmViewReport)
      thisViewReportForm.Opacity = 0
      thisViewReportForm.Text = pReportTitle
      MainForm.BuildWindowsMenu()
    Catch ex As Exception
      Exit Sub
    End Try

    If (thisViewReportForm Is Nothing) Then
      Exit Sub
    End If

    ' Render Report to Preview object

    Dim thisCursor As Cursor = Cursors.Default
    Dim OrgTitle As String = ""
    Try

      OrgTitle = thisViewReportForm.Text
      thisViewReportForm.Text = "Rendering " & OrgTitle
      thisCursor = thisViewReportForm.Cursor
      thisViewReportForm.Cursor = Cursors.WaitCursor

      thisViewReportForm.ReportPreview.Document = pPrintDocument

    Catch ex As Exception
    Finally
      thisViewReportForm.Cursor = thisCursor
      thisViewReportForm.Text = OrgTitle
    End Try

    ' Show form (probably unnecessary as it is shown by default by the New_VeniceForm() routine)

    Try
      If Not (thisViewReportForm Is Nothing) Then
        thisViewReportForm.Show()

        Dim OCount As Integer

        If MainForm.EnableReportFadeIn Then

          Try
            Dim TStart As Date = Now()
            Dim TEnd As Date

            For OCount = 0 To 100 Step 5
              thisViewReportForm.Opacity = OCount / 100
              Threading.Thread.Sleep(10)
            Next

            TEnd = Now()
            If (TEnd - TStart).TotalMilliseconds >= 1000 Then
              MainForm.DisableReportFadeIn()
            End If

          Catch ex As Exception
          End Try

        Else
          thisViewReportForm.Opacity = 1.0#
        End If

      End If
    Catch ex As Exception
    End Try

  End Sub

  Private Sub IncrementStatusBar(ByVal sender As Object, ByVal e As ReportEventArgs)
    ' ****************************************************************************************
    '
    '
    '
    ' ****************************************************************************************

    Static LastUpdateTime As Date = #1/1/1900#

    ' Validate Sender.
    If (Not (TypeOf sender Is C1Report)) Then
      Exit Sub
    End If

    Try
      ' Cast Sender to C1Report in order to get at the 'Tag' property
      Dim thisReport As C1Report = CType(sender, C1Report)

      ' If the 'Tag' is a Progress Bar.
      If (Not (thisReport.Tag Is Nothing)) AndAlso (TypeOf thisReport.Tag Is ToolStripProgressBar) Then
        Dim thisProgressbar As ToolStripProgressBar = CType(thisReport.Tag, ToolStripProgressBar)

        If (thisProgressbar.IsDisposed) Then
          Exit Sub
        End If

        ' Make sure the bar is Visible.
        If (thisProgressbar.Visible = False) Then
          thisProgressbar.Visible = True
        End If

        ' Impose a minimum time between status bar updates.
        If (Now() - LastUpdateTime).TotalSeconds >= 0.2 Then
          LastUpdateTime = Now()

          ' Progress the Status Bar....
          If (thisProgressbar.Value < thisProgressbar.Maximum) Then
            thisProgressbar.Value += thisProgressbar.Step
          Else
            thisProgressbar.Value = thisProgressbar.Minimum
          End If
        End If
      End If
    Catch ex As Exception
    End Try

  End Sub

  Public Function GetReportDefinition(ByVal pFundID As Integer, ByVal pReportName As String) As C1Report
    ' ***********************************************************************************
    '
    ' ***********************************************************************************

    Return GetReportDefinition(pFundID, pReportName, pReportName)
  End Function

  Public Function GetReportDefinition(ByVal pFundID As Integer, ByVal pReportFile As String, ByVal pReportName As String) As C1Report
    ' ******************************************************************************
    ' Return an instance of the given report name from the given report file.
    '
    ' This routine is designed to search upwards from the current executable directory
    ' for the given report file and the given report within it.
    ' In addition to the executable path, it will check for the 'ReportDefinitions'
    ' subdirectory on it's way up the directory tree.
    ' 
    ' ******************************************************************************

    Return SetCustomReportDetails(MainForm, pFundID, pReportFile, pReportName)

  End Function

  Private Sub SetFieldValue(ByRef pReport As C1Report, ByRef pFieldValue As classReportFieldValue)
    ' ******************************************************************************
    '
    '
    ' ******************************************************************************
    Try
      If (pFieldValue Is Nothing) OrElse (pReport Is Nothing) Then
        Exit Sub
      End If
    Catch ex As Exception
      Exit Sub
    End Try

    Try
      Dim FieldValue As String
      Dim SystemStringValue As String

      ' Establish  Field Value.
      ' By Default this is the class 'FieldValue' unless a System String name is given.

      FieldValue = pFieldValue.ReportFieldValue

      If pFieldValue.SystemString.Length > 0 Then
        SystemStringValue = MainForm.Get_SystemString(pFieldValue.SystemString)
        If SystemStringValue.Length > 0 Then
          FieldValue = SystemStringValue
        End If
      End If

      ' Set Report Field

      Dim TypeInfo As Type
      Dim thisProperty As PropertyInfo

      ' Exit if Report field does not exist.
      If Not pReport.Fields.Contains(pFieldValue.ReportFieldName) Then
        Exit Sub
      End If

      TypeInfo = pReport.Fields(pFieldValue.ReportFieldName).GetType
      thisProperty = TypeInfo.GetProperty(pFieldValue.ReportPropertyName)

      If thisProperty.PropertyType.UnderlyingSystemType Is GetType(System.String) Then
        thisProperty.SetValue(pReport.Fields(pFieldValue.ReportFieldName), FieldValue, Nothing)
      ElseIf thisProperty.PropertyType.UnderlyingSystemType Is GetType(System.DateTime) Then
        thisProperty.SetValue(pReport.Fields(pFieldValue.ReportFieldName), CDate(FieldValue), Nothing)
      ElseIf thisProperty.PropertyType.UnderlyingSystemType Is GetType(System.Boolean) Then
        thisProperty.SetValue(pReport.Fields(pFieldValue.ReportFieldName), CBool(FieldValue), Nothing)
      ElseIf thisProperty.PropertyType.UnderlyingSystemType Is GetType(System.Single) Then
        thisProperty.SetValue(pReport.Fields(pFieldValue.ReportFieldName), CSng(FieldValue), Nothing)
      ElseIf thisProperty.PropertyType.UnderlyingSystemType Is GetType(System.Double) Then
        thisProperty.SetValue(pReport.Fields(pFieldValue.ReportFieldName), CDbl(FieldValue), Nothing)
      ElseIf thisProperty.PropertyType.UnderlyingSystemType Is GetType(System.Int16) Then
        thisProperty.SetValue(pReport.Fields(pFieldValue.ReportFieldName), CShort(FieldValue), Nothing)
      ElseIf thisProperty.PropertyType.UnderlyingSystemType Is GetType(System.Int32) Then
        thisProperty.SetValue(pReport.Fields(pFieldValue.ReportFieldName), CInt(FieldValue), Nothing)
      ElseIf thisProperty.PropertyType.UnderlyingSystemType Is GetType(System.Int64) Then
        thisProperty.SetValue(pReport.Fields(pFieldValue.ReportFieldName), CLng(FieldValue), Nothing)
      ElseIf thisProperty.PropertyType.UnderlyingSystemType Is GetType(System.Drawing.Color) Then
        If IsNumeric(FieldValue) Then
          Dim IntValue As Integer
          IntValue = CInt(FieldValue)
          thisProperty.SetValue(pReport.Fields(pFieldValue.ReportFieldName), System.Drawing.Color.FromArgb(CInt((IntValue And 16777215) / 65536), CInt((IntValue And 65535) / 256), CInt(IntValue And 255)), Nothing)
        Else
          thisProperty.SetValue(pReport.Fields(pFieldValue.ReportFieldName), System.Drawing.Color.FromName(CStr(FieldValue)), Nothing)
        End If
      ElseIf thisProperty.PropertyType.UnderlyingSystemType.IsEnum Then
        Dim EnumObject As Object

        If IsNumeric(FieldValue) Then
          EnumObject = System.Enum.ToObject(thisProperty.PropertyType.UnderlyingSystemType, FieldValue)
        Else
          EnumObject = System.Enum.Parse(thisProperty.PropertyType.UnderlyingSystemType, FieldValue, True)
        End If

        thisProperty.SetValue(pReport.Fields(pFieldValue.ReportFieldName), EnumObject, Nothing)
      Else
        thisProperty.SetValue(pReport.Fields(pFieldValue.ReportFieldName), CDbl(FieldValue), Nothing)
      End If

    Catch ex As Exception
    End Try

  End Sub

#End Region

#Region " Specific Report Routines"

  Public Function rptTableAuditReport(ByRef thisReportTable As DataTable, Optional ByRef pProgressBar As ToolStripProgressBar = Nothing) As Boolean
    ' ************************************************************************
    '
    ' ************************************************************************

    Try
      DisplayReport(0, "rptTableAuditReport", thisReportTable, pProgressBar)
    Catch ex As Exception
    End Try

  End Function

  Public Sub InformationReport(ByVal SelectedIDs() As Integer, ByVal pReferenceID As Integer, ByVal StatusLabel As System.Windows.Forms.ToolStripStatusLabel)
    ' ************************************************************************
    '
    ' SelectedIDs() - Array of Pertrac IDs to report on.
    ' pReferenceID  - Id of Instrument to use as a reference instrument.
    ' 
    ' ************************************************************************

    Try

      ' Declare Locals.

      Dim IdCounter As Integer
      Dim ThisID As Integer
      Dim InformationReport As C1Report
      Dim ReturnsReport As C1Report
      Dim DrawDownReport As C1Report
      Dim StatisticsReport As C1Report
      Dim ReferenceID As Integer
      Dim DataStartDate As Date
      Dim DataDateSeries() As Date

      Dim tblMastername As New RenaissanceDataClass.DSMastername.tblMasternameDataTable
      Dim rowMastername As RenaissanceDataClass.DSMastername.tblMasternameRow

      Dim InformationArray() As Object

      ' Exit if no Instruments selected to report on.

      If (SelectedIDs Is Nothing) OrElse (SelectedIDs.Length <= 0) Then
        Exit Sub
      End If

      ' Validate Reference ID.

      ReferenceID = 0
      If (pReferenceID >= 0) Then
        ReferenceID = pReferenceID
      End If

      ' 

      Dim newForm As frmViewReport
      Dim tempPrintPreview As C1.Win.C1Preview.C1PrintPreviewControl = Nothing
      Dim PageImageArray As New ArrayList

      newForm = MainForm.New_CTAForm(CTAFormID.frmViewReport).Form
      newForm.Opacity = 0

      ' Get Holdings by Pertrac ID
      '
      ' I know that this may not appear, at first sight, to be the most effecient way
      ' of getting Holding information, but combining these queries into a function or Stored Procedure
      ' added about 35 Seconds to the calculation time. (To be investigated further).
      ' Since I have already wasted half a day on this, I decided to change my approach!

      Dim IDPairs As New DataTable
      Dim HoldingInfo As New DataTable

      Dim tmpSQLCommand As New SqlCommand
      Try
        tmpSQLCommand.CommandType = CommandType.Text
        tmpSQLCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT
        tmpSQLCommand.Parameters.Add(New SqlParameter("@KnowledgeDate", SqlDbType.DateTime)).Value = MainForm.Main_Knowledgedate
        tmpSQLCommand.Connection = MainForm.GetCTAConnection

        tmpSQLCommand.CommandText = "SELECT InstrumentPertracCode, InstrumentParent FROM fn_tblInstrument_SelectKD(@KnowledgeDate) WHERE ISNULL(InstrumentPertracCode, 0) > 0 GROUP BY InstrumentPertracCode, InstrumentParent"

        IDPairs.Load(tmpSQLCommand.ExecuteReader)

        tmpSQLCommand.CommandText = "SELECT Instruments.InstrumentParent, Sum(Units.SignedUnits * Units.InstrumentContractSize * Prices.PriceLevel) AS HoldingValue, Min(Units.ValueDate) AS TransactionValueDate FROM fn_AggregatedInstrumentHoldings(@KnowledgeDate) Units LEFT JOIN fn_tblInstrument_SelectKD(@KnowledgeDate) Instruments ON (Units.TransactionInstrument = Instruments.InstrumentID) LEFT JOIN fn_BestPrice(Null, 0, @KnowledgeDate) Prices ON (Units.TransactionInstrument = Prices.InstrumentID) GROUP BY InstrumentParent"

        HoldingInfo.Load(tmpSQLCommand.ExecuteReader)

      Catch ex As Exception
        Try
          IDPairs.Rows.Clear()
          HoldingInfo.Rows.Clear()
        Catch ex_Inner As Exception
        End Try

      Finally
        Try
          If (tmpSQLCommand.Connection IsNot Nothing) Then
            tmpSQLCommand.Connection.Close()
          End If
        Catch ex As Exception
        End Try

        tmpSQLCommand.Connection = Nothing
      End Try

      ' OK, Process Instruments

      Try

        For IdCounter = 0 To (SelectedIDs.Length - 1)
          Dim memStream As MemoryStream = New MemoryStream

          ThisID = SelectedIDs(IdCounter)

          InformationArray = MainForm.PertracData.GetInformationValues(ThisID)

          ' Get ThisID Data start Date - Used to set VAMI Chart.

          DataStartDate = Renaissance_BaseDate

          DataDateSeries = MainForm.StatFunctions.DateSeries(MainForm.PertracData.GetPertracDataPeriod(ThisID), CULng(ThisID), False, 12, 1, True, Renaissance_BaseDate, Renaissance_EndDate_Data)
          If (DataDateSeries IsNot Nothing) AndAlso (DataDateSeries.Length > 0) Then
            DataStartDate = DataDateSeries(0)
          End If

          ' Set Status label.

          Try
            If (StatusLabel IsNot Nothing) Then
              If (InformationArray(PertracInformationFields.FundName) Is DBNull.Value) Then
                StatusLabel.Text = ""
              Else
                StatusLabel.Text = Nz(InformationArray(PertracInformationFields.FundName), "").ToString
              End If
              Application.DoEvents()
            End If
          Catch ex As Exception
          End Try

          ' Set Report Data.

          If (tblMastername.Rows.Count > 0) Then
            tblMastername.Rows.Clear()
          End If

          rowMastername = tblMastername.NewtblMasternameRow
          rowMastername.ID = ThisID
          rowMastername.Mastername = Nz(InformationArray(PertracInformationFields.FundName), "").ToString
          rowMastername.IsUsed = True
          tblMastername.Rows.Add(rowMastername)

          ' Get Main report definition

          InformationReport = Me.GetReportDefinition(0, "rptInformationReport", "InformationReport")

          'Pass the chart images for the report

          InformationReport.Fields("VAMIChart").Picture = GetVAMIChartImage(New Integer() {ThisID, ReferenceID}, DataStartDate, 1.0#)

          InformationReport.Fields("MonthlyChart").Picture = GetMonthlyReturnChartImage(ThisID, 1.0#)

          ' Get Sub Report Definitions

          ReturnsReport = Me.GetReportDefinition(0, "rptInformationReport", "MonthlyTableReport")
          DrawDownReport = Me.GetReportDefinition(0, "rptInformationReport", "DrawDownReport")
          StatisticsReport = Me.GetReportDefinition(0, "rptInformationReport", "StatisticsReport")

          ' Populate (Sub)Report Data Sources

          InformationReport.DataSource.Recordset = tblMastername
          ReturnsReport.DataSource.Recordset = GetReturnsDataSet(ThisID)
          DrawDownReport.DataSource.Recordset = GetDrawDownDataSet(ThisID)
          StatisticsReport.DataSource.Recordset = GetStatisticsDataSet(ThisID)

          'Link the SectorSubReport field on MonthlyTableReport

          InformationReport.Fields("MonthlyTableReportField").Subreport = ReturnsReport
          InformationReport.Fields("DrawDownReportField").Subreport = DrawDownReport
          InformationReport.Fields("StatisticsReportField").Subreport = StatisticsReport

          ' Populate Information Fields

          InformationReport.Fields("Field_Description").Text = Nz(InformationArray(PertracInformationFields.Description), "").ToString
          InformationReport.Fields("Field_MgmtFee").Text = Nz(InformationArray(PertracInformationFields.ManagementFee), "").ToString
          InformationReport.Fields("Field_PerfFee").Text = Nz(InformationArray(PertracInformationFields.IncentiveFee), "").ToString
          InformationReport.Fields("Field_Hurdle").Text = Nz(InformationArray(PertracInformationFields.Hurdle), "").ToString
          InformationReport.Fields("Field_Subscription").Text = Nz(InformationArray(PertracInformationFields.Subscription), "").ToString
          InformationReport.Fields("Field_Redemption").Text = Nz(InformationArray(PertracInformationFields.Redemption), "").ToString
          InformationReport.Fields("Field_Lockup").Text = Nz(InformationArray(PertracInformationFields.Lockup), "").ToString
          InformationReport.Fields("Field_Notice").Text = Nz(InformationArray(PertracInformationFields.Notice), "").ToString
          InformationReport.Fields("Field_Contact").Text = Nz(InformationArray(PertracInformationFields.Contact), "").ToString
          InformationReport.Fields("Field_Phone").Text = Nz(InformationArray(PertracInformationFields.ContactPhone), "").ToString
          InformationReport.Fields("Field_Fax").Text = Nz(InformationArray(PertracInformationFields.ContactFax), "").ToString
          InformationReport.Fields("Field_EMail").Text = Nz(InformationArray(PertracInformationFields.Email), "").ToString
          'InformationReport.Fields("Field_MarketingContact").Text = Nz(InformationArray(PertracInformationFields.MarketingContact), "").ToString
          'InformationReport.Fields("Field_MarketingPhone").Text = Nz(InformationArray(PertracInformationFields.MarketingPhone), "").ToString
          'InformationReport.Fields("Field_MarketingFax").Text = Nz(InformationArray(PertracInformationFields.MarketingFax), "").ToString
          InformationReport.Fields("Field_Managers").Text = Nz(InformationArray(PertracInformationFields.ManagerName), "").ToString

          ' Get Current holding and First Investment Date values

          Dim CurrentHolding As Double = 0
          Dim FirstInvestmentDate As Date = Renaissance_BaseDate
          Dim SelectedIDPairs() As DataRow
          Dim SelectedHoldings() As DataRow
          Dim PairCounter As Integer

          If (IDPairs.Rows.Count > 0) AndAlso (HoldingInfo.Rows.Count > 0) Then

            SelectedIDPairs = IDPairs.Select("InstrumentPertracCode=" & ThisID.ToString)

            If (SelectedIDPairs IsNot Nothing) AndAlso (SelectedIDPairs.Length > 0) Then
              For PairCounter = 0 To (SelectedIDPairs.Length - 1)

                SelectedHoldings = HoldingInfo.Select("InstrumentParent=" & SelectedIDPairs(PairCounter)("InstrumentParent").ToString)

                If (SelectedHoldings IsNot Nothing) AndAlso (SelectedHoldings.Length > 0) Then
                  If (FirstInvestmentDate <= Renaissance_BaseDate) Then
                    FirstInvestmentDate = CDate(SelectedHoldings(0)("TransactionValueDate"))
                  ElseIf (CDate(SelectedHoldings(0)("TransactionValueDate")) < FirstInvestmentDate) Then
                    FirstInvestmentDate = CDate(SelectedHoldings(0)("TransactionValueDate"))
                  End If

                  CurrentHolding += CDbl(SelectedHoldings(0)("HoldingValue"))

                End If

              Next
            End If
          End If

          Try
            If (FirstInvestmentDate <= Renaissance_BaseDate) Then
              InformationReport.Fields("Field_CurrentHolding").Text = "0"
              InformationReport.Fields("Field_FirstInvestmentDate").Text = "N/A"
            Else
              InformationReport.Fields("Field_CurrentHolding").Text = CurrentHolding.ToString("#,##0")
              InformationReport.Fields("Field_FirstInvestmentDate").Text = FirstInvestmentDate.ToString(DISPLAYMEMBER_DATEFORMAT)
            End If
          Catch ex As Exception
          End Try

          ' Render Report

          Try

            InformationReport.Render()
            PageImageArray.AddRange(InformationReport.PageImages)

          Catch ex As Exception
            MainForm.LogError("InformationReport()", LOG_LEVELS.Error, ex.Message, "Error generating CTA Information Report.", ex.StackTrace, True)
            Exit Sub
          End Try

          'End If

        Next
      Catch ex As Exception
      Finally

        newForm.ReportPreview.Document = PageImageArray

      End Try

      ' Show Report.

      Try
        If Not (newForm Is Nothing) Then
          Dim OCount As Integer
          newForm.Show()

          If MainForm.EnableReportFadeIn Then

            Try
              Dim TStart As Date = Now()
              Dim TEnd As Date

              For OCount = 0 To 100 Step 5
                newForm.Opacity = OCount / 100
                Threading.Thread.Sleep(10)
              Next

              TEnd = Now()
              If (TEnd - TStart).TotalMilliseconds >= 1000 Then
                MainForm.DisableReportFadeIn()
              End If

            Catch ex As Exception
            End Try

          Else
            newForm.Opacity = 1.0#
          End If

        End If
      Catch ex As Exception
      End Try

    Catch ex As Exception
      MainForm.LogError("InformationReport()", LOG_LEVELS.Error, ex.Message, "Error generating CTA Information Report.", ex.StackTrace, True)
    Finally
      Try
        If (StatusLabel IsNot Nothing) Then
          StatusLabel.Text = ""
          Application.DoEvents()
        End If
      Catch ex As Exception
      End Try
    End Try

  End Sub

  Private Function GetGroupReport(ByVal GroupName As String, ByVal GroupID As Integer, ByVal SelectedIDs() As Integer, ByVal DisplayCustomFieldID As Integer, ByVal GroupByCustomFieldID As Integer, ByVal pOrderByName As Boolean, ByVal StatusLabel As System.Windows.Forms.ToolStripStatusLabel) As C1Report
    ' ************************************************************************
    '
    '
    '
    ' ************************************************************************

    Dim IdCounter As Integer
    Dim ThisID As Integer
    Dim InformationArray() As Object
    Dim CustomData As Object
    Dim DisplayCustomDataType As RenaissanceDataType
    Dim GroupByCustomDataType As RenaissanceDataType
    Dim StatsDatePeriod As DealingPeriod = DealingPeriod.Daily
    Dim thisStatsDatePeriod As DealingPeriod

    Dim ReportTable As New dsGroupReport.tblGroupReportDataTable
    Dim thisReportRow As dsGroupReport.tblGroupReportRow
    Dim InformationReport As C1Report = Nothing

    Dim ExistingPositions As DataTable
    Dim ExistingRows() As DataRow

    ' Determine ReferenceMonth for Monthly Data. Calculated in the same way as the StatFunctions.GetSimpleStats() function.
    Dim ReferenceMonth As Date = New Date(Now.Year, Now.Month, 1)

    ExistingPositions = MainForm.PertracData.GetActivePertracInstruments(False, 0)

    DisplayCustomDataType = RenaissanceDataType.TextType
    If (DisplayCustomFieldID > 0) Then
      If (MainForm.CustomFieldDataCache.GetCustomFieldDataStatus(DisplayCustomFieldID, GroupID) <> CustomFieldDataCacheClass.CacheStatus.FullyPopulated) Then
        MainForm.CustomFieldDataCache.LoadCustomFieldData(DisplayCustomFieldID, GroupID)
      End If
      DisplayCustomDataType = MainForm.CustomFieldDataCache.GetCustomFieldDataType(DisplayCustomFieldID)
    End If

    If (GroupByCustomFieldID > 0) Then
      If (MainForm.CustomFieldDataCache.GetCustomFieldDataStatus(GroupByCustomFieldID, GroupID) <> CustomFieldDataCacheClass.CacheStatus.FullyPopulated) Then
        MainForm.CustomFieldDataCache.LoadCustomFieldData(GroupByCustomFieldID, GroupID)
      End If
      GroupByCustomDataType = MainForm.CustomFieldDataCache.GetCustomFieldDataType(GroupByCustomFieldID)
    End If

    Try
      ' Get 'Best' StatsDatePeriod.
      For IdCounter = 0 To (SelectedIDs.Length - 1)
        ThisID = SelectedIDs(IdCounter)

        thisStatsDatePeriod = MainForm.PertracData.GetPertracDataPeriod(ThisID)
        If (MainForm.PertracData.AnnualPeriodCount(thisStatsDatePeriod) < MainForm.PertracData.AnnualPeriodCount(StatsDatePeriod)) Then
          StatsDatePeriod = thisStatsDatePeriod
        End If
      Next

      For IdCounter = 0 To (SelectedIDs.Length - 1)

        ThisID = SelectedIDs(IdCounter)

        InformationArray = MainForm.PertracData.GetInformationValues(ThisID)

        thisReportRow = ReportTable.NewtblGroupReportRow

        Try
          If (InformationArray IsNot Nothing) Then
            StatusLabel.Text = GroupName & ", " & Nz(InformationArray(PertracInformationFields.FundName), "").ToString
            Application.DoEvents()

            thisReportRow.grGroupName = GroupName
            thisReportRow.grPertracCode = ThisID
            thisReportRow.grDataVendorName = Nz(InformationArray(PertracInformationFields.DataVendorName), "").ToString
            thisReportRow.grDealingPeriod = Nz(InformationArray(PertracInformationFields.Subscription), "").ToString
            thisReportRow.grName = Nz(InformationArray(PertracInformationFields.FundName), "").ToString
            thisReportRow.grManager = Nz(InformationArray(PertracInformationFields.ManagerName), "").ToString
            thisReportRow.grNoticePeriod = Nz(InformationArray(PertracInformationFields.Notice), "").ToString
            thisReportRow.grLockup = Nz(InformationArray(PertracInformationFields.Lockup), "").ToString
            thisReportRow.grExistingPosition = 0
            thisReportRow.grHighlight = 0
            thisReportRow.grCustom1 = ""
            thisReportRow.grCustomGroupOrder = 0
            thisReportRow.grCustomGroupText = ""
          Else
            StatusLabel.Text = GroupName & ", <No 'Information'>"
            Application.DoEvents()

            thisReportRow.grGroupName = GroupName
            thisReportRow.grPertracCode = ThisID
            thisReportRow.grDataVendorName = ""
            thisReportRow.grDealingPeriod = ""
            thisReportRow.grName = ThisID.ToString & " has no record in the 'Information' Table"
            thisReportRow.grManager = ""
            thisReportRow.grNoticePeriod = ""
            thisReportRow.grLockup = ""
            thisReportRow.grExistingPosition = 0
            thisReportRow.grHighlight = 0
            thisReportRow.grCustom1 = ""
            thisReportRow.grCustomGroupOrder = 0
            thisReportRow.grCustomGroupText = ""
          End If

          ' Get Report Column Custom Data.

          If (DisplayCustomFieldID > 0) Then
            CustomData = MainForm.CustomFieldDataCache.GetDataPoint(DisplayCustomFieldID, GroupID, ThisID)

            If (DisplayCustomDataType And RenaissanceDataType.DateType) Then
              thisReportRow.grCustom1 = CDate(CustomData).ToString(DISPLAYMEMBER_DATEFORMAT)
            ElseIf (DisplayCustomDataType And RenaissanceDataType.PercentageType) Then
              thisReportRow.grCustom1 = CDbl(CustomData).ToString(DISPLAYMEMBER_PERCENTAGEFORMAT)
            ElseIf (DisplayCustomDataType And RenaissanceDataType.NumericType) Then
              thisReportRow.grCustom1 = CDbl(CustomData).ToString(DISPLAYMEMBER_DOUBLEFORMAT)
            Else
              thisReportRow.grCustom1 = CustomData.ToString
            End If
          End If

          ' Get GroupBy Custom Data.

          If (GroupByCustomFieldID > 0) Then
            CustomData = MainForm.CustomFieldDataCache.GetDataPoint(GroupByCustomFieldID, GroupID, ThisID)

            If (GroupByCustomDataType And RenaissanceDataType.DateType) Then
              thisReportRow.grCustomGroupOrder = CDate(CustomData).ToOADate
              thisReportRow.grCustomGroupText = CDate(CustomData).ToString(DISPLAYMEMBER_DATEFORMAT)
            ElseIf (GroupByCustomDataType And RenaissanceDataType.PercentageType) Then
              thisReportRow.grCustomGroupOrder = CDbl(CustomData)
              thisReportRow.grCustomGroupText = CDbl(CustomData).ToString(DISPLAYMEMBER_PERCENTAGEFORMAT)
            ElseIf (GroupByCustomDataType And RenaissanceDataType.NumericType) Then
              thisReportRow.grCustomGroupOrder = CDbl(CustomData)
              thisReportRow.grCustomGroupText = CDbl(CustomData).ToString(DISPLAYMEMBER_DOUBLEFORMAT)
            ElseIf (GroupByCustomDataType And RenaissanceDataType.BooleanType) Then
              If CBool(CustomData) Then
                thisReportRow.grCustomGroupOrder = 1
              Else
                thisReportRow.grCustomGroupOrder = 0
              End If
              thisReportRow.grCustomGroupText = CustomData.ToString
            Else
              thisReportRow.grCustomGroupText = CustomData.ToString
            End If
          End If

        Catch ex As Exception
        End Try

        Try
          If (ExistingPositions IsNot Nothing) AndAlso (ExistingPositions.Rows.Count > 0) Then
            ExistingRows = ExistingPositions.Select("PertracCode=" & ThisID.ToString)
            If (ExistingRows IsNot Nothing) AndAlso (ExistingRows.Length > 0) Then
              thisReportRow.grExistingPosition = 1
            End If
          End If
        Catch ex As Exception
        End Try

        Try
          If (InformationArray IsNot Nothing) Then
            thisReportRow.grFee = Nz(InformationArray(PertracInformationFields.ManagementFee), "").ToString
            If (IsNumeric(thisReportRow.grFee)) Then thisReportRow.grFee = CDbl(thisReportRow.grFee).ToString("#,##0.00") & "%"
            thisReportRow.grIncentiveFee = Nz(InformationArray(PertracInformationFields.IncentiveFee), "").ToString
            If (IsNumeric(thisReportRow.grIncentiveFee)) Then thisReportRow.grIncentiveFee = CDbl(thisReportRow.grIncentiveFee).ToString("#,##0.00") & "%"
          Else
            thisReportRow.grFee = "0.00%"
            thisReportRow.grIncentiveFee = "0.00%"
          End If
        Catch ex As Exception
        End Try

        Dim ThisStats As StatFunctions.SeriesStatsClass
        ThisStats = MainForm.StatFunctions.GetSimpleStats(StatsDatePeriod, CULng(ThisID), False, 0.04, 1.0#)

        Try

          thisReportRow.grMonth1 = ThisStats.MonthReturns.P1 * 100.0#
          thisReportRow.grMonth2 = ThisStats.MonthReturns.P2 * 100.0#
          thisReportRow.grMonth3 = ThisStats.MonthReturns.P3 * 100.0#

          thisReportRow.grYTD = ThisStats.YearReturns.P1 * 100.0#

          thisReportRow.GRYear1 = ThisStats.YearReturns.P2 * 100.0#
          thisReportRow.GRYear2 = ThisStats.YearReturns.P3 * 100.0#

          thisReportRow.GRReturn12m = ThisStats.SimpleReturn.M12 * 100.0#
          thisReportRow.GRVolatility36m = ThisStats.Volatility.M36 * 100.0#
          thisReportRow.GRReturn36m = ThisStats.SimpleReturn.M36 * 100.0#
          thisReportRow.GRObservations = ThisStats.PeriodCount
        Catch ex As Exception
        End Try

        ReportTable.Rows.Add(thisReportRow)

      Next
    Catch ex As Exception
    End Try

    ExistingRows = Nothing

    ' Set Highlight flag and Report Datasource.

    Try
      Dim SelectedRows() As dsGroupReport.tblGroupReportRow
      Dim ReportView As DataView
      Dim HighlightCounter As Integer

      If (GroupByCustomFieldID > 0) Then
        If (GroupByCustomDataType And RenaissanceDataType.TextType) Then
          If pOrderByName Then
            SelectedRows = ReportTable.Select("True", "grCustomGroupText, GRName")
          Else
            SelectedRows = ReportTable.Select("True", "grCustomGroupText, GRReturn12m DESC")
          End If
        Else
          If pOrderByName Then
            SelectedRows = ReportTable.Select("True", "grCustomGroupOrder, GRName")
          Else
            SelectedRows = ReportTable.Select("True", "grCustomGroupOrder, GRReturn12m DESC")
          End If
        End If
      Else
        If pOrderByName Then
          SelectedRows = ReportTable.Select("True", "GRName")
        Else
          SelectedRows = ReportTable.Select("True", "GRReturn12m DESC")
        End If
      End If

      For HighlightCounter = 0 To (SelectedRows.Length - 1)
        If ((HighlightCounter Mod 2) = 1) Then
          SelectedRows(HighlightCounter).grHighlight = 1
        End If
      Next

      If pOrderByName Then
        ReportView = New DataView(ReportTable, "True", "grCustomGroupOrder, GRName", DataViewRowState.CurrentRows)
      Else
        ReportView = New DataView(ReportTable, "True", "grCustomGroupOrder, GRReturn12m DESC", DataViewRowState.CurrentRows)
      End If

      InformationReport = Me.GetReportDefinition(0, "rptGroupReport")
      InformationReport.DataSource.Recordset = ReportView ' ReportTable
    Catch ex As Exception
    End Try

    ' Set Report Fields - Month and Year labels

    ReferenceMonth = FitDateToPeriod(StatsDatePeriod, ReferenceMonth, True).AddMonths(-1)

    Try
      Call SetFieldValue(InformationReport, New classReportFieldValue("Label_LatestMonth", ReferenceMonth.ToString("MMM")))
      Call SetFieldValue(InformationReport, New classReportFieldValue("Label_Month2", (ReferenceMonth.AddMonths(-1)).ToString("MMM")))
      Call SetFieldValue(InformationReport, New classReportFieldValue("Label_Month3", (ReferenceMonth.AddMonths(-2)).ToString("MMM")))
      Call SetFieldValue(InformationReport, New classReportFieldValue("Label_Year1", (ReferenceMonth.AddYears(-1)).ToString("yyyy")))
      Call SetFieldValue(InformationReport, New classReportFieldValue("Label_Year2", (ReferenceMonth.AddYears(-2)).ToString("yyyy")))

      If (DisplayCustomFieldID > 0) Then
        Call SetFieldValue(InformationReport, New classReportFieldValue("Title_Custom1", CStr(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblPertracCustomFields, DisplayCustomFieldID, "FieldName"))))
        Call SetFieldValue(InformationReport, New classReportFieldValue("Field_Custom1", "True", "Visible"))
      Else
        Call SetFieldValue(InformationReport, New classReportFieldValue("Title_Custom1", ""))
        Call SetFieldValue(InformationReport, New classReportFieldValue("Field_Custom1", "False", "Visible"))
      End If

      If (ReferenceMonth.Month = 12) Then
        Call SetFieldValue(InformationReport, New classReportFieldValue("Label_YTD", ReferenceMonth.ToString("yyyy")))
      End If

      If (GroupByCustomFieldID > 0) Then
        If (GroupByCustomDataType And RenaissanceDataType.TextType) Then
          InformationReport.Groups("OuterGroup").GroupBy = "grCustomGroupText"
        Else
          InformationReport.Groups("OuterGroup").GroupBy = "grCustomGroupOrder"
        End If

        InformationReport.Groups("OuterGroup").SectionHeader.Visible = True
        InformationReport.Groups("OuterGroup").SectionFooter.Visible = True
        InformationReport.Fields("Text_OuterGroupField").Text = "grCustomGroupText"
        InformationReport.Fields("Text_OuterGroupName").Text = LookupTableValue(MainForm, RenaissanceStandardDatasets.tblPertracCustomFields, GroupByCustomFieldID, "FieldName").ToString
      End If

      If pOrderByName Then
        InformationReport.Groups("12m").GroupBy = "grName"
        InformationReport.Groups("12m").Sort = SortEnum.Ascending
      End If

    Catch ex As Exception
    End Try

    'Pass the chart images for the report

    'InformationReport.Fields("VAMIChart").Picture = GetVAMIChartImage(ThisID)
    'InformationReport.Fields("MonthlyChart").Picture = GetMonthlyReturnChartImage(ThisID)

    Return InformationReport

  End Function


  Public Function GroupReport(ByVal GroupName As String, ByVal pGroupID As Integer, ByVal CustomFieldID As Integer, ByVal GroupByCustomFieldID As Integer, ByVal pOrderByName As Boolean, ByVal StatusLabel As System.Windows.Forms.ToolStripStatusLabel) As C1Report
    ' ************************************************************************
    '
    '
    '
    ' ************************************************************************
    Dim SelectedIDs(-1) As Integer

    Dim thisDSGroupItems As RenaissanceDataClass.DSGroupItems
    Dim thisGroupRow As RenaissanceDataClass.DSGroupItems.tblGroupItemsRow
    Dim SelectedGroupRow() As RenaissanceDataClass.DSGroupItems.tblGroupItemsRow

    Dim RowCounter As Integer

    Try
      thisDSGroupItems = MainForm.Load_Table(RenaissanceGlobals.RenaissanceStandardDatasets.tblGroupItems)
      If (thisDSGroupItems Is Nothing) Then
        MainForm.LogError("GroupReport()", LOG_LEVELS.Warning, "", "Failed to get tblGroupItems.", "", True)
        Return Nothing
        Exit Function
      End If
      SelectedGroupRow = thisDSGroupItems.tblGroupItems.Select("GroupID=" & pGroupID)

      If (SelectedGroupRow.Length <= 0) Then
        MainForm.LogError("GroupReport()", LOG_LEVELS.Warning, "", "The Selected Group is empty. Report aborted.", "", True)
        Return Nothing
        Exit Function
      End If

      ReDim SelectedIDs(SelectedGroupRow.Length - 1)
      For RowCounter = 0 To (SelectedGroupRow.Length - 1)
        thisGroupRow = SelectedGroupRow(RowCounter)

        SelectedIDs(RowCounter) = thisGroupRow.GroupPertracCode
      Next
    Catch ex As Exception
      MainForm.LogError("GroupReport()", LOG_LEVELS.Error, ex.Message, "Error Running the Group Comparison Report.", ex.StackTrace, True)
      Return Nothing
    End Try

    Return GetGroupReport(GroupName, pGroupID, SelectedIDs, CustomFieldID, GroupByCustomFieldID, pOrderByName, StatusLabel)
  End Function

  Public Sub GroupReportOnGroups(ByVal pGroupIDs() As Integer, ByVal CustomFieldID As Integer, ByVal GroupByCustomFieldID As Integer, ByVal pOrderByName As Boolean, ByVal StatusLabel As System.Windows.Forms.ToolStripStatusLabel)

    Dim ThisGroupID As Integer
    Dim GroupIdCounter As Integer
    Dim GName As String

    Dim newForm As frmViewReport
    Dim PageImageArray As New ArrayList

    newForm = MainForm.New_CTAForm(CTAFormID.frmViewReport).Form
    newForm.Opacity = 0

    Dim thisGroupReport As C1Report

    For GroupIdCounter = 0 To (pGroupIDs.Length - 1)
      ThisGroupID = pGroupIDs(GroupIdCounter)

      If (ThisGroupID > 0) Then

        Try
          GName = LookupTableValue(MainForm, RenaissanceGlobals.RenaissanceStandardDatasets.tblGroupList, ThisGroupID, "GroupListName").ToString
        Catch ex As Exception
          GName = "<Unknown Group>"
        End Try

        thisGroupReport = GroupReport(GName, ThisGroupID, CustomFieldID, GroupByCustomFieldID, pOrderByName, StatusLabel)

        If (thisGroupReport IsNot Nothing) Then

          thisGroupReport.Render()
          PageImageArray.AddRange(thisGroupReport.PageImages)

        End If

      End If
    Next

    Try
      newForm.ReportPreview.Document = PageImageArray

      If Not (newForm Is Nothing) Then
        Dim OCount As Integer
        newForm.Show()

        If MainForm.EnableReportFadeIn Then

          Try
            Dim TStart As Date = Now()
            Dim TEnd As Date

            For OCount = 0 To 100 Step 5
              newForm.Opacity = OCount / 100
              Threading.Thread.Sleep(10)
            Next

            TEnd = Now()
            If (TEnd - TStart).TotalMilliseconds >= 1000 Then
              MainForm.DisableReportFadeIn()
            End If

          Catch ex As Exception
          End Try

        Else
          newForm.Opacity = 1.0#
        End If

      End If
    Catch ex As Exception
    End Try

  End Sub

  Public Sub GroupReportSelectedIDs(ByVal GName As String, ByVal pInstrumentIDs() As Integer, ByVal CustomFieldID As Integer, ByVal GroupByCustomFieldID As Integer, ByVal pOrderByName As Boolean, ByVal StatusLabel As System.Windows.Forms.ToolStripStatusLabel)

    Dim newForm As frmViewReport
    newForm = MainForm.New_CTAForm(CTAFormID.frmViewReport).Form
    newForm.Opacity = 0

    Dim thisGroupReport As C1Report

    thisGroupReport = GetGroupReport(GName, 0, pInstrumentIDs, CustomFieldID, GroupByCustomFieldID, pOrderByName, StatusLabel)

    newForm.ReportPreview.Document = thisGroupReport.Document

    Try
      If Not (newForm Is Nothing) Then
        Dim OCount As Integer
        newForm.Show()

        If MainForm.EnableReportFadeIn Then
          Try
            Dim TStart As Date = Now()
            Dim TEnd As Date

            For OCount = 0 To 100 Step 5
              newForm.Opacity = OCount / 100
              Threading.Thread.Sleep(10)
            Next

            TEnd = Now()
            If (TEnd - TStart).TotalMilliseconds >= 1000 Then
              MainForm.DisableReportFadeIn()
            End If

          Catch ex As Exception
          End Try
        Else
          newForm.Opacity = 1.0#
        End If

      End If
    Catch ex As Exception
    End Try

  End Sub

  Public Sub CompetitorGroupsReport(ByVal Group1_Label As String, ByVal Group1_ID As Integer, ByVal Group2_Label As String, ByVal Group2_ID As Integer, ByVal Group3_Label As String, ByVal Group3_ID As Integer, ByVal Group4_Label As String, ByVal Group4_SelectedIDs() As Integer, ByVal pReportEndDate As Date, ByVal Column2DisplayValue As Integer, ByVal Column3DisplayValue As Integer, ByVal Column3StartDate As Date, ByVal StatusLabel1 As System.Windows.Forms.ToolStripStatusLabel)
    ' *************************************************************
    '
    '
    '
    '
    ' *************************************************************

    Dim ReportTable As New dsCompetitorGroupReport.tblCompetitorGroupReportDataTable
    Dim thisReportRow As dsCompetitorGroupReport.tblCompetitorGroupReportRow

    Dim ThisGroupID As Integer
    Dim ThisGroupLabel As String
    Dim ThisGroupCounter As Integer
    Dim ThisGroupPertracCodes() As Integer
    Dim ThisPertracID As Integer
    Dim ThisInstrumentCounter As Integer
    Dim thisDSGroupItems As RenaissanceDataClass.DSGroupItems
    Dim SelectedGroupRows() As RenaissanceDataClass.DSGroupItems.tblGroupItemsRow

    Dim InformationArray() As Object
    Dim FundStats As StatFunctions.SeriesStatsClass = Nothing
    Dim ThisDrawdowns() As StatFunctions.DrawDownInstanceClass
    Dim StatsDatePeriod As DealingPeriod
    Dim thisStatsDatePeriod As DealingPeriod

    thisDSGroupItems = MainForm.Load_Table(RenaissanceGlobals.RenaissanceStandardDatasets.tblGroupItems)

    For ThisGroupCounter = 1 To 4
      ThisGroupID = 0
      ThisGroupPertracCodes = Nothing

      Select Case ThisGroupCounter
        Case 1
          ThisGroupID = Group1_ID
          ThisGroupLabel = Group1_Label

        Case 2
          ThisGroupID = Group2_ID
          ThisGroupLabel = Group2_Label

        Case 3
          ThisGroupID = Group3_ID
          ThisGroupLabel = Group3_Label

        Case 4
          ThisGroupID = -1
          ThisGroupLabel = Group4_Label
          ThisGroupPertracCodes = Group4_SelectedIDs

        Case Else
          ThisGroupLabel = ""

      End Select

      If (ThisGroupID <> 0) Then

        StatsDatePeriod = DealingPeriod.Daily

        ' Get Pertrac Codes :
        If (ThisGroupCounter <= 3) Then
          SelectedGroupRows = thisDSGroupItems.tblGroupItems.Select("GroupID=" & ThisGroupID)

          If (SelectedGroupRows IsNot Nothing) AndAlso (SelectedGroupRows.Length > 0) Then
            ReDim ThisGroupPertracCodes(SelectedGroupRows.Length - 1)

            For ThisInstrumentCounter = 0 To (SelectedGroupRows.Length - 1)
              ThisGroupPertracCodes(ThisInstrumentCounter) = SelectedGroupRows(ThisInstrumentCounter).GroupPertracCode

              ' Get 'Best' Satts Date period for this group.

              thisStatsDatePeriod = MainForm.PertracData.GetPertracDataPeriod(ThisGroupPertracCodes(ThisInstrumentCounter))
              If (MainForm.PertracData.AnnualPeriodCount(thisStatsDatePeriod) < MainForm.PertracData.AnnualPeriodCount(StatsDatePeriod)) Then
                StatsDatePeriod = thisStatsDatePeriod
              End If
            Next

          Else
            ThisGroupPertracCodes = Nothing
          End If
        End If

        If (ThisGroupPertracCodes IsNot Nothing) AndAlso (ThisGroupPertracCodes.Length > 0) Then
          ' OK, WE now have an array of Pertrac Codes, Build the Report Dataset.

          For ThisInstrumentCounter = 0 To (ThisGroupPertracCodes.Length - 1)
            Try
              ThisPertracID = ThisGroupPertracCodes(ThisInstrumentCounter)
              InformationArray = MainForm.PertracData.GetInformationValues(ThisPertracID)

              FundStats = MainForm.StatFunctions.GetSimpleStats(StatsDatePeriod, CULng(ThisPertracID), False, Renaissance_BaseDate, Column3StartDate, Renaissance_EndDate_Data, pReportEndDate, 0.0, 1.0#, -1)  ' Zero Risk free rate - GROSS Sharpe.

              thisReportRow = ReportTable.NewtblCompetitorGroupReportRow

              thisReportRow.grGroupName = ThisGroupLabel
              thisReportRow.grGroupSortOrder = ThisGroupCounter
              thisReportRow.grPertracCode = ThisPertracID
              thisReportRow.grName = Nz(InformationArray(PertracInformationFields.FundName), "").ToString
              thisReportRow.grManager = Nz(InformationArray(PertracInformationFields.CompanyName), "").ToString
              thisReportRow.grCurrency = Nz(InformationArray(PertracInformationFields.Currency), "").ToString
              thisReportRow.M1_Return = FundStats.MonthReturns.P1
              thisReportRow.M2_Return = FundStats.MonthReturns.P2
              thisReportRow.M3_Return = FundStats.MonthReturns.P3
              thisReportRow.YTD_Return = FundStats.SimpleReturn.YTD

              ' Column 2

              Select Case Column2DisplayValue

                Case 0 ' 1 Yr
                  thisReportRow.M24_AnnualisedReturn = FundStats.AnnualisedReturn.M12
                  thisReportRow.M24_Volatility = FundStats.Volatility.M12
                  thisReportRow.M24_Sharpe = FundStats.SharpeRatio.M12
                  thisReportRow.M24_WorstDrawDown = 0

                Case 2 ' 3 Yr
                  thisReportRow.M24_AnnualisedReturn = FundStats.AnnualisedReturn.M36
                  thisReportRow.M24_Volatility = FundStats.Volatility.M36
                  thisReportRow.M24_Sharpe = FundStats.SharpeRatio.M36
                  thisReportRow.M24_WorstDrawDown = 0

                Case 3 ' 5 Yr
                  thisReportRow.M24_AnnualisedReturn = FundStats.AnnualisedReturn.M60
                  thisReportRow.M24_Volatility = FundStats.Volatility.M60
                  thisReportRow.M24_Sharpe = FundStats.SharpeRatio.M60
                  thisReportRow.M24_WorstDrawDown = 0

                Case 4 ' ITD
                  thisReportRow.M24_AnnualisedReturn = FundStats.AnnualisedReturn.ITD
                  thisReportRow.M24_Volatility = FundStats.Volatility.ITD
                  thisReportRow.M24_Sharpe = FundStats.SharpeRatio.ITD
                  thisReportRow.M24_WorstDrawDown = 0

                Case Else ' 2 Yr - Default or Screw up
                  thisReportRow.M24_AnnualisedReturn = FundStats.AnnualisedReturn.M24
                  thisReportRow.M24_Volatility = FundStats.Volatility.M24
                  thisReportRow.M24_Sharpe = FundStats.SharpeRatio.M24
                  thisReportRow.M24_WorstDrawDown = 0

              End Select

              ' Column 3 ' CTD ?

              Select Case Column3DisplayValue

                Case 0 ' 1 Yr
                  thisReportRow.ITD_AnnualisedReturn = FundStats.AnnualisedReturn.M12
                  thisReportRow.ITD_Volatility = FundStats.Volatility.M12
                  thisReportRow.ITD_Sharpe = FundStats.SharpeRatio.M12
                  thisReportRow.ITD_WorstDrawDown = 0

                Case 1 ' 2 Yr
                  thisReportRow.ITD_AnnualisedReturn = FundStats.AnnualisedReturn.M24
                  thisReportRow.ITD_Volatility = FundStats.Volatility.M24
                  thisReportRow.ITD_Sharpe = FundStats.SharpeRatio.M24
                  thisReportRow.ITD_WorstDrawDown = 0

                Case 2 ' 3 Yr
                  thisReportRow.ITD_AnnualisedReturn = FundStats.AnnualisedReturn.M36
                  thisReportRow.ITD_Volatility = FundStats.Volatility.M36
                  thisReportRow.ITD_Sharpe = FundStats.SharpeRatio.M36
                  thisReportRow.ITD_WorstDrawDown = 0

                Case 3 ' 5 Yr
                  thisReportRow.ITD_AnnualisedReturn = FundStats.AnnualisedReturn.M60
                  thisReportRow.ITD_Volatility = FundStats.Volatility.M60
                  thisReportRow.ITD_Sharpe = FundStats.SharpeRatio.M60
                  thisReportRow.ITD_WorstDrawDown = 0

                Case 5 ' CTD
                  thisReportRow.ITD_AnnualisedReturn = FundStats.AnnualisedReturn.CTD
                  thisReportRow.ITD_Volatility = FundStats.Volatility.CTD
                  thisReportRow.ITD_Sharpe = FundStats.SharpeRatio.CTD
                  thisReportRow.ITD_WorstDrawDown = 0

                Case Else ' ITD - Default or screw up.

                  thisReportRow.ITD_AnnualisedReturn = FundStats.AnnualisedReturn.ITD
                  thisReportRow.ITD_Volatility = FundStats.Volatility.ITD
                  thisReportRow.ITD_Sharpe = FundStats.SharpeRatio.ITD
                  thisReportRow.ITD_WorstDrawDown = 0

              End Select

              thisReportRow.ITD_StartDate = FundStats.FirstReturnDate ' Fund Start date on the report.

              ' Column 2 (24 Month) DrawDown.

              Select Case Column2DisplayValue

                Case 0 ' 1 Yr
                  ThisDrawdowns = MainForm.StatFunctions.GetDrawDownDetails(StatsDatePeriod, CULng(ThisPertracID), False, FundStats.MonthReturns.ReferenceDate.AddMonths(-11), Renaissance_EndDate_Data, 1.0#)

                Case 2 ' 3 Yr
                  ThisDrawdowns = MainForm.StatFunctions.GetDrawDownDetails(StatsDatePeriod, CULng(ThisPertracID), False, FundStats.MonthReturns.ReferenceDate.AddMonths(-35), Renaissance_EndDate_Data, 1.0#)

                Case 3 ' 5 Yr
                  ThisDrawdowns = MainForm.StatFunctions.GetDrawDownDetails(StatsDatePeriod, CULng(ThisPertracID), False, FundStats.MonthReturns.ReferenceDate.AddMonths(-59), Renaissance_EndDate_Data, 1.0#)

                Case 4 ' 
                  ThisDrawdowns = MainForm.StatFunctions.GetDrawDownDetails(StatsDatePeriod, CULng(ThisPertracID), False, FundStats.MonthReturns.ReferenceDate.AddMonths(-23), Renaissance_EndDate_Data, 1.0#)

                Case Else ' 2 Yr or other.
                  ThisDrawdowns = MainForm.StatFunctions.GetDrawDownDetails(StatsDatePeriod, CULng(ThisPertracID), False, FundStats.FirstReturnDate, Renaissance_EndDate_Data, 1.0#)

              End Select

              If (ThisDrawdowns.Length > 0) Then
                thisReportRow.M24_WorstDrawDown = ThisDrawdowns(0).DrawDown
              End If

              ' Column 3 (ITD) DrawDowns :-

              Select Case Column3DisplayValue
                Case 0 ' 1 Yr
                  ThisDrawdowns = MainForm.StatFunctions.GetDrawDownDetails(StatsDatePeriod, CULng(ThisPertracID), False, FundStats.MonthReturns.ReferenceDate.AddMonths(-11), Renaissance_EndDate_Data, 1.0#)

                Case 1 ' 2 Yr
                  ThisDrawdowns = MainForm.StatFunctions.GetDrawDownDetails(StatsDatePeriod, CULng(ThisPertracID), False, FundStats.MonthReturns.ReferenceDate.AddMonths(-23), Renaissance_EndDate_Data, 1.0#)

                Case 2 ' 3 Yr
                  ThisDrawdowns = MainForm.StatFunctions.GetDrawDownDetails(StatsDatePeriod, CULng(ThisPertracID), False, FundStats.MonthReturns.ReferenceDate.AddMonths(-35), Renaissance_EndDate_Data, 1.0#)

                Case 3 ' 5 Yr
                  ThisDrawdowns = MainForm.StatFunctions.GetDrawDownDetails(StatsDatePeriod, CULng(ThisPertracID), False, FundStats.MonthReturns.ReferenceDate.AddMonths(-59), Renaissance_EndDate_Data, 1.0#)

                Case 5 ' CTD
                  ThisDrawdowns = MainForm.StatFunctions.GetDrawDownDetails(StatsDatePeriod, CULng(ThisPertracID), False, Column3StartDate, Renaissance_EndDate_Data, 1.0#)

                Case Else ' ITD or Else.
                  ThisDrawdowns = MainForm.StatFunctions.GetDrawDownDetails(StatsDatePeriod, CULng(ThisPertracID), False)

              End Select

              If (ThisDrawdowns.Length > 0) Then
                thisReportRow.ITD_WorstDrawDown = ThisDrawdowns(0).DrawDown
              End If

              ' AUM - not as easy as it looks !

              ' 1st get Pertrac Information Row
              ' 2nd get AUM figure there in.
              thisReportRow.grAUM = CDbl(ConvertValue(Nz(InformationArray(PertracInformationFields.FundAssets), 0), GetType(Double)))

              ' 3rd get Numeric multiplier relating to that Field for that Data Provider

              Dim Multiplier As Double = 1.0

              Try
                Multiplier = CDbl(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblPertracFieldMapping, 0, "Multiplier", "RN", "(DataProvider='" & Nz(InformationArray(PertracInformationFields.DataVendorName), "").ToString & "') AND (FCP_FieldName='" & PertracInformationFields.FundAssets.ToString & "')"))
              Catch ex As Exception
              End Try

              ' 4th Ta Da!

              thisReportRow.grAUM = (thisReportRow.grAUM * Multiplier / 1000000.0)

              ReportTable.AddtblCompetitorGroupReportRow(thisReportRow)

            Catch ex As Exception
            End Try
          Next

        End If
      End If

    Next

    ' Get Main report definition
    Dim CompetitorReport As C1.C1Report.C1Report

    CompetitorReport = Me.GetReportDefinition(0, "rptCompetitorGroupsReport", "rptCompetitorGroupsReport")

    ' Set Report Fields - Month and Year labels

    Try
      If (FundStats IsNot Nothing) Then
        Call SetFieldValue(CompetitorReport, New classReportFieldValue("Label_LatestMonth", pReportEndDate.ToString("MMM")))
        Call SetFieldValue(CompetitorReport, New classReportFieldValue("Label_Month2", (pReportEndDate.AddMonths(-1)).ToString("MMM")))
        Call SetFieldValue(CompetitorReport, New classReportFieldValue("Label_Month3", (pReportEndDate.AddMonths(-2)).ToString("MMM")))

      End If

    Catch ex As Exception
    End Try

    Try

      ' Set Column 2 and Column 3 headings.

      ' titleLbl24

      Select Case Column2DisplayValue
        Case 0 ' 1 Yr
          Call SetFieldValue(CompetitorReport, New classReportFieldValue("titleLbl24", "1 Year Performance (%)"))

        Case 2 ' 3 Yr
          Call SetFieldValue(CompetitorReport, New classReportFieldValue("titleLbl24", "3 Year Performance (%)"))

        Case 3 ' 5 Yr
          Call SetFieldValue(CompetitorReport, New classReportFieldValue("titleLbl24", "5 Year Performance (%)"))

        Case 4 ' ITD
          Call SetFieldValue(CompetitorReport, New classReportFieldValue("titleLbl24", "ITD Performance (%)"))

        Case Else ' 2 Yr or Else
          Call SetFieldValue(CompetitorReport, New classReportFieldValue("titleLbl24", "2 Year Performance (%)"))

      End Select

      ' titleLblITD

      Select Case Column3DisplayValue
        Case 0 ' 1 Yr
          Call SetFieldValue(CompetitorReport, New classReportFieldValue("titleLblITD", "1 Year Performance (%)"))

        Case 1 ' 2 Yr
          Call SetFieldValue(CompetitorReport, New classReportFieldValue("titleLblITD", "2 Year Performance (%)"))

        Case 2 ' 3 Yr
          Call SetFieldValue(CompetitorReport, New classReportFieldValue("titleLblITD", "3 Year Performance (%)"))

        Case 3 ' 5 Yr
          Call SetFieldValue(CompetitorReport, New classReportFieldValue("titleLblITD", "5 Year Performance (%)"))

        Case 5 ' CTD
          Call SetFieldValue(CompetitorReport, New classReportFieldValue("titleLblITD", "From " & Column3StartDate.ToString("MMM yyyy") & " (%)"))

        Case Else ' ITD or Else
          Call SetFieldValue(CompetitorReport, New classReportFieldValue("titleLblITD", "ITD Performance (%)"))

      End Select

    Catch ex As Exception
    End Try

    Call DisplayReport(CompetitorReport, ReportTable, pReportEndDate, MainForm.Main_Knowledgedate, Nothing)

  End Sub

#End Region

#Region " DataSet Creation Functions (For Sub Reports)"

  Private Function GetReturnsDataSet(ByVal pPertracID As Integer) As dsMonthlyTable.tblMonthlyTableDataTable
    ' ****************************************************************************
    '
    '
    ' ****************************************************************************

    Dim RVal As dsMonthlyTable.tblMonthlyTableDataTable = Nothing

    Try
      RVal = GetReturnsDataSet(MainForm.StatFunctions.CombinedStatsID(pPertracID, False))
    Catch ex As Exception
    End Try

    Return RVal

  End Function

  Private Function GetReturnsDataSet(ByVal pPertracID As ULong) As dsMonthlyTable.tblMonthlyTableDataTable
    ' ****************************************************************************
    ' Construct a returns dataset based on the returns data for the given instrument.
    '
    ' This function used to use the pertrac data directly, but multiple returns for any
    ' month would distort the YTD returns.
    '
    ' ****************************************************************************

    Dim RVal As New dsMonthlyTable.tblMonthlyTableDataTable

    Try
      RVal = MainForm.StatFunctions.GetMonthlyReturnsDataSet(pPertracID, RVal)
    Catch ex As Exception
    End Try

    Return RVal

  End Function

  Private Function GetDrawDownDataSet(ByVal pPertracID As Integer) As dsDrawDown.tblDrawDownDataTable

    Dim RValTable As New dsDrawDown.tblDrawDownDataTable
    Dim ThisRvalRow As dsDrawDown.tblDrawDownRow
    Dim RowCount As Integer
    Dim StatsDatePeriod As DealingPeriod = MainForm.PertracData.GetPertracDataPeriod(pPertracID)

    Dim DrawDownDetails() As StatFunctions.DrawDownInstanceClass = MainForm.StatFunctions.GetDrawDownDetails(StatsDatePeriod, CULng(pPertracID), False)

    For RowCount = 0 To (DrawDownDetails.Length - 1)
      Try
        ThisRvalRow = RValTable.NewtblDrawDownRow

        ThisRvalRow.ddRank = RowCount + 1
        ThisRvalRow.ddFrom = DrawDownDetails(RowCount).DateFrom
        ThisRvalRow.ddTo = DrawDownDetails(RowCount).DateTo
        ThisRvalRow.ddDrawDown = DrawDownDetails(RowCount).DrawDown
        If (DrawDownDetails(RowCount).RecoveryOngoing = True) Then
          ThisRvalRow.ddRecovery = -999
        Else
          ThisRvalRow.ddRecovery = (GetPeriodCount(StatsDatePeriod, DrawDownDetails(RowCount).DateFrom, DrawDownDetails(RowCount).DateTo)).ToString & " " & PeriodName(StatsDatePeriod)
        End If

        RValTable.Rows.Add(ThisRvalRow)
      Catch ex As Exception
      End Try
    Next

    Return RValTable

  End Function

  Private Function GetStatisticsDataSet(ByVal pPertracID As Integer) As dsStatistics.tblStatisticsDataTable
    Dim RValTable As New dsStatistics.tblStatisticsDataTable
    Dim ThisRvalRow As dsStatistics.tblStatisticsRow

    Dim FundStats As StatFunctions.SeriesStatsClass = MainForm.StatFunctions.GetSimpleStats(MainForm.PertracData.GetPertracDataPeriod(pPertracID), CULng(pPertracID), False, 0.04, 1.0#)

    Try
      ThisRvalRow = RValTable.NewtblStatisticsRow
      ThisRvalRow.stType = "Count"

      If (FundStats.PeriodCount >= 12) Then
        ThisRvalRow.St12m = "12"
      End If
      If (FundStats.PeriodCount >= 36) Then
        ThisRvalRow.St36m = "36"
      End If
      If (FundStats.PeriodCount >= 60) Then
        ThisRvalRow.St60m = "60"
      End If
      ThisRvalRow.StInception = FundStats.PeriodCount.ToString

      RValTable.Rows.Add(ThisRvalRow)

      ThisRvalRow = RValTable.NewtblStatisticsRow
      ThisRvalRow.stType = "Total Return"
      If (FundStats.PeriodCount >= 12) Then ThisRvalRow.St12m = FundStats.SimpleReturn.M12.ToString("#,##0.0%")
      If (FundStats.PeriodCount >= 36) Then ThisRvalRow.St36m = FundStats.SimpleReturn.M36.ToString("#,##0.0%")
      If (FundStats.PeriodCount >= 60) Then ThisRvalRow.St60m = FundStats.SimpleReturn.M60.ToString("#,##0.0%")
      ThisRvalRow.StInception = FundStats.SimpleReturn.ITD.ToString("#,##0.0%")
      RValTable.Rows.Add(ThisRvalRow)

      ThisRvalRow = RValTable.NewtblStatisticsRow
      ThisRvalRow.stType = "Annualised Return"
      If (FundStats.PeriodCount >= 12) Then ThisRvalRow.St12m = FundStats.AnnualisedReturn.M12.ToString("#,##0.0%")
      If (FundStats.PeriodCount >= 36) Then ThisRvalRow.St36m = FundStats.AnnualisedReturn.M36.ToString("#,##0.0%")
      If (FundStats.PeriodCount >= 60) Then ThisRvalRow.St60m = FundStats.AnnualisedReturn.M60.ToString("#,##0.0%")
      ThisRvalRow.StInception = FundStats.AnnualisedReturn.ITD.ToString("#,##0.0%")
      RValTable.Rows.Add(ThisRvalRow)

      ThisRvalRow = RValTable.NewtblStatisticsRow
      ThisRvalRow.stType = "Volatility"
      If (FundStats.PeriodCount >= 12) Then ThisRvalRow.St12m = FundStats.Volatility.M12.ToString("#,##0.0%")
      If (FundStats.PeriodCount >= 36) Then ThisRvalRow.St36m = FundStats.Volatility.M36.ToString("#,##0.0%")
      If (FundStats.PeriodCount >= 60) Then ThisRvalRow.St60m = FundStats.Volatility.M60.ToString("#,##0.0%")
      ThisRvalRow.StInception = FundStats.Volatility.ITD.ToString("#,##0.0%")
      RValTable.Rows.Add(ThisRvalRow)

      ThisRvalRow = RValTable.NewtblStatisticsRow
      ThisRvalRow.stType = "Sharpe Ratio (4%)"
      If (FundStats.PeriodCount >= 12) Then ThisRvalRow.St12m = FundStats.SharpeRatio.M12.ToString("#,##0.00")
      If (FundStats.PeriodCount >= 36) Then ThisRvalRow.St36m = FundStats.SharpeRatio.M36.ToString("#,##0.00")
      If (FundStats.PeriodCount >= 60) Then ThisRvalRow.St60m = FundStats.SharpeRatio.M60.ToString("#,##0.00")
      ThisRvalRow.StInception = FundStats.SharpeRatio.ITD.ToString("#,##0.00")
      RValTable.Rows.Add(ThisRvalRow)

      ThisRvalRow = RValTable.NewtblStatisticsRow
      ThisRvalRow.stType = "Percent Positive"
      If (FundStats.PeriodCount >= 12) Then ThisRvalRow.St12m = FundStats.PercentPositive.M12.ToString("#,##0.0%")
      If (FundStats.PeriodCount >= 36) Then ThisRvalRow.St36m = FundStats.PercentPositive.M36.ToString("#,##0.0%")
      If (FundStats.PeriodCount >= 60) Then ThisRvalRow.St60m = FundStats.PercentPositive.M60.ToString("#,##0.0%")
      ThisRvalRow.StInception = FundStats.PercentPositive.ITD.ToString("#,##0.0%")
      RValTable.Rows.Add(ThisRvalRow)

    Catch ex As Exception
    End Try

    Return RValTable
  End Function

  Public Function GetVAMIChartImage(ByVal pPertracID As Integer, ByVal pStartDate As Date, ByVal ScalingFactor As Double) As Image
    Return GetVAMIChartImage(New Integer() {pPertracID}, pStartDate, ScalingFactor)
  End Function

  Public Function GetVAMIChartImage(ByVal pPertracIDs() As Integer, ByVal pStartDate As Date, ByVal ScalingFactor As Double) As Image ' Renaissance_BaseDate
    Dim Chart_Prices As New Dundas.Charting.WinControl.Chart
    Dim ChartArea8 As Dundas.Charting.WinControl.ChartArea = New Dundas.Charting.WinControl.ChartArea
    Dim Legend8 As Dundas.Charting.WinControl.Legend = New Dundas.Charting.WinControl.Legend
    Dim Series10 As Dundas.Charting.WinControl.Series = New Dundas.Charting.WinControl.Series

    Chart_Prices.BackColor = System.Drawing.Color.Azure
    Chart_Prices.BackGradientEndColor = System.Drawing.Color.SkyBlue
    Chart_Prices.BackGradientType = Dundas.Charting.WinControl.GradientType.DiagonalLeft
    Chart_Prices.BorderLineColor = System.Drawing.Color.LightGray
    Chart_Prices.BorderSkin.FrameBackColor = System.Drawing.Color.LightSkyBlue
    Chart_Prices.BorderSkin.FrameBackGradientEndColor = System.Drawing.Color.DodgerBlue

    ChartArea8.Area3DStyle.Light = Dundas.Charting.WinControl.LightStyle.Realistic
    ChartArea8.AxisX.LabelsAutoFitStyle = CType((((Dundas.Charting.WinControl.LabelsAutoFitStyle.DecreaseFont Or Dundas.Charting.WinControl.LabelsAutoFitStyle.OffsetLabels) _
     Or Dundas.Charting.WinControl.LabelsAutoFitStyle.LabelsAngleStep30) _
     Or Dundas.Charting.WinControl.LabelsAutoFitStyle.WordWrap), Dundas.Charting.WinControl.LabelsAutoFitStyle)
    ChartArea8.AxisX.LabelStyle.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
    ChartArea8.AxisX.LabelStyle.Format = "Y"
    ChartArea8.AxisX.LineColor = System.Drawing.Color.DimGray
    ChartArea8.AxisX.MajorGrid.LineColor = System.Drawing.Color.DimGray
    ChartArea8.AxisX.MajorGrid.LineStyle = Dundas.Charting.WinControl.ChartDashStyle.Dot
    ChartArea8.AxisX.MajorTickMark.LineColor = System.Drawing.Color.DimGray
    ChartArea8.AxisX2.LineColor = System.Drawing.Color.DimGray
    ChartArea8.AxisY.LabelsAutoFitStyle = CType((((Dundas.Charting.WinControl.LabelsAutoFitStyle.DecreaseFont Or Dundas.Charting.WinControl.LabelsAutoFitStyle.OffsetLabels) _
     Or Dundas.Charting.WinControl.LabelsAutoFitStyle.LabelsAngleStep30) _
     Or Dundas.Charting.WinControl.LabelsAutoFitStyle.WordWrap), Dundas.Charting.WinControl.LabelsAutoFitStyle)
    ChartArea8.AxisY.LabelStyle.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
    ChartArea8.AxisY.LabelStyle.Format = "N0"
    ChartArea8.AxisY.LineColor = System.Drawing.Color.DimGray
    ChartArea8.AxisY.MajorGrid.LineColor = System.Drawing.Color.DimGray
    ChartArea8.AxisY.MajorGrid.LineStyle = Dundas.Charting.WinControl.ChartDashStyle.Dot
    ChartArea8.AxisY.MajorTickMark.LineColor = System.Drawing.Color.DimGray
    ChartArea8.AxisY.StartFromZero = False
    ChartArea8.AxisY2.LineColor = System.Drawing.Color.DimGray
    ChartArea8.BackColor = System.Drawing.Color.Transparent
    ChartArea8.BorderColor = System.Drawing.Color.DimGray
    ChartArea8.Name = "Default"
    Chart_Prices.ChartAreas.Add(ChartArea8)
    Legend8.BackColor = System.Drawing.Color.Transparent
    Legend8.BorderColor = System.Drawing.Color.Transparent
    Legend8.Docking = Dundas.Charting.WinControl.LegendDocking.Left
    Legend8.DockToChartArea = "Default"
    Legend8.Enabled = False
    Legend8.Name = "Default"
    Chart_Prices.Legends.Add(Legend8)
    Chart_Prices.Location = New System.Drawing.Point(0, 0)
    Chart_Prices.Margin = New System.Windows.Forms.Padding(1)
    Chart_Prices.Name = "Chart_Prices"
    Chart_Prices.Palette = Dundas.Charting.WinControl.ChartColorPalette.SemiTransparent
    Chart_Prices.Size = New System.Drawing.Size(700, 500)
    Chart_Prices.TabIndex = 0
    Chart_Prices.Text = ""

    Dim IDCounter As Integer
    Dim thisID As Integer

    For IDCounter = 0 To (pPertracIDs.Length - 1)
      thisID = pPertracIDs(IDCounter)

      If (thisID > 0) Then
        Set_LineChart(MainForm, thisID, Chart_Prices, -1, Nz(MainForm.PertracData.GetInformationValue(thisID, PertracInformationFields.FundName), ""), 12, 1, ScalingFactor, pStartDate, Renaissance_EndDate_Data)
      End If
    Next

    Dim memStream As MemoryStream = New MemoryStream
    Chart_Prices.SaveAsImage(memStream, System.Drawing.Imaging.ImageFormat.Tiff)
    Return Image.FromStream(memStream)

  End Function

  'Private Function GetHighResMetafile(ByVal chart As C1.Win.C1Chart.C1Chart) As Image

  '	' create reference dc
  '	Dim doc As New Printing.PrintDocument()
  '	Dim gref As Graphics = doc.PrinterSettings.CreateMeasurementGraphics()

  '	' create high-res metafile
  '	Dim hdc As IntPtr = gref.GetHdc()
  '	Dim m As Imaging.Metafile = New Imaging.Metafile(hdc, EmfType.EmfOnly, "test")
  '	Using g As Graphics = Graphics.FromImage(m)

  '		' draw chart into metafile
  '		chart.Draw(g, New Rectangle(Point.Empty, chart.Size))
  '		GetHighResMetafile = m

  '	End Using
  '	gref.ReleaseHdc(hdc)

  'End Function

  Public Function GetMonthlyReturnChartImage(ByVal pPertracID As Integer, ByVal ScalingFactor As Double) As Image
    Dim Chart_Returns As New Dundas.Charting.WinControl.Chart
    Dim ChartArea9 As Dundas.Charting.WinControl.ChartArea = New Dundas.Charting.WinControl.ChartArea
    Dim Series12 As Dundas.Charting.WinControl.Series = New Dundas.Charting.WinControl.Series

    Chart_Returns.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
     Or System.Windows.Forms.AnchorStyles.Left) _
     Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Chart_Returns.BackColor = System.Drawing.Color.Azure
    Chart_Returns.BackGradientEndColor = System.Drawing.Color.SkyBlue
    Chart_Returns.BackGradientType = Dundas.Charting.WinControl.GradientType.DiagonalLeft
    Chart_Returns.BorderLineColor = System.Drawing.Color.LightGray
    Chart_Returns.BorderSkin.FrameBackColor = System.Drawing.Color.LightSkyBlue
    Chart_Returns.BorderSkin.FrameBackGradientEndColor = System.Drawing.Color.DodgerBlue
    ChartArea9.Area3DStyle.Light = Dundas.Charting.WinControl.LightStyle.Realistic
    ChartArea9.AxisX.LabelsAutoFitStyle = CType((((Dundas.Charting.WinControl.LabelsAutoFitStyle.DecreaseFont Or Dundas.Charting.WinControl.LabelsAutoFitStyle.OffsetLabels) _
     Or Dundas.Charting.WinControl.LabelsAutoFitStyle.LabelsAngleStep30) _
     Or Dundas.Charting.WinControl.LabelsAutoFitStyle.WordWrap), Dundas.Charting.WinControl.LabelsAutoFitStyle)
    ChartArea9.AxisX.LabelStyle.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
    ChartArea9.AxisX.LabelStyle.Format = "Y"
    ChartArea9.AxisX.LineColor = System.Drawing.Color.DimGray
    ChartArea9.AxisX.MajorGrid.LineColor = System.Drawing.Color.DimGray
    ChartArea9.AxisX.MajorGrid.LineStyle = Dundas.Charting.WinControl.ChartDashStyle.Dot
    ChartArea9.AxisX.MajorTickMark.LineColor = System.Drawing.Color.DimGray
    ChartArea9.AxisX2.LineColor = System.Drawing.Color.DimGray
    ChartArea9.AxisY.LabelsAutoFitStyle = CType((((Dundas.Charting.WinControl.LabelsAutoFitStyle.DecreaseFont Or Dundas.Charting.WinControl.LabelsAutoFitStyle.OffsetLabels) _
     Or Dundas.Charting.WinControl.LabelsAutoFitStyle.LabelsAngleStep30) _
     Or Dundas.Charting.WinControl.LabelsAutoFitStyle.WordWrap), Dundas.Charting.WinControl.LabelsAutoFitStyle)
    ChartArea9.AxisY.LabelStyle.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
    ChartArea9.AxisY.LabelStyle.Format = "P1"
    ChartArea9.AxisY.LineColor = System.Drawing.Color.DimGray
    ChartArea9.AxisY.MajorGrid.LineColor = System.Drawing.Color.DimGray
    ChartArea9.AxisY.MajorGrid.LineStyle = Dundas.Charting.WinControl.ChartDashStyle.Dot
    ChartArea9.AxisY.MajorTickMark.LineColor = System.Drawing.Color.DimGray
    ChartArea9.AxisY.StartFromZero = False
    ChartArea9.AxisY2.LineColor = System.Drawing.Color.DimGray
    ChartArea9.BackColor = System.Drawing.Color.Transparent
    ChartArea9.BorderColor = System.Drawing.Color.DimGray
    ChartArea9.Name = "Default"
    Chart_Returns.ChartAreas.Add(ChartArea9)
    Chart_Returns.Location = New System.Drawing.Point(0, 0)
    Chart_Returns.Margin = New System.Windows.Forms.Padding(1)
    Chart_Returns.Name = "Chart_Returns"
    Chart_Returns.Palette = Dundas.Charting.WinControl.ChartColorPalette.SemiTransparent
    'Series12.BorderColor = System.Drawing.Color.FromArgb(CType(CType(120, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer))
    'Series12.Name = "Series1"
    'Series12.XValueType = Dundas.Charting.WinControl.ChartValueTypes.DateTime
    'Series12.YValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
    'Chart_Returns.Series.Add(Series12)
    Chart_Returns.Size = New System.Drawing.Size(700, 500)
    Chart_Returns.TabIndex = 1
    Chart_Returns.Text = "Monthly Returns"

    Chart_Returns.Legends.Clear()

    Set_ReturnsBarChart(MainForm, pPertracID, Chart_Returns, -1, Nz(MainForm.PertracData.GetInformationValue(pPertracID, PertracInformationFields.FundName), ""), 12, 1, ScalingFactor, Renaissance_BaseDate, Renaissance_EndDate_Data, True)
    Chart_Returns.Refresh()

    Dim memStream As MemoryStream = New MemoryStream
    Chart_Returns.SaveAsImage(memStream, System.Drawing.Imaging.ImageFormat.Tiff)
    Return Image.FromStream(memStream)

  End Function



#End Region

#Region " Form Control Disable Routines"

  Friend Function DisableFormControls(ByRef thisForm As Form) As ArrayList
    Dim thisControl As Control
    Dim ReturnArray As New ArrayList

    For Each thisControl In thisForm.Controls
      If (thisControl.Visible = True) AndAlso (thisControl.Enabled = True) Then

        If Not (TypeOf thisControl Is StatusStrip) Then
          ReturnArray.Add(thisControl)
          thisControl.Enabled = False
        End If

      End If
    Next

    Return ReturnArray
  End Function

  Friend Sub EnableFormControls(ByVal ControlArray As ArrayList)
    Dim thisControl As Control

    If (ControlArray Is Nothing) Then
      Exit Sub
    End If

    For Each thisControl In ControlArray
      If (thisControl IsNot Nothing) Then
        If (thisControl.IsDisposed = False) Then
          thisControl.Enabled = True
        End If
      End If
    Next
  End Sub

#End Region

End Class
