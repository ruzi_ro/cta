<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class CTAMain
  Inherits System.Windows.Forms.Form

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing AndAlso components IsNot Nothing Then
      components.Dispose()
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(CTAMain))
    Me.CTAMenu = New System.Windows.Forms.MenuStrip
    Me.Menu_File = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_File_ToggleMDI = New System.Windows.Forms.ToolStripMenuItem
    Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator
    Me.Menu_File_EnableReportFadeIn = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Font = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Font_Small = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Font_Medium = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Font_Large = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Font_XLarge = New System.Windows.Forms.ToolStripMenuItem
    Me.ToolStripSeparator7 = New System.Windows.Forms.ToolStripSeparator
    Me.Menu_File_Close = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_StaticData = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_FundBrowser = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_ManageFolders = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_About = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Reports = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_KnowledgeDate = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_KD_SetKD = New System.Windows.Forms.ToolStripMenuItem
    Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator
    Me.Menu_KD_Refresh = New System.Windows.Forms.ToolStripMenuItem
    Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator
    Me.Menu_KD_Display = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Windows = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_System = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_System_UserPermissions = New System.Windows.Forms.ToolStripMenuItem
    Me.ToolStripSeparator6 = New System.Windows.Forms.ToolStripSeparator
    Me.Menu_System_ReviewCC = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_System_SelectCC = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_System_SetMAC = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_System_SetExpiryDate = New System.Windows.Forms.ToolStripMenuItem
    Me.ToolStripSeparator8 = New System.Windows.Forms.ToolStripSeparator
    Me.Menu_System_UpdateDatabase = New System.Windows.Forms.ToolStripMenuItem
    Me.CTAStatusStrip = New System.Windows.Forms.StatusStrip
    Me.MainProgressBar = New System.Windows.Forms.ToolStripProgressBar
    Me.CTAStatusLabel = New System.Windows.Forms.ToolStripStatusLabel
    Me.Button_Debug = New System.Windows.Forms.Button
    Me.DsCompetitorGroupReport1 = New CTA.dsCompetitorGroupReport
    Me.Panel_Debug = New System.Windows.Forms.Panel
    Me.CTAMenu.SuspendLayout()
    Me.CTAStatusStrip.SuspendLayout()
    CType(Me.DsCompetitorGroupReport1, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.Panel_Debug.SuspendLayout()
    Me.SuspendLayout()
    '
    'CTAMenu
    '
    Me.CTAMenu.AllowMerge = False
    Me.CTAMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Menu_File, Me.Menu_StaticData, Me.Menu_About, Me.Menu_Reports, Me.Menu_KnowledgeDate, Me.Menu_Windows, Me.Menu_System})
    Me.CTAMenu.Location = New System.Drawing.Point(0, 0)
    Me.CTAMenu.Name = "CTAMenu"
    Me.CTAMenu.Size = New System.Drawing.Size(811, 24)
    Me.CTAMenu.TabIndex = 0
    Me.CTAMenu.Text = "CTAMenu"
    '
    'Menu_File
    '
    Me.Menu_File.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Menu_File_ToggleMDI, Me.ToolStripSeparator1, Me.Menu_File_EnableReportFadeIn, Me.Menu_Font, Me.ToolStripSeparator7, Me.Menu_File_Close})
    Me.Menu_File.Name = "Menu_File"
    Me.Menu_File.Size = New System.Drawing.Size(35, 20)
    Me.Menu_File.Text = "&File"
    '
    'Menu_File_ToggleMDI
    '
    Me.Menu_File_ToggleMDI.Name = "Menu_File_ToggleMDI"
    Me.Menu_File_ToggleMDI.Size = New System.Drawing.Size(194, 22)
    Me.Menu_File_ToggleMDI.Text = "Toggle &MDI"
    '
    'ToolStripSeparator1
    '
    Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
    Me.ToolStripSeparator1.Size = New System.Drawing.Size(191, 6)
    '
    'Menu_File_EnableReportFadeIn
    '
    Me.Menu_File_EnableReportFadeIn.Name = "Menu_File_EnableReportFadeIn"
    Me.Menu_File_EnableReportFadeIn.Size = New System.Drawing.Size(194, 22)
    Me.Menu_File_EnableReportFadeIn.Text = "Enable Report Fade-In"
    '
    'Menu_Font
    '
    Me.Menu_Font.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Menu_Font_Small, Me.Menu_Font_Medium, Me.Menu_Font_Large, Me.Menu_Font_XLarge})
    Me.Menu_Font.Enabled = False
    Me.Menu_Font.Name = "Menu_Font"
    Me.Menu_Font.Size = New System.Drawing.Size(194, 22)
    Me.Menu_Font.Text = "Font Size"
    '
    'Menu_Font_Small
    '
    Me.Menu_Font_Small.Name = "Menu_Font_Small"
    Me.Menu_Font_Small.Size = New System.Drawing.Size(144, 22)
    Me.Menu_Font_Small.Text = "Small (6)"
    '
    'Menu_Font_Medium
    '
    Me.Menu_Font_Medium.Checked = True
    Me.Menu_Font_Medium.CheckState = System.Windows.Forms.CheckState.Checked
    Me.Menu_Font_Medium.Name = "Menu_Font_Medium"
    Me.Menu_Font_Medium.Size = New System.Drawing.Size(144, 22)
    Me.Menu_Font_Medium.Text = "Medium (8)"
    '
    'Menu_Font_Large
    '
    Me.Menu_Font_Large.Name = "Menu_Font_Large"
    Me.Menu_Font_Large.Size = New System.Drawing.Size(144, 22)
    Me.Menu_Font_Large.Text = "Large (10)"
    '
    'Menu_Font_XLarge
    '
    Me.Menu_Font_XLarge.Name = "Menu_Font_XLarge"
    Me.Menu_Font_XLarge.Size = New System.Drawing.Size(144, 22)
    Me.Menu_Font_XLarge.Text = "X Large (12)"
    '
    'ToolStripSeparator7
    '
    Me.ToolStripSeparator7.Name = "ToolStripSeparator7"
    Me.ToolStripSeparator7.Size = New System.Drawing.Size(191, 6)
    '
    'Menu_File_Close
    '
    Me.Menu_File_Close.Name = "Menu_File_Close"
    Me.Menu_File_Close.Size = New System.Drawing.Size(194, 22)
    Me.Menu_File_Close.Text = "&Close"
    '
    'Menu_StaticData
    '
    Me.Menu_StaticData.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Menu_FundBrowser, Me.Menu_ManageFolders})
    Me.Menu_StaticData.Name = "Menu_StaticData"
    Me.Menu_StaticData.Size = New System.Drawing.Size(73, 20)
    Me.Menu_StaticData.Text = "&ScratchPad"
    '
    'Menu_FundBrowser
    '
    Me.Menu_FundBrowser.Name = "Menu_FundBrowser"
    Me.Menu_FundBrowser.Size = New System.Drawing.Size(162, 22)
    Me.Menu_FundBrowser.Tag = "frmSimulationBrowser"
    Me.Menu_FundBrowser.Text = "CTA ScratchPad"
    '
    'Menu_ManageFolders
    '
    Me.Menu_ManageFolders.Name = "Menu_ManageFolders"
    Me.Menu_ManageFolders.Size = New System.Drawing.Size(162, 22)
    Me.Menu_ManageFolders.Tag = "frmManageFolders"
    Me.Menu_ManageFolders.Text = "Manage Folders"
    '
    'Menu_About
    '
    Me.Menu_About.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
    Me.Menu_About.Name = "Menu_About"
    Me.Menu_About.Size = New System.Drawing.Size(48, 20)
    Me.Menu_About.Tag = "frmAbout"
    Me.Menu_About.Text = "&About"
    '
    'Menu_Reports
    '
    Me.Menu_Reports.Enabled = False
    Me.Menu_Reports.Name = "Menu_Reports"
    Me.Menu_Reports.Size = New System.Drawing.Size(52, 20)
    Me.Menu_Reports.Text = "&Report"
    '
    'Menu_KnowledgeDate
    '
    Me.Menu_KnowledgeDate.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Menu_KD_SetKD, Me.ToolStripSeparator2, Me.Menu_KD_Refresh, Me.ToolStripSeparator3, Me.Menu_KD_Display})
    Me.Menu_KnowledgeDate.Name = "Menu_KnowledgeDate"
    Me.Menu_KnowledgeDate.Size = New System.Drawing.Size(94, 20)
    Me.Menu_KnowledgeDate.Text = "&KnowledgeDate"
    '
    'Menu_KD_SetKD
    '
    Me.Menu_KD_SetKD.Name = "Menu_KD_SetKD"
    Me.Menu_KD_SetKD.Size = New System.Drawing.Size(179, 22)
    Me.Menu_KD_SetKD.Tag = "frmSetKnowledgeDate"
    Me.Menu_KD_SetKD.Text = "&Set KnowledgeDate"
    '
    'ToolStripSeparator2
    '
    Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
    Me.ToolStripSeparator2.Size = New System.Drawing.Size(176, 6)
    '
    'Menu_KD_Refresh
    '
    Me.Menu_KD_Refresh.Name = "Menu_KD_Refresh"
    Me.Menu_KD_Refresh.Size = New System.Drawing.Size(179, 22)
    Me.Menu_KD_Refresh.Text = "&Refresh All Tables"
    '
    'ToolStripSeparator3
    '
    Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
    Me.ToolStripSeparator3.Size = New System.Drawing.Size(176, 6)
    '
    'Menu_KD_Display
    '
    Me.Menu_KD_Display.Name = "Menu_KD_Display"
    Me.Menu_KD_Display.Size = New System.Drawing.Size(179, 22)
    Me.Menu_KD_Display.Text = " "
    '
    'Menu_Windows
    '
    Me.Menu_Windows.Name = "Menu_Windows"
    Me.Menu_Windows.Size = New System.Drawing.Size(62, 20)
    Me.Menu_Windows.Text = "&Windows"
    '
    'Menu_System
    '
    Me.Menu_System.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Menu_System_UserPermissions, Me.ToolStripSeparator6, Me.Menu_System_ReviewCC, Me.Menu_System_SelectCC, Me.Menu_System_SetMAC, Me.Menu_System_SetExpiryDate, Me.ToolStripSeparator8, Me.Menu_System_UpdateDatabase})
    Me.Menu_System.Name = "Menu_System"
    Me.Menu_System.Size = New System.Drawing.Size(54, 20)
    Me.Menu_System.Text = "&System"
    '
    'Menu_System_UserPermissions
    '
    Me.Menu_System_UserPermissions.Name = "Menu_System_UserPermissions"
    Me.Menu_System_UserPermissions.Size = New System.Drawing.Size(232, 22)
    Me.Menu_System_UserPermissions.Tag = "frmUserPermissions"
    Me.Menu_System_UserPermissions.Text = "User &Permissions"
    '
    'ToolStripSeparator6
    '
    Me.ToolStripSeparator6.Name = "ToolStripSeparator6"
    Me.ToolStripSeparator6.Size = New System.Drawing.Size(229, 6)
    '
    'Menu_System_ReviewCC
    '
    Me.Menu_System_ReviewCC.Name = "Menu_System_ReviewCC"
    Me.Menu_System_ReviewCC.Size = New System.Drawing.Size(232, 22)
    Me.Menu_System_ReviewCC.Tag = "frmChangeControlReview"
    Me.Menu_System_ReviewCC.Text = "Add / Review &Change Controls"
    '
    'Menu_System_SelectCC
    '
    Me.Menu_System_SelectCC.Name = "Menu_System_SelectCC"
    Me.Menu_System_SelectCC.Size = New System.Drawing.Size(232, 22)
    Me.Menu_System_SelectCC.Tag = "frmSelectChangeControl"
    Me.Menu_System_SelectCC.Text = "&Select Change Controls"
    '
    'Menu_System_SetMAC
    '
    Me.Menu_System_SetMAC.Name = "Menu_System_SetMAC"
    Me.Menu_System_SetMAC.Size = New System.Drawing.Size(232, 22)
    Me.Menu_System_SetMAC.Text = "Set MAC"
    '
    'Menu_System_SetExpiryDate
    '
    Me.Menu_System_SetExpiryDate.Name = "Menu_System_SetExpiryDate"
    Me.Menu_System_SetExpiryDate.Size = New System.Drawing.Size(232, 22)
    Me.Menu_System_SetExpiryDate.Text = "Set Expiry Date"
    '
    'ToolStripSeparator8
    '
    Me.ToolStripSeparator8.Name = "ToolStripSeparator8"
    Me.ToolStripSeparator8.Size = New System.Drawing.Size(229, 6)
    '
    'Menu_System_UpdateDatabase
    '
    Me.Menu_System_UpdateDatabase.Name = "Menu_System_UpdateDatabase"
    Me.Menu_System_UpdateDatabase.Size = New System.Drawing.Size(232, 22)
    Me.Menu_System_UpdateDatabase.Text = "Update Database"
    '
    'CTAStatusStrip
    '
    Me.CTAStatusStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MainProgressBar, Me.CTAStatusLabel})
    Me.CTAStatusStrip.Location = New System.Drawing.Point(0, 124)
    Me.CTAStatusStrip.Name = "CTAStatusStrip"
    Me.CTAStatusStrip.Size = New System.Drawing.Size(811, 22)
    Me.CTAStatusStrip.TabIndex = 1
    Me.CTAStatusStrip.Text = "CTAStatusStrip"
    '
    'MainProgressBar
    '
    Me.MainProgressBar.Maximum = 20
    Me.MainProgressBar.Name = "MainProgressBar"
    Me.MainProgressBar.Size = New System.Drawing.Size(150, 16)
    Me.MainProgressBar.Step = 1
    Me.MainProgressBar.Visible = False
    '
    'CTAStatusLabel
    '
    Me.CTAStatusLabel.Name = "CTAStatusLabel"
    Me.CTAStatusLabel.Size = New System.Drawing.Size(0, 17)
    '
    'Button_Debug
    '
    Me.Button_Debug.Location = New System.Drawing.Point(43, 35)
    Me.Button_Debug.Name = "Button_Debug"
    Me.Button_Debug.Size = New System.Drawing.Size(50, 23)
    Me.Button_Debug.TabIndex = 2
    Me.Button_Debug.Text = "Debug"
    Me.Button_Debug.UseVisualStyleBackColor = True
    '
    'DsCompetitorGroupReport1
    '
    Me.DsCompetitorGroupReport1.DataSetName = "dsCompetitorGroupReport"
    Me.DsCompetitorGroupReport1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
    '
    'Panel_Debug
    '
    Me.Panel_Debug.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Panel_Debug.Controls.Add(Me.Button_Debug)
    Me.Panel_Debug.Enabled = False
    Me.Panel_Debug.Location = New System.Drawing.Point(12, 36)
    Me.Panel_Debug.Name = "Panel_Debug"
    Me.Panel_Debug.Size = New System.Drawing.Size(787, 85)
    Me.Panel_Debug.TabIndex = 23
    Me.Panel_Debug.Visible = False
    '
    'CTAMain
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(811, 146)
    Me.Controls.Add(Me.Panel_Debug)
    Me.Controls.Add(Me.CTAStatusStrip)
    Me.Controls.Add(Me.CTAMenu)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
    Me.Name = "CTAMain"
    Me.Text = "CTA"
    Me.CTAMenu.ResumeLayout(False)
    Me.CTAMenu.PerformLayout()
    Me.CTAStatusStrip.ResumeLayout(False)
    Me.CTAStatusStrip.PerformLayout()
    CType(Me.DsCompetitorGroupReport1, System.ComponentModel.ISupportInitialize).EndInit()
    Me.Panel_Debug.ResumeLayout(False)
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub
  Friend WithEvents CTAMenu As System.Windows.Forms.MenuStrip
  Friend WithEvents Menu_File As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_File_ToggleMDI As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
  Friend WithEvents Menu_File_Close As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents CTAStatusStrip As System.Windows.Forms.StatusStrip
  Friend WithEvents CTAStatusLabel As System.Windows.Forms.ToolStripStatusLabel
  Friend WithEvents Menu_KnowledgeDate As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_KD_SetKD As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
  Friend WithEvents Menu_KD_Refresh As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_About As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
  Friend WithEvents Menu_KD_Display As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_Reports As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_StaticData As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_System As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_System_UserPermissions As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents MainProgressBar As System.Windows.Forms.ToolStripProgressBar
  Friend WithEvents Menu_Windows As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_FundBrowser As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_System_ReviewCC As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripSeparator6 As System.Windows.Forms.ToolStripSeparator
  Friend WithEvents Menu_System_SelectCC As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_Font As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_Font_Small As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripSeparator7 As System.Windows.Forms.ToolStripSeparator
  Friend WithEvents Menu_Font_Medium As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_Font_Large As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_Font_XLarge As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Button_Debug As System.Windows.Forms.Button
  Friend WithEvents DsCompetitorGroupReport1 As CTA.dsCompetitorGroupReport
  Friend WithEvents Menu_System_SetMAC As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_System_SetExpiryDate As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Panel_Debug As System.Windows.Forms.Panel
  Friend WithEvents ToolStripSeparator8 As System.Windows.Forms.ToolStripSeparator
  Friend WithEvents Menu_System_UpdateDatabase As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_File_EnableReportFadeIn As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_ManageFolders As System.Windows.Forms.ToolStripMenuItem

End Class
